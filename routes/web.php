<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
*/

// Homepage Route
Route::get('/', 'WelcomeController@welcome')->name('welcome');
// Authentication Routes
Auth::routes();

Route::post('logindata','Auth\LoginController@loginschk'); 
// Public Routes
Route::group(['middleware' => 'web'], function() {

    // Activation Routes
    Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);

    Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);
    Route::get('/activation', ['as' => 'authenticated.activation-resend', 'uses' => 'Auth\ActivateController@resend']);
    Route::get('/exceeded', ['as' => 'exceeded', 'uses' => 'Auth\ActivateController@exceeded']);

    // Socialite Register Routes
    Route::get('/social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}', ['as' => 'social.handle', 'uses' => 'Auth\SocialController@getSocialHandle']);

});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated']], function() {

    // Activation Routes
    Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
    Route::get('/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');

    //  Homepage Route - Redirect based on user role is in controller.
    Route::get('/home', ['as' => 'public.home',   'uses' => 'UserController@index']);

    // Show users profile - viewable by other users.
    Route::get('profile/{username}', [
        'as'        => '{username}',
        'uses'      => 'ProfilesController@show'
    ]);

});

// Registered, activated, and is current user routes.
Route::group(['middleware'=> ['auth', 'activated', 'currentUser']], function () {

    Route::resource(
        'profile',
        'ProfilesController', [
            'only'  => [
                'show',
                'edit',
                'update',
                'create'
            ]
        ]
    );

    // Route to show user avatar
    Route::get('images/profile/{id}/avatar/{image}', [
        'uses'      => 'ProfilesController@userProfileAvatar'
    ]);

    // Route to upload user avatar.
    Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'ProfilesController@upload']);

});

// Registered, activated, and is admin routes.
Route::group(['middleware'=> ['auth', 'activated', 'role:admin']], function () {

    Route::resource('users', 'UsersManagementController', [
        'names' => [
            'index' => 'users',
            'destroy' => 'user.destroy'
        ]
    ]);

    Route::resource('themes', 'ThemesManagementController', [
        'names' => [
            'index' => 'themes',
            'destroy' => 'themes.destroy'
        ]
    ]);

    Route::get('admin/profile','Admin\ProfileController@index');
    Route::post('admin/profile','Admin\ProfileController@post');

    //Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    //Route::get('php', 'AdminDetailsController@listPHPInfo');
    //Route::get('routes', 'AdminDetailsController@listRoutes');
    //xeme solutions admin routes 

    Route::get('admin/contact_us', 'Admin\ContactuspageController@index');
    Route::post('admin/contact_us','Admin\ContactuspageController@update');

    Route::get('admin/fraud', 'Admin\FraudController@index');
    Route::post('admin/fraud','Admin\FraudController@update');

    Route::get('admin/about_us','Admin\AboutuspageController@index');
    Route::post('admin/about_us','Admin\AboutuspageController@aboutone');
    Route::post('admin/about_us2','Admin\AboutuspageController@abouttwo');
    Route::post('admin/about_us3','Admin\AboutuspageController@aboutthree');
    Route::post('admin/about_us4','Admin\AboutuspageController@aboutfour');
    Route::get('admin/aboutdelete/{id}','Admin\AboutuspageController@aboutdelete');


    Route::get('admin/faqs','Admin\FaqsController@get');
    Route::get('admin/faqs/post','Admin\FaqsController@add');
    Route::post('admin/faqs/post','Admin\FaqsController@post'); 
	Route::get('admin/faqs/postcategory','Admin\FaqsController@postcategory');
	Route::post('admin/faqs/addpostcategory','Admin\FaqsController@addpostcategory');
    Route::get('admin/faqs/put/{id}','Admin\FaqsController@single');
	Route::get('admin/faqs/putcategory/{id}','Admin\FaqsController@singlecategory');
    Route::post('admin/faqs/put','Admin\FaqsController@put');
	Route::post('admin/faqs/putcategory','Admin\FaqsController@updatecategory');
    Route::get('admin/faqs/delete/{id}','Admin\FaqsController@delete');
	Route::get('admin/faqs/deletecategory/{id}','Admin\FaqsController@deletecategory');

    Route::get('admin/blogs','Admin\BlogsController@get'); 
    Route::get('admin/blogs/post','Admin\BlogsController@add');
    Route::post('admin/blogs/post','Admin\BlogsController@post');
    Route::get('admin/blogs/put/{id}','Admin\BlogsController@single');
    Route::post('admin/blogs/put','Admin\BlogsController@put');
    Route::get('admin/blogs/delete/{id}','Admin\BlogsController@delete');    

    Route::get('admin/blog_category','Admin\Blogs_categoryController@get');
    Route::get('admin/blog_category/post','Admin\Blogs_categoryController@add');
    Route::post('admin/blog_category/post','Admin\Blogs_categoryController@post');
    Route::get('admin/blog_category/put/{id}','Admin\Blogs_categoryController@single');
    Route::post('admin/blog_category/put','Admin\Blogs_categoryController@put');
    Route::get('admin/blog_category/delete/{id}','Admin\Blogs_categoryController@delete');
    Route::get('admin/blog_comments','Admin\Blogs_commentsController@index');
    Route::get('admin/blogs_comment/delete/{id}','Admin\Blogs_commentsController@delete');


    Route::get('admin/entrepreneurship','Admin\EntrepreneurshipController@get');
    Route::get('admin/entrepreneurship/post','Admin\EntrepreneurshipController@add');
    Route::post('admin/entrepreneurship/post','Admin\EntrepreneurshipController@post');
    Route::get('admin/entrepreneurship/put/{id}','Admin\EntrepreneurshipController@single');
    Route::post('admin/entrepreneurship/put','Admin\EntrepreneurshipController@put');
    Route::get('admin/entrepreneurship/delete/{id}','Admin\EntrepreneurshipController@delete');
    Route::get('admin/events','Admin\EntrepreneurshipController@events');
    Route::post('admin/events','Admin\EntrepreneurshipController@eventssave');

    Route::get('admin/servicespage','Admin\ServicesController@index');
    Route::post('admin/servicespage','Admin\ServicesController@servicesone');
    Route::post('admin/servicespage2','Admin\ServicesController@servicestwo');

    Route::get('admin/services','Admin\ServicesController@get');
    Route::get('admin/services/post','Admin\ServicesController@add');
    Route::post('admin/services/post','Admin\ServicesController@post');
    Route::get('admin/services/put/{id}','Admin\ServicesController@single');
    Route::post('admin/services/put','Admin\ServicesController@put');
    Route::get('admin/services/delete/{id}','Admin\ServicesController@delete');

    Route::get('admin/careers','Admin\CareersController@get');
    Route::get('admin/careers/post','Admin\CareersController@add');
    Route::post('admin/careers/post','Admin\CareersController@post');
    Route::get('admin/careers/put/{id}','Admin\CareersController@single');
    Route::post('admin/careers/put','Admin\CareersController@put');
    Route::get('admin/careers/delete/{id}','Admin\CareersController@delete');

    Route::get('admin/course','Admin\CareersController@courses');
    Route::post('admin/course','Admin\CareersController@coursesdata');

    Route::get('admin/privacy_policy', 'Admin\PrivacypolicyController@index');
    Route::post('admin/privacy_policy','Admin\PrivacypolicyController@put');

    Route::get('admin/terms_of_use', 'Admin\TermsofuseController@index');
    Route::post('admin/terms_of_use','Admin\TermsofuseController@put');

    // job category

    Route::get('admin/job_category','Admin\Job_categoryController@get');
    Route::get('admin/job_category/post','Admin\Job_categoryController@add');
    Route::post('admin/job_category/post','Admin\Job_categoryController@post');
    Route::get('admin/job_category/put/{id}','Admin\Job_categoryController@single');
    Route::post('admin/job_category/put','Admin\Job_categoryController@put');
    Route::get('admin/job_category/delete/{id}','Admin\Job_categoryController@delete');

    Route::get('admin/jobs_comments','Admin\Job_commentsController@index');
    Route::get('admin/jobs_comments/delete/{id}','Admin\Job_commentsController@delete');

    Route::get('admin/employment_type','Admin\Employment_typeController@get');
    Route::get('admin/employment_type/post','Admin\Employment_typeController@add');
    Route::post('admin/employment_type/post','Admin\Employment_typeController@post');
    Route::get('admin/employment_type/put/{id}','Admin\Employment_typeController@single');
    Route::post('admin/employment_type/put','Admin\Employment_typeController@put');
    Route::get('admin/employment_type/delete/{id}','Admin\Employment_typeController@delete');


    Route::get('admin/district','Admin\DistrictController@get');
    Route::get('admin/district/post','Admin\DistrictController@add');
    Route::post('admin/district/post','Admin\DistrictController@post');
    Route::get('admin/district/put/{id}','Admin\DistrictController@single');
    Route::post('admin/district/put','Admin\DistrictController@put');
    Route::get('admin/district/delete/{id}','Admin\DistrictController@delete');

    Route::get('admin/country','Admin\CountryController@get');
    Route::get('admin/country/post','Admin\CountryController@add');
    Route::post('admin/country/post','Admin\CountryController@post');
    Route::get('admin/country/put/{id}','Admin\CountryController@single');
    Route::post('admin/country/put','Admin\CountryController@put');
    Route::get('admin/country/delete/{id}','Admin\CountryController@delete');

    Route::get('admin/jobs','Admin\JobsController@get');
    Route::get('admin/jobs/post','Admin\JobsController@add');
    Route::post('admin/jobs/post','Admin\JobsController@post');
    Route::get('admin/jobs/put/{id}','Admin\JobsController@single');
    Route::post('admin/jobs/put','Admin\JobsController@put');
    Route::get('admin/jobs/delete/{id}','Admin\JobsController@delete');
    Route::get('admin/jobs/search','Admin\JobsController@search');

    Route::get('admin/testimonials','Admin\TestimonialsController@get');
    Route::get('admin/testimonials/post','Admin\TestimonialsController@add');
    Route::post('admin/testimonials/post','Admin\TestimonialsController@post');
    Route::get('admin/testimonials/put/{id}','Admin\TestimonialsController@single');
    Route::post('admin/testimonials/put','Admin\TestimonialsController@put');
    Route::get('admin/testimonials/delete/{id}','Admin\TestimonialsController@delete');

    Route::get('admin/school','Admin\SchoolController@get');
    Route::get('admin/school/post','Admin\SchoolController@add');
    Route::post('admin/school/post','Admin\SchoolController@post');
    Route::get('admin/school/put/{id}','Admin\SchoolController@single');
    Route::post('admin/school/put','Admin\SchoolController@put');
    Route::get('admin/school/delete/{id}','Admin\SchoolController@delete');

    Route::get('admin/school_type','Admin\SchooltypeController@get');
    Route::get('admin/school_type/post','Admin\SchooltypeController@add');
    Route::post('admin/school_type/post','Admin\SchooltypeController@post');
    Route::get('admin/school_type/put/{id}','Admin\SchooltypeController@single');
    Route::post('admin/school_type/put','Admin\SchooltypeController@put');
    Route::get('admin/school_type/delete/{id}','Admin\SchooltypeController@delete');

    Route::get('admin/applicants','Admin\ApplicantsController@get');

    Route::get('admin/general','Admin\GeneralController@get');
    Route::post('admin/generallogo','Admin\GeneralController@logo');
    Route::post('admin/generallinks','Admin\GeneralController@links');
    Route::post('admin/fabout','Admin\GeneralController@fabout');
    Route::post('admin/testimonialslogo','Admin\GeneralController@testimonialslogo');

    Route::get('admin/newsletter','Admin\NewsletterController@get');
    Route::get('admin/newsletter/delete/{id}','Admin\NewsletterController@delete');

    Route::post('admin/newsletter/search','Admin\NewsletterController@search');
    
    Route::get('admin/jobseekers','Admin\JobseekersController@get');
    Route::get('admin/jobseekers/add','Admin\JobseekersController@add');
    Route::post('admin/jobseekers/post','Admin\JobseekersController@post');
    Route::get('admin/jobseekers/put/{id}','Admin\JobseekersController@single');
    Route::post('admin/jobseekers/put','Admin\JobseekersController@put');
    Route::get('admin/jobseekers/delete/{id}','Admin\JobseekersController@delete');
    Route::get('admin/jobseekers/deactive/{id}','Admin\JobseekersController@deactive');
    Route::get('admin/jobseekers/active/{id}','Admin\JobseekersController@active');
    Route::get('admin/jobseekers/search','Admin\JobseekersController@search');
    Route::get('admin/jobseekers/cv/{id}','Admin\JobseekersController@cv');
    Route::post('admin/jobseekers/cvput','Admin\JobseekersController@cvpost');

    Route::get('admin/employers','Admin\EmployersController@get');
    Route::get('admin/employers/add','Admin\EmployersController@add');
    Route::post('admin/employers/post','Admin\EmployersController@post');
    Route::get('admin/employers/put/{id}','Admin\EmployersController@single');
    Route::post('admin/employers/put','Admin\EmployersController@put');
    Route::get('admin/employers/delete/{id}','Admin\EmployersController@delete');
    Route::get('admin/employers/deactive/{id}','Admin\EmployersController@deactive');
    Route::get('admin/employers/active/{id}','Admin\EmployersController@active');
    Route::get('admin/employers/search','Admin\EmployersController@search');
    Route::get('admin/employers/disallowed/{id}','Admin\EmployersController@disallowed');
    Route::get('admin/employers/allowed/{id}','Admin\EmployersController@allowed');


    Route::get('admin/partners','Admin\PartnersController@get');
    Route::get('admin/partners/post','Admin\PartnersController@add');
    Route::post('admin/partners/post','Admin\PartnersController@post');
    Route::get('admin/partners/put/{id}','Admin\PartnersController@single');
    Route::post('admin/partners/put','Admin\PartnersController@put');
    Route::get('admin/partners/delete/{id}','Admin\PartnersController@delete');

    Route::get('admin/sidebars','Admin\SidebarController@index');
    Route::post('admin/sidebar/common','Admin\SidebarController@common');
    Route::post('admin/sidebar/enter','Admin\SidebarController@enter');

    Route::get('admin/newsletters','Admin\NewsletterController@all');
    Route::get('admin/newsletter/post','Admin\NewsletterController@post');
    Route::post('admin/newsletter/post','Admin\NewsletterController@insert');
    Route::get('admin/newsletter/put/{id}','Admin\NewsletterController@edit');
    Route::post('admin/newsletter/put','Admin\NewsletterController@update');
    Route::get('admin/newsletters/delete/{id}','Admin\NewsletterController@deletenews');
    Route::get('admin/newsletter/search','Admin\NewsletterController@search');

    Route::get('admin/newslettermember/add','Admin\NewsletterController@memberadd');
    Route::get('admin/newslettermember/put/{id}','Admin\NewsletterController@membersingle');
    Route::post('admin/memberadd/puts','Admin\NewsletterController@memberupdate');
    Route::get('admin/memberadd/post','Admin\NewsletterController@membersave');
    Route::get('admin/sendnewsletter','Admin\NewsletterController@send');
    Route::post('admin/newsletter/send','Admin\NewsletterController@sends');

    Route::get('admin/industry','Admin\IndustryController@index');
    Route::post('admin/industry/post','Admin\IndustryController@post');
    Route::get('admin/industry/delete/{id}','Admin\IndustryController@delete');
    Route::get('admin/industry/put/{id}','Admin\IndustryController@single');
    Route::post('admin/industry/put','Admin\IndustryController@put');

    Route::get('admin/position','Admin\PositionController@index');
    Route::post('admin/position/post','Admin\PositionController@post');
    Route::get('admin/position/delete/{id}','Admin\PositionController@delete');
    Route::get('admin/position/put/{id}','Admin\PositionController@single');
    Route::post('admin/position/put','Admin\PositionController@put');

    Route::get('admin/requests','Admin\TradesController@index');
    Route::get('admin/requests/delete/{id}','Admin\TradesController@delete');
    Route::get('/admin/requests/post','Admin\TradesController@add');
    Route::post('/admin/requests/insert','Admin\TradesController@post');
    Route::get('/admin/requests/put/{id}','Admin\TradesController@single');
    Route::post('/admin/requests/update','Admin\TradesController@put');
    Route::get('/admin/requests/search','Admin\TradesController@search');
    Route::get('admin/requests/view/{id}','Admin\TradesController@view');

    Route::get('/admin/trade','Admin\VettedController@index');
    Route::get('admin/trade/add','Admin\VettedController@add');
    Route::post('admin/trade/post','Admin\VettedController@post');
    Route::get('admin/trade/put/{id}','Admin\VettedController@single');
    Route::post('admin/trade/put','Admin\VettedController@put');
    Route::get('admin/trade/delete/{id}','Admin\VettedController@delete');
    Route::get('admin/trade/deactive/{id}','Admin\VettedController@deactive');
    Route::get('admin/trade/active/{id}','Admin\VettedController@active');
    Route::get('admin/trade/search','Admin\VettedController@search');

    Route::get('/admin/staff-requester','Admin\ArtisanController@index'); 
    Route::get('admin/staff-requester/add','Admin\ArtisanController@add');
    Route::post('admin/staff-requester/post','Admin\ArtisanController@post');
    Route::get('admin/staff-requester/put/{id}','Admin\ArtisanController@single');
    Route::post('admin/staff-requester/put','Admin\ArtisanController@put');
    Route::get('admin/staff-requester/delete/{id}','Admin\ArtisanController@delete');
    Route::get('admin/staff-requester/deactive/{id}','Admin\ArtisanController@deactive');
    Route::get('admin/staff-requester/active/{id}','Admin\ArtisanController@active');
    Route::get('admin/staff-requester/search','Admin\ArtisanController@search');

    Route::get('admin/courses','Admin\CoursesController@index');
    Route::get('admin/courses/add','Admin\CoursesController@add');
    Route::post('admin/courses/post','Admin\CoursesController@post');
    Route::get('admin/courses/put/{id}','Admin\CoursesController@single');
    Route::post('admin/courses/put','Admin\CoursesController@put');
    Route::get('admin/courses/delete/{id}','Admin\CoursesController@delete');

    Route::get('admin/coursescategory','Admin\CoursesController@category');
    Route::post('admin/coursescategory/post','Admin\CoursesController@categoryadd');
    Route::get('admin/coursescategory/put/{id}','Admin\CoursesController@categorysingle');
    Route::post('admin/coursescategory/put','Admin\CoursesController@categoryupdate');
    Route::get('admin/coursescategory/delete/{id}','Admin\CoursesController@categorydelete');

    Route::get('admin/city','Admin\CityController@index');
    Route::post('admin/city/post','Admin\CityController@post');
    Route::get('admin/city/put/{id}','Admin\CityController@single');
    Route::post('admin/city/put','Admin\CityController@update');
    Route::get('admin/city/delete/{id}','Admin\CityController@delete');

    Route::get('admin/speciality','Admin\SpecialityController@index');
    Route::post('admin/speciality/post','Admin\SpecialityController@post');
    Route::get('admin/speciality/put/{id}','Admin\SpecialityController@single');
    Route::post('admin/speciality/put','Admin\SpecialityController@update');
    Route::get('admin/speciality/delete/{id}','Admin\SpecialityController@delete');


    Route::get('admin/newsletter/excel','Admin\ExcelController@newsletter');
    Route::get('admin/jobs/excel','Admin\ExcelController@jobs');
    Route::get('admin/jobseekers/excel','Admin\ExcelController@jobseekers');
    Route::get('admin/employers/excel','Admin\ExcelController@employers'); 

    Route::get('admin/paymentmode','Admin\PaymentmodeController@index');
    Route::post('admin/paymentmode/post','Admin\PaymentmodeController@post');
    Route::get('admin/paymentmode/put/{id}','Admin\PaymentmodeController@single');
    Route::post('admin/paymentmode/update','Admin\PaymentmodeController@update');
    Route::get('admin/paymentmode/delete/{id}','Admin\PaymentmodeController@delete');

    Route::get('admin/facebook','FacebookkeyController@index');
    Route::post('admin/facebook/update','FacebookkeyController@update');
    

});


Route::group(['middleware'=> ['auth', 'activated', 'role:user']], function () {

    Route::get('/jobsearch','JobController@index');
    Route::post('/jobsearch','JobController@searchdata');

    Route::get('cv/{id}','CvController@index');
    Route::post('/cvs','CvController@post');

    Route::post('/apply','ApplicationsController@apply');
    Route::get('/jobapplications','ApplicationsController@index');
    Route::get('/application/delete/{id}','ApplicationsController@delete');
    
    Route::post('/jobseekerprofile','JobseekerController@put');
    Route::get('/jobseekerdelete','JobseekerController@deletejobseeker');
});

Route::group(['middleware'=> ['auth', 'activated', 'role:employer','check:1']], function () {
 
    Route::get('/managejob','EmployerController@managejob');
    Route::get('/jobpost','EmployerController@index');
    Route::post('/job/post','EmployerController@postjobs');
    Route::get('/jobdelete/{slug}','EmployerController@deletejob');
    Route::get('/jobedit/{slug}','EmployerController@editjob');
    Route::post('/jobedit','EmployerController@updatejob');

    Route::get('/candidates','CandidatesController@index');
    Route::get('/viewprofile/{id}','CandidatesController@profiles');
    Route::get('/jobcandidates/{slug}','CandidatesController@jobcandidates');
    Route::get('/application/remove/{id}','CandidatesController@delete'); 
 
    Route::get('/cvsearch','SearchcvController@index');
    Route::post('/cvsearch','SearchcvController@searchcv');
    Route::get('/viewprofiles/{id}','SearchcvController@profile');

    Route::post('/employerprofile','EmployerController@profile');
    Route::get('/employerdelete','EmployerController@deleteemployer');
    

});


Route::group(['middleware'=> ['auth', 'activated', 'role:provider','check:1']], function () {
       
    Route::get('/staff','StaffController@index');
    Route::post('/providerprofile','ProviderController@index');

    Route::get('/managerequest','StaffController@managerequest');
    Route::get('staffdelete/{id}','StaffController@deleterequest');
    Route::get('staffedit/{id}','StaffController@singlerequest');
    Route::post('staffedit','StaffController@updaterequest');
    Route::get('deletevetted','StaffController@deletevetted');

});


Route::group(['middleware'=> ['auth', 'activated', 'role:customer','check:1']], function () {
       
    Route::post('/customerprofile','CustomerController@index');
    Route::get('/deleterequest','CustomerController@delete');

    Route::get('/photogallery','CustomerController@portfolio');
    Route::post('/customer/image','CustomerController@portfoliosave');
    Route::get('/photogallery/delete/{id}','CustomerController@portfoliodelete');

});


Route::get('admin/setjobs','Admin\SetjobsController@allset');
Route::get('setuserdata','SettingsdataController@userset');
Route::get('setresumedata','SettingsdataController@resumeset');
// xeme it solutions routes

Route::get('logins/{slug}','CheckController@index');

Route::get('/registers','RegistersController@index');
Route::get('/register_job_seeker','RegistersController@jobseeker');
Route::get('/register_employer','RegistersController@employer');
Route::get('/register_trade','RegistersController@services');
Route::get('/register_staff_requester','RegistersController@services_provider');


Route::post('/searchjobs','WelcomeController@searchjob');
Route::get('/searchlocation/{id}','WelcomeController@searchjoblocation');
Route::get('/searchjobcategory/{id}','WelcomeController@searchjobcategory');


Route::get('/contact_us','ContactusController@index');
Route::post('/contactsend','ContactusController@send');

Route::post('/requiestsend','ContactusController@requiestsend');

Route::get('/about_us', 'AboutusController@index');

Route::get('/faqs', 'FaqsController@index');

Route::get('/partners', 'PartnersController@index');
Route::get('/partner/{slug}', 'PartnersController@getpartners');
Route::get('/partner/single/{id}','PartnersController@getusers');

Route::post('partner/search','PartnersController@search');

Route::get('/entrepreneurship', 'EntrepreneurshipController@index');
Route::get('/entrepreneurship/{slug}', 'EntrepreneurshipController@single');

Route::get('/careers','CareersController@index');
Route::get('/career/{slug}','CareersController@career');
Route::get('/educational/{slug}','CareersController@educational');
Route::get('/schools/{slug}','CareersController@schools');
Route::get('/school/{slug}','CareersController@school');
Route::post('/schoolcategory/post','CareersController@schoolcategory');

Route::get('/events/{id}','EventsController@index');
Route::get('/events/single/{id}','EventsController@single');
Route::post('/searchevents','EventsController@searchevent');

Route::get('/employers','ServicesController@index');
Route::get('/employers/{slug}','ServicesController@single');

Route::get('/jobs','JobsController@index');
Route::get('/job/{slug}','JobsController@single');
Route::post('/jobcomment/send','JobsController@sendcomment');
Route::get('/jobfillter/get/{id}/{loc}','JobsController@filtersdata');
Route::get('/jobfillterlocation/get/{id}/{loc}','JobsController@filterslocations');
Route::get('/jobsfillter/get/{id}','JobsController@filters');
 

Route::get('/terms_of_use','TermsofuseController@index');
Route::get('/privacy_policy','PrivacypolicyController@index');

Route::get('/blog','BlogController@index');
Route::get('/blogs/{slug}','BlogController@blogscat');
Route::get('/blog/{slug}','BlogController@single');
Route::post('/blogcomment/post','BlogController@postcomment');

Route::post('/newsletter/send','NewsletterController@send');
Route::get('/testimonials','TestimonialsController@index');

Route::get('/donate','DonateController@index');

Route::get('/recruiter','RecruiterController@index');

Route::get('/requeststaff','RequeststaffController@index');

Route::get('/trades','TradesController@index');
Route::match(['GET', 'POST'],'/searchtrade', array(
    'as' => 'trades',
    'uses' => 'TradesController@search'
));

Route::match(['GET', 'POST'],'/searchtrades', array(
    'as' => 'trades',
    'uses' => 'TradesController@searchdata'
));
Route::get('/trade/single/{id}/{name}','TradesController@single');
Route::get('/trade/speciality/{id}','TradesController@speciality');
//Route::post('/searchtrade','TradesController@search');
//Route::post('/searchtrades','TradesController@searchdata');

Route::get('/sitemap','SitemapController@index');

Route::get('/forgotpassword','ForgotPasswordController@index');
Route::post('/forgotpassword','ForgotPasswordController@emailsend');

Route::get('/resetpassword/{token}','ForgotPasswordController@resetpass');
Route::post('/resetpassword','ForgotPasswordController@savepass');

Route::get('accountpending','AccountpaddingController@index');

Route::get('fraudandsecurity','FraudController@index');

Route::get('jobsdata','JobsapiController@display');
