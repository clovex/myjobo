<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" >
$(function() {
$(".submit").click(function() {

var name_news = $("#name_news").val();
var email_news = $("#email_news").val();

var dataString = 'name_news=' + name_news + '&email_news=' + email_news;
if(name_news=='' || email_news=='')
{
$('.success').fadeOut(200).hide();
$('.error').fadeOut(200).show();
}
else
{
var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
if(reg.test(email_news) == false) {
$('.success').fadeOut(200).hide();
$('.error').fadeOut(200).hide();
$('.error_email').fadeOut(200).show();
}else{
	$.ajax({
type: "POST",
url: "code/news.php",
data: dataString,
success: function(msg){
if(msg=='ok')
{ 
	 document.getElementById('name_news').value="";
	 document.getElementById('email_news').value="";
	$('.success').fadeOut(200).show();
	$('.error').fadeOut(200).hide();
	$('.error_email').fadeOut(200).hide();
	 
}else{
	$('.error').fadeOut(200).show();
}

}
});

}	
}
//return false;
});
});
</script>

<div class="Receive-sec">
  <h2> Receive job alerts via email </h2>
  <div class="mail-form">
    <form method="post" name="form">
      <span class="error" style="display:none; color:#F00"> Please Enter Valid Data</span> <span class="error_email" style="display:none; color:#F00"> Please Enter Valid Email Address</span> <span class="success" style="display:none; color:#090"> You have registered successfully</span>
      <div class="fieldset">
        <input type="text" placeholder="Fullname" name="name_news" id="name_news" />
      </div>
      <div class="fieldset">
        <input type="text" placeholder="Email" name="email_news" id="email_news" />
      </div>
      <div class="fieldset"> <a class="submit">
        <input type="image" src="images/submit.png" style="float:right; margin-bottom:20px;" />
        </a>
        <div id="loadmoreajaxloader" style="display:none; color:#000000; font-size:36px">
          <center>
            <img src="images/loader.gif" />
          </center>
        </div>
      </div>
    </form>
  </div>
</div>
