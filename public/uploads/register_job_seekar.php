<?php 
include "function/classes.php";
include "function/function.php";
$country_object = new Website;
$county_list = $country_object->country_list();
$company = $country_object->company_for_front();
include "header.php";?>
<div class="container">
<?php //include "slider.php";?>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<script language="javascript" type="text/javascript">
function reg_validation(){
	
	if(document.getElementById('email').value=='')
	  {
		  document.getElementById('email').style.border= '1px solid #ff0000';
		  return false;
	  }
	  
	  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var address = document.getElementById('email').value;
		if(reg.test(address) == false) {
		document.getElementById('email').style.border= '1px solid #ff0000';
		return false;
		}

	if(document.getElementById('password').value=='')
	  {
		  document.getElementById('password').style.border= '1px solid #ff0000';
		  return false;
	  }
	if(document.getElementById('confirm_pass').value=='')
	  {
		  document.getElementById('confirm_pass').style.border= '1px solid #ff0000';
		  return false;
	  }
	if(document.getElementById('confirm_pass').value!=document.getElementById('password').value)
	  {
		  document.getElementById('confirm_pass').style.border= '1px solid #ff0000';
		  return false;
	  }


	if(document.getElementById('f_name').value=='')
	  {
		  document.getElementById('f_name').style.border= '1px solid #ff0000';
		  return false;
	  }
	if(document.getElementById('l_name').value=='')
	  {
		  document.getElementById('l_name').style.border= '1px solid #ff0000';
		  return false;
	  }
	if(document.getElementById('country').value=='select')
	  {
		  document.getElementById('country').style.border= '1px solid #ff0000';
		  return false;
	  }

	if(document.getElementById('city').value=='')
	  {
		  document.getElementById('city').style.border= '1px solid #ff0000';
		  return false;
	  }


	if(document.getElementById('terms').checked==false)
	  {
		  document.getElementById('check').style.border= '1px solid #ff0000';
		  return false;
	  }
	}
	
	function border(id){
		if(id=='terms')
		{
		document.getElementById('check').style.border= '1px solid #ccc';
		return false;

			}else{
		document.getElementById(id).style.border= '1px solid #ccc';
		return false;
			}
		}
</script>
<SCRIPT type="text/javascript">
<!--
/*
Credits: Bit Repository
Source: http://www.bitrepository.com/web-programming/ajax/username-checker.html 
*/

pic1 = new Image(16, 16); 
pic1.src="images/loader.gif";

$(document).ready(function(){

$("#email").change(function() { 

var usr = $("#email").val();

if(usr.length >= 4)
{
$("#status").html('<img src="images/loader.gif" align="absmiddle">&nbsp;Checking availability...');

    $.ajax({  
    type: "POST",  
    url: "code/check.php",  
    data: "email="+ usr,  
    success: function(msg){  
   
   $("#status").ajaxComplete(function(event, request, settings){ 

	if(msg == 'OK')
	{ 
        $("#email").removeClass('object_error'); // if necessary
		$("#email").addClass("object_ok");
		document.getElementById('email').style.border= '1px solid #ccc';
		$(this).html('&nbsp;<img src="images/tick.gif" align="absmiddle">');
	}  
	else  
	{  
		$("#email").removeClass('object_ok'); // if necessary
		$("#email").addClass("object_error");
		$(this).html(msg);
	}  
   
   });

 } 
   
  }); 

}
else
	{
	$("#status").html('<font color="red">' +
'The username should have at least <strong>4</strong> characters.</font>');
	$("#email").removeClass('object_ok'); // if necessary
	$("#email").addClass("object_error");
	}

});

});

//-->
function numericFilter(txb) {
             txb.value = txb.value.replace(/[^\0-9]/ig, "");
             }
</SCRIPT>
<section class="mid" style="background-color:#F5F6F7">
<?php //include "left.php";?>
<section class="mid-zone" style="margin-left:300px;">
<div class="recent-jobs">
<table style="margin-left:150px;">
<tr>
<td><h2><strong>Job Seeker: &nbsp;&nbsp;Sign Up Now</strong></h2></td>
<td> <span style="color:#F00; margin-left:11px">*</span> <span style="color:#000; font-family:Arial, Helvetica, sans-serif;">Required Fields</span></td>
</tr>
</table>

 <span style="color:#093; margin-left:20px"><?php echo $_SESSION['success']; if($_SESSION['success']!=""){unset($_SESSION['success']);}?></span>
<span style="color:#f00; margin-left:20px"><?php echo $_SESSION['error']; if($_SESSION['error']!=""){unset($_SESSION['error']);}?></span>

<div class="mail-form" style="margin-left:20px; margin-top:15px">
<form action="code/post_data.php" method="post" name="reg_form" id="reg_form" onSubmit="return reg_validation();">
<table width="84%" border="0" style="margin-left:58px">
  <tr>
    <td width="34%" height="56"><span style="font-size:15px; color:#000; font-family:Arial, Helvetica, sans-serif;">Email address</span><span style="color:#F00">*</span></td>
    <td width="66%">
    <div class="fieldset">
    <input type="text" id="email" name="email"/><input type="hidden" name="user_type" value="2" /><div id="status" style="float:right"></div>
    </div>
    </td>
  </tr>
  <tr>
    <td width="34%" height="56"><span style="font-size:15px; color:#000; font-family:Arial, Helvetica, sans-serif;">Password</span><span style="color:#F00">*</span></td>
    <td width="66%">
    <div class="fieldset">
    <input type="password" id="password" name="password" onKeyPress="border(this.id)"/>
    </div>
    </td>
  </tr>
  <tr>
    <td width="34%" height="56"><span style="font-size:15px; color:#000; font-family:Arial, Helvetica, sans-serif;">Confirm Password</span><span style="color:#F00">*</span></td>
    <td width="66%">
    <div class="fieldset">
    <input type="password" id="confirm_pass" name="confirm_pass" onKeyPress="border(this.id)"/>
    </div>
    </td>
  </tr>
  
  <tr>
    <td width="34%" height="56"><span style="font-size:15px; color:#000; font-family:Arial, Helvetica, sans-serif;">First Name</span><span style="color:#F00">*</span></td>
    <td width="66%">
    <div class="fieldset">
    <input type="text" id="f_name" name="f_name" onKeyPress="border(this.id)"/>
    </div>
    </td>
  </tr>
  <tr>
    <td width="34%" height="56"><span style="font-size:15px; color:#000; font-family:Arial, Helvetica, sans-serif;">Last Name</span><span style="color:#F00">*</span></td>
    <td width="66%">
    <div class="fieldset">
    <input type="text" id="l_name" name="l_name" onKeyPress="border(this.id)"/>
    </div>
    </td>
  </tr>
  <!--<tr>
    <td width="34%" height="56"><span style="font-size:15px; color:#000; font-family:Arial, Helvetica, sans-serif;">District</span><span style="color:#F00">*</span></td>
    <td width="66%">
    <div class="fieldset">
    <select name="country" id="country" class="select_box" onChange="border(this.id)">
    <option value="select">-----------------------------------------------------------------</option>
    <?php foreach($county_list as $country_data):?>
    <option value="<?= $country_data['country_id'];?>"><?= $country_data['name'];?></option>
    <?php endforeach;?>
    </select>
    </div>
    </td>
  </tr>  -->
  
  <tr>
    <td width="34%" height="56"><span style="font-size:15px; color:#000; font-family:Arial, Helvetica, sans-serif;">Phone Number</span></td>
    <td width="66%">
    <div class="fieldset">
    <input type="text" id="phone" name="phone" onKeyPress="border(this.id);numericFilter(this)" onkeyup="numericFilter(this)"/>
    </div>
    </td>
  </tr>
  <tr>
    <td width="34%" height="56"></td>
    <td width="66%">
    <div class="fieldset">&nbsp;&nbsp;
    <input type="checkbox" checked id="terms" value="yes" name="terms" onChange="border(this.id)"/>
    <span style="font-size:11px">I have read and agree to Myjobo.com's terms of use and privacy policy</span>
    </div>
    </td>
  </tr>
  <tr>
    <td width="34%" height="56"></td>
    <td width="66%">
    <div class="fieldset">&nbsp;&nbsp;
    <input type="checkbox" checked="checked" id="mail" value="yes" name="mail" onChange="border(this.id)"/>
    <span style="font-size:11px">Email me Myjobo.com updates,job opportunities,  newletters, announcements, and events</span>  

    </div>
    </td>
  </tr>
  <tr>
    <td width="34%" height="56"></td>
    <td width="66%">
    <div class="fieldset">
    <input type="image" src="images/register.png" name="seekar_reg"/> OR <a href="?login&oauth_provider=facebook"><img src="images/fb.png"/></a>

    </div>

    </td>
  </tr>

</table>
</form>
</div>

</div>
</section>
<?php //include "right.php";?>
<div class="clr"></div>
</section>
</div>
<?php include "footer.php";?>