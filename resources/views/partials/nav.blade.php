<style>
.facebook.setjobfb
{
     background-color: #4a6d9d;
}
.twitter.setjobtw
{
    background-color: #3bc1ed;   
}
</style>
<header class="sticky-header alternative">

<div class="container">
    <div class="sixteen columns">
        <div class="row" style="margin: 0px !important;">
        <!-- Logo -->
        <div id="logo" class="three columns">
            @foreach($logoData as $logo)
            <h1><a href="{{ URL::to('') }}">
                   <img src="{{ URL::to('public/uploads') }}/{{ $logo->contents }}" alt="My jobo" style="width: 196px;" />
                </a>
            </h1>
            @endforeach
        </div>
        <div class="ten columns">
            <a href="http://www.tnm.co.mw" target="_blank"><img src="{{ URL::to('public/uploads/4G-Lite Flash ad.gif') }}" alt="My jobo" style="margin-top: 24px;" /></a>
        </div>
        
        <nav id="navigation" class="menu">
            <ul id="responsive">

                <li><a id="current" class="setmenu" href="{{ URL::to('') }}">Home</a></li>
                <li><a class="jobs" href="{{ URL::to('jobs') }}">Jobs</a></li>
                <li><a class="services" href="{{ URL::to('employers') }}">Services</a></li>
                <li><a class="tradesdata" href="{{ URL::to('trades') }}">Artisans/Trades</a></li>
                <li><a class="career" href="{{ URL::to('careers') }}">Learning Resources</a></li>
                <!--<li><a class="entrepreneurship" href="{{ URL::to('entrepreneurship') }}">Entrepreneurship</a></li>-->
                <li><a class="partners" href="{{ URL::to('partners') }}">Partners</a></li>
                <li><a class="blogss" href="{{ URL::to('blog') }}">Career Resources</a></li>
                <li><a class="faqs" href="{{ URL::to('faqs') }}">FAQs</a></li>
                <li><a class="about_us" href="{{ URL::to('about_us') }}">About Us</a></li>
                <li><a class="contact_us" href="{{ URL::to('contact_us') }}">Contact Us</a></li>
            </ul>


            <ul class="float-right">
                 @if (Auth::guest())                    
                 <!--<li><a href="{{ route('login') }}"><i class="fa fa-lock"></i>  {!! trans('titles.login') !!}</a></li>-->
                 <li><a href="{{ route('login') }}" style="background-color: #47a447;color: #fff"><i class="fa fa-lock"></i>  Log in</a></li>
                 <li><a href="{{ URL::to('registers') }}" style="background-color: #F45B06;color: #fff"><i class="fa fa-user"></i>{!! trans('titles.register') !!}</a></li>
                 <label>Email: info@myjobo.com</label>
                @else
                 <div class="dropdown">
                    <a class="dropdown-toggle" role="button" data-toggle="dropdown" style="background-color: #f0f0f0; color: #333; padding: 10px 14px;">{{ Auth::user()->email }}
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li {{ Request::is('profile/'.Auth::user()->name, 'profile/'.Auth::user()-> name . '/edit') ? 'class=active' : null }}>
                                {!! HTML::link(url('home'), trans('titles.profile')) !!}
                            </li>
                          <li>
                                <a class="setcolorulli" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    {!! trans('titles.logout') !!}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                    <!--li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                            @if ((Auth::User()->profile) && Auth::user()->profile->avatar_status == 1)
                                <img src="{{ Auth::user()->profile->avatar }}" alt="{{ Auth::user()->name }}" class="user-avatar-nav">
                            @else
                                <div class="user-avatar-nav"></div>
                            @endif

                            {{ Auth::user()->email }} 
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li {{ Request::is('profile/'.Auth::user()->name, 'profile/'.Auth::user()-> name . '/edit') ? 'class=active' : null }}>
                                {!! HTML::link(url('home'), trans('titles.profile')) !!}
                            </li>
                            <li>
                                <a class="setcolorulli" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    {!! trans('titles.logout') !!}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li-->
                    <label>Email: info@myjobo.com</label>
                @endif
            </ul>

        </nav>
        </div>
        <!-- Navigation -->
        <div id="mobile-navigation">
            <a href="index-2.html#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
        </div>

    </div>
</div>
</header>




<!--nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">{!! trans('titles.toggleNav') !!}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="{{ url('/') }}">
                {!! trans('titles.app') !!}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav">
                @role('admin')
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Admin <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li {{ Request::is('users', 'users/' . Auth::user()->id, 'users/' . Auth::user()->id . '/edit') ? 'class=active' : null }}>{!! HTML::link(url('/users'), Lang::get('titles.adminUserList')) !!}</li>
                            <li {{ Request::is('users/create') ? 'class=active' : null }}>{!! HTML::link(url('/users/create'), Lang::get('titles.adminNewUser')) !!}</li>
                            <li {{ Request::is('themes','themes/create') ? 'class=active' : null }}>{!! HTML::link(url('/themes'), Lang::get('titles.adminThemesList')) !!}</li>
                            <li {{ Request::is('logs') ? 'class=active' : null }}>{!! HTML::link(url('/logs'), Lang::get('titles.adminLogs')) !!}</li>
                            <li {{ Request::is('php') ? 'class=active' : null }}>{!! HTML::link(url('/php'), Lang::get('titles.adminPHP')) !!}</li>
                            <li {{ Request::is('routes') ? 'class=active' : null }}>{!! HTML::link(url('/routes'), Lang::get('titles.adminRoutes')) !!}</li>
                        </ul>
                    </li>
                @endrole
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())                    <li><a href="{{ route('login') }}">{!! trans('titles.login') !!}</a></li>
                    <li><a href="{{ route('register') }}">{!! trans('titles.register') !!}</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                            @if ((Auth::User()->profile) && Auth::user()->profile->avatar_status == 1)
                                <img src="{{ Auth::user()->profile->avatar }}" alt="{{ Auth::user()->name }}" class="user-avatar-nav">
                            @else
                                <div class="user-avatar-nav"></div>
                            @endif

                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li {{ Request::is('profile/'.Auth::user()->name, 'profile/'.Auth::user()->name . '/edit') ? 'class=active' : null }}>
                                {!! HTML::link(url('/profile/'.Auth::user()->name), trans('titles.profile')) !!}
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    {!! trans('titles.logout') !!}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
@if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

-->