<!-- Footer
================================================== -->
<div class="margin-top-0"></div>

<div id="footer">
	<!-- Main -->
	<div class="container">

		<div class="four columns">
			<h4>About Us</h4>
			<ul class="footer-links">
				<li><a href="{{ URL::to('about_us') }}">About Myjobo.com</a></li>
				<li><a href="{{ URL::to('contact_us') }}">Contact us</a></li>
				<li><a href="{{ URL::to('partners') }}">Partner with us</a></li>
				<li><a href="{{ URL::to('testimonials') }}">Testimonials</a></li>
				<li><a href="{{ URL::to('fraudandsecurity') }}">Fraud and Security</a></li>
				<li><a href="{{ URL::to('terms_of_use') }}">Terms of Service</a></li>
				<li><a href="{{ URL::to('privacy_policy') }}">Privacy Policy</a></li>
				<li><a href="{{ URL::to('sitemap') }}">Sitemap</a></li>
				
			</ul>
			<!--<h4>About</h4>
			<p>
				@foreach($abouts as $abt)
					{{ $abt->contents }}
				@endforeach
			</p>
			<a href="{{ URL::to('about_us') }}" class="btn btn-success">Get Started</a>-->
		</div>

		<div class="four columns">
			<h4>For Employers</h4>
			<ul class="footer-links">
				<li><a href="{{ URL::to('jobpost') }}">Advertise a job</a></li>
				<li><a href="{{ URL::to('cvsearch') }}">CV Search</a></li>
				<li><a href="{{ URL::to('contact_us') }}">Contact us</a></li>
				<li><a href="{{ URL::to('services') }}">Psychodiagnostics Tests</a></li>
				<li><a href="{{ URL::to('services') }}">Reference checks</a></li>
				<li><a href="{{ URL::to('recruiter') }}">Recruiter resources</a></li>
				<li><a href="{{ URL::to('register_employer') }}">Register</a></li>
				
			</ul>
			
		</div>		

		<div class="four columns">
			<h4>Jobseeker</h4>
			<ul class="footer-links">
				<li><a href="{{ URL::to('jobs') }}">Find Jobs</a></li>
				<li><a href="{{ URL::to('cv') }}">Upload CV</a></li>
				<li><a href="">Register to be vetted</a></li>
				<li><a href="{{ URL::to('register_trade') }}">Register as a Trade or Artisan</a></li>
				<li><a href="{{ URL::to('blog') }}">Tips for Jobseekers</a></li>
				<li><a href="{{ URL::to('partners') }}">Browse Employers</a></li>
			</ul>
		</div>

		<div class="four columns">
			<h4>Receive job alerts via email</h4>
			<div id="sendnews">

				{{ csrf_field() }}
				<div class="form-group" style="margin-bottom: 15px;">
					<input type="text" name="name" id="name" class="form-control" placeholder="Full Name" style="padding: 6px 12px;">
					<label id="msgname" style="margin: 0px !important"></label>
				</div>

				<div class="form-group" style="margin-bottom: 15px;">
					<input type="email" name="email" id="email" class="form-control" placeholder="Email" style="padding: 6px 12px;">
					<label id="msgemail" style="margin: 0px !important"></label>
				</div>

				<div class="form-group" style="margin-bottom: 15px;">
					<button id="send" class="btn btn-success">Submit</button>
				</div>
				<figure id="submessage" style="font-size: 12px;"></figure>
			</div>	
		</div>

	</div>

	<!-- Bottom -->
	<div class="container">
		<div class="footer-bottom">
			<div class="sixteen columns">
				<h4>Follow Us</h4>
				<ul class="social-icons">
					@foreach($links as $s22) 
					<li>
						<a class="facebook" href="{{ json_decode($s22->contents)->facebook }}" target="_blank">
							<i class="icon-facebook"></i>
						</a>
					</li>
					<li>
						<a class="twitter" href="{{ json_decode($s22->contents)->google }}" target="_blank">
							<i class="icon-twitter"></i>
						</a>
					</li>
					<li>
						<a class="gplus" href="{{ json_decode($s22->contents)->linkedin }}" target="_blank">
							<i class="icon-gplus"></i>
						</a>
					</li>
					<li>
						<a class="linkedin" href="{{ json_decode($s22->contents)->youtube }}" target="_blank">
							<i class="icon-linkedin"></i>
						</a>
					</li>
					<li>
						<a class="youtube" href="{{ json_decode($s22->contents)->twitter }}" target="_blank">
							<i class="icon-youtube"></i>
						</a>
					</li>
					@endforeach
				</ul>
				<div class="copyrights">Copyright © 2017 MyJobo.com. All Rights Reserved</div>
			</div>
		</div>
	</div>

</div>

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div>

<script>
	$(function(){
		$("#send").click(function(){

			var x = $("#name").val();
    		if (x == "") {
        		$("#msgname").html("Name Required").fadeIn().delay(1000).fadeOut('fast');
        		return false;
    		}
    		
    		var y = $("#email").val();
    		var atpos = y.indexOf("@");
    		var dotpos = y.lastIndexOf(".");
    		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=y.length) {
        		$("#msgemail").html("Not a valid e-mail address").fadeIn().delay(1000).fadeOut('fast');
        		return false;
    		}

			$.ajax({
					'url':'{{ URL::to("newsletter/send") }}',
					'type':'POST',
					'data':{'email':$('#sendnews input[name=email]').val(),'name':$('#sendnews input[name=name]').val(),'_token': $('#sendnews input[name=_token]').val()},
					'success':function(message)
					{	
						
						if(message == 1)
						{
							$("#email").val("");
							$("#name").val("");
							$("#submessage").html("Your Added our mailing list and Newsletter").fadeIn('show').delay(5000).fadeOut('fast');
						}
						else
						{
							$("#email").val("");
							$("#name").val("");
							$("#submessage").html("Your Email Already Added Our Newsletter").fadeIn('show').delay(5000).fadeOut('fast');
						}
					}
					});
		});
	});
</script>
<style type="text/css">
	.sethover:hover
	{
		color:#26ae61 !important;
	}
</style>
