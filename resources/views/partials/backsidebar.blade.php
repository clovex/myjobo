<aside class="main-sidebar">
<section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">Menu</li>
        
        <li class="mainmenuset active">
          <a href="{{ URL::to('home') }}">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        
        <li class="treeview mainmenuset users">
          <a href="#">
            <i class="fa fa-users"></i> <span>Manage Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu "> 
            <li class="users1">
              <a href="{{ URL::to('admin/jobseekers') }}">
              <i class="fa fa-circle-o"> </i>Job Seekers</a>
            </li> 
            <li class="users2">
              <a href="{{ URL::to('admin/employers') }}">
              <i class="fa fa-circle-o"> </i>Employers</a>
            </li>
            <li class="users4">
              <a href="{{ URL::to('admin/staff-requester') }}">
              <i class="fa fa-circle-o"> </i>Staff Requester</a>
            </li>
          </ul>
        </li>

        <li class="treeview mainmenuset trade">
          <a href="#">
            <i class="fa fa-users"></i> <span>Manage Trade</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu "> 
            <li class="trade3">
              <a href="{{ URL::to('admin/trade') }}">
              <i class="fa fa-circle-o"> </i>Trade or Artisan</a>
            </li> 
            <!--li class="trade4">
              <a href="{{ URL::to('admin/city') }}">
              <i class="fa fa-circle-o"> </i>City</a>
            </li-->
            <li class="trade5">
              <a href="{{ URL::to('admin/speciality') }}">
              <i class="fa fa-circle-o"> </i>Speciality</a>
            </li>
          </ul>
        </li>

        <li class="treeview mainmenuset jobs">
          <a href="#">
            <i class="fa fa-check-square"></i> <span>Manage Jobs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu "> 
            <li class="job1">
              <a href="{{ URL::to('admin/jobs') }}">
              <i class="fa fa-circle-o"> </i>Jobs</a>
            </li> 
            <li class="job2">
              <a href="{{ URL::to('admin/applicants') }}">
              <i class="fa fa-circle-o"> </i>Applicants Jobs</a>
            </li>
            <li class="job3">
              <a href="{{ URL::to('admin/job_category') }}">
              <i class="fa fa-circle-o"> </i>Job Categories</a>
            </li>
            <li class="job4">
              <a href="{{ URL::to('admin/employment_type') }}">
              <i class="fa fa-circle-o"> </i>Employment Type</a>
            </li>
            <li class="job5">
              <a href="{{ URL::to('admin/jobs_comments') }}">
              <i class="fa fa-circle-o"> </i>Jobs Comments</a>
            </li>
          </ul>
        </li>

        <li class="treeview mainmenuset trades">
          <a href="#">
            <i class="fa fa-bookmark-o"></i> <span>Staff Request</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu "> 
            <li class="trades1">
              <a href="{{ URL::to('admin/requests') }}">
              <i class="fa fa-circle-o"> </i>Requests</a>
            </li> 
            <li class="trades2">
              <a href="{{ URL::to('admin/industry') }}">
              <i class="fa fa-circle-o"> </i>Industry</a>
            </li>
            <li class="trades3">
              <a href="{{ URL::to('admin/position') }}">
              <i class="fa fa-circle-o"> </i>Position</a>
            </li>
          </ul>
        </li>

        <li class="treeview mainmenuset edication">
          <a href="#">
            <i class="fa fa-graduation-cap"></i> <span>Careers & Education</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"> 
            <li class="edu1">
              <a href="{{ URL::to('admin/careers') }}">
              <i class="fa fa-circle-o"> </i>Careers & Education</a>
            </li> 
            <li class="edu2">
              <a href="{{ URL::to('admin/course') }}">
              <i class="fa fa-circle-o"> </i>Courses and Events</a>
            </li>
          </ul>
        </li>

        <li class="treeview mainmenuset courses">
          <a href="#">
            <i class="fa fa-calendar"></i> <span>Manage Learning Events</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu "> 
            <li class="courses1">
              <a href="{{ URL::to('admin/courses') }}">
              <i class="fa fa-circle-o"> </i>Learning Events</a>
            </li> 
            <li class="courses2">
              <a href="{{ URL::to('admin/coursescategory') }}">
              <i class="fa fa-circle-o"> </i>Learning Events Category</a>
            </li>
          </ul>
        </li>


        <li class="treeview mainmenuset newsletter">
          <a href="#">
            <i class="fa fa-check-square"></i> <span>Newsletter</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu "> 
            <li class="newsletter1">
              <a href="{{ URL::to('admin/newsletter') }}">
              <i class="fa fa-circle-o"> </i>Members</a>
            </li> 
            <li class="newsletter2">
              <a href="{{ URL::to('admin/newsletters') }}">
              <i class="fa fa-circle-o"> </i>Newsletters</a>
            </li>
            <li class="newsletter3">
              <a href="{{ URL::to('admin/sendnewsletter') }}">
              <i class="fa fa-circle-o"> </i>Send Newsletter</a>
            </li>
          </ul>
        </li>

       

        <li class="treeview mainmenuset pages">
          <a href="#">
            <i class="fa fa-file"></i> <span>Pages</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="page1">
              <a href="{{ URL::to('admin/about_us') }}">
              <i class="fa fa-circle-o"> </i>About Us</a>
            </li>
            <li class="page2">
              <a href="{{ URL::to('admin/faqs') }}">
              <i class="fa fa-circle-o"> </i>Faqs</a>
            </li>
            <li class="page4">
              <a href="{{ URL::to('admin/contact_us') }}">
              <i class="fa fa-circle-o"> </i>Contact Us</a>
            </li>
            <li class="page5">
              <a href="{{ URL::to('admin/privacy_policy') }}">
              <i class="fa fa-circle-o"> </i>Privacy Policy</a>
            </li>
            <li class="page6">
              <a href="{{ URL::to('admin/terms_of_use') }}">
              <i class="fa fa-circle-o"> </i>Terms of Use</a>
            </li>
            <li class="page7">
              <a href="{{ URL::to('admin/fraud') }}">
              <i class="fa fa-circle-o"> </i>Fraud and Security</a>
            </li>
          </ul>
        </li>

        <!--li class="treeview mainmenuset entrepreneurship">
          <a href="#">
            <i class="fa fa-briefcase"></i> <span>Entrepreneurship</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="entrepreneurship1">
              <a href="{{ URL::to('admin/entrepreneurship') }}">
              <i class="fa fa-circle-o"> </i>Entrepreneurship</a>
            </li>
            <li class="entrepreneurship2">
              <a href="{{ URL::to('admin/events') }}">
              <i class="fa fa-circle-o"> </i>Events</a>
            </li>  
          </ul>
        </li-->




        <li class="treeview mainmenuset blogs">
          <a href="#">
            <i class="fa fa-rss"></i> <span>Blog</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="blog1">
              <a href="{{ URL::to('admin/blogs') }}">
              <i class="fa fa-circle-o"> </i>Blogs</a>
            </li>
            <li class="blog2">
              <a href="{{ URL::to('admin/blog_category') }}">
              <i class="fa fa-circle-o"> </i>Blog Category</a>
            </li>
            <li class="blog3">
              <a href="{{ URL::to('admin/blog_comments') }}">
              <i class="fa fa-circle-o"> </i>Blog Comments</a>
            </li>
          </ul>
        </li>

        <li class="treeview mainmenuset services">
          <a href="#">
            <i class="fa fa-calendar-check-o"></i> <span>Services</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"> 
            <li class="services1">
              <a href="{{ URL::to('admin/servicespage') }}">
              <i class="fa fa-circle-o"> </i>Page Content</a>
            </li> 
            <li class="services2">
              <a href="{{ URL::to('admin/services') }}">
              <i class="fa fa-circle-o"> </i>All Services</a>
            </li>
          </ul>
        </li>

        

        <li class="treeview mainmenuset schools">
          <a href="#">
            <i class="fa fa-university"></i> <span>Schools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"> 
            <li class="school1">
              <a href="{{ URL::to('admin/school') }}">
              <i class="fa fa-circle-o"> </i>School</a>
            </li>
            <li class="school2">
              <a href="{{ URL::to('admin/school_type') }}">
              <i class="fa fa-circle-o"> </i>School Type</a>
            </li>
          </ul>
        </li>

        <li class="treeview mainmenuset settings">
          <a href="#">
            <i class="fa fa-cog"></i> <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"> 
            <li class="settings1">
              <a href="{{ URL::to('admin/district') }}">
              <i class="fa fa-circle-o"> </i>District</a>
            </li> 
            <li class="settings2">
              <a href="{{ URL::to('admin/country') }}">
              <i class="fa fa-circle-o"> </i>Country</a>
            </li>
            <li class="settings3"> 
              <a href="{{ URL::to('admin/testimonials') }}">
              <i class="fa fa-circle-o"> </i>Testimonials</a>
            </li>
            <li class="settings4">
              <a href="{{ URL::to('admin/general') }}">
              <i class="fa fa-circle-o"> </i>General</a>
            </li>
            <li class="settings5">
              <a href="{{ URL::to('admin/partners') }}">
              <i class="fa fa-circle-o"> </i>Partners Categories</a>
            </li>
            <li class="settings6">
              <a href="{{ URL::to('admin/sidebars') }}">
              <i class="fa fa-circle-o"> </i>Page Sidebars</a>
            </li>
            <li class="settings7">
              <a href="{{ URL::to('admin/paymentmode') }}">
              <i class="fa fa-circle-o"> </i>Payment Mode</a>
            </li>
            <li class="settings8">
              <a href="{{ URL::to('admin/facebook') }}">
              <i class="fa fa-circle-o"> </i>Facebook</a>
            </li>
          </ul>
        </li>        
      </ul>
    </section>
  </aside>