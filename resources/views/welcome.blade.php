@extends('layouts.app')

@section('template_title')
    Find jobs in Malawi- Search for jobs on myjobo.com
@endsection
@section('template_description')
Start your job search now with Myjobo.com. Malawi's #1 job site.  Find jobs and career information, recruit quality staff, or hire skilled trades. Myjobo.com
@endsection
@section('content')
<script src="{{ URL::to('frontend/scripts/jssor.slider-23.1.6.mini.js') }}" type="text/javascript"></script>
<script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_options = {
              $AutoPlay: 1,
              $Idle: 0,
              $SlideDuration: 5000,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 150,
              $Cols: 7
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 980);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/
        });
    </script>

<style type="text/css">
.setimg {
    border: 2px solid #ccc;
    height: 50px;
    margin: 10px 0;
    width: 100%;
}
.footer-links.datasetul a
    {
        color: #333 !important;
    }
    .footer-links.datasetul a:hover
    {
        color: #333 !important;
    }
    .post-container
    {
        margin-bottom: 0px !important;
    }
    .post-content
    {
        padding: 15px 0px !important;
    }
    .sociadata li a {
        background-color: #26ae61;
    }
    .sociadata li .twitter::before, .sociadata li .facebook::before
    {
        color: #fff !important;
    }
    .sociadata li a
    {
        text-decoration: none !important; 
    }
    .menu1 ul > li > a
    {
        background-color: rgba(255, 255, 255, 0.1);
        border: 1px solid rgba(255, 255, 255, 0.3);
        color: #fff !important;
        margin: 5px 5px !important;
        padding: 5px 10px !important;
    }
    .setacol
    {
        background-color: none !important;
        line-height: 14px !important;

    }
    .menu1 > ul ul li:hover > a
    {
        background-color:#26ae61 !important;
    }
    .setstyle:hover
    {
        background:rgba(38, 174, 97, 0.05);
        border-bottom: 1px solid #26ae61;
        border-top: 1px solid #26ae61;
    }
    .setstyle
    {
        padding-top: 15px;
        padding-bottom: 15px;
        border-bottom: 1px solid #e0e0e0;
        border-top: 1px solid #e0e0e0;
    }
    .setjobfb, .setjobtw
    {
        width: 30px !important;
        height: 30px !important;
    }
    .setjobtw i, .setjobtw::before
    {
        margin: 8px 0px 0px 8px !important;
    }
    .setjobfb i, .setjobfb::before
    {
        margin: 8px 0px 0px 10px !important;
    }
    .setsliderbtn
    {
        background-color: #26ae61; 
        box-shadow: 5px 5px 5px #888888; 
        font-size: 17px !important; 
        margin: 10px 0px !important;
        padding: 10px 5px !important;
    }
    .setslider2btn
    {
        background-color: #f45b06 !important; 
        box-shadow: 5px 5px 5px #888888; 
        font-size: 17px !important; 
        margin: 10px 0px !important;
        padding: 10px 5px !important;
    }
    .setslider2btn2
    {
        background-color: #269F5B !important; 
        box-shadow: 5px 5px 5px #888888; 
        font-size: 17px !important; 
        margin: 10px 0px !important;
        padding: 10px 5px !important;
    }
    .button.widget-btn.btn-block.setsliderbtn
    {
        font-size: 16px!important;
        background-color: #F45B06;
        padding: 3px!important;
    }
    .button.widget-btn.btn-block.setslider2btn
    {
        font-size: 17px!important;
    }
    .menu ul li.sfHover ul li a.sf-with-ul, .menu ul ul li a
    {
        font-size: 13px!important;
    }
    .widget-box
    {
    	padding: 15px 35px!important;
    }
    .searbtn
    {
        /*position: absolute !important; */
        right: 0px !important; 
        background-color: #F45B06 !important; 
        color: #fff !important; 
        width: auto !important; 
        /*right: 5px !important; */
        height: 45px !important; 
        line-height: 20px !important;
        width: 23% !important;
        margin-left: 2% !important;
        padding: 0px 2% !important
    }
    .testimonials-slider li p
    {
    	padding: 50px 60px!important;
    }
    .flex-control-nav
    {
    	bottom:20px!important;
    }
</style>
<div class="clearfix"></div>


<!-- Banner
================================================== -->
<div id="banner" style="background-image: url({{ URL::to('frontend/images/2017-06-16.jpg') }}); z-index:9;" class="parallax background" data-img-width="2000" data-img-height="1330" data-diff="400">
    <div class="container">
        <div class="search-container">
            <div class="col-md-8 col-lg-8 col-sm-8">
                <h2 style="text-align: center;font-size: 2.8em;font-weight: 600">Start your job search now...</h2>
                    
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('searchjobs') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" class="ico-01" name="keyword" style="padding:0px;height:45px;width: 75%; margin: 0px;text-align: center;" placeholder="job title, keywords or company name"/>
                          
                        <!--<button style="position: absolute; right: 0px; background-color: transparent; color: #494949; width: 10%; right: 5px; height: 64px;"><i class="fa fa-search"></i></button>-->
                            
                        <button type="submit" class="searbtn">Search</button>
                    </div>
                </form>
                
                <div class="col-md-12 col-lg-12 col-sm-12" style="padding: 0px;">
                    <hr style="margin:15px 0px 10px 0px !important;">
                </div>
                
                <div class="browse-jobs" style="margin-top: 0px;">
                    
                    <div class="menu menu1">
                        <ul style="margin: 0px !important;">
                            <li style="padding: 10px 0px; margin: 0px;">Browse job vacancies by</li>
                            <li style="cursor: pointer; margin-left: 4px !important;"><a>Category</a>
                                <ul style="width: auto;">
                                @foreach($category as $cat)
                                    <li><a href="{{ URL::to('searchjobcategory') }}/{{ $cat->id }}" class="setacol" style="padding:5px 5px !important; ">{{ $cat->name }}</a></li>
                                @endforeach  
                                    <li><a class="setacol" style="padding:5px 5px !important; " href="{{ URL::to('jobs') }}">More</a></li>      
                                </ul>
                            </li>
                            <li style="padding: 10px 5px;">or</li>
                            <li style="cursor: pointer; margin-left: 4px !important;"><a>Location</a>
                                <ul style="width: auto;">
                                @foreach($dist as $dis)
                                    <li>
                                        <a href="{{ URL::to('searchlocation') }}/{{ $dis->name }}" class="setacol">{{ $dis->name }}</a>
                                    </li>
                                @endforeach  
                                <li><a class="setacol" href="{{ URL::to('jobs') }}">More</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <a href="" style="visibility: hidden;"></a>     
                </div>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-4">
                <div class="row">
                    <div class="col-md-2 col-lg-2 col-sm-2">&nbsp;</div>
                
                    <div class="col-md-8 col-lg-8 col-sm-8 text-center" style="background-color: rgba(255,255,255,0.7); padding: 5% 5%;">
                    
                        <h3 style="color: #494949; font-size: 20px;font-weight: 600">How can we help you?</h3>
                        <hr style="margin: 0px 0 15px;">
                    
                        <a href="{{ URL::to('register_job_seeker') }}" class="button widget-btn btn-block setsliderbtn"><i class="fa fa-graduation-cap" aria-hidden="true"></i> I am looking for a job</a>

                        <a href="{{ URL::to('register_employer') }}" class="button widget-btn btn-block setsliderbtn" style="background-color: #269F5B;"><i class="fa fa-users" aria-hidden="true"></i> I want to recruit staff</a>

                        <a href="{{ URL::to('register_employer') }}" class="button widget-btn btn-block setsliderbtn" ><i class="fa fa-graduation-cap" aria-hidden="true"></i> I want to advertise jobs</a>
                    </div>
                    
                    <div class="col-md-2 col-lg-2 col-sm-2">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container">
    <div class="col-md-12 col-lg-12 col-sm-12 text-center">
        <h2 style="font-size: 2.33333rem;">Find your dream job with Malawi's #1 job site</h2>
        <h4 class="margin-bottom-25">Browse our latest vacancies now</h4>
    </div>
    
    <div class="col-md-8 col-lg-8 col-sm-8">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                @foreach($jobs as $j)
                <div class="col-md-12 col-lg-12 col-sm-12 setstyle">
                    
                    <div class="col-md-1 col-lg-1 col-sm-1" style="padding: 0px !important;">
                       @if($j->logo != null)
                        <img src="{{ URL::to('public/uploads') }}/{{ $j->logo }}" alt="" style="width: 100%;">
                        @else    
                        <img src="{{ URL::to('public/uploads/company.png') }}" alt="" style="width: 100%;">
                        @endif
                    </div>

                    <div class="col-md-11 col-lg-11 col-sm-11">
                        <h4 style="font-size: 16px; line-height: 32px;">{{ $j->title }} 
                        </h4>
                        
                        <label>
                        <span><i class="fa fa-calendar"> Posted On: </i> <span style="color: #23527c;">{{ Carbon\Carbon::parse($j->created_at)->format('d-m-Y') }}</span></span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-map-marker"> Location: </i> <span style="color: #23527c;">{{ $j->city }}</span></span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-calendar-times-o"> Job Deadline: </i> <span style="color: #23527c;">{{ Carbon\Carbon::parse($j->deadline)->format('d-m-Y') }}</span></span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <strong><a href="{{ URL::to('job') }}/{{ $j->slug }}">View</a></strong>
                        </label>
                    </div> 
                </div>
                @endforeach
            </div>
            <div class="text-right" style="margin-bottom: 25px;font-weight: 700;">        
                <a href="{{ URL::to('jobs') }}" style="color: #f45b06;"><i class="fa fa-plus-circle"></i> Show me more job vacancies</a>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-4">
            <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <h3 class="text-center">Featured Jobs</h3>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <!-- Controls -->
                <div class="controls pull-right hidden-xs">
                    <a class="left fa fa-chevron-left btn btn-success" href="#carousel-example"
                        data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-success" href="#carousel-example"
                            data-slide="next"></a>
                </div>
            </div>
            </div>
            <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" style="background-color: #fdfdfd; border:1px solid #e2e2e2; min-height: 300px;">
                @php $ll = 1; @endphp
                @foreach($featured as $ferd)
                @if($ll == 1)
                <div class="item active" style="padding: 35px 38px 31px;">
                @else
                <div class="item" style="padding: 35px 38px 31px;">
                @endif
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-lg-3 col-md-3 col-sm-3">&nbsp;</div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            @if($ferd->logo != null)
                            <img src="{{ URL::to('public/uploads') }}/{{ $ferd->logo }}" style="width: 100%">
                             @else    
                            <img src="{{ URL::to('public/uploads/nologo.png') }}" alt="" style="width: 100%;">
                            @endif
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">&nbsp;</div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                            <h4>{{ $ferd->title }}</h4>
                            <span><i class="fa fa-calendar"></i> {{ Carbon\Carbon::parse($ferd->created_at)->format('jS F, Y') }}</span>
                            <span><i class="fa fa-map-marker"></i> {{ $ferd->city }}</span>
                            <a href="{{ URL::to('job') }}/{{ $ferd->slug }}" class="button btn-block text-center" style="margin-top: 10px;">View</a>
                            </div>
                        </div>
                    </div>
                </div>
                @php $ll++; @endphp
                @endforeach  
            </div>
            </div>
        <div class="widget text-center">
            <div class="widget-box" style="text-align: center;">
                <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
            </div>
            <div class="widget-box" style="text-align: center;">
                <a href="https://talentguardians.org/" target="_blank" class="button widget-btn" style="background-color: #26ae61; box-shadow: 10px 10px 5px #888888;">Internship & Volunteering Opportunities</a>
            </div>
        </div>

       

        <div class="widget">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:block;"
                 data-ad-client="ca-pub-7726299907154841"
                 data-ad-slot="2292045413"
                 data-ad-format="auto"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>

</div>

<div class="container">
    <hr>    
</div>

<div class="container">
    <div class="col-md-12 text-center">
        <h3 class="" style="font-size: 2.33333rem;">Recruitment made simple</h3>
        <h6 class="margin-bottom-25">Recruit screened, vetted staff in the quickest and least expensive way ever</h6>
        <ul id="popular-categories" style="list-style:none;">
            <li><a href="{{ URL::to('requeststaff') }}" style="color: #26ae61;"><i class="ln ln-icon-Cash-register2" style="color: #26ae61;"></i> Cashiers</a></li>
            <li><a href="{{ URL::to('requeststaff') }}" style="color: #26ae61;"><i class="ln ln-icon-Truck" style="color: #26ae61;"></i> Drivers</a></li>
            <li><a href="{{ URL::to('requeststaff') }}" style="color: #26ae61;"><i class="ln ln-icon-Student-Female" style="color: #26ae61;"></i> Receptionists</a></li>
            <li><a href="{{ URL::to('requeststaff') }}" style="color: #26ae61;"><i class="ln ln-icon-Female-2" style="color: #26ae61;"></i> Secretaries</a></li>
            <li><a href="{{ URL::to('requeststaff') }}" style="color: #26ae61;"><i class="ln ln-icon-Plates" style="color: #26ae61;"></i> Waiters</a></li>
            <li><a href="{{ URL::to('requeststaff') }}" style="color: #26ae61;"><i class="ln ln-icon-Male" style="color: #26ae61;"></i> Security Guards</a></li>
            <li><a href="{{ URL::to('requeststaff') }}" style="color: #26ae61;"><i class="ln ln-icon-Beer" style="color: #26ae61;"></i> Shop Assistants</a></li>
            <li><a href="{{ URL::to('requeststaff') }}" style="color: #26ae61;"><i class="ln  ln-icon-Laptop-3" style="color: #26ae61;"></i> Data Clerks</a></li>
        </ul>
        <div class="clearfix"></div> 
        <div class="margin-top-30"></div>

        <a href="{{ URL::to('requeststaff') }}" class="centered" style="color: #f45b06;font-size: 18px!important;font-weight: 700;">Request Staff Now</a>
        <div class="margin-bottom-50"></div>
    </div>
</div>
<!-- background-color: #26ae61;  id="banner"-->
<div style="background-color: rgba(0, 0, 0, 0.75); /*background-image: url({{ URL::to('frontend/images/Matthijs-de-Bruijne-via-httpgdr.cascoprojects.org_.jpg') }}); margin-bottom: 0px;*/" class="parallax background" data-img-width="2000" data-img-height="1330" data-diff="400">

    <div class="container" style="padding:50px 0px;">
        
        <h2 style="width:100%;padding-bottom:20px;text-align: center;font-size: 2.8em;font-weight: 600;color: #fff;">Amisili aku Malawi</h2>
        
        <div class="col-md-8 col-sm-8 col-lg-8">    
            <h2 style="color: #fff; font-size: 46px; letter-spacing: -1px; margin-bottom: 30px;">Helping you find vetted artisans and skilled trades.
            </h2>
            
            <p style="font-size:18px !important; color:#fff;">Search through our reviewed and monitored trades and artisans for free.</p>
            <form method="POST" enctype="multipart/form-data" action="{{ URL::to('searchtrade') }}">
            {{ csrf_field() }}        
            <input class="ico-01" name="keyword" placeholder="Trade or Artisan (eg plumber, electrician, carpenter etc)" type="text">
            
            <div class="caption sfb" data-x="center" data-y="400" data-speed="400" data-start="1600" data-easing="easeOutExpo" style="margin-top: 30px;">
                <button type="submit" class="slider-button">Get Started</button>
            </div>
            </form>
        </div>

        <div class="col-md-4 col-sm-4 col-lg-4">
            <div class="row">
                <div class="col-md-2 col-sm-2 col-lg-2">&nbsp;</div>
                <div class="col-md-8 col-sm-8 col-lg-8 text-center" style="background-color: rgb(255,255,255); padding: 5% 5%;">
                    <h3 style="color: #494949; font-size: 20px;font-weight: 600">How can we help you?</h3>
                    <hr style="margin: 0px 0 15px;">
                    <a href="{{ URL::to('trades') }}" class="button widget-btn btn-block setslider2btn2">
                        <i class="fa fa-wrench" aria-hidden="true"></i>  I am looking for skilled trades</a>
                    <a href="{{ URL::to('register_trade') }}" class="button widget-btn btn-block setslider2btn" ><i class="fa fa-briefcase" aria-hidden="true"></i> I want to register as a trade or artisan</a>
                </div>
                <div class="col-md-2 col-sm-2 col-lg-2">&nbsp;</div>
            </div>     
        </div>
    </div>
</div>        


<div class="section-background top-0" style="border:none !important; margin: 0px !important; padding: 25px 0px;">
    <div class="container">
         <div class="col-md-12 text-center">
            <h2 style="font-size: 2.33333rem;">Our Services</h2>
        </div>
        <div class="col-md-3 text-center">
            <a href="{{ URL::to('services') }}">
            <div class="icon-box rounded alt" style="margin: 10px auto;">
                <i class="ln ln-icon-Note " style="color: #26ae61;"></i>
                <h4>Job Advertising</h4>
            </div>
            </a>
        </div>

        <div class="col-md-3 text-center">
            <a href="{{ URL::to('services') }}">
            <div class="icon-box rounded alt" style="margin: 10px auto;">
                <i class="ln ln-icon-Business-Man" style="color: #26ae61;"></i>
                <h4>Vetted Staff</h4>
            </div>
            </a>
        </div>

        <div class="col-md-3 text-center" style="margin: 10px auto;">
            <a href="{{ URL::to('services') }}">
            <div class="icon-box rounded alt">
                <i class="ln ln-icon-Search-onCloud" style="color: #26ae61;"></i>
                <h4>CV Database</h4>
            </div>
            </a>
        </div>

        <div class="col-md-3 text-center" style="margin: 10px auto;">
            <a href="{{ URL::to('services') }}">
            <div class="icon-box rounded alt">
                <i class="ln ln-icon-Business-ManWoman" style="color: #26ae61;"></i>
                <h4>Psychodiagnostics Tests</h4>
            </div>
            </a>
        </div>

    </div>
</div>
<!-- Testimonials id="testimonials" rgba(0,0,255,0.8) -->
<div style="background-color: rgba(0, 0, 0, 0.75);">
    <!-- Slider -->
    <div class="container">
        <div class="sixteen columns">
            <div class="testimonials-slider">
                  <ul class="slides">
                    @foreach($testimonials as $test)
                    <li>  
                      <p><b style="font-size: 2.8em; font-weight: 600;">Partner Success Stories</b><br>
                       <b style="font-size: 20px; letter-spacing: -1px;">What they say about Myjobo.com</b><br>
                      {!! substr(strip_tags($test->description),0,300) !!}...
                      <span>{{ $test->name }}</span>
                      <a href="{{ URL::to('testimonials') }}"><strong style="color: #f45b06;">View More</strong></a>
                      </p>
                    </li>
                    @endforeach
                  </ul>
            </div>
        </div>
    </div>
</div>
<h3 class="centered-headline" style="margin-top: 0px; background-color: #fff;">Partners Who Have Trusted Us <span>Here are some of the Employers who have used our services</span></h3>
<div class="clearfix"></div>

<div class="container" style="padding-bottom: 15px;">
    
    <div class="sixteen columns"> 

        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:150px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0, 0, 0, 0.7);"></div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:150px;overflow:hidden;">
            @foreach($logos as $l)
            @php $id = base64_encode($l->id); @endphp
            <div>
            <a href="{{ URL::to('partner/single') }}/{{ $id }}" style="margin: 0px 10px;">
                <img data-u="image" src="{{ URL::to('public/uploads') }}/{{ $l->image }}" />
            </a>
            </div>
            @endforeach 
        </div>
    </div>
</div>

</div>
@endsection            
