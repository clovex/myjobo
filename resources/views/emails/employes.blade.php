<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
	/* Base */

body, body *:not(html):not(style):not(br):not(tr):not(code) {
    font-family: Avenir, Helvetica, sans-serif;
    box-sizing: border-box;
}

body {
    background-color: #F2F4F6;
    color: #74787E;
    height: 100%;
    line-height: 1.4;
    margin: 0;
    width: 100% !important;
    -webkit-text-size-adjust: none;
}

p,
ul,
ol,
blockquote {
    line-height: 1.4;
    text-align: left;
}

a {
    color: #3869D4;
}

a img {
    border: none;
}

/* Typography */

h1 {
    color: #2F3133;
    font-size: 19px;
    font-weight: bold;
    margin-top: 0;
    text-align: left;
}

h2 {
    color: #2F3133;
    font-size: 16px;
    font-weight: bold;
    margin-top: 0;
    text-align: left;
}

h3 {
    color: #2F3133;
    font-size: 14px;
    font-weight: bold;
    margin-top: 0;
    text-align: left;
}

p {
    color: #74787E;
    font-size: 16px;
    line-height: 1.5em;
    margin-top: 0;
    text-align: left;
}

p.sub {
    font-size: 12px;
}

img {
    max-width: 100%;
}

/* Layout */

.wrapper {
    background-color: #f5f8fa;
    margin: 0;
    padding: 0;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.content {
    margin: 0;
    padding: 0;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

/* Header */

.header {
    padding: 25px 0;
    text-align: center;
}

.header a {
    color: #bbbfc3;
    font-size: 19px;
    font-weight: bold;
    text-decoration: none;
    text-shadow: 0 1px 0 white;
}

/* Body */

.body {
    background-color: #FFFFFF;
    border-bottom: 1px solid #EDEFF2;
    border-top: 1px solid #EDEFF2;
    margin: 0;
    padding: 0;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.inner-body {
    background-color: #FFFFFF;
    margin: 0 auto;
    padding: 0;
    width: 700px;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 700px;
}

/* Subcopy */

.subcopy {
    border-top: 1px solid #EDEFF2;
    margin-top: 25px;
    padding-top: 25px;
}

.subcopy p {
    font-size: 12px;
}

/* Footer */

.footer {
    margin: 0 auto;
    padding: 0;
    text-align: center;
    width: 570px;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 570px;
}

.footer p {
    color: #AEAEAE;
    font-size: 12px;
    text-align: center;
}

/* Tables */

.table table {
    margin: 30px auto;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.table th {
    border-bottom: 1px solid #EDEFF2;
    padding-bottom: 8px;
}

.table td {
    color: #74787E;
    font-size: 15px;
    line-height: 18px;
    padding: 10px 0;
}

.content-cell {
    padding: 35px;
}

/* Buttons */

.action {
    margin: 30px auto;
    padding: 0;
    text-align: center;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.button {
    border-radius: 3px;
    box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
    color: #FFF;
    display: inline-block;
    text-decoration: none;
    -webkit-text-size-adjust: none;
}

.button-blue {
    background-color: #3097D1;
    border-top: 10px solid #3097D1;
    border-right: 18px solid #3097D1;
    border-bottom: 10px solid #3097D1;
    border-left: 18px solid #3097D1;
}

.button-green {
    background-color: #2ab27b;
    border-top: 10px solid #2ab27b;
    border-right: 18px solid #2ab27b;
    border-bottom: 10px solid #2ab27b;
    border-left: 18px solid #2ab27b;
}

.button-red {
    background-color: #bf5329;
    border-top: 10px solid #bf5329;
    border-right: 18px solid #bf5329;
    border-bottom: 10px solid #bf5329;
    border-left: 18px solid #bf5329;
}

/* Panels */

.panel {
    margin: 0 0 21px;
}

.panel-content {
    background-color: #EDEFF2;
    padding: 16px;
}

.panel-item {
    padding: 0;
}

.panel-item p:last-of-type {
    margin-bottom: 0;
    padding-bottom: 0;
}

/* Promotions */

.promotion {
    background-color: #FFFFFF;
    border: 2px dashed #9BA2AB;
    margin: 0;
    margin-bottom: 25px;
    margin-top: 25px;
    padding: 24px;
    width: 100%;
    -premailer-cellpadding: 0;
    -premailer-cellspacing: 0;
    -premailer-width: 100%;
}

.promotion h1 {
    text-align: center;
}

.promotion p {
    font-size: 15px;
    text-align: center;
}

</style>
</head>
<body>
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>

    <table class="wrapper" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table class="content" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
    					<td class="header">
        					<a href="{{ URL::to('') }}">MyJobo</a>
    					</td>
					</tr>


                    <!-- Email Body -->
                    <tr>
                        <td class="body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="inner-body" align="center" width="700" cellpadding="0" cellspacing="0">
                                <!-- Body content -->
                                <tr>
                                    <td class="content-cell">
                                    
                                    <p>Dear Employer,<br></p>
                                    
                                    <p>WE have advertised your job on Myjobo.com and it is now live. To view the advert, please click the following link: <a href="{{ URL::to('job') }}/@php print_r($data['slug']); @endphp" target='_blank'>Click Here</a><br></p>    

                                    <p>Your job advert instantly reaches out to thousands of our users who can now apply immediately. Normally, within an hour of posting, you start receiving responses from job seekers. Some applicants apply using Myjobo.com and these are automatically forwarded to your email address.<br>
We are now transitioning to Paying Services and if you have not yet heard from us about our exciting packages that make finding talent faster, easier and cost-effective, please contact us on: info@myjobo.com . The first step is to register as an Employer and be in control of your job advertising and recruitment needs.
Why use Myjobo.com: 
<br>
• Save time and your recruitment costs while in control of the advertising process.<br>
• Your advert reaches out to job seekers 24 hours a day, 7 days a week until the deadline; You can extend the deadline at no extra cost.<br>
• You can also receive job applications through our website straight to your dashboard or email at no extra cost<br>
• You reach out to thousands of our users instantly and through our Email service sent to over 40,000 job seekers daily<br>
• You promote your brand in Malawi and internationally on our website
<br>
Please let us know if you have any reservations with us posting your advert on Myjobo.com. For further information send us an email: info@myjobo.com
<br></p>

    <div style="margin-top: 10px; margin-bottom: 10px;">
        Sincerely Yours,<br>
        Myjobo.com Team<br>
        <a href="http://www.myjobo.com" target="_blank">www.myjobo.com</a>
    </div>
    <div style="margin-top: 20px; margin-bottom: 20px; text-align: center;">
        Powered by Myjobo.com<br>
        This email has been sent to you by:<br>
        Myjobo.com Enviro Future Limited, P.O. Box 30511, Lilongwe 3, Malawi.<br>
        Email: info@myjobo.com<br>
        To unsubscribe click here
    </div>    
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
    					<td>
					        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
					            <tr>
					                <td class="content-cell" align="center">
					                    © 2017 Myjobo. All rights reserved.
					                </td>
					            </tr>
					        </table>
    					</td>
					</tr>

                </table>
            </td>
        </tr>
    </table>
</body>
</html>