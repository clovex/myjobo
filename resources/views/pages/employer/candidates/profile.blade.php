@extends('layouts.app')

@section('template_title')
    Jobseeker Profile | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Jobseeker Profile</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('home') }}">Profile</a></li>
                    <li><a href="{{ URL::to('candidates') }}">Candidates</a></li>
                    <li>Jobseeker Profile</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12 text-right">
        <a class="button" href="{{ URL::to('candidates') }}">Back</a>
    </div>
    @foreach($users as $u)
    	<div class="col-md-3 col-lg-3 col-sm-3 text-center">
            @if($u->image != null)
                <img src="{{ URL::to('public/uploads') }}/{{ $u->image }}" style="width:200px; height: 200px; border-radius:50%; margin:0px auto;">
            @else
            <img src="{{ URL::to('frontend/images/profile-placeholder.jpg') }}" style="width:200px; height: 200px; border-radius:50%; margin:0px auto;">
            @endif
            <hr>
            <div class="col-md-12 col-lg-12 col-sm-12"><strong>{{ $u->first_name }} {{ $u->last_name }}</strong></div>
            <div class="col-md-12 col-lg-12 col-sm-12"><strong><a href="mailto:{{ $u->email }}">{{ $u->email }}</a></strong></div>
            <div class="col-md-12 col-lg-12 col-sm-12"><strong>{{ $u->phone }}</strong></div>
        </div>

        <div class="col-md-9 col-lg-9 col-sm-9">
            <div class="col-md-6 col-lg-6 col-sm-6">
                <strong>Years Of Experience:</strong> {{ $u->resume->total_exp }}
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6">
                <strong>Desired Salry:</strong> {{ $u->resume->salary }}
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6">
                <strong>Job Type:</strong> {{ $u->resume->job_type }}
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6">
                <strong>Salry Type:</strong> {{ $u->resume->salary_type }}
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12">
                <strong>Cover Letter:</strong><br>
                {{ $u->letter->letter }}
            </div>
        </div>
    @endforeach        
    </div>
</div>
@endsection

@section('footer_scripts')
@endsection