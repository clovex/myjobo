@extends('layouts.app')

@section('template_title')
    Candidates | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Candidates</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('home') }}">Profile</a></li>
                    <li>Candidates</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    	@foreach($jobs as $j)
        <div class="col-md-6">
            <div class="application">
                <div class="app-content">
                    <div class="info">
                        @if($j->image != null)
                        <img src="{{ URL::to('public/uploads') }}/{{ $j->image }}" alt="">
                        @else
                        <img src="{{ URL::to('frontend/images/profile-placeholder.jpg') }}" alt="">
                        @endif
                        <span>{{ $j->first_name }} {{ $j->last_name }}</span>
                        @if($j->cv != null)
                        <ul>
                            <li>
                                <a href="{{ URL::to('public/cv') }}/{{ $j->cv }}" target="_blank">
                                <i class="fa fa-file-text"></i> Download CV</a>
                            </li>
                        </ul>
                        @else
                        @endif
                    </div>
                    
                    <div class="buttons"> 
                        <a href="{{ URL::to('viewprofile') }}/{{ $j->id }}" class="button gray">
                            <i class="fa fa-plus-circle"></i> View Profile
                        </a>
                        <a href="{{ URL::to('application/remove') }}/{{ $j->aid }}" class="button gray"><i class="fa fa-trash-o" aria-hidden="true" style="font-size: 16px;"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="app-footer">
                    <span>{{ $j->title}}</span>
                    <ul>
                        <li><i class="fa fa-calendar"></i> {{ Carbon\Carbon::parse($j->created_at)->format('jS F, Y') }}</li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>            
        </div>
        @endforeach
        <div class="col-md-12">
            <div class="pagination-container text-center">
                <nav class="pagination"> 
                    {{ $jobs->links() }}
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
@endsection