@extends('layouts.app')

@section('template_title')
    Job Post | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
{!! HTML::script('frontend/text/ckeditor.js') !!}    
{!! HTML::script('frontend/text/sample.js') !!}   
{!! HTML::style('frontend/text/neo.css') !!}
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Job Post</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('home') }}">Profile</a></li>
                    <li>Job Post</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    	<div class="col-md-12 col-lg-12 col-sm-12">
            <form method="POST" enctype="multipart/form-data" action="{{ URL::to('job/post') }}">
            {{ csrf_field() }}

                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">    
                <input type="hidden" name="company_name" value="{{ Auth::user()->name }}">
                <input type="hidden" name="contact_name" value="{{ Auth::user()->contact_name }}">
                <input type="hidden" name="number" value="{{ Auth::user()->phone }}">
                <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                <input type="hidden" name="website" value="{{ Auth::user()->website }}">

                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                        <label>Job Title</label>
                        <input type="text" name="job_title" class="form-control" required>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                        <label>City / Town</label>
                        <input type="text" name="city" class="form-control" required>
                    </div>
                </div>
                
                <div class="row">    
                    <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                        <label>District</label>
                        <select class="form-control" name="district" required>
                            <option value="">Select</option>
                            @foreach($district as $d)
                                <option value="{{ $d->id }}">{{ $d->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                        <label>Country</label>
                        <select class="form-control" name="country" required>
                            <option value="">Select</option>
                            @foreach($country as $c)
                                <option value="{{ $c->country_id }}">{{ $c->name }}</option>
                            @endforeach
                        </select>
                    </div>                
                </div>

                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                        <label>Job Category</label>
                        <select class="form-control" name="job_cat" required>
                            <option value="">Select</option>
                            @foreach($job_category as $jc)
                            <option value="{{ $jc->id }}">{{ $jc->name }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                        <label>Application deadline</label>
                        <input type="text" name="deadline" id="datepicker" class="form-control" required>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                        <label>Employment Type</label>
                        <select class="form-control" name="emp_type" required>
                            <option value="">Select</option>
                            @foreach($employment_type as $et)
                            <option value="{{ $et->id }}">{{ $et->name }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                        <label>Salary</label>
                        <input type="text" name="salary" class="form-control">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                        <label>Company Logo</label>
                        <input type="file" name="logo" class="form-control">
                    </div>
                </div>
                <div class="row">    
                    <div class="col-md-12 form-goup">
                        <label>Job Description</label>
                        <textarea name="description" class="form-control" id="editor"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 form-goup">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>    
        </div>
    </div>
</div>
    
<script>
  initSample();
</script>
<script>
  $(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>
<style type="text/css">
    input[type="text"]
    {
        padding: 6px 12px;
    }
</style>

@endsection

@section('footer_scripts')
@endsection