@extends('layouts.app')

@section('template_title')
    Manage Jobs | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
{!! HTML::script('backend/bootstrap/js/ckeditor.js') !!}
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Manage Jobs</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('home') }}">Profile</a></li>
                    <li>Manage Jobs</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    	<div class="col-md-12">
            <div class="text-right">
                <a href="{{ URL::to('jobpost') }}" class="button"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Job</a>
            </div>
            <table class="manage-table responsive-table">

            <tr>
                <th><i class="fa fa-file-text"></i> Title</th>
                <th><i class="fa fa-calendar"></i> Date Posted</th>
                <th><i class="fa fa-calendar"></i> Date Expires</th>
                <th>Action</th>
            </tr>
                    
            @foreach($jobs as $j)
            <tr>
                <td class="title"><a>{{ $j->title }}</a></td>
                <td>{{ Carbon\Carbon::parse($j->created_at)->format('jS F, Y') }}</td>
                <td> {{ Carbon\Carbon::parse($j->deadline)->format('jS F, Y') }}</td>
                <td class="action">
                    <a href="{{ URL::to('jobedit') }}/{{ $j->slug }}">
                        <i class="fa fa-pencil"></i> Edit
                    </a>
                    <a href="{{ URL::to('jobdelete') }}/{{ $j->slug }}" class="delete">
                        <i class="fa fa-remove"></i> Delete
                    </a>
                </td>
            </tr>
            @endforeach        
        </table>
         <div class="col-md-12">
            <div class="pagination-container text-center">
                <nav class="pagination"> 
                    {{ $jobs->links() }}
                </nav>
            </div>
        </div>    
        </div>
    </div>
</div>
<script>
  CKEDITOR.replace('editor2');
</script>
@endsection

@section('footer_scripts')
@endsection