@extends('layouts.app')

@section('template_title')
    Search CV | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Search CV</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('home') }}">Profile</a></li>
                    <li>Search CV</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    	<div class="col-lg-12 col-md-12 col-sm-12">
        <form method="POST" enctype="multipart/form-data" action="{{ URL::to('cvsearch') }}">
            {{ csrf_field() }}
            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                <h3>Jobs</h3>
                <input type="text" name="jobs" class="form-control" placeholder="Like job title" style="padding: 6px 12px">
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                <h3>Job Category</h3>
                <select name="category" class="form-control">
                    <option value="">Select</option>
                    @foreach($job_category as $jc)
                    @if($jc->id == 0)

                    @else
                        <option value="{{ $jc->id }}">{{ $jc->name }}</option>
                    @endif
                    @endforeach    
                </select>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                <h3>Location</h3>
                <select name="location" class="form-control">
                    <option value="">Select</option>
                    @foreach($district as $dist)
                        <option value="{{ $dist->id }}">{{ $dist->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 form-group">
                <h3>&nbsp;</h3>
                <button class="btn btn-block" style="background-color: #F45B06;color: #fff;">Search</button>
            </div>
        </form>     
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12 text-center">

            <h3>Find Candidates</h3>

        </div>
    </div>
</div>        
@endsection

@section('footer_scripts')
@endsection