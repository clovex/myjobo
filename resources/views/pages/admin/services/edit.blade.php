@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.services').each(function(){
      $(this).addClass('active');
    });
    $('.services2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Services Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/services/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                      @foreach($services as $s)
                      <input type="hidden" name="id" value="{{ $s->id }}">
                      <div class="col-md-12 form-group">  
                        <label>Title</label>
                        <input type="text" name="title" value="{{ $s->title }}" class="form-control" required>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Type</label>
                        <select name="type" class="form-control" required>
                          <option value="">Select</option>
                          @foreach($type as $t)  
                          <option value="{{ $t->id }}" @if($s->type == $t->id) {{ 'selected' }} @endif>
                            {{ $t->name }}
                          </option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" class="form-control" id="editor" required>{{ $s->description }}</textarea>
                      </div>  

                      @endforeach
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection