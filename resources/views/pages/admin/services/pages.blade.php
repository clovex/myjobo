@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.services').each(function(){
      $(this).addClass('active');
    });
    $('.services1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Services
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <!--li class="active">
                      <a data-toggle="tab" href="#tab_1" aria-expanded="true">Section 1</a>
                    </li-->
                    <li class="active">
                      <a data-toggle="tab" href="#tab_2" aria-expanded="false">Section 2</a>
                    </li>
                  </ul>
  
                  <div class="tab-content">


                    <!--div id="tab_1" class="tab-pane active">
                       <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/servicespage') }}">
                        {{ csrf_field() }}
                        @foreach($s1 as $s11)
                        <textarea id="editor1" name="services" rows="10" cols="80">{{ $s11->contents }}</textarea>
                        @endforeach
                        <div class="form-group">  
                          <br>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </form>  
                      <script>
                        CKEDITOR.replace('editor1');
                      </script>
                    </div-->
                    




                    <div id="tab_2" class="tab-pane active">
                      <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/servicespage2') }}">
                        {{ csrf_field() }}
                        <div class="row">
                          <div class="col-md-12 form-group">                        
                            @foreach($s2 as $s22) 
                            <label>Title1</label>
                            <input type="text" name="stitle1" value="{{ json_decode($s22->contents)->title }}" class="form-control">
                            <label>Description1</label>
                            <textarea name="sdes1" class="form-control" id="editors1">{{ json_decode($s22->contents)->dec }}</textarea>
                            @endforeach
                          </div>  

                          <div class="col-md-12 form-group">
                            @foreach($s3 as $s33)                        
                            <label>Title2</label>
                            <input type="text" name="stitle2" value="{{ json_decode($s33->contents)->title }}" class="form-control">
                            <label>Description2</label>
                            <textarea name="sdes2" class="form-control" id="editors2">{{ json_decode($s33->contents)->dec }}</textarea>
                            @endforeach
                          </div>  

                          <div class="col-md-12 form-group">                  
                            @foreach($s4 as $s44)      
                            <label>Title3</label>
                            <input type="text" name="stitle3" class="form-control" value="{{ json_decode($s44->contents)->title }}">
                            <label>Description3</label>
                            <textarea name="sdes3" class="form-control" id="editors3">{{ json_decode($s44->contents)->dec }}</textarea>
                            @endforeach
                          </div>  

                          <div class="col-md-12 form-group">
                            @foreach($s5 as $s55)                        
                            <label>Title4</label>
                            <input type="text" name="stitle4" class="form-control" value="{{ json_decode($s55->contents)->title }}">
                            <label>Description4</label>
                            <textarea name="sdes4" class="form-control" id="editors4">{{ json_decode($s55->contents)->dec }}</textarea>
                            @endforeach
                          </div>  


                          <div class="form-group col-md-12">  
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>

                        </div>
                      </form>  
                    </div>  
                    
                    </div>
                  </div>  
                </div>  
              </div>
          </div>
      </div>
    </section>
</div>
@endsection
