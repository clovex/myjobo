@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.services').each(function(){
      $(this).addClass('active');
    });
    $('.services2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Services 
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12 text-right">
                    <a class="btn btn-primary" href="{{ URL::to('admin/services/post') }}">Add</a>
                    <hr>
                  </div>
                  <div class="col-md-12">
                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Title</th>
                          <th>Type</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Title</th>
                          <th>Type</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @foreach($services as $f)
                        <tr>
                          <td>{{ $f->title }}</td>
                          
                          <td>
                          @foreach($type as $t)  
                          @if($f->type == $t->id)
                              {{ $t->name }} 
                          @endif
                          @endforeach  
                          </td>
                          
                          <td>
                            <a href="{{ URL::to('admin/services/put') }}/{{ $f->id }}">
                              <i class="fa fa-pencil-square" aria-hidden="true" style="font-size: 18px; color: green;"></i>
                            </a>
                            <a href="{{ URL::to('admin/services/delete') }}/{{ $f->id }}">
                              <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px; color: red;"></i>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>    
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
