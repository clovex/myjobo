@extends('layouts.back')


@section('contents')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Home
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ URL::to('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    

    <section class="content"> 
 
 	  	<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $empcount }}</h3>

              <p>Employers</p>
            </div>
            <div class="icon">
              <i class="ion-person-stalker"></i>
            </div>
            <a href="{{ URL::to('admin/employers') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $jobseekercount }}</h3>

              <p>Job Seekers</p>
            </div>
            <div class="icon">
              <i class="ion-ios-people"></i>
            </div>
            <a href="{{ URL::to('admin/jobseekers') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $jobscount }}</h3>

              <p>Posted Jobs</p>
            </div>
            <div class="icon">
              <i class="ion-ios-folder"></i>
            </div>
            <a href="{{ URL::to('admin/jobs') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $applyjobscount }}</h3>

              <p>Job Applicants</p>
            </div>
            <div class="icon">
              <i class="ion-paper-airplane"></i>
            </div>
            <a href="{{ URL::to('admin/applicants') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-orange">
            <div class="inner">
              <h3>{{ $cvcount }}</h3>

              <p>Uploaded CVs</p>
            </div>
            <div class="icon">
              <i class="ion-ios-compose"></i>
            </div>
            <a href="" class="small-box-footer">&nbsp;</a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-purple">
            <div class="inner">
              <h3>{{ $newslettercount }}</h3>

              <p>Newsletter Registrants</p>
            </div>
            <div class="icon">
              <i class="ion-ios-email-outline"></i>
            </div>
            <a href="{{ URL::to('admin/newsletter') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{ $viewcount }}</h3>

              <p>Visits</p>
            </div>
            <div class="icon">
              <i class="ion-ios-pie-outline"></i>
            </div>
            <a class="small-box-footer">&nbsp;</a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-maroon">
            <div class="inner">
              <h3>{{ $tradesregcount }}</h3>

              <p>Trades Registered</p>
            </div>
            <div class="icon">
              <i class="ion-android-person-add"></i>
            </div>
            <a href="{{ URL::to('admin/trade') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-olive">
            <div class="inner">
              <h3>{{ $staffregcount }}</h3>

              <p>Vetted Staff Registered</p>
            </div>
            <div class="icon">
              <i class="ion-ios-people-outline"></i>
            </div>
            <a href="{{ URL::to('admin/staff-requester') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>


        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-fuchsia">
            <div class="inner">
              <h3>{{ $schoolcount }}</h3>

              <p>Schools Registered</p>
            </div>
            <div class="icon">
              <i class="ion-university"></i>
            </div>
            <a href="{{ URL::to('admin/school') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>


        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $coursescount }}</h3>

              <p>Courses/Events Posted</p>
            </div>
            <div class="icon">
              <i class="ion-speakerphone"></i>
            </div>
            <a href="{{ URL::to('admin/courses') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-lime">
            <div class="inner">
              <h3>{{ $featuredjobscount }}</h3>

              <p>Featured Jobs</p>
            </div>
            <div class="icon">
              <i class="ion-ios-filing"></i>
            </div>
            <a class="small-box-footer">&nbsp;</a>
          </div>
        </div>

        </div>

    

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Current Month Report</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>

          <div class="box-body">
            <div class="row">
            
            <div class="col-md-3">  
              <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="ion-android-contacts"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Employers</span>
                  <span class="info-box-number">{{ $empcurrent }}</span>
                </div>
              </div>
            </div>

            <div class="col-md-3">  
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion-android-people"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Job Seekers</span>
                  <span class="info-box-number">{{ $jobseekercurrent }}</span>
                </div>
              </div>
            </div>  

            <div class="col-md-3">
              <div class="info-box bg-red">
                <span class="info-box-icon"><i class="ion-android-checkbox-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Posted Jobs</span>
                  <span class="info-box-number">{{ $jobcurrent }}</span>
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion-android-search"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Visits</span>
                  <span class="info-box-number">{{ $viewcurrent }}</span>
                </div>
              </div>        
            </div>

            <div class="col-md-3">
              <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="ion-ios-personadd"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Trades</span>
                  <span class="info-box-number">{{ $tradecurrent }}</span>
                </div>
              </div>        
            </div>

            <div class="col-md-3">
              <div class="info-box bg-orange">
                <span class="info-box-icon"><i class="ion-person-stalker"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Vetted Staff</span>
                  <span class="info-box-number">{{ $staffrcurrent }}</span>
                </div>
              </div>        
            </div>

            <div class="col-md-3">
              <div class="info-box bg-purple">
                <span class="info-box-icon"><i class="ion-android-checkmark-circle"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Courses/Events</span>
                  <span class="info-box-number">{{ $coursecurrent }}</span>
                </div>
              </div>        
            </div>

            <div class="col-md-3">
              <div class="info-box bg-olive">
                <span class="info-box-icon"><i class="ion-ios-contact-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Newsletter</span>
                  <span class="info-box-number">{{ $newslettercurrent }}</span>
                </div>
              </div>        
            </div>  

            </div>
          </div>
        </div>
      </div>
    </div>
    



    </section>
    
</div>
@endsection