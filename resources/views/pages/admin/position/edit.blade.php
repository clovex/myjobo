@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.trades').each(function(){
      $(this).addClass('active');
    });
    $('.trades3').each(function(){
      $(this).addClass('active');
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Position Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/position/put') }}">
                    
                    {{ csrf_field() }}
                    
                    <div class="row">
                      @foreach($position as $i)
                      <input type="hidden" name="id" value="{{ $i->id }}">
                      <div class="col-md-11 form-group">  
                        <input type="text" name="name" value="{{ $i->name }}" class="form-control" required>
                      </div>
                      @endforeach

                      <div class="form-group col-md-1">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                    </form>
                    <hr>
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
