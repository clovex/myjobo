@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.settings').each(function(){
      $(this).addClass('active');
    });
    $('.settings5').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Partners Category Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/partners/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                    @foreach($partners as $t)
                      <input type="hidden" name="id" value="{{ $t->id }}">  
                      <div class="col-md-12 form-group">  
                        <label>Name</label>
                        <input type="text" name="name" value="{{ $t->name }}" class="form-control" >
                      </div>
                    @endforeach  
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection
