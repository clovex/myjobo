@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.courses').each(function(){
      $(this).addClass('active');
    });
    $('.courses1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Learning Events Add
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body">
                <form method="post" enctype="multipart/form-data" action="{{ URL::to('admin/courses/post') }}">

                  {{ csrf_field() }}

                  <div class="col-md-12 form-group">
                    <label>Title</label>
                    <input type="text" name="name" class="form-control">
                  </div>

                  <div class="col-md-6 form-group">
                    <label>Image</label>
                    <input type="file" name="image">
                  </div>

                  <div class="col-md-6 form-group">
                    <label>Category</label>
                    <select name="category" class="form-control">
                      <option value="">Select</option>
                      @foreach($coursescategory as $cc)
                        <option value="{{ $cc->id }}">{{ $cc->name }}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="col-md-6 form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control">
                  </div>

                  <div class="col-md-6 form-group">
                    <label>Duration</label>
                    <input type="text" name="duration" class="form-control" placeholder="like  Part-time">
                  </div>

                  <div class="col-md-6 form-group">
                    <label>Qualification</label>
                    <input type="text" name="qualification" class="form-control" placeholder="like Admin, Secretarial & PA Diploma">
                  </div>

                  <div class="col-md-6 form-group">
                    <label>Study Method</label>
                    <input type="text" name="study_method" class="form-control" placeholder="like Online, self-paced">
                  </div>

                  <div class="col-md-6 form-group">
                    <label>Price</label>
                    <input type="text" name="price" class="form-control">
                  </div>

                  <div class="col-md-12 form-group">
                    <label>Requirements</label>
                    <textarea name="requirement" class="form-control"></textarea>
                  </div>

                  <div class="col-md-12 form-group">
                    <label>Description</label>
                    <textarea name="description" id="editor" class="form-control"></textarea>
                  </div>

                  <div class="col-md-12 form-group">
                    <button type="submit" class="btn btn-success">Save</button>
                  </div>

                </form>
              </div>
          </div>
      </div>
    </section>
</div>
@endsection