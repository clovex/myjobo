@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.courses').each(function(){
      $(this).addClass('active');
    });
    $('.courses2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Learning Events Category
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                  <form method="post" enctype="multipart/form-data" action="{{ URL::to('admin/coursescategory/put') }}">
                    {{ csrf_field() }}
                    @foreach($coursescategory as $cc)
                    <input type="hidden" name="id" value="{{ $cc->id }}">
                    <div class="col-md-5 form-group">
                      <label>Name</label>
                      <input type="text" name="name" class="form-control" value="{{ $cc->name }}" required>
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Image</label>
                      <input type="file" name="image">
                    </div>
                    <div class="col-md-2">
                      <img src="{{ URL::to('public/uploads') }}/{{ $cc->image }}" style="width: 100%;">
                    </div>
                    @endforeach  
                    <div class="col-md-12 form-group">
                      <label>&nbsp;</label><br>
                      <button name="submit" type="submit" class="btn btn-success">Save</button>
                    </div>
                    
                  </form> 
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
