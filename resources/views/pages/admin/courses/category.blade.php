@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.courses').each(function(){
      $(this).addClass('active');
    });
    $('.courses2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Learning Events Category
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                  <form method="post" enctype="multipart/form-data" action="{{ URL::to('admin/coursescategory/post') }}">
                    {{ csrf_field() }}
                    <div class="col-md-5 form-group">
                      <label>Name</label>
                      <input type="text" name="name" class="form-control" required>
                    </div>
                    <div class="col-md-5 form-group">
                      <label>Image</label>
                      <input type="file" name="image" required>
                    </div>
                    <div class="col-md-2 form-group">
                      <label>&nbsp;</label><br>
                      <button name="submit" type="submit" class="btn btn-success">Save</button>
                    </div>
                    
                  </form>
                    
                  </div>
                  <div class="col-md-12">
                  <hr>
                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Name</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @foreach($coursescategory as $cc)
                        <tr>
                          <td>{{ $cc->name }}</td>
                          <td>
                            <a href="{{ URL::to('admin/coursescategory/put') }}/{{ $cc->id }}">
                              <i class="fa fa-pencil-square" aria-hidden="true" style="font-size: 18px; color: green;"></i>
                            </a>
                            <a href="{{ URL::to('admin/coursescategory/delete') }}/{{ $cc->id }}">
                              <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px; color: red;"></i>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>    
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
