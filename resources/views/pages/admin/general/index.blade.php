@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.settings').each(function(){
      $(this).addClass('active');
    });
    $('.settings4').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        General Settings
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a data-toggle="tab" href="#tab_1" aria-expanded="true">Logo</a>
                    </li>
                    <li class="">
                      <a data-toggle="tab" href="#tab_2" aria-expanded="false">Social link</a>
                    </li>
                    <li class="">
                      <a data-toggle="tab" href="#tab_3" aria-expanded="false">Footer About</a>
                    </li>
                  </ul>
  
                  <div class="tab-content">


                    <div id="tab_1" class="tab-pane active">
                       <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/generallogo') }}">
                        {{ csrf_field() }}
                        
                        <div class="row">
                          <div class="col-md-12 form-group">
                            <input type="file" name="logo">
                          </div>
                        <div class="form-group col-md-12">  
                          <br>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                        <br>
                        <div class="col-md-12">
                          @foreach($s1 as $s11)
                          <img src="{{ URL::to('public/uploads')}}/{{ $s11->contents }}" style="width: 200px;">
                          @endforeach
                        </div>
                        </div>  
                      </form>  
                      <hr>
                      <h3>Testimonials logo</h3>
                      <hr>
                      <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/testimonialslogo') }}">
                        {{ csrf_field() }}
                        
                        <div class="row">
                          <div class="col-md-12 form-group">
                            <input type="file" name="testlogo">
                          </div>
                        <div class="form-group col-md-12">  
                          <br>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                        <br>
                        <div class="col-md-12">
                          @foreach($s4 as $s44)
                          <img src="{{ URL::to('public/uploads')}}/{{ $s44->contents }}" style="width: 200px;">
                          @endforeach
                        </div>
                        </div>  
                      </form>  
                    </div>
                    




                    <div id="tab_2" class="tab-pane ">
                      <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/generallinks') }}">
                        {{ csrf_field() }}
                        <div class="row">
                          <div class="col-md-12 form-group">                        
                          @foreach($s2 as $s22) 
                            <label>Facebook</label>
                            <input type="text" name="facebook" value="{{ json_decode($s22->contents)->facebook }}" class="form-control">

                            <label>Twitter</label>
                            <input type="text" name="google" value="{{ json_decode($s22->contents)->google }}" class="form-control">

                            <label>Google Plus</label>
                            <input type="text" name="linkedin" value="{{ json_decode($s22->contents)->linkedin }}" class="form-control">

                            <label>Linkedin</label>
                            <input type="text" name="youtube" value="{{ json_decode($s22->contents)->youtube }}" class="form-control">

                            <label>Youtube</label>
                            <input type="text" name="twitter" value="{{ json_decode($s22->contents)->twitter }}" class="form-control">
                          @endforeach
                          </div>  
                          <div class="form-group col-md-12">  
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>

                        </div>
                      </form>  
                    </div>



                    <div id="tab_3" class="tab-pane">
                       <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/fabout') }}">
                        {{ csrf_field() }}
                        
                        <div class="row">
                        @foreach($s3 as $s33)
                          <div class="col-md-12 form-group">
                            <textarea name="content" class="form-control" >{{ $s33->contents }}</textarea>   
                          </div>
                        @endforeach  
                          <div class="form-group col-md-12">  
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>
                      </form>  
                    </div>


                    
                    </div>
                  </div>  
                </div>  
              </div>
          </div>
      </div>
    </section>
</div>
@endsection
