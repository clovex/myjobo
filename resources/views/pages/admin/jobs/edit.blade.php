@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.jobs').each(function(){
      $(this).addClass('active');
    });
    $('.job1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Jobs Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/jobs/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                      
                    @foreach($jobs as $j)
                      <div class="col-md-12 text-center">
                        <input type="radio" name="type" id="radio1" value="0" @if ($j->type == 0) {{ 'checked' }} @endif> Job Post
                        <input type="radio" name="type" id="radio2" value="1" @if ($j->type == 1) {{ 'checked' }} @endif> Training Session
                        <hr>
                      </div>

                      <input type="hidden" name="id" value="{{ $j->id }}">
                      <div class="col-md-6 form-group">  
                        <label>Company Name</label>
                        <input type="text" name="company_name" value="{{ $j->company_name }}" class="form-control">
                      </div>
                      
                      <div class="col-md-6 form-group">  
                        <div class="col-md-10" style="padding: 0px;">
                          <label>Company Logo</label>
                          <input type="file" name="logo" class="form-control">
                        </div>
                        <div class="col-md-2">
                          <img src="{{ URL::to('public/uploads')}}/{{ $j->logo }}" style="width: 100%;">
                        </div>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Contact Name</label>
                        <input type="text" name="contact_name" value="{{ $j->contact_name }}" class="form-control">
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Phone Number</label>
                        <input type="text" name="number" value="{{ $j->number }}" class="form-control">
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Primary Email</label>
                        <input type="text" name="email" value="{{ $j->email }}" class="form-control" required>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Secondary Email</label>
                        <input type="text" name="secondary_email" value="{{ $j->secondary_email }}" class="form-control">
                      </div> 

                      <div class="col-md-6 form-group">  
                        <label>Website</label>
                        <input type="text" name="website" value="{{ $j->website }}" class="form-control">
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Job Title</label>
                        <input type="text" name="job_title" value="{{ $j->title }}" class="form-control" required>
                      </div>


                      <div class="col-md-6 form-group">  
                        <label>City / Town</label>
                        <input type="text" name="city" value="{{ $j->city }}" class="form-control" required>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>District/City</label>
                        <select class="form-control" name="district">
                          <option value="">Select</option>
                          @foreach($district as $d)
                            <option value="{{ $d->id }}" @if ($d->id == $j->district) {{ 'selected' }}   @endif>
                            {{ $d->name }}
                            </option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Country</label>
                        <select class="form-control" name="country">
                          <option value="">Select</option>
                          @foreach($country as $c)
                            <option value="{{ $c->country_id }}" @if ($c->country_id == $j->country) {{ 'selected' }}   @endif>{{ $c->name }}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Job Category</label>
                        <select class="form-control" name="job_cat">
                          @foreach($job_category as $jc)
                            <option value="{{ $jc->id }}" @if ($jc->id == $j->job_cat) {{ 'selected' }}   @endif>{{ $jc->name }}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Job deadline</label>
                        <input type="text" name="deadline" id="datepicker" value="{{ $j->deadline }}" class="form-control" required>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Employment Type</label>
                        <select class="form-control" name="emp_type">
                          @foreach($employment_type as $et)
                            <option value="{{ $et->id }}" @if ($et->id == $j->emp_type) {{ 'selected' }}   @endif>{{ $et->name }}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Featured Job</label>
                        <input id="featured_check" name="featured" value="1" type="checkbox" class="" @if ($j->featured == 1) {{ 'checked' }}  @endif >
                      </div>

                      <div class="col-md-6 form-group" id="salary">  
                        <label>Salary</label>
                        <input type="text" name="salary" value="{{ $j->salary }}" class="form-control" id="valsalary">
                      </div>


                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" class="form-control" id="editor" required>{{ $j->des }}</textarea>
                      </div>  
                    @endforeach  

                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
<script>
$(document).ready(function() {
  if ($("#radio2").prop("checked")) {
      $('#salary').hide();
      $("#valsalary").val(""); 
  }


  $("input[name$='type']").click(function() {
      var test = $(this).val();
      if(test == 0)
      {
        $('#salary').show();
      } 
      else
      {
        $('#salary').hide();
        $("#valsalary").val(""); 
      }  
  });
});
</script>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
  $(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>    
@endsection
