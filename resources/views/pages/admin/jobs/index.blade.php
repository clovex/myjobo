@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.jobs').each(function(){
      $(this).addClass('active');
    });
    $('.job1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       jobs
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    <form method="GET" enctype="multipart/form-data" action="{{ URL::to('admin/jobs/search') }}">
                    {{ csrf_field() }}
                    <!--div class="col-md-4 form-group">
                      <select name="category" class="form-control">
                        <option class="">Select Job Category</option>
                        @foreach($category as $c)
                          @if($c->id == 0)

                          @else
                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                          @endif  
                        @endforeach
                      </select>
                    </div-->
                    <div class="col-md-6 form-group">
                      <input type="text" name="search" class="form-control" placeholder="Search by Company or Jobs ">
                    </div>
                    <div class="col-md-2">
                      <button name="submit" type="submit" class="btn btn-success">Search</button>
                    </div>
                  </form>
                    <div class="col-md-2 text-right">
                    <a class="btn btn-primary" href="{{ URL::to('admin/jobs/post') }}">Add</a>
                    </div>
                    <div class="col-md-2 text-right">
                      <a onclick="exportdata()" class="btn btn-warning">Export</a>
                  </div>
                  </div>
                  

                  <div id="setbtn" data-toggle="modal" data-target="#myModal"></div>
                        <div class="modal fade" id="myModal" role="dialog">
                          <div class="modal-dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Jobs</h4>
                              </div> 
                              <div class="modal-body">
                                <a href="{{ URL::to('storage/jobs.xlsx') }}" target="_blank" class="btn btn-success">Download</a>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                            
                          </div>
                        </div>

                  <div class="col-md-12">
                  <hr>
                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>S. No.</th>
                          <th>Company Name</th>
                          <th>Job Tilte</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>S. No.</th>
                          <th>Company Name</th>
                          <th>Job Tilte</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @php $i = 1; @endphp
                        @php $i = $jobs->perPage() * ($jobs->currentPage()-1); @endphp
                        @foreach($jobs as $f)
                        <tr>
                          <td>{{ $i+1 }}</td>
                          <td>{{ $f->company_name }}</td>
                          <td>{{ $f->title }}</td>
                          <td>
                            <a href="{{ URL::to('admin/jobs/put') }}/{{ $f->id }}">
                              <i class="fa fa-pencil-square" aria-hidden="true" style="font-size: 18px; color: green;"></i>
                            </a>
                            <a href="{{ URL::to('admin/jobs/delete') }}/{{ $f->id }}">
                              <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px; color: red;"></i>
                            </a>
                          </td>
                        </tr>
                        @php $i++; @endphp
                        @endforeach
                      </tbody>
                    </table>    
                  </div>
                  <div class="col-md-12 text-right">
                    <div class="pagination"> {{ $jobs->links() }} </div>
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>

<script>
function exportdata () 
{
  $.ajax({
    'url':'{{ URL::to("admin/jobs/excel") }}',
    'success':function(message)
    {
      $("#setbtn").click();
    } 
  }); 
}
</script>

@endsection
