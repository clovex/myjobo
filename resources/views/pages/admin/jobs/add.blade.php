@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.jobs').each(function(){
      $(this).addClass('active');
    });
    $('.job1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Featured Job Add
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info"> 
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/jobs/post') }}">
                    
                    {{ csrf_field() }}
                    <input type="hidden" name="user_id" value="0">
                    <div class="row">
                      <div class="col-md-12 text-center">
                        <input type="radio" name="type" value="0" checked="checked"> Job Post
                        <input type="radio" name="type" value="1"> Training Session
                        <hr>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Company Name</label>
                        <input type="text" name="company_name" class="form-control" required>
                      </div>
                      
                      <div class="col-md-6 form-group">  
                        <label>Company Logo</label>
                        <input type="file" name="logo" class="form-control">
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Contact Name</label>
                        <input type="text" name="contact_name" class="form-control" required>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Phone Number</label>
                        <input type="text" name="number" class="form-control" required>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Primary Email</label>
                        <input type="text" name="email" class="form-control" required>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Secondary Email</label>
                        <input type="text" name="secondary_email" class="form-control">
                      </div> 

                      <div class="col-md-6 form-group">  
                        <label>Website</label>
                        <input type="text" name="website" class="form-control">
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Job Title</label>
                        <input type="text" name="job_title" class="form-control" required>
                      </div>


                      <div class="col-md-6 form-group">  
                        <label>City / Town</label>
                        <input type="text" name="city" class="form-control" required>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>District/City</label>
                        <select class="form-control" name="district" required>
                          <option value="">Select</option>
                          @foreach($district as $d)
                            <option value="{{ $d->id }}">{{ $d->name }}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Country</label>
                        <select class="form-control" name="country" >
                          <option value="">Select</option>
                          @foreach($country as $c)
                            <option value="{{ $c->country_id }}" @if ($c->country_id == "136") {{ 'selected' }} @endif>{{ $c->name }}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Job Category</label>
                        <select class="form-control" name="job_cat" required>
                          @foreach($job_category as $jc)
                            <option value="{{ $jc->id }}">{{ $jc->name }}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Job deadline</label>
                        <input type="text" name="deadline" id="datepicker" class="form-control" required>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Employment Type</label>
                        <select class="form-control" name="emp_type">
                          @foreach($employment_type as $et)
                            <option value="{{ $et->id }}">{{ $et->name }}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Featured Job</label>
                        <input id="featured_check" name="featured" value="1" type="checkbox">
                      </div>

                      <div class="col-md-6 form-group" id="salary">  
                        <label>Salary</label>
                        <input type="text" name="salary" class="form-control" id="valsalary">
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" class="form-control" id="editor" required></textarea>
                      </div>  

                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
  $(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>   
<script>
$(document).ready(function() {
  $("input[name$='type']").click(function() {
      var test = $(this).val();
      if(test == 0)
      {
        $('#salary').show();
      } 
      else
      {
        $('#salary').hide();
        $("#valsalary").val(""); 
      }  
  });
});
</script>
 
@endsection
