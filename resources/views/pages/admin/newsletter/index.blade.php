@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.newsletter').each(function(){
      $(this).addClass('active');
    });

    $('.newsletter1').each(function(){
      $(this).addClass('active');
    });

});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Newsletter Members
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    <form method="GET" enctype="multipart/form-data" action="{{ URL::to('admin/newsletter/search') }}">
                    {{ csrf_field() }}
            
                      <div class="col-md-6 form-group">
                        <input type="text" name="search" class="form-control" placeholder="Like Email and Full Name">
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-primary">Search</button>
                      </div>
                    </form>
                      <div class="col-md-2 text-right">
                        <a href="{{ URL::to('admin/newslettermember/add') }}" class="btn btn-success">Add</a>
                      </div> 

                      <div class="col-md-2 text-right">
                        <a onclick="exportdata()" class="btn btn-warning">Export</a>
                      </div>

                      <div id="setbtn" data-toggle="modal" data-target="#myModal"></div>
                        <div class="modal fade" id="myModal" role="dialog">
                          <div class="modal-dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Newsletter Members</h4>
                              </div>
                              <div class="modal-body">
                                <a href="{{ URL::to('storage/Newsletter.xlsx') }}" target="_blank" class="btn btn-success">Download</a>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                    <hr>
                  </div>
                  <div class="col-md-12">
                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Full name</th>
                          <th>Email</th>
                          <th>Date</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Full name</th>
                          <th>Email</th>
                          <th>Date</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @foreach($newsletter as $f)
                        <tr>
                          <td>{{ $f->name }}</td>
                          <td>{{ $f->email }}</td>
                          <td>{{ Carbon\Carbon::parse($f->created_at)->format('d-m-Y') }}</td>
                          <td>
                            <a href="{{ URL::to('admin/newslettermember/put') }}/{{ $f->id }}">
                              <i class="fa fa-pencil-square" aria-hidden="true" style="font-size: 18px; color: green;"> </i>
                            </a>
                            <a href="{{ URL::to('admin/newsletter/delete') }}/{{ $f->id }}">
                              <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px; color: red;"></i>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>    
                  </div>
                  <div class="col-md-12 text-right">
                    <div class="pagination"> {{ $newsletter->links() }} </div>
                  </div>
              </div>
          </div>
      </div>
    </section>                                                                                                                
</div>
                                                                                                                                                                                                                                                                                                                                                          
<script>
function exportdata () 
{
  $.ajax({
    'url':'{{ URL::to("admin/newsletter/excel") }}',
    'success':function(message)
    {
      $("#setbtn").click();
    } 
  }); 
}
</script>                                  
@endsection
