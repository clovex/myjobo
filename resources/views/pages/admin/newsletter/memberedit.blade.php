@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.newsletter').each(function(){
      $(this).addClass('active');
    });

    $('.newsletter1').each(function(){
      $(this).addClass('active');
    });

});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Newsletter Members Edit
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{ URL::to('admin/memberadd/puts') }}">
                    
                    {{ csrf_field() }}
                    @foreach($newsletter as $news)
                      <input type="hidden" name="id" value="{{ $news->id }}">
                      <div class="col-md-6 form-group">
                        <label>Full Name</label>
                        <input type="text" name="name" value="{{ $news->name }}" class="form-control" required>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Email</label>
                        <input type="email" name="email" value="{{ $news->email }}" class="form-control" required>
                      </div>
                    @endforeach  
                      <div class="col-md-12">
                        <button class="btn btn-primary">Save</button>
                      </div>
                    </form>
                    <hr>
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
