@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.newsletter').each(function(){
      $(this).addClass('active');
    });
    $('.newsletter2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Newsletter Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/newsletter/put') }}">
                    
                    {{ csrf_field() }}
                    @foreach($newsletter as $n)
                    <input type="hidden" name="id" value="{{ $n->id }}">
                    <div class="row">
                      <div class="col-md-12 form-group">  
                        <label>Subject</label>
                        <input type="text" name="subject" value="{{ $n->subject }}" class="form-control" required>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Message</label>
                        <textarea name="message" class="form-control" id="editor" required>{!! $n->text !!}</textarea>
                      </div>  
                      @endforeach
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection
