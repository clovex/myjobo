@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.newsletter').each(function(){
      $(this).addClass('active');
    });

    $('.newsletter3').each(function(){
      $(this).addClass('active');
    });

});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Send Newsletters
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/newsletter/send') }}">
                    
                    {{ csrf_field() }}
                      <div class="row">
                        <div class="col-md-12 form-group">  
                          <label>Select Newsletter </label>
                          <select class="form-control" name="newsletter">
                            <option value="">Select</option>
                            @foreach($newsletter as $news)
                              <option value="{{ $news->id }}">{{ $news->subject }}</option>
                            @endforeach
                          </select>
                        </div>

                        <div class="col-md-12 form-group">
                          <label>Send To</label><br>
                          <input id="employers" name="employers" value="1" type="checkbox">
                          Employers   
                          <input id="job_seekers" name="job_seekers" value="2" type="checkbox">
                          Job Seekers   
                          <input id="others" name="others" value="3" type="checkbox">
                          Others
                        </div>
                      
                        <div class="form-group col-md-12">  
                          <button type="submit" class="btn btn-success">Send</button>
                        </div>
                    
                      </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
