@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.schools').each(function(){
      $(this).addClass('active');
    });
    $('.school1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
       School
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12 text-right">
                    <a class="btn btn-primary" href="{{ URL::to('admin/school/post') }}">Add</a>
                    <hr>
                  </div>
                  <div class="col-md-12">
                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Type</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Name</th>
                          <th>Type</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @foreach($school as $f)
                        <tr>
                          <td>{{ $f->name }}</td>
                          <td>{{ $f->types->name }}</td>
                          <td>
                            <a href="{{ URL::to('admin/school/put') }}/{{ $f->id }}">
                              <i class="fa fa-pencil-square" aria-hidden="true" style="font-size: 18px; color: green;"></i>
                            </a>
                            <a href="{{ URL::to('admin/school/delete') }}/{{ $f->id }}">
                              <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px; color: red;"></i>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>    
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
