@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.schools').each(function(){
      $(this).addClass('active');
    });
    $('.school1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        School Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/school/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">

                    @foreach($school as $s)
                      <input type="hidden" name="id" value="{{ $s->id }}">
                      <div class="col-md-12 form-group">  
                        <label>Name</label>
                        <input type="text" name="name" value="{{ $s->name }}" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Principal</label>
                        <input type="text" name="principal" value="{{ $s->principal }}" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Email</label>
                        <input type="text" name="email" value="{{ $s->email }}" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Phone</label>
                        <input type="text" name="phone" value="{{ $s->phone }}" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Address</label>
                        <input type="text" name="address" value="{{ $s->address }}" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Website</label>
                        <input type="text" name="website" value="{{ $s->website }}" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Type</label>
                        <select name="type" class="form-control">
                          <option value="">Select</option>
                          @foreach($schooltype as $st)
                            <option value="{{ $st->id }}" @if($st->id == $s->type) {!! 'selected' !!}  @endif>{{ $st->name }}</option>
                          @endforeach
                        </select>
                      </div>                      
                        
                      <div class="col-md-12 form-group">
                        <div class="col-md-10">  
                          <label>Logo</label>
                          <input type="file" name="logo" class="form-control">
                        </div>
                        @if($s->logo != null)
                          <img src="{{ URL::to('public/uploads') }}/{{ $s->logo }}" style="width: 100%;">
                        @endif
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" id="editor" class="form-control">{{ $s->description }}</textarea>
                      </div>
                    @endforeach    
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection
