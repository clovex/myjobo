@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.blogs').each(function(){
      $(this).addClass('active');
    });
    $('.blog1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Blog Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/blogs/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                      @foreach($blogs as $b)  
                      <input type="hidden" name="id" value="{{ $b->id }}">
                      <div class="col-md-12 form-group">  
                        <label>Title</label>
                        <input type="text" name="title" value="{{ $b->title }}" class="form-control" required>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Author</label>
                        <input type="text" name="author" value="{{ $b->author }}" class="form-control" required>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Category</label>
                        <select name="category" class="form-control">
                          @foreach($category as $c)
                            <option value="{{ $c->id }}" @if ($b->blog_category == $c->id) {{ 'selected' }}   @endif
                            >{{ $c->name }}</option>
                          @endforeach      
                        </select>
                      </div>
                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" class="form-control" id="editor" required>{{ $b->description }}</textarea>
                      </div>  
                      @endforeach
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection
