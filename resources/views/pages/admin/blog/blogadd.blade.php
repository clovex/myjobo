@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.blogs').each(function(){
      $(this).addClass('active');
    });
    $('.blog1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Blogs Add
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/blogs/post') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-12 form-group">  
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" required>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Author</label>
                        <input type="text" name="author" class="form-control" required>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Category</label>
                        <select name="category" class="form-control" required>
                          <option value="">Select</option>
                          @foreach($category as $c)
                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                          @endforeach      
                        </select>
                      </div>



                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" class="form-control" id="editor" required></textarea>
                      </div>  

                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection
