@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.blogs').each(function(){
      $(this).addClass('active');
    });
    $('.blog2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Blog Category Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/blog_category/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                      @foreach($blog_category as $cat)
                      <input type="hidden" name="id" value="{{ $cat->id }}">
                      <div class="col-md-12 form-group">  
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" value="{{ $cat->name }}" required>
                      </div>
                      @endforeach
                      
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection
