@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.trades').each(function(){
      $(this).addClass('active');
    });
    $('.trades1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Requests
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                  <form method="GET" enctype="multipart/form-data" action="{{ URL::to('admin/requests/search') }}">
                    {{ csrf_field() }}
                    <div class="col-md-8 form-group">
                      <input type="text" name="search" class="form-control" placeholder="Search by Industry or Category">
                    </div>
                    <div class="col-md-2">
                      <button name="submit" type="submit" class="btn btn-success">Search</button>
                    </div>
                  </form>
                  <!--div class="col-md-2 text-right">
                      <a class="btn btn-primary" href="{{ URL::to('admin/requests/post') }}">Add</a>
                  </div-->
                    
                  </div>

                  <div class="col-md-12">
                  <hr>
                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>S. No.</th>
                          <th>Company</th>
                          <th>Industry</th>
                          <th>Position</th>
                          <th>Deadline</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>S. No.</th>
                          <th>Company</th>
                          <th>Industry</th>
                          <th>Position</th>
                          <th>Deadline</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @php $i = 1; @endphp
                        @php $i = $trades->perPage() * ($trades->currentPage()-1); @endphp
                        @foreach($trades as $f)
                        <tr>
                          <td>{{ $i+1 }}</td>
                          
                          <td>
                              {{ $f->name }}
                          </td>
                          <td>
                              @foreach($industry as $ind)
                                @if($ind->id == $f->industry)
                                  {{ $ind->name }}
                                @endif
                              @endforeach
                          </td>
                          <td>
                              @foreach($position as $pos)
                                @if($pos->id == $f->position)
                                  {{ $pos->name }}
                                @endif
                              @endforeach
                          </td>
                          <td>{{ $f->deadline }}</td>
                          <td>
                            <a href="{{ URL::to('admin/requests/view') }}/{{ $f->id }}">
                              <i class="fa fa-eye" aria-hidden="true" style="font-size: 20px; color: green;"></i>
                            </a>
                            <!--a href="{{ URL::to('admin/requests/put') }}/{{ $f->id }}">
                              <i class="fa fa-pencil-square" aria-hidden="true" style="font-size: 18px; color: green;"></i>
                            </a-->
                            <a href="{{ URL::to('admin/requests/delete') }}/{{ $f->id }}">
                              <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px; color: red;"></i>
                            </a>
                          </td>
                        </tr>
                        @php $i++; @endphp
                        @endforeach
                      </tbody>
                    </table>    
                  </div>
                  <div class="col-md-12 text-right">
                    <div class="pagination"> {{ $trades->links() }} </div>
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
