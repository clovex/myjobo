@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.trades').each(function(){
      $(this).addClass('active');
    });
    $('.trades1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Requests Add
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">

                  <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/requests/insert') }}">
                    
                    {{ csrf_field() }}
                    <input type="hidden" name="provider_id" value="0">
                    <div class="row">
                      <div class="col-md-6 form-group">  
                        <label>Industry</label>
                        <select class="form-control" name="industry">
                          <option value="">Select</option>
                          @foreach($industry as $ind)
                            <option value="{{ $ind->id }}">{{ $ind->name }}</option>
                          @endforeach
                        </select>
                      </div>
                      
                      <div class="col-md-6 form-group">  
                        <label>Position</label>
                        <select name="position" class="form-control">
                          <option value="">Select</option>
                          @foreach($position as $pos)
                            <option value="{{ $pos->id }}">{{ $pos->name }}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Staff</label>
                        <input type="number" name="staff" class="form-control" min="1" max="50" value="1">
                      </div>

                      <div class="col-md-6 form-group">  
                        <div class="col-md-12">
                        <label>Age</label>
                        </div>
                        <div class="col-md-5 form-group">
                          <input type="number" name="age1" class="form-control" min="18" max="65" value="18">
                        </div>
                        <div class="col-md-2 form-group text-center">To</div>
                        
                        <div class="col-md-5 form-group">
                          <input type="number" name="age2" class="form-control" min="18" max="65" value="65">
                        </div>  
                      </div>

                      <div class="col-md-6 form-group">
                        <label>Gender</label>
                        <select class="form-control" name="gender">
                          <option value="">No preference</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                        </select>
                      </div>

                      <div class="col-md-6 form-group">
                        <label>Experience</label>
                        <select class="form-control" name="experience">
                          <option value="">No preference</option>
                          <option value="At least 1 years">At least 1 years</option>
                          <option value="At least 3 years">At least 3 years</option>
                        </select>
                      </div>

                      <div class="col-md-6 form-group">
                        <label>Education</label>
                        <select class="form-control" name="education">
                          <option value="">No preference</option>
                          <option value="Matric">Matric</option>
                          <option value="National Certificate">National Certificate</option>
                          <option class="Diploma and Higher">Diploma and Higher</option>
                        </select>
                      </div> 

                      <div class="col-md-6 form-group">
                          <div class="col-md-12">
                            <label>BASIC salary</label>
                          </div>
                          <div class="col-md-6 form-group">
                            <input type="text" name="salary1" class="form-control">
                          </div>
                          <div class="col-md-6 form-group">
                            <select class="form-control" name="salary1type" required="">
                              <option value="">Select</option>
                              <option value="per month">per month</option>
                              <option value="per day">per day</option>
                              <option value="per hour">per hour</option>
                            </select>
                          </div>
                      </div>

                      <!--div class="col-md-6 ">
                          <div class="col-md-12">
                            <label>COMMISSION salary</label>
                          </div>
                          <div class="col-md-6 form-group">
                            <input type="text" name="salary2" class="form-control">
                          </div>
                          <div class="col-md-6 form-group">
                            <select class="form-control" name="salary2type" required="">
                              <option value="">Select</option>
                              <option value="per month">per month</option>
                              <option value="per day">per day</option>
                              <option value="per hour">per hour</option>
                            </select>
                          </div>
                      </div--> 

                      <div class="col-md-6 form-group">
                        <label>Deadline</label>  
                        <input type="text" name="deadline" class="form-control datepicker">  
                      </div>

                      <div class="col-md-6 form-group">
                        <label>Interview Date</label>
                        <input class="form-control datepicker" name="interview_date" required="" type="text">
                      </div>

                      <div class="col-md-6 form-group">
                        <div class="col-md-12">Interview Time</div>
                        <div class="col-md-6 form-group">
                          <select class="form-control" name="interview_time1" required>
                            <option value="">HH</option>
                            @for($i=8; $i<=19; $i++)
                              <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                          </select>
                        </div>

                        <div class="col-md-6 form-group">
                          <select class="form-control" name="interview_time2" required>
                            <option value="">MM</option>
                            <option value="00">00</option>
                            <option value="15">15</option> 
                            <option value="30">30</option>
                            <option value="45">45</option>
                          </select>
                        </div>
                      </div>  

                      <div class="col-md-12 form-group">
                        <label>Description</label>
                        <textarea name="description" class="form-control"></textarea>
                      </div>

                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                  </form>

                  </div>
              </div>
          </div>
      </div>
    </section>
</div>
<script type="text/javascript">
  $(function () {
    $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>
@endsection
