@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.trades').each(function(){
      $(this).addClass('active');
    });
    $('.trades1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Requests View
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12 text-right">
                    <a href="{{ URL::to('admin/requests') }}" class="btn btn-primary">Back</a>
                  </div>
                  <div class="col-md-12">
                  <h3>Staff Requester Details</h3>
                  <hr>
                  @foreach($trades as $t)
                  <div class="row">
                    <div class="col-md-4">  
                      <label>Company Name : </label> {{ $t->name }}
                    </div>

                    <div class="col-md-4">  
                      <label>Email : </label> {{ $t->email }}
                    </div>

                    <div class="col-md-4">  
                      <label>Phone Number : </label> {{ $t->phone }}
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4">  
                      <label>Contact Name : </label> {{ $t->contact_name }}
                    </div>

                    <div class="col-md-4">  
                      <label>Website : </label> {{ $t->website }}
                    </div>
                  </div>

                  <h3>Request Details</h3>
                  <hr>  
                  
                  <div class="row">
                    <div class="col-md-4">  
                      <label>Industry : </label> @foreach($industry as $ind)
                                @if($ind->id == $t->industry)
                                  {{ $ind->name }}
                                @endif
                              @endforeach
                    </div>

                    <div class="col-md-4">  
                      <label>Position : </label> @foreach($position as $pos)
                                @if($pos->id == $t->position)
                                  {{ $pos->name }}
                                @endif
                              @endforeach
                    </div>

                    <div class="col-md-4">  
                      <label>Staff : </label> {{ $t->staff }}
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4">  
                      <label>Age Range : </label> {{ $t->age1 }} - {{ $t->age2 }}
                    </div>

                    <div class="col-md-4">  
                      <label>Gender : </label> {{ $t->gender }}
                    </div>

                    <div class="col-md-4">  
                      <label>Level of Experience : </label> {{ $t->experience }}
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4">  
                      <label>Education : </label> {{ $t->education }}
                    </div>

                    <div class="col-md-4">  
                      <label>Basic Salary : </label> {{ $t->salary1 }}
                    </div>

                    <div class="col-md-4">  
                      <label>Interview Date : </label> {{ $t->interview_date }}
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4">  
                      <label>Deadline : </label> {{ $t->deadline }}
                    </div>
                  </div> 

                  <div class="row">
                    <div class="col-md-12">
                    <label>Description</label><br>
                    {{ $t->description }}
                    </div> 
                  </div> 
                  
                  @endforeach  
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>
<script type="text/javascript">
  $(function () {
    $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>
@endsection
