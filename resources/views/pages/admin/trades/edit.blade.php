@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.trades').each(function(){
      $(this).addClass('active');
    });
    $('.trades1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Requests Edit
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">

                  <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/requests/update') }}">
                    
                    {{ csrf_field() }}

                    @foreach($trades as $t)
                    <input type="hidden" name="id" value="{{ $t->id }}">
                    <input type="hidden" name="provider_id" value="0">
                    <div class="row">
                      <div class="col-md-6 form-group">  
                        <label>Industry</label>
                        <select class="form-control" name="industry">
                          <option value="">Select</option>
                          @foreach($industry as $ind)
                            <option value="{{ $ind->id }}" @if ($ind->id == $t->industry) {{ 'selected' }}   @endif>{{ $ind->name }}</option>
                          @endforeach
                        </select>
                      </div>
                      
                      <div class="col-md-6 form-group">  
                        <label>Position</label>
                        <select name="position" class="form-control">
                          <option value="">Select</option>
                          @foreach($position as $pos)
                            <option value="{{ $pos->id }}" @if ($pos->id == $t->position) {{ 'selected' }}   @endif>{{ $pos->name }}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-6 form-group">  
                        <label>Staff</label>
                        <input type="number" name="staff" class="form-control" min="1" max="50" value="{{ $t->staff }}">
                      </div>

                      <div class="col-md-6 form-group">  
                        <div class="col-md-12">
                        <label>Age</label>
                        </div>
                        <div class="col-md-5 form-group" style="padding-left: 0px;">
                          <input type="number" name="age1" class="form-control" min="18" max="65" value="{{ $t->age1 }}">
                        </div>
                        <div class="col-md-2 form-group text-center">To</div>
                        
                        <div class="col-md-5 form-group" style="padding-right: 0px;">
                          <input type="number" name="age2" class="form-control" min="18" max="65" value="{{ $t->age2 }}">
                        </div>  
                      </div>

                      <div class="col-md-6 form-group">
                        <label>Gender</label>
                        <select class="form-control" name="gender">
                          <option value="">No preference</option>
                          <option value="Male" @if($t->gender == 'Male'){{ 'selected' }} @endif>Male</option>
                          <option value="Female" @if($t->gender == 'Female'){{ 'selected' }} @endif>Female</option>
                        </select>
                      </div>

                      <div class="col-md-6 form-group">
                        <label>Experience</label>
                        <select class="form-control" name="experience">
                          <option value="">No preference</option>
                          <option value="At least 1 years" @if($t->experience == 'At least 1 years'){{ 'selected' }} @endif>At least 1 years</option>
                          <option value="At least 3 years" @if($t->experience == 'At least 3 years'){{ 'selected' }} @endif>At least 3 years</option>
                        </select>
                      </div>

                      <div class="col-md-6 form-group">
                        <label>Education</label>
                        <select class="form-control" name="education">
                          <option value="">No preference</option>
                          <option value="Matric" @if($t->education == 'Matric'){{ 'selected' }} @endif>Matric</option>
                          <option value="National Certificate" @if($t->education == 'National Certificate'){{ 'selected' }} @endif>National Certificate</option>
                          <option class="Diploma and Higher" @if($t->education == 'Diploma and Higher'){{ 'selected' }} @endif>Diploma and Higher</option>
                        </select>
                      </div> 

                      @php $salary1 = explode('-',$t->salary1); @endphp

                      <div class="col-md-6 form-group">
                          <div class="col-md-12">
                            <label>BASIC salary</label>
                          </div>
                          <div class="col-md-6 form-group" style="padding-left: 0px;">
                            <input type="text" name="salary1" value="@php echo $salary1[0] @endphp" class="form-control">
                          </div>
                          <div class="col-md-6 form-group" style="padding-right: 0px;">
                            <select class="form-control" name="salary1type" required="">
                              <option value="">Select</option>
                              <option value="per month" @if($salary1[1] == 'per month') {{ 'selected' }} @endif>per month</option>
                              <option value="per day" @if($salary1[1] == 'per day') {{ 'selected' }} @endif>per day</option>
                              <option value="per hour" @if($salary1[1] == 'per hour') {{ 'selected' }} @endif>per hour</option>
                            </select>
                          </div>
                      </div>

                      @php $salary2 = explode('-',$t->salary2); @endphp

                      <!--div class="col-md-6 ">
                          <div class="col-md-12">
                            <label>COMMISSION salary</label>
                          </div>
                          <div class="col-md-6 form-group" style="padding-left: 0px;">
                            <input type="text" name="salary2" value="@php echo $salary2[0]  @endphp" class="form-control">
                          </div>
                          <div class="col-md-6 form-group" style="padding-right: 0px;">
                            <select class="form-control" name="salary2type" required="">
                              <option value="">Select</option>
                              <option value="per month" @if($salary2[1] == 'per month') {{ 'selected' }} @endif>per month</option>
                              <option value="per day" @if($salary2[1] == 'per day') {{ 'selected' }} @endif>per day</option>
                              <option value="per hour" @if($salary2[1] == 'per hour') {{ 'selected' }} @endif>per hour</option>
                            </select>
                          </div>
                      </div--> 

                      <div class="col-md-6 form-group">
                        <label>Deadline</label>  
                        <input type="text" name="deadline" value="{{ $t->deadline }}" class="form-control datepicker">  
                      </div>

                      <div class="col-md-6 form-group">
                        <label>Interview Date</label>
                        <input class="form-control datepicker" name="interview_date" value="{{ $t->interview_date }}" required="" type="text">
                      </div>

                     @php $interview_time = explode(':',$t->interview_time); @endphp
                      <div class="col-md-6 form-group">
                        <div class="col-md-12"><label>Interview Time</label></div>
                        <div class="col-md-6 form-group" style="padding-left: 0px;">
                          <select class="form-control" name="interview_time1" required>
                            <option value="">HH</option>
                            @for($i=8; $i<=19; $i++)
                              <option value="{{ $i }}" @if ($i == $interview_time[0]) {{ 'selected' }} @endif>{{ $i }}</option>
                            @endfor
                          </select>
                        </div>

                        <div class="col-md-6 form-group" style="padding-right: 0px;">
                          <select class="form-control" name="interview_time2" required>
                            <option value="">MM</option>
                            <option value="00" @if($interview_time[1] == '00') {{ 'selected' }} @endif>00</option>
                            <option value="15" @if($interview_time[1] == '15') {{ 'selected' }} @endif>15</option> 
                            <option value="30" @if($interview_time[1] == '30') {{ 'selected' }} @endif>30</option>
                            <option value="45" @if($interview_time[1] == '45') {{ 'selected' }} @endif>45</option>
                          </select>
                        </div>
                      </div> 

                      <div class="col-md-12 form-group">
                        <label>Description</label>
                        <textarea name="description" class="form-control">{{ $t->description }}</textarea>
                      </div>

                      @endforeach
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                  </form>

                  </div>
              </div>
          </div>
      </div>
    </section>
</div>
<script type="text/javascript">
  $(function () {
    $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>
@endsection
