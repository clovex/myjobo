@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.users').each(function(){
      $(this).addClass('active');
    });
     $('.users1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       CV Edit
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/jobseekers/cvput') }}">
                    {{ csrf_field() }}
                      <div class="row">
                      @foreach($cvs as $cv) 
                        <input type="hidden" name="user_id" value="{{ $cv->user_id }}">

                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                          <label>Job Title</label>
                          <input type="text" name="job_title" value="{{ $cv->job_title }}" class="form-control setpddd">
                        </div>
                        
                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                          <label>Job Category</label>
                          <select class="form-control" name="job_category">
                            <option value="">Select</option>
                            @foreach($job_category as $jc)
                                @if($jc->id == 0)

                                @else
                                <option value="{{ $jc->id }}" @if ($jc->id == $cv->job_category) {{ 'selected' }} @endif>{{ $jc->name }}</option>
                                @endif
                            @endforeach
                          </select>
                        </div>

                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                          <label>City / Town</label>
                          <input type="text" name="city" value="{{ $cv->city }}" class="form-control setpddd">
                        </div>
                    
                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                          <label>District</label>
                          <select class="form-control" name="district">
                            <option value="">Select</option>
                            @foreach($district as $dist)
                                <option value="{{ $dist->id }}" @if ($dist->id == $cv->district) {{ 'selected' }} @endif>{{ $dist->name }}</option>
                            @endforeach
                          </select>
                        </div> 

                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                          <label>Country</label>
                          <select class="form-control" name="country">
                            <option value="">Select</option>
                            @foreach($country as $coun)
                                <option value="{{ $coun->country_id }}" @if ($coun->country_id == $cv->country) {{ 'selected' }} @endif>{{ $coun->name }}</option>
                            @endforeach
                          </select>
                        </div>
                    
                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                          <label>Years Of Experience</label>
                          <select class="form-control" name="total_exp">
                            <option value="0">Select</option>
                            @php $i=25; @endphp
                            @for($i=1; $i<=25; $i++)
                                @if($i == 1)
                                    <option value="{{ $i }}" @if ($i == $cv->total_exp) {{ 'selected' }} @endif>{{ $i }} Year Experience</option>
                                @else
                                    <option value="{{ $i }}" @if ($i == $cv->total_exp) {{ 'selected' }} @endif>{{ $i }} Years Experience</option>
                                @endif
                            @endfor    
                          </select>
                        </div>


                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                          <label>Desired Salry</label>
                          <input type="text" name="salary" value="{{ $cv->salary }}" class="form-control setpddd">
                        </div> 
                    
                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                          <label>Salry Type</label>
                          <select class="form-control" name="salary_type">
                            <option value="">Select</option>
                            <option value="Per Yea" @if ($cv->salary_type == 'Per Yea') {{ 'selected' }} @endif>Per Year</option>
                            <option value="Per Day" @if ($cv->salary_type == 'Per Day') {{ 'selected' }} @endif>Per Day</option>
                            <option value="Per Month" @if ($cv->salary_type == 'Per Month') {{ 'selected' }} @endif>Per Month</option>
                            <option value="Per Hour" @if ($cv->salary_type == 'Per Hour') {{ 'selected' }} @endif>Per Hour</option>
                            <option value="Other" @if ($cv->salary_type == 'Other') {{ 'selected' }} @endif>Other</option>
                          </select>
                        </div>

                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                          <label>Job Type</label>
                          <select class="form-control" name="job_type">
                            <option value="">Select</option>
                            <option value="Full time" @if ($cv->job_type == 'Full time') {{ 'selected' }} @endif>Full time</option>
                            <option value="Part time" @if ($cv->job_type == 'Part time') {{ 'selected' }} @endif>Part time</option>
                            <option value="Contractual" @if ($cv->job_type == 'Contractual') {{ 'selected' }} @endif>Contractual</option>
                            <option value="Work from home" @if ($cv->job_type == 'Work from home') {{ 'selected' }} @endif>Work from home</option>
                            <option value="Volunteer" @if ($cv->job_type == 'Volunteer') {{ 'selected' }} @endif>Volunteer</option>
                            <option value="Internship" @if ($cv->job_type == 'Internship') {{ 'selected' }} @endif>Internship</option>
                            <option value="To be Determined" @if ($cv->job_type == 'To be Determined') {{ 'selected' }} @endif>To be Determined</option>
                          </select>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                            <label>CV</label>
                            <input type="file" name="resume" class="form-control">
                        </div> 

                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                          <label>Education</label>
                          <textarea class="form-control" name="education">{{ $cv->education }}</textarea>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6 form-goup">
                            <label>Skills</label>
                            <textarea class="form-control" name="skill">{{ $cv->skill }}</textarea>
                        </div>

                        <div class="col-md-12 form-goup">
                          <label>Objective</label>
                          <textarea class="form-control" name="objective" id="editor1">{!! $cv->objective !!}</textarea>
                        </div>
                        
                        <div class="col-md-12 form-goup">
                            <label>Work Experience</label>
                            <textarea class="form-control" name="work_exeperence" id="editor">{!! $cv->work_exeperence !!}</textarea>
                        </div>
                      

                      @endforeach
                        <div class="form-group col-md-12">  
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    
                      </div>
                    </form>
                  </div>  
              </div>
          </div>
      </div>
    </section>
</div>
<script>
  $(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>
@endsection
