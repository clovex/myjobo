@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.users').each(function(){
      $(this).addClass('active');
    });
     $('.users1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Job Seekers Edit
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/jobseekers/put') }}">
                    {{ csrf_field() }}
                      <div class="row">

                      @foreach($users as $u)

                        <input type="hidden" name="id" value="{{ $u->id }}">

                        <div class="col-md-6 form-group">
                          <label>User Name</label>
                          <input type="text" name="name" class="form-control" value="{{ $u->name }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>First name</label>
                          <input type="text" name="first_name" class="form-control" value="{{ $u->first_name }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Last name</label>
                          <input type="text" name="last_name" class="form-control" value="{{ $u->last_name }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Email</label>
                          <input type="text" name="email" class="form-control" value="{{ $u->email }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Phone</label>
                          <input type="text" name="phone" class="form-control" value="{{ $u->phone }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Date of Birth</label>
                          <input type="text" name="dob" id="datepicker" class="form-control" value="{{ $u->dob }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>City/Town</label>
                          <input type="text" name="city" class="form-control" value="{{ $u->city }}">
                        </div>


                        <div class="col-md-6 form-group">
                          <label>District/County</label>
                          <input type="text" name="dist" class="form-control" value="{{ $u->dist }}">
                        </div>

                      @endforeach  
                      
                        <div class="form-group col-md-12">  
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    
                      </div>
                    </form>
                  </div>  
              </div>
          </div>
      </div>
    </section>
</div>
<script>
  $(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>
@endsection
