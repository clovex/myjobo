@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.edication').each(function(){
      $(this).addClass('active');
    });
    $('.edu1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Careers & Education Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/careers/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                    @foreach($careers as $c)
                      <input type="hidden" name="id" value="{{ $c->id }}">
                      <div class="col-md-12 form-group">  
                        <label>Title</label>
                        <input type="text" name="title" value="{{ $c->title }}" class="form-control" required>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Type</label>
                        <select class="form-control" name="type">
                          <option value="">Select</option>
                          <option value="0" @if($c->type == 0) {{ 'selected' }} @endif>Careers</option>
                          <option value="1" @if($c->type == 1) {{ 'selected' }} @endif>Education</option>
                        </select>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" class="form-control" id="editor" required>{{ $c->description }}</textarea>
                      </div>  
                      @endforeach
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection