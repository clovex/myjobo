@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.users').each(function(){
      $(this).addClass('active');
    });
     $('.users2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Employer Edit
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/employers/put') }}">
                    {{ csrf_field() }}
                      <div class="row">

                      @foreach($users as $u)

                        <input type="hidden" name="id" value="{{ $u->id }}">

                        <div class="col-md-6 form-group">
                          <label>Company Name</label>
                          <input type="text" name="name" class="form-control" value="{{ $u->name }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <div class="col-md-8">
                          <label>Logo</label>
                          <input type="file" name="logo" class="form-control">
                          </div>
                          <div class="col-md-2">
                            @if($u->image != null)
                            <img src="{{ URL::to('public/uploads') }}/{{ $u->image }}" style="width: 100%">
                            @endif
                          </div>
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Contact Name</label>
                          <input type="text" name="contact_name" class="form-control" value="{{ $u->contact_name }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Email</label>
                          <input type="text" name="email" class="form-control" value="{{ $u->email }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Phone</label>
                          <input type="text" name="phone" class="form-control" value="{{ $u->phone }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Website</label>
                          <input type="text" name="website" class="form-control" value="{{ $u->website }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>District</label>
                          <select class="form-control" name="district">
                            <option value="">Select</option>
                            @foreach($district as $d)
                              <option value="{{ $d->id }}" @if($d->id == $u->district) {{ 'selected' }} @endif>{{ $d->name }}</option>
                            @endforeach
                          </select>
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Country</label>
                          <select class="form-control" name="country">
                            <option value="">Select</option>
                            @foreach($country as $c)
                              <option value="{{ $c->country_id }}" @if($c->country_id == $u->country) {{ 'selected' }} @endif>{{ $c->name }}</option>
                            @endforeach
                          </select>
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Address</label>
                          <input type="text" name="address" class="form-control" value="{{ $u->address }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Sector</label>
                          <select class="form-control" name="sector">
                            <option value="">Select</option>
                            @foreach($partners as $p)
                              <option value="{{ $p->id }}" @if($p->id == $u->sector) {{ 'selected' }} @endif>{{ $p->name }}</option>
                            @endforeach  
                          </select>
                        </div>
                        <div class="col-md-6 form-group">
                          <label>Show logo in Home page</label>
                          <input type="checkbox" name="front_logo" class="form-control" value="1" @if ($u->front_logo == 1) {{ 'checked' }}  @endif>
                        </div>
                        
                        <div class="col-md-12 form-group">
                          <label>Description</label>
                          <textarea name="description" id="editor">{!! $u->description !!}</textarea>
                        </div>

                      @endforeach  
                      
                        <div class="form-group col-md-12">  
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    
                      </div>
                    </form>
                  </div>  
              </div>
          </div>
      </div>
    </section>
</div>
@endsection