@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.users').each(function(){
      $(this).addClass('active');
    });
     $('.users2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Employer Add
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/employers/post') }}">
                    {{ csrf_field() }}
                      <div class="row"> 

                        <div class="col-md-6 form-group">
                          <label>Company Name</label>
                          <input type="text" name="name" class="form-control" required>
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Email</label>
                          <input type="email" name="email" class="form-control" required>
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Password</label>
                          <input type="password" name="password" class="form-control" required>
                        </div> 

                        <div class="form-group col-md-12">  
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>  
              </div>
          </div>
      </div>
    </section>
</div>
<script>
  $(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>
@endsection
