@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.users').each(function(){
      $(this).addClass('active');
    });
     $('.users2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Employers
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <div class="col-md-12">
                  <form method="GET" enctype="multipart/form-data" action="{{ URL::to('admin/employers/search') }}">
                    {{ csrf_field() }}
                    <div class="col-md-8 form-group">
                      <input type="text" name="search" class="form-control" placeholder="Search by name or email">
                    </div>
                    <div class="col-md-2">
                      <button name="submit" type="submit" class="btn btn-success">Search</button>
                    </div>
                  </form>  
                  <div class="col-md-2 text-right">
                    <a href="{{ URL::to('admin/employers/add') }}" class="btn btn-success">Add</a>    
                  </div>  
                </div>
                <div class="col-md-12">
                <hr>
                  <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr. No</th>
                          <th>Company Name</th>
                          <th>Email</th>
                          <th>Status</th>
                          <th>CV Search</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Sr. No</th>
                          <th>Company Name</th>
                          <th>Email</th>
                          <th>Status</th>
                          <th>CV Search</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                      <tbody>
                      @php $i = 1; @endphp
                      @php $i = $users->perPage() * ($users->currentPage()-1); @endphp
                      @foreach($users as $u)
                        <tr>
                          <td>{{ $i+1 }}</td>
                          <td>{{ $u->name }}</td>
                          <td>{{ $u->email }}</td>
                          <td>
                           @if($u->activated == 1) 
                              @if($u->status == 1)
                              <a class="label label-success" href="{{ URL::to('admin/employers/deactive') }}/{{ $u->user_id }}">Active</a>
                              @else
                              <a class="label label-danger" href="{{ URL::to('admin/employers/active') }}/{{ $u->user_id }}">Deactive</a>
                              @endif
                            @else
                                <a class="label label-danger" href="{{ URL::to('admin/employers/active') }}/{{ $u->user_id }}">Deactive</a>
                            @endif
                          </td>
                          <td>
                              @if($u->cv_search == 1)
                              <a class="label label-success" href="{{ URL::to('admin/employers/disallowed') }}/{{ $u->user_id }}">Allowed</a>
                              @else
                              <a class="label label-danger" href="{{ URL::to('admin/employers/allowed') }}/{{ $u->user_id }}">Disallowed</a>
                              @endif
                          </td>
                          <td>
                            <a href="{{ URL::to('admin/employers/put') }}/{{ $u->user_id }}">
                              <i class="fa fa-pencil-square" aria-hidden="true" style="font-size: 18px; color: green;"> </i>
                            </a>
                            <a href="{{ URL::to('admin/employers/delete') }}/{{ $u->user_id }}">
                              <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px; color: red;"> </i>
                            </a>
                          </td>
                        </tr>
                      @php $i++; @endphp
                      @endforeach
                      </tbody>
                    </table>    

                  </div>
                  <div class="col-md-12 text-right">
                    <div class="pagination"> {{ $users->links() }} </div>
                  </div>

              </div>
          </div>
      </div>
    </section>
</div>

@endsection
