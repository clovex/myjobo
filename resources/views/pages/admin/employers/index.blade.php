@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.users').each(function(){
      $(this).addClass('active');
    });
     $('.users2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Employers
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <div class="col-md-12">
                  <form method="GET" enctype="multipart/form-data" action="{{ URL::to('admin/employers/search') }}">
                    {{ csrf_field() }}
                    <div class="col-md-6 form-group">
                      <input type="text" name="search" class="form-control" placeholder="Search by name or email">
                    </div>
                    <div class="col-md-2">
                      <button name="submit" type="submit" class="btn btn-success">Search</button>
                    </div>
                  </form>    
                  <div class="col-md-2 text-right">
                    <a href="{{ URL::to('admin/employers/add') }}" class="btn btn-success">Add</a>    
                  </div>
                  <div class="col-md-2 text-right">
                      <a onclick="exportdata()" class="btn btn-warning">Export</a>
                  </div>     
                </div>
                <div id="setbtn" data-toggle="modal" data-target="#myModal"></div>
                        <div class="modal fade" id="myModal" role="dialog">
                          <div class="modal-dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Employers</h4>
                              </div>
                              <div class="modal-body">
                                <a href="{{ URL::to('storage/employers.xlsx') }}" target="_blank" class="btn btn-success">Download</a>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                <div class="col-md-12">
                <hr>
                  <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Sr. No</th>
                          <th>Company Name</th>
                          <th>Email</th>
                          <th>Status</th>
                          <th>CV Search</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Sr. No</th>
                          <th>Company Name</th>
                          <th>Email</th>
                          <th>Status</th>
                          <th>CV Search</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                      <tbody>
                      @php $i = 1; @endphp
                      @php $i = $users->perPage() * ($users->currentPage()-1); @endphp
                      @foreach($users as $u)
                        <tr>
                          <td>{{ $i+1 }}</td>
                          <td>{{ $u->users->name }}</td>
                          <td>{{ $u->users->email }}</td>
                          <td>
                           @if($u->users->activated == 1) 
                              @if($u->users->status == 1)
                              <a class="label label-success" href="{{ URL::to('admin/employers/deactive') }}/{{ $u->users->id }}">Active</a>
                              @else
                              <a class="label label-danger" href="{{ URL::to('admin/employers/active') }}/{{ $u->users->id }}">Deactive</a>
                              @endif
                            @else
                              <a class="label label-danger" href="{{ URL::to('admin/employers/active') }}/{{ $u->user_id }}">Deactive</a>
                            @endif
                          </td>
                          <td>
                              @if($u->users->cv_search == 1)
                              <a class="label label-success" href="{{ URL::to('admin/employers/disallowed') }}/{{ $u->users->id }}">Allowed</a>
                              @else
                              <a class="label label-danger" href="{{ URL::to('admin/employers/allowed') }}/{{ $u->users->id }}">Disallowed</a>
                              @endif
                          </td>
                          <td>
                            <a href="{{ URL::to('admin/employers/put') }}/{{ $u->users->id }}">
                              <i class="fa fa-pencil-square" aria-hidden="true" style="font-size: 18px; color: green;"> </i>
                            </a>
                            <a href="{{ URL::to('admin/employers/delete') }}/{{ $u->users->id }}">
                              <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px; color: red;"> </i>
                            </a>
                          </td>
                        </tr>
                      @php $i++; @endphp
                      @endforeach
                      </tbody>
                    </table>    

                  </div>
                  <div class="col-md-12 text-right">
                    <div class="pagination"> {{ $users->links() }} </div>
                  </div>

              </div>
          </div>
      </div>
    </section>
</div>
<script>
function exportdata ()  
{
  $.ajax({
    'url':'{{ URL::to("admin/employers/excel") }}',
    'success':function(message)
    {
      $("#setbtn").click();
    } 
  }); 
}
</script>
@endsection
