@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.settings').each(function(){
      $(this).addClass('active');
    });
    $('.settings3').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Testimonials Add
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/testimonials/post') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-12 form-group">  
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Company</label>
                        <input type="text" name="company" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Position</label>
                        <input type="text" name="position" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Photo</label>
                        <input type="file" name="photo" class="form-control">
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Logo</label>
                        <input type="file" name="logo" class="form-control">
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" id="editor" class="form-control"></textarea>
                      </div>
                      
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection