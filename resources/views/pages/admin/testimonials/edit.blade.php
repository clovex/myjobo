@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.settings').each(function(){
      $(this).addClass('active');
    });
    $('.settings3').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Testimonials Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/testimonials/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                    @foreach($testimonials as $t)
                      <input type="hidden" name="id" value="{{ $t->id }}">  
                      <div class="col-md-12 form-group">  
                        <label>Name</label>
                        <input type="text" name="name" value="{{ $t->name }}" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Company</label>
                        <input type="text" name="company" value="{{ $t->company }}" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Position</label>
                        <input type="text" name="position" value="{{ $t->position }}" class="form-control" >
                      </div>

                      <div class="col-md-12 form-group"> 
                        <div class="col-md-10"> 
                          <label>Photo</label>
                          <input type="file" name="photo" class="form-control">
                        </div>
                        <div class="col-md-2">
                          <img src="{{ URL::to('public/uploads') }}/{{ $t->photo }}" style="width: 100%;">  
                        </div>
                      </div>

                      <div class="col-md-12 form-group">
                        <div class="col-md-10">  
                          <label>Logo</label>
                          <input type="file" name="logo" class="form-control">
                        </div>
                        <div class="col-md-2">
                          <img src="{{ URL::to('public/uploads') }}/{{ $t->logo }}" style="width: 100%;">  
                        </div>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" id="editor" class="form-control">{{ $t->description }}</textarea>
                      </div>
                    @endforeach  
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection