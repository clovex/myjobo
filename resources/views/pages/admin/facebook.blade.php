@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.settings').each(function(){
      $(this).addClass('active');
    });
    $('.settings8').each(function(){
      $(this).addClass('active');
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Facebook Key
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/facebook/update') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-11 form-group">  
                        <input type="text" value="{{ $facebook->token }}" name="type" class="form-control" required>
                      </div>
                      <div class="form-group col-md-1">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                    </form>
                    <hr>
                  </div>
                  
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
