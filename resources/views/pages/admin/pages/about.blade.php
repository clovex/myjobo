@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.pages').each(function(){
      $(this).addClass('active');
    });
    $('.page1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        About Us
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a data-toggle="tab" href="#tab_1" aria-expanded="true">Section 1</a>
                    </li>
                    <li class="">
                      <a data-toggle="tab" href="#tab_2" aria-expanded="false">Section 2</a>
                    </li>
                    <li class="">
                      <a data-toggle="tab" href="#tab_3" aria-expanded="false">Section 3</a>
                    </li>
                    <li class="">
                      <a data-toggle="tab" href="#tab_4" aria-expanded="false">Section 4</a>
                    </li>
                  </ul>
  
                  <div class="tab-content">


                    <div id="tab_1" class="tab-pane active">
                       <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/about_us') }}">
                        {{ csrf_field() }}
                        @foreach($about1 as $a1)
                        <textarea id="editor" name="aboutus" class="jqte-test">{{ $a1->contents }}</textarea>
                        @endforeach
                        <div class="form-group">  
                          <br>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </form>  

                    </div>
                    




                    <div id="tab_2" class="tab-pane ">
                      <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/about_us2') }}">
                        {{ csrf_field() }}
                        <div class="row">
                        @foreach($about2 as $a2)
                        <div class="form-group col-md-12">
                          <label>Description</label>
                          <input type="text" name="title" class="form-control" value="{{ $a2->contents }}">
                        </div>
                        @endforeach
                      
                        <div class="col-md-4">
                          @foreach($about3 as $a3)
                          <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name1" class="form-control" value="{{ json_decode($a3->contents)->name }}">
                          </div>

                          <div class="form-group">
                            <label>qualification</label>
                            <input type="text" name="qua1" class="form-control" value="{{ json_decode($a3->contents)->qua }}">
                          </div>
                          
                          <div class="form-group">
                            <label>designation</label>
                            <input type="text" name="des1" class="form-control" value="{{ json_decode($a3->contents)->des }}">
                          </div>

                          <div class="form-group">
                            <label>email</label>
                            <input type="text" name="email1" class="form-control" value="{{ json_decode($a3->contents)->email }}">
                          </div>
                          @endforeach  

                          <div class="form-group">
                            <input type="file" name="image1">
                          </div>
                          @foreach($about6 as $a6)
                              <img src="{{ URL::to('public/uploads') }}/{{ $a6->contents }}" style="width: 100%;">
                          @endforeach
                        </div>

                       

                        <div class="col-md-4">
                          @foreach($about4 as $a4)   
                          <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name2" class="form-control" value="{{ json_decode($a4->contents)->name }}">
                          </div>

                          <div class="form-group">
                            <label>qualification</label>
                            <input type="text" name="qua2" class="form-control" value="{{ json_decode($a4->contents)->qua }}">
                          </div>
                          
                          <div class="form-group">
                            <label>designation</label>
                            <input type="text" name="des2" class="form-control" value="{{ json_decode($a4->contents)->des }}">
                          </div>

                          <div class="form-group">
                            <label>email</label>
                            <input type="text" name="email2" class="form-control" value="{{ json_decode($a4->contents)->email }}">
                          </div>
                          @endforeach
                          <div class="form-group">
                            <input type="file" name="image2">
                          </div>
                           @foreach($about7 as $a7)
                              <img src="{{ URL::to('public/uploads') }}/{{ $a7->contents }}" style="width: 100%;">
                          @endforeach

                        </div>
                        

                        
                        <div class="col-md-4">
                          @foreach($about5 as $a5)
                          <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name3" class="form-control" value="{{ json_decode($a5->contents)->name }}">
                          </div>

                          <div class="form-group">
                            <label>qualification</label>
                            <input type="text" name="qua3" class="form-control" value="{{ json_decode($a5->contents)->qua }}">
                          </div>
                          
                          <div class="form-group">
                            <label>designation</label>
                            <input type="text" name="des3" class="form-control" value="{{ json_decode($a5->contents)->des }}">
                          </div>

                          <div class="form-group">
                            <label>email</label>
                            <input type="text" name="email3" class="form-control" value="{{ json_decode($a5->contents)->email }}">
                          </div>
                          @endforeach

                          <div class="form-group">
                            <input type="file" name="image3">
                          </div>
                           @foreach($about8 as $a8)
                              <img src="{{ URL::to('public/uploads') }}/{{ $a8->contents }}" style="width: 100%;">
                          @endforeach

                        </div>
                        <div class="form-group col-md-12">  
                          <br>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>

                        </div>
                      </form>  
                    </div>  
                    



                    <div id="tab_3" class="tab-pane ">
                      <h3>Why you should Advertise Your Jobs with Myjobo.com</h3>
                      <hr>
                       <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/about_us3') }}">
                        {{ csrf_field() }}
                        <div class="row">

                          @foreach($about9 as $a9)
                          <div class="col-md-12 form-group">  
                            <label>Title 1</label>
                            <input type="text" name="title1" class="form-control" value="{{ json_decode($a9->contents)->title }}">
                          </div>
                          <div class="col-md-12 form-group">  
                            <label>Description 1</label>
                            <textarea name="dec1" class="form-control">{{ json_decode($a9->contents)->dec }}</textarea>
                          </div>
                          @endforeach

                          @foreach($about10 as $a10)
                          <div class="col-md-12 form-group">  
                            <label>Title 2</label>
                            <input type="text" name="title2" class="form-control" value="{{ json_decode($a10->contents)->title }}">
                          </div>
                          <div class="col-md-12 form-group">  
                            <label>Description 2</label>
                            <textarea name="dec2" class="form-control">{{ json_decode($a10->contents)->dec }}</textarea>
                          </div>
                          @endforeach

                          @foreach($about11 as $a11)
                          <div class="col-md-12 form-group">  
                            <label>Title 3</label>
                            <input type="text" name="title3" class="form-control" value="{{ json_decode($a11->contents)->title }}">
                          </div>
                          <div class="col-md-12 form-group">  
                            <label>Description 3</label>
                            <textarea name="dec3" class="form-control">{{ json_decode($a11->contents)->dec }}</textarea>
                          </div>
                          @endforeach

                          @foreach($about12 as $a12)
                          <div class="col-md-12 form-group">  
                            <label>Title 4</label>
                            <input type="text" name="title4" class="form-control" value="{{ json_decode($a12->contents)->title }}">
                          </div>
                          <div class="col-md-12 form-group">  
                            <label>Description 4</label>
                            <textarea name="dec4" class="form-control">{{ json_decode($a12->contents)->dec }}</textarea>
                          </div>
                          @endforeach                      

                        <div class="form-group col-md-12">  
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>

                        </div>
                      </form>  
                  
                    </div>  



                    <div id="tab_4" class="tab-pane ">
                      <h3>As featured in</h3>  
                      <hr>
                      <div class="row">
                      <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/about_us4') }}">
                        {{ csrf_field() }}
                        <div class="row">
                          <div class="col-md-12 form-group">
                            <label>Image:</label>
                            <input type="file" name="img">
                          </div>
                          <div class="col-md-12 form-group">
                            <label>Link:</label>
                            <input type="text" name="link" class="form-control">
                          </div>
                          <div class="col-md-12 form-group">  
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>
                      </form>  
                       <div class="col-md-12">
                          <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Link</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Image</th>
                                    <th>Link</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($about13 as $a13)
                                <tr>
                                    <td> <img src="{{ URL::to('public/uploads') }}/{{ $a13->image }}" style="width: 50px; height: 50px;"></td>
                                    <td>{{ $a13->link }}</td>
                                    <td>
                                      <a href="{{ URL::to('admin/aboutdelete') }}/{{ $a13->f_id }}">
                                        <i class="fa fa-trash" aria-hidden="true" style="font-size: 15px;"></i>
                                      </a>
                                    </td>
                                </tr>
                                @endforeach  
                              </tbody>
                            </table>    
                       </div>
                      </div>
                    </div>
                  </div>  
                </div>  
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
