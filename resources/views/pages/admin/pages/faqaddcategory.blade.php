@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.pages').each(function(){
      $(this).addClass('active');
    });
    $('.page2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Faq Category Add
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/faqs/addpostcategory') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-4 form-group">  
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" required>
                      </div>

                      <div class="col-md-4 form-group">  
                        <label>Icon</label>
                        <div class="page">
      					<input type="text" class="input form-control" name="inputid2" id="inputid2" value="fa-amazon"/>
      
                      </div>  
                      
					</div>
					<div class="col-md-4 form-group">  
                        <label>Order By</label>
                        <div class="page">
      					<input type="text" class="input form-control" name="orderby" id="orderby"/>
      
                      </div>
                     </div>
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
            <div class="col-md-12">
                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Title</th>
                          <th>Icon</th>
                          <th>Order By</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Title</th>
                          <th>Icon</th>
                          <th>Order By</th>
                          <th>Action</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @foreach($faqscategory as $f)
                        <tr>
                          <td>{{ $f->title }}</td>
                          <td><i class="fa {{ $f->icon }}" aria-hidden="true" style="font-size: 18px; "></i></td>
                          <td>{{ $f->orderby }}</td>
                          <td>
                            <a href="{{ URL::to('admin/faqs/putcategory') }}/{{ $f->id }}">
                              <i class="fa fa-pencil-square" aria-hidden="true" style="font-size: 18px; color: green;"></i>
                            </a>
                            <a href="{{ URL::to('admin/faqs/deletecategory') }}/{{ $f->id }}">
                              <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px; color: red;"></i>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>    
                  </div>
          </div>
		
      </div>
    </section>
</div>

<style>
.howl-iconpicker .geticonval
{
	padding:5px 5px!important;
	width: 39px!important;
	height: 39px!important;
}
</style>
     
     
<script>
    var whichInput = 0;

    $(document).ready(function(){
      $('.input1').iconpicker(".input1");
      $('#inputid2').iconpicker("#inputid2");
      $('.input3').iconpicker(".input3");
    });
    </script>
@endsection
