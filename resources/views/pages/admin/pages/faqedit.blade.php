@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.pages').each(function(){
      $(this).addClass('active');
    });
    $('.page2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Faq Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/faqs/put') }}">
                    
                    {{ csrf_field() }}
                    
                    <div class="row">
                      @foreach($faqs as $f)
                      <input type="hidden" name="id" value="{{ $f->id }}">
                      
                      <div class="col-md-12 form-group">  
                        <label>Category</label>
                        <select name="faqcat" class="form-control" required>
                        	 @foreach($faqscategory as $ff)
                        	<option value="{{ $ff->id }}" @if($ff->id==$f->cat_id) {{ 'selected' }} @endif>{{ $ff->title }}</option>
                        	 @endforeach
                        </select>
                        
                      </div>
                      
                      <div class="col-md-12 form-group">  
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" value="{{ $f->title }}" required>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" class="form-control" id="editor" >{{ $f->description }}</textarea>
                        <!--<textarea name="description" class="form-control" id="editor1" required>{{ $f->description }}</textarea>-->
                      </div>  
                      @endforeach
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection