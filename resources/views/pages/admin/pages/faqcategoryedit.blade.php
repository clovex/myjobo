@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.pages').each(function(){
      $(this).addClass('active');
    });
    $('.page2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Faq Category Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/faqs/putcategory') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                    	 @foreach($faqscategory as $f)
                      <input type="hidden" name="id" value="{{ $f->id }}">
                      <div class="col-md-4 form-group">  
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" value="{{ $f->title }}" required>
                      </div>
					   
                      <div class="col-md-4 form-group">  
                        <label>Icon</label>
                        <div class="page">
      					<input type="text" class="input form-control" name="inputid2" id="inputid2" value="{{ $f->icon }}"/>
      
                      </div>
                      </div> 
                     <div class="col-md-4 form-group">  
                        <label>Order By</label>
                        <input type="text" name="orderby" class="form-control" value="{{ $f->orderby }}" required>
                      </div>
                       @endforeach 
					
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
           
          </div>
		
      </div>
    </section>
</div>

<style>
.howl-iconpicker .geticonval
{
	padding:5px 5px!important;
	width: 39px!important;
	height: 39px!important;
}
</style>
<script>
    var whichInput = 0;

    $(document).ready(function(){
      $('.input1').iconpicker(".input1");
      $('#inputid2').iconpicker("#inputid2");
      $('.input3').iconpicker(".input3");
    });
    </script>
@endsection
