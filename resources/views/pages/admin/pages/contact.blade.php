@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.pages').each(function(){
      $(this).addClass('active');
    });
    $('.page4').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Contact Us
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/contact_us') }}">
                    {{ csrf_field() }}
                    @foreach($pages as $p)
                    <textarea id="editor" name="contactus" rows="10" cols="80">{{ $p->contents }}</textarea>
                    @endforeach
                    <div class="form-group">  
                      <br>
                      <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection
