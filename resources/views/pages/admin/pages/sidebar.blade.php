@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.settings').each(function(){
      $(this).addClass('active');
    });
    $('.settings6').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sidebar
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a data-toggle="tab" href="#tab_1" aria-expanded="true">Common Sidebar</a>
                    </li>
                    <li class="">
                      <a data-toggle="tab" href="#tab_2" aria-expanded="false">Entrepreneurship Sidebar</a>
                    </li>
                  </ul>
  
                  <div class="tab-content">


                    <div id="tab_1" class="tab-pane active">
                       <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/sidebar/common') }}">
                        {{ csrf_field() }}
                        <div class="row">
                        @foreach($common as $com)
                          <div class="col-md-12 form-group">
                          <label>Links</label>
                            <input type="text" name="link" value="{{ json_decode($com->contents)->link }}" class="form-control">
                          </div>

                          <div class="col-md-12 form-group">
                            <div class="col-md-8">  
                              <input type="file" name="image">
                            </div>
                            <div class="col-md-4">  
                            <img src="{{ URL::to('public/uploads') }}/{{ json_decode($com->contents)->image }}" style="width: 150px; height: 150px;">
                            </div>
                          </div>
                          @endforeach
                          <div class="col-md-12 form-group">  
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                        </div>  
                      </form>  
                    </div>
                    




                    <div id="tab_2" class="tab-pane ">
                      
                      <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/sidebar/enter') }}">
                        {{ csrf_field() }}
                        <div class="row">
                        @foreach($enter as $en)
                          <div class="col-md-12 form-group">
                            <label>Links</label>
                            <input type="text" name="link" value="{{ json_decode($en->contents)->link }}" class="form-control">
                          </div>

                          <div class="col-md-12 form-group">
                            <div class="col-md-8">  
                              <input type="file" name="image">
                            </div>
                            <div class="col-md-4">  
                            <img src="{{ URL::to('public/uploads') }}/{{ json_decode($en->contents)->image }}" style="width: 150px; height: 150px;">
                            </div>
                          </div>
                        @endforeach  
                          <div class="form-group col-md-12">  
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>

                        </div>
                      </form>

                    </div>  

                  </div>  
                </div>  
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
