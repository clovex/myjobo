@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.entrepreneurship').each(function(){
      $(this).addClass('active');
    });
    $('.entrepreneurship2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Events and Exhibitions
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/events') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                      @foreach($about12 as $c)
                      <div class="col-md-12 form-group">  
                        <textarea name="description" class="form-control" id="editor" required>{{ $c->contents }}</textarea>
                      </div>  
                      @endforeach
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
              </div>
          </div>
      </div>
    </section>
</div>
@endsection
