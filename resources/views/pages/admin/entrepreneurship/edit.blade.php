@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.entrepreneurship').each(function(){
      $(this).addClass('active');
    });
    $('.entrepreneurship1').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Entrepreneurship Add
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/entrepreneurship/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                    @foreach($entrep as $e)
                      <input type="hidden" name="id" value="{{ $e->id }}">
                      <div class="col-md-12 form-group">  
                        <label>Title</label>
                        <input type="text" name="title" value="{{ $e->title }}" class="form-control" required>
                      </div>

                      <div class="col-md-12 form-group">  
                        <label>Description</label>
                        <textarea name="description" class="form-control" id="editor" required>{{ $e->description }}</textarea>
                      </div>  
                    @endforeach
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection