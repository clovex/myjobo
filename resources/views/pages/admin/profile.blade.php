@extends('layouts.back')


@section('contents')
<div class="content-wrapper">
  <section class="content-header">
    <h1>Profile</h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header">
            <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/profile') }}">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <input type="text" name="first_name" value="{{ Auth::user()->first_name }}" class="form-control setpad" placeholder="First Name">
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" name="last_name" value="{{ Auth::user()->last_name }}" class="form-control setpad" placeholder="Last Name">
                    </div>  
                  </div>

                  <div class="row">
                    <div class="col-md-6 form-group">
                      <input type="text" name="email" value="{{ Auth::user()->email }}" class="form-control setpad" placeholder="Email">
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" name="password" class="form-control setpad" placeholder="password">
                    </div>  
                  </div>

                  <div class="row">
                    <div class="col-md-6 form-group">
                      <input type="file" name="image" placeholder="image">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 form-group">
                      <button class="btn btn-success" type="submit">Save</button>
                    </div>
                  </div>  
                </form>
            </div>    
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection
