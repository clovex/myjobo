@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.settings').each(function(){
      $(this).addClass('active');
    });
    $('.settings2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Country Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/country/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                    @foreach($country as $jc)
                      <input type="hidden" name="id" value="{{ $jc->country_id }}">
                      <div class="col-md-12 form-group">  
                        <label>name</label>
                        <input type="text" name="name" value="{{ $jc->name }}" class="form-control" required>
                      </div>
                    @endforeach  
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection
