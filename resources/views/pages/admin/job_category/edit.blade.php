@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.jobs').each(function(){
      $(this).addClass('active');
    });
    $('.job3').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Job Category Edit
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/job_category/put') }}">
                    
                    {{ csrf_field() }}
                    <div class="row">
                    @foreach($job_category as $jc)
                      <input type="hidden" name="id" value="{{ $jc->id }}">
                      <div class="col-md-12 form-group">  
                        <label>name</label>
                        <input type="text" name="name" value="{{ $jc->name }}" class="form-control" required>
                      </div>
                    @endforeach  
                      <div class="form-group col-md-12">  
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    
                    </div>

                </form>
            </div>
          </div>

      </div>
    </section>
</div>
@endsection
