@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.trade').each(function(){
      $(this).addClass('active');
    });
     $('.trade3').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Trade or Artisan Edit
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/trade/put') }}">
                    {{ csrf_field() }}
                      <div class="row">

                      @foreach($users as $u)

                        <input type="hidden" name="id" value="{{ $u->id }}">

                        <div class="col-md-6 form-group">
                          <label>User Name</label>
                          <input type="text" name="name" class="form-control" value="{{ $u->name }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>First name</label>
                          <input type="text" name="first_name" class="form-control" value="{{ $u->first_name }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Last name</label>
                          <input type="text" name="last_name" class="form-control" value="{{ $u->last_name }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Email</label>
                          <input type="text" name="email" class="form-control" value="{{ $u->email }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Phone</label>
                          <input type="text" name="phone" class="form-control" value="{{ $u->phone }}">
                        </div>

                        <!--div class="col-md-6 form-group">
                          <label>Age</label>
                          <input type="text" name="age" id="datepicker" class="form-control" value="{{ $u->age }}">
                        </div-->

                        <div class="col-md-6 form-group">
                          <label>Gender</label>
                          <select class="form-control" name="gender">
                            <option value="">Select Gender</option>
                            <option value="Male" @if ($u->gender == 'Male') {{ 'selected' }}   @endif>Male</option>
                            <option value="Female" @if ($u->gender == 'Female') {{ 'selected' }}   @endif>Female</option>
                          </select>
                        </div>


                        <div class="col-md-6 form-group">
                          <label>Experience</label>
                          <input type="text" name="experience" class="form-control" value="{{ $u->experience }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Education</label>
                          <input type="text" name="education" class="form-control" value="{{ $u->education }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Speciality</label>
                          <select class="form-control" name="speciality">
                            <option value="">Select Speciality</option>
                            @foreach($speciality as $pos)
                              <option value="{{ $pos->id }}" @if ($pos->id == $u->speciality) {{ 'selected' }} @endif>{{ $pos->name }}</option>
                            @endforeach
                          </select> 
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Based in District</label>
                          <select class="form-control" name="city">
                            <option value="">Based in District</option>
                            @foreach($city as $c)
                              <option value="{{ $c->id }}" @if ($c->id == $u->city) {{ 'selected' }} @endif>{{ $c->name }}</option>
                            @endforeach
                          </select> 
                        </div>
                        @php $selected = explode(',',$u->cities); @endphp
                        <div class="col-md-6 form-group">
                          <label>(* ctrl click to multi select Works in)</label>
                          <select id="job" class="form-control" name="cities[]" multiple="">
                            @foreach($city as $jc) 
                            <option value="{{ $jc->name }}" 
                              {{ (in_array($jc->name, $selected)) ? ' selected=selected' : '' }}> {{ $jc->name }}</option>
                            @endforeach
                          </select>
                        </div>

                        <div class="col-md-6 form-group">
                          <div class="col-md-8">
                            <label>Image</label>
                            <input type="file" name="image">
                          </div>
                          <div class="col-md-4">
                            @if($u->image != null)
                              <img src="{{ URL::to('public/uploads') }}/{{ $u->image }}" style="height: 100px; width: 100px;">
                            @endif
                          </div>
                        </div>

                      @endforeach  
                      
                        <div class="form-group col-md-12">  
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    
                      </div>
                    </form>
                  </div>  
              </div>
          </div>
      </div>
    </section>
</div>
<script>
  $(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>
@endsection
