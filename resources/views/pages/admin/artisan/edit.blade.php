@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.users').each(function(){
      $(this).addClass('active');
    });
     $('.users4').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Staff Requester Edit
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('admin/staff-requester/put') }}">
                    {{ csrf_field() }}
                      <div class="row">

                      @foreach($users as $u)

                        <input type="hidden" name="id" value="{{ $u->id }}">

                        <div class="col-md-6 form-group">
                          <label>Company Name</label>
                          <input type="text" name="name" class="form-control" value="{{ $u->name }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Contact Name</label>
                          <input type="text" name="contact_name" class="form-control" value="{{ $u->contact_name }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Email</label>
                          <input type="text" name="email" class="form-control" value="{{ $u->email }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Phone</label>
                          <input type="text" name="phone" class="form-control" value="{{ $u->phone }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Website</label>
                          <input type="text" name="website" class="form-control" value="{{ $u->website }}">
                        </div>

                        <div class="col-md-6 form-group">
                          <label>Address</label>
                          <input type="text" name="address" class="form-control" value="{{ $u->address }}">
                        </div>

                      @endforeach  
                      
                        <div class="form-group col-md-12">  
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    
                      </div>
                    </form>
                  </div>  
              </div>
          </div>
      </div>
    </section>
</div>
@endsection
