@extends('layouts.back')


@section('contents')
<script>
$(document).ready(function(){
    $('.mainmenuset').each(function(){
      $(this).removeClass('active');
    });
    $('.jobs').each(function(){
      $(this).addClass('active');
    });
    $('.job2').each(function(){
      $(this).addClass('active');
    });
});
</script>
<div class="content-wrapper">
    <section class="content-header">
      <h1> 
       Manage Applicant for Jobs
      </h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <div class="box-body pad">
                  <div class="col-md-12">
                    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Apllied</th>
                          <th>Job Post</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Name</th>
                          <th>Apllied</th>
                          <th>Job Post</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @foreach($applicants as $a)
                        @if (!empty($a->jobs))
                        <tr>
                          <td>
                          @if(!empty($a->users))  
                            {{ $a->users->first_name }} {{ $a->users->last_name }}
                          @endif
                          </td>
                          <td>{{ Carbon\Carbon::parse($a->created_at)->format('d-m-Y') }}</td>
                          <td>{{ $a->jobs->title }}</td>
                        </tr>
                        @endif
                        @endforeach
                      </tbody>
                    </table>    
                  </div>
                  <div class="col-md-12 text-right">
                    <div class="pagination"> {{ $applicants->links() }} </div>
                  </div>
              </div>
          </div>
      </div>
    </section>
</div>

@endsection
