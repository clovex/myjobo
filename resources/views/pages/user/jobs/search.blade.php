@extends('layouts.app')

@section('template_title')
    Jobs Search | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<style type="text/css">
    .setfacebook 
    {
        background-color: #4a6d9d; 
    }
    .setfacebook i
    {
        margin: 0px !important;
        color: #fff !important;
        font-size: 20px !important;
    }
    .settwitter
    {
        background-color: #3bc1ed; 
    }
    .settwitter i
    {
        margin: 0px !important;
        color: #fff !important;
        font-size: 20px !important;
    }
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Search Jobs</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('home') }}">Profile</a></li>
                    <li>Search Jobs</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    	<div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row" id="searchdata">

                {{ csrf_field() }}
                <div class="col-md-5 col-lg-5 col-sm-5 form-group">
                    <input type="text" id="jobname" class="form-control" placeholder="Job Title" style="padding: 6px 12px;">
                </div>
                <div class="col-md-5 col-lg-5 col-sm-5 form-group">
                    <select class="form-control" id="category">
                            <option value="">Select Jobs</option>
                        @foreach($job_category as $jc)
                            @if($jc->id == 0)
                                <option value="{{ $jc->id }}">Other</option>
                            @else
                                <option value="{{ $jc->id }}">{{ $jc->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 form-group">
                    <button class="btn btn-success btn-block" id="search">Search</button>
                </div>
            </div>
        </div>

    <script>
    $(function(){
        $("#search").click(function(){

            $.ajax({
                    'url':'{{ URL::to("jobsearch") }}',
                    'type':'POST',
                    'data':{'job':$('#jobname').val(),'category':$('#category').val(),'_token': $('#searchdata input[name=_token]').val()},
                    'success':function(message)
                    {   
                        
                        $(".job-list").html(message);  
                    }
            });
        });
    });
    </script>


        <div class="col-md-12 col-lg-12 col-sm-12" id="jobdata">
            <ul class="job-list">


            </ul>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
@endsection