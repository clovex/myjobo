@extends('layouts.app')

@section('template_title')
    Pofile | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<style type="text/css">
    .setpad
    {
        padding: 6px 12px !important;
    }
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Profile</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Profile</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

    	<div class="col-md-9 col-lg-9 col-sm-9">
    		<h2>Welcome, {{ Auth::user()->first_name }}</h2>
    		<hr>
            <form method="POST" enctype="multipart/form-data" action="{{ URL::to('jobseekerprofile') }}">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
    		<div class="row">
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="first_name" value="{{ Auth::user()->first_name }}" class="form-control setpad" placeholder="First Name">
    			</div>
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="last_name" value="{{ Auth::user()->last_name }}" class="form-control setpad" placeholder="Last Name">
    			</div>	
    		</div>

    		<div class="row">
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="phone" value="{{ Auth::user()->phone }}" class="form-control setpad" placeholder="Phone Number">
    			</div>
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="dob" value="{{ Auth::user()->dob }}" id="datepicker" class="form-control setpad" placeholder="Date of Birth">
    			</div>	
    		</div>

    		<div class="row">
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="city" value="{{ Auth::user()->city }}" class="form-control setpad" placeholder="City/Town">
    			</div>
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="dist" value="{{ Auth::user()->dist }}" class="form-control setpad" placeholder="District/County">
    			</div>	
    		</div>
            @php $selected = explode(',',Auth::user()->job_category); @endphp

    		<div class="row">

    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
                <label>(* ctrl click to multi select Job Category)</label>
    				<select id="job" class="form-control" name="job[]" multiple="" required>
    				@foreach($job_category as $jc)
                        <option value="{{ $jc->id }}" 
                         {{ (in_array($jc->id, $selected)) ? ' selected=selected' : '' }}> {{ $jc->name }}</option>
                    @endforeach
    				</select>
    			</select>
    			</div>
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <input type="file" name="image">
                </div>	
    		</div>
    		
    		<div class="row">
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<button class="btn btn-success" type="submit">Save</button>
    			</div>
    		</div>	

            </form>
    	</div>



    	<div class="col-md-3 col-lg-3 col-sm-3 text-center">
            @if(Auth::user()->image != null)
                <img src="{{ URL::to('public/uploads') }}/{{ Auth::user()->image }}" style="width:200px; height: 200px; border-radius:50%; margin:0px auto;">
            @else
    		<img src="{{ URL::to('frontend/images/profile-placeholder.jpg') }}" style="width:200px; height: 200px; border-radius:50%; margin:0px auto;">
    		@endif
            <hr>
    		<a class="button widget-btn btn-block" href="{{ URL::to('jobs') }}"><i class="fa fa-search" aria-hidden="true" style="transform: rotate(90deg);"></i> Search Jobs</a>
    		<a class="button widget-btn btn-block" href="{{ URL::to('jobapplications') }}"><i class="fa fa-check-square-o" aria-hidden="true"></i> Job Applications</a>
    		<a class="button widget-btn btn-block" href="{{ URL::to('cv') }}/{{ Auth::user()->id }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Manage CV</a>
    		<a class="button widget-btn btn-block" href="{{ URL::to('jobseekerdelete') }}"><i class="fa fa-trash" aria-hidden="true"></i> Delete My Account</a>

    	</div>
    </div>
</div>


<script>
  $(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>    

@endsection

@section('footer_scripts')
@endsection