@extends('layouts.app')

@section('template_title')
    Job Applications | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Job Applications</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('home') }}">Profile</a></li>
                    <li>Job Applications</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    	
        @foreach($jobs as $j)
        @if(isset($j->job))
        <div class="col-md-6">    
            <div class="application">
                <div class="app-content">
                    <div class="info">
                        @if($j->job->logo != null)
                        <img src="{{ URL::to('public/uploads') }}/{{ $j->job->logo }}" alt="">
                        @else
                            <img src="{{ URL::to('frontend/images/No_Image_Available.png') }}" alt="">
                        @endif
                        <span>{{ $j->job->company_name }}</span>
                            @if($j->cv != null)
                                <p><a href="{{ URL::to('public/cv') }}/{{ $j->cv }}" target="_blank">CV</a></p>
                            @else
                            @foreach($cvs as $cv) 
                                @if($cv->resume != null)
                                    <p><a href="{{ URL::to('public/cv') }}/{{ $cv->resume }}" target="_blank">CV</a></p>
                                @endif
                            @endforeach
                            @endif
                    </div>
                    
                    <div class="text-right">
                        <a href="{{ URL::to('job') }}/{{ $j->job->slug }}" class="button gray">Show Details</a>
                        <a href="{{ URL::to('application/delete') }}/{{ $j->id }}" class="button gray"><i class="fa fa-trash-o" aria-hidden="true" style="font-size: 16px;"></i></a> 
                    </div>
                    <div class="clearfix"></div> 
                </div>
                <div class="app-footer">
                    <span>{{ $j->job->title }}</span>
                    <ul>
                        <li><i class="fa fa-calendar"></i> {{ Carbon\Carbon::parse($j->created_at)->format('jS F, Y') }}</li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
</div>
@endsection

@section('footer_scripts')
@endsection