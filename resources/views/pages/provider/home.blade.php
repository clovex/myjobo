@extends('layouts.app')

@section('template_title')
    Pofile | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
{!! HTML::style('frontend/text/jquery-te.css') !!}
{!! HTML::script('frontend/text/jquery-te.min.js') !!}
<style type="text/css">
	.setpad
	{
		padding: 6px 12px !important;
	}
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Profile</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Profile</li>
                </ul>
            </nav>
        </div>
    </div>
</div> 

<div class="container">
    <div class="row">
    	<div class="col-md-9 col-lg-9 col-sm-9">
    		<h2>Welcome, {{ Auth::user()->name }}</h2>
    		<hr>
             <form method="POST" enctype="multipart/form-data" action="{{ URL::to('providerprofile') }}">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
    		<div class="row">
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="email" value="{{ Auth::user()->email }}" class="form-control setpad" placeholder="E-mail">
    			</div>
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="companyname" value="{{ Auth::user()->name }}" class="form-control setpad" placeholder="Company Name">
    			</div>	
    		</div>

    		<div class="row">
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="contact_name" value="{{ Auth::user()->contact_name }}" class="form-control setpad" placeholder="Contact Name">
    			</div>
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="website" value="{{ Auth::user()->website }}" class="form-control setpad" placeholder="Web Site">
    			</div>	
    		</div>

    		<div class="row">
    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="address" value="{{ Auth::user()->address }}" class="form-control setpad" placeholder="Address">
    			</div>

    			<div class="col-md-6 col-lg-6 col-sm-6 form-group">
    				<input type="text" name="phone" value="{{ Auth::user()->phone }}" class="form-control setpad" placeholder="Phone Number">
    			</div>	
    		</div>
    		
    		<div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                <input type="file" name="image">
                </div>
            </div>

    		
    		<div class="row">
    			<div class="col-md-6 form-group">
    				<button class="btn btn-success">Save</button>
    			</div>
    		</div>	
            </form>
    	</div>



    	<div class="col-md-3 col-lg-3 col-sm-3 text-center">
    		@if(Auth::user()->image != null)
                <img src="{{ URL::to('public/uploads') }}/{{ Auth::user()->image }}" style="width:200px; height: 200px; border-radius:50%; margin:0px auto;">
            @else
            <img src="{{ URL::to('frontend/images/profile-placeholder.jpg') }}" style="width:200px; height: 200px; border-radius:50%; margin:0px auto;">
            @endif
    		<hr>
    		<a class="button widget-btn btn-block" href="{{ URL::to('requeststaff') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> Request Staff</a>
            <a class="button widget-btn btn-block" href="{{ URL::to('managerequest') }}"><i class="fa fa-recycle" aria-hidden="true"></i> Manage Request</a>
            <a class="button widget-btn btn-block" href="{{ URL::to('deletevetted') }}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete My Account</a>
    	</div>
    </div>
</div>
<script>
  $('#editor1').jqte();
</script>

@endsection

@section('footer_scripts')
@endsection