@extends('layouts.app')

@section('template_title')
    Request Edit | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<style type="text/css">
.form-control
{
    padding:6px 12px !important;
}
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Request Edit</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('home') }}">Profile</a></li>
                    <li><a href="{{ URL::to('managerequest') }}">Manage Request</a></li>
                    <li>Request Edit</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            
            <div class="col-md-3 col-lg-3 col-sm-3">
            &nbsp;
            </div>

            <div class="col-md-6 col-lg-6 col-sm-6" style="box-shadow: 0 0 0 2px transparent, 1px 1px 6px rgba(0, 0, 0, 0.2)">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('staffedit') }}">
                    {{ csrf_field() }}
                    @foreach($staff as $t)
                    <input type="hidden" name="id" value="{{ $t->id }}">
                    <input type="hidden" name="provider_id" value="{{ Auth::user()->id }}">
                        <div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <span class="ln ln-icon-Approved-Window" style="font-size: 50px !important; color: #47a447;"></span>
                        </div>

                        <div class="col-lg-10 col-md-10 col-sm-10">
                            <h3 style="color: #F45B06;">Position</h3>

                            <div class="form-group">
                                <label>What industry are you hiring for?</label>
                                <select class="form-control" name="industry" required>
                                    <option value="">Select</option>
                                    @foreach($industry as $ind)
                                        <option value="{{ $ind->id }}" @if ($ind->id == $t->industry) {{ 'selected' }}   @endif>{{ $ind->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>What position are you hiring for?</label>
                                <select class="form-control" name="position" required>
                                    <option value="">Select</option>
                                    @foreach($position as $pos)
                                        <option value="{{ $pos->id }}" @if ($pos->id == $t->position) {{ 'selected' }}   @endif>{{ $pos->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>How many staff would you like to hire?</label>
                                <input type="number" name="staff" class="form-control" min="1" value="{{ $t->staff }}" max="50">
                            </div>

                            <div class="form-group">
                                <label>Tell us more about the role or if you have specific requirements</label>
                                <textarea class="form-control" name="description">{{ $t->description }}</textarea>
                            </div>
                        </div>
                        </div>

                        <div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <span class="ln ln-icon-Bar-Chart" style="font-size: 50px !important; color: #47a447;"></span>
                        </div>

                        <div class="col-lg-10 col-md-10 col-sm-10">
                            <h3 style="color: #F45B06;">Criteria</h3>

                            <div class="form-group">
                                <label>What age range do you require?</label>
                                <div class="col-md-5 col-lg-5 col-sm-5 form-group" style="padding: 0px;">
                                    <input type="number" class="form-control" name="age1" value="{{ $t->age1 }}" min="18" max="65">
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-2 form-group text-center">to</div>
                                <div class="col-md-5 col-lg-5 col-md-5 form-group" style="padding: 0px;">
                                    <input type="number" class="form-control" name="age2" min="18" max="65" value="{{ $t->age2 }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>What gender do you prefer?</label>
                                <select class="form-control" name="gender">
                                    <option value="">No preference</option>
                                    <option value="Male" @if($t->gender == 'Male'){{ 'selected' }} @endif>Male</option>
                                    <option value="Female" @if($t->gender == 'Female'){{ 'selected' }} @endif>Female</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Level of experience?</label>
                                <select class="form-control" name="experience">
                                    <option value="">No preference</option>
                                    <option value="At least 1 years" @if($t->experience == 'At least 1 years'){{ 'selected' }} @endif>At least 1 years</option>
                                    <option value="At least 3 years" @if($t->experience == 'At least 3 years'){{ 'selected' }} @endif>At least 3 years</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Minimum level of education?</label>
                                <select class="form-control" name="education">
                                    <option value="">No preference</option>
                                    <option value="Matric" @if($t->education == 'Matric'){{ 'selected' }} @endif>Matric</option>
                                    <option value="National Certificate" @if($t->education == 'National Certificate'){{ 'selected' }} @endif>National Certificate</option>
                                    <option class="Diploma and Higher" @if($t->education == 'Diploma and Higher'){{ 'selected' }} @endif>Diploma and Higher</option>
                                </select>
                            </div>
                        </div>
                        </div>


                        <div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <span class="ln ln-icon-Credit-Card2" style="font-size: 50px !important; color: #47a447;"></span>
                        </div>

                        <div class="col-lg-10 col-md-10 col-sm-10">
                            <h3 style="color: #F45B06;">Salary</h3>
                            @php $salary1 = explode('-',$t->salary1); @endphp
                            <div class="form-group">
                            <label>What BASIC salary are you offering?</label>
                            <div class="input-group" style="width: 50%; float: left;">
                                <span id="basic-addon1" class="input-group-addon">MK</span>
                                <input class="form-control" placeholder="" name="salary1" aria-describedby="basic-addon1" type="text" value="@php echo $salary1[0] @endphp" required>
                            </div>
                            <div class="input-group" style="width: 50%; padding-left: 5%;">
                                <select class="form-control" name="salary1type" required>
                                    <option value="">Select</option>
                                    <option value="per month" @if($salary1[1] == 'per month') {{ 'selected' }} @endif>per month</option>
                                    <option value="per day" @if($salary1[1] == 'per day') {{ 'selected' }} @endif>per day</option>
                                    <option value="per hour" @if($salary1[1] == 'per hour') {{ 'selected' }} @endif>per hour</option>
                                </select>
                            </div>
                            </div>

                             <!--@php $salary2 = explode('-',$t->salary2); @endphp
                            <div class="form-group">
                                <label>What COMMISSION salary are you offering? (Average ZAR value)</label>
                                <div class="input-group" style="width: 50%; float: left;">
                                    <span id="basic-addon1" class="input-group-addon">MK</span>
                                    <input class="form-control" placeholder="" name="salary2" aria-describedby="basic-addon1" type="text" value="@php echo $salary2[0]  @endphp" required>
                                </div>
                                
                                <div class="input-group" style="width: 50%; padding-left: 5%;">
                                    <select class="form-control" name="salary2type" required>
                                        <option value="">Select</option>
                                        <option value="per month" @if($salary2[1] == 'per month') {{ 'selected' }} @endif>per month</option>
                                        <option value="per day" @if($salary2[1] == 'per day') {{ 'selected' }} @endif>per day</option>
                                        <option value="per hour" @if($salary2[1] == 'per hour') {{ 'selected' }} @endif>per hour</option>
                                    </select>
                                </div>
                            </div-->  
                        </div>
                        </div>


                        <div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <span class="ln ln-icon-Calendar-3" style="font-size: 50px !important; color: #47a447;"></span>
                        </div>

                        <div class="col-lg-10 col-md-10 col-sm-10">
                            <h3 style="color: #F45B06;">Interview Details</h3>

                            <div class="form-group">
                                <label>Interview Date</label>
                                <input type="text" name="interview_date" class="form-control datepicker" value="{{ $t->deadline }}" required>
                            </div>

                            @php $interview_time = explode(':',$t->interview_time); @endphp

                            <div class="form-group">
                                <label>Interview Time</label>
                                <div class="col-lg-6 col-md-6 col-sm-6 form-group" style="padding-left: 0px;">
                                    <select class="form-control" name="interview_time1" required>
                                        <option value="">HH</option>
                                        @for($i=8; $i<=19; $i++)
                                            <option value="{{ $i }}" @if ($i == $interview_time[0]) {{ 'selected' }} @endif>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 form-group" style="padding-right: 0px;">
                                    <select class="form-control" name="interview_time2" required>
                                        <option value="">MM</option>
                                        <option value="00" @if($interview_time[1] == '00') {{ 'selected' }} @endif>00</option>
                                        <option value="15" @if($interview_time[1] == '15') {{ 'selected' }} @endif>15</option>
                                        <option value="30" @if($interview_time[1] == '30') {{ 'selected' }} @endif>30</option>
                                        <option value="45" @if($interview_time[1] == '45') {{ 'selected' }} @endif>45</option>
                                    </select>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label>Deadline</label>
                                <input type="text" name="deadline" value="{{ $t->deadline }}" class="form-control datepicker" required>
                            </div>
                            @endforeach
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <button class="button btn-block" style="padding: 6px 12px;">Submit</button>
                            </div>


                        </div>
                        </div>



                    </form>
                </div>
            </div>

            <div class="col-md-3 col-lg-3 col-sm-3">
            &nbsp;
            </div>

        </div>
    </div>
<div class="margin-bottom-40"></div>
</div> 

<script>
  $(function () {
    $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>
@endsection