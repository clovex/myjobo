@extends('layouts.app')

@section('template_title')
    Manage Request | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Manage Request</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('home') }}">Profile</a></li>
                    <li>Manage Request</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    	<div class="col-md-12">
            <div class="text-right">
                <a href="{{ URL::to('requeststaff') }}" class="button"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Staff Request</a>
            </div>
            <table class="manage-table responsive-table">

            <tr>
                <th><i class="fa fa-file-text"></i> Industry & Category</th>
                <th><i class="fa fa-calendar"></i> Date Interview</th>
                <th><i class="fa fa-calendar"></i> Date Expires</th>
                <th>Action</th>
            </tr>
                    
            @foreach($staff as $j)
            <tr>
                <td class="title"><a>
                    @foreach($industry as $ind)
                        @if($ind->id == $j->industry)
                            {{ $ind->name }}
                        @endif
                    @endforeach
                    -
                    @foreach($position as $pos)
                        @if($pos->id == $j->position)
                            {{ $pos->name }}
                        @endif
                    @endforeach        
                </a></td>
                <td>{{ Carbon\Carbon::parse($j->interview_date)->format('jS F, Y') }}</td>
                <td> {{ Carbon\Carbon::parse($j->deadline)->format('jS F, Y') }}</td>
                <td class="action">
                    <a href="{{ URL::to('staffedit') }}/{{ $j->id }}">
                        <i class="fa fa-pencil"></i> Edit
                    </a>
                    <a href="{{ URL::to('staffdelete') }}/{{ $j->id }}" class="delete">
                        <i class="fa fa-remove"></i> Delete
                    </a>
                </td>
            </tr>
            @endforeach        
        </table>
         <div class="col-md-12">
            <div class="pagination-container text-center">
                <nav class="pagination"> 
                    {{ $staff->links() }}
                </nav>
            </div>
        </div>    
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
@endsection