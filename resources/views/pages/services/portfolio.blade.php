@extends('layouts.app')

@section('template_title')
    photo gallery | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Photo Gallery</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('home') }}">Photo Gallery</a></li>
                    <li>Portfolio</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    	<div class="col-md-12 col-lg-12 col-sm-12">
            <form method="POST" enctype="multipart/form-data" action="{{ URL::to('customer/image') }}">
            {{ csrf_field() }}

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                        <label>Image :-</label>
                        <input type="file" name="image">
                    </div>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                        <input type="submit" name="submit" value="Submit" class="btn btn-success">
                    </div>    
                </div>
            
            </form>    
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($portfolio as $por)
                    <tr>
                        <td><img src="{{ URL::to('public/uploads') }}/{{ $por->image }}" style="width: 100px; height: 100px;"></td>
                        <td>
                            <a href="{{ URL::to('photogallery/delete') }}/{{ $por->id }}">
                              <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px; color: red;"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<style>
.dataTables_paginate.paging_simple_numbers
{
  text-align: right;
}
.pagination
{
  margin: 0px;
}
.dataTables_filter
{
  text-align: right;
}
.content-wrapper
{
  min-height: 700px !important;
}
</style>

@endsection

@section('footer_scripts')
@endsection