@extends('layouts.app')

@section('template_title')
    Pofile | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('template_fastload_css')
@endsection

@section('content')
{!! HTML::script('frontend/text/ckeditor.js') !!}    
{!! HTML::script('frontend/text/sample.js') !!}   
{!! HTML::style('frontend/text/neo.css') !!}
<style type="text/css">
    .setpad
    {
        padding: 6px 12px !important;
    }
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Profile</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Profile</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-md-9 col-lg-9 col-sm-9">
            <h2>Welcome, {{ Auth::user()->first_name }}</h2>
            <hr>
            <form method="POST" enctype="multipart/form-data" action="{{ URL::to('customerprofile') }}">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <input type="text" name="first_name" value="{{ Auth::user()->first_name }}" class="form-control setpad" placeholder="First Name">
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <input type="text" name="last_name" value="{{ Auth::user()->last_name }}" class="form-control setpad" placeholder="Last Name">
                </div>  
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <input type="text" name="phone" value="{{ Auth::user()->phone }}" class="form-control setpad" placeholder="Phone (Mobile)">
                </div>

                 <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <input type="text" name="officephone" value="{{ Auth::user()->officephone }}" class="form-control setpad" placeholder="Phone (Office)">
                </div>
                  
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <select class="form-control" name="gender">
                        <option value="">Select Gender</option>
                        <option value="Male" @if (Auth::user()->gender == 'Male') {{ 'selected' }}   @endif>Male</option>
                        <option value="Female" @if (Auth::user()->gender == 'Female') {{ 'selected' }}   @endif>Female</option>
                    </select>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <input type="text" name="education" value="{{ Auth::user()->education }}" class="form-control setpad" placeholder="Education">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <select class="form-control" name="speciality">
                        <option value="">Select Speciality</option>
                        @foreach($speciality as $pos)
                            <option value="{{ $pos->id }}" @if ($pos->id == Auth::user()->speciality) {{ 'selected' }} @endif>{{ $pos->name }}</option>
                        @endforeach
                    </select> 
                </div>

                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <input type="text" name="experience" value="{{ Auth::user()->experience }}" class="form-control setpad" placeholder="Experience">
                </div>    
                
            </div>


             <div class="row">
                @php $datas = explode('-',Auth::user()->hoursOperation); @endphp
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <label>Hours Operation</label>
                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                    <select class="form-control" name="hoursOperationone" required="">
                        <option value="">Select</option>
                        <option value="Monday to Friend" @if(Auth::user()->hoursOperation != null) @if($datas[0] == "Monday to Friend") {{ 'selected' }} @endif @endif>Monday to Friend</option>
                        <option value="Monday to saturday" @if(Auth::user()->hoursOperation != null) @if($datas[0] == "Monday to saturday") {{ 'selected' }} @endif @endif>Monday to saturday</option>
                    </select> 
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                        <select class="form-control" name="hoursOperationtwo" required="">
                            <option value="">Select Start Time</option>
                            @for($ii=1; $ii<=12; $ii++)
                                <option value="{{ $ii }} AM" @if(Auth::user()->hoursOperation != null) @if($datas[1] == $ii." AM") {{ 'selected' }} @endif @endif>{{ $ii }} AM</option>
                            @endfor
                            @for($jj=1; $jj<=12; $jj++)
                                <option value="{{ $jj }} PM" @if(Auth::user()->hoursOperation != null) @if($datas[1] == $jj." PM") {{ 'selected' }} @endif @endif>{{ $jj }} PM</option>
                            @endfor
                        </select>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 form-group">
                        <select class="form-control" name="hoursOperationthree" required="">
                            <option value="">Select End Time</option>
                            @for($iii=1; $iii<=12; $iii++)
                                <option value="{{ $iii }} AM" @if(Auth::user()->hoursOperation != null) @if($datas[2] == $iii." AM") {{ 'selected' }} @endif @endif>{{ $iii }} AM</option>
                            @endfor
                            @for($jjj=1; $jjj<=12; $jjj++)
                                <option value="{{ $jjj }} PM" @if(Auth::user()->hoursOperation != null) @if($datas[2] == $jjj." PM") {{ 'selected' }} @endif @endif>{{ $jjj }} PM</option>
                            @endfor

                        </select>
                    </div>   

                </div>

                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                @php $selecteds = explode(',',Auth::user()->modesPayment); @endphp
                    <label>(* ctrl click to multi select Payment mode)</label>
                    <select id="job" class="form-control" name="paymentmode[]" multiple="" required="">
                    @foreach($paymentmode as $pm) 
                        <option value="{{ $pm->type }}" {{ (in_array($pm->type, $selecteds)) ? ' selected=selected' : '' }} > {{ $pm->type }}</option>
                    @endforeach
                    </select>
                    </select>
                </div>    
                
            </div>

            <div class="row">
                 <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <input type="hidden" name="sunavi" value="0">
                    <input type="checkbox" name="sunavi" value="1" @if(Auth::user()->sunavi == 1) {{ 'checked' }} @endif> Sunday Available
                </div>  
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <input type="file" name="image">
                </div>   
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <div class="form-group">
                    <!--select class="form-control" name="city">
                        <option value="">Based in</option>
                        @foreach($city as $c)
                            <option value="{{ $c->id }}" @if ($c->id == Auth::user()->city) {{ 'selected' }} @endif>{{ $c->name }}</option>
                        @endforeach
                    </select-->
                    <label>Address</label>
                    <textarea class="" name="location">{{ Auth::user()->location }}</textarea>
                    </div>
                    
                </div>
                @php $selected = explode(',',Auth::user()->cities); @endphp

                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                <label>(* ctrl click to multi select Works in)</label>
                    <select id="job" class="form-control" name="cities[]" multiple="" required="">
                    @foreach($city as $jc) 
                        <option value="{{ $jc->name }}" 
                         {{ (in_array($jc->name, $selected)) ? ' selected=selected' : '' }}> {{ $jc->name }}</option>
                    @endforeach
                    </select>
                </select>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12 form-group">
                    <h4>Business Information</h4>
                    <textarea class="form-control" name="description" id="editor" placeholder="Company Description">{!! Auth::user()->description !!}</textarea>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                    <button class="btn btn-success" type="submit">Save</button>
                </div>
            </div>  

            </form>
        </div>



        <div class="col-md-3 col-lg-3 col-sm-3 text-center">
            @if(Auth::user()->image != null)
                <img src="{{ URL::to('public/uploads') }}/{{ Auth::user()->image }}" style="width:200px; height: 200px; border-radius:50%; margin:0px auto;">
            @else
            <img src="{{ URL::to('frontend/images/profile-placeholder.jpg') }}" style="width:200px; height: 200px; border-radius:50%; margin:0px auto;">
            @endif
            <hr>

            <a class="button widget-btn btn-block" href="{{ URL::to('photogallery') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> Photo Gallery</a>

            <a class="button widget-btn btn-block" href="{{ URL::to('deleterequest') }}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete My Account</a>

        </div>
    </div>
</div>
<script>
  $('#editor1').jqte();
</script>

<script>
  $(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });  
</script>
<script>
  initSample();
</script>
@endsection

@section('footer_scripts')
@endsection