<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Myjobo</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  {!! HTML::script('frontend/text/ckeditor.js') !!}    
  {!! HTML::script('frontend/text/sample.js') !!}   
  {!! HTML::style('frontend/text/neo.css') !!} 
  <!-- Bootstrap 3.3.6 -->
  {!! HTML::style('backend/bootstrap/css/bootstrap.min.css') !!}
  {!! HTML::style('backend/bootstrap/css/font-awesome.min.css') !!}
  {!! HTML::style('backend/bootstrap/css/ionicons.min.css') !!}
  <!-- Theme style -->
  {!! HTML::style('backend/dist/css/AdminLTE.min.css') !!}
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  {!! HTML::style('backend/dist/css/skins/_all-skins.min.css') !!}
  {!! HTML::style('backend/plugins/iCheck/flat/blue.css') !!}
  {!! HTML::style('backend/plugins/morris/morris.css') !!}
  {!! HTML::style('backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
  {!! HTML::style('backend/plugins/datepicker/datepicker3.css') !!}
  {!! HTML::style('backend/plugins/daterangepicker/daterangepicker.css') !!}
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  {!! HTML::script('backend/bootstrap/js/jquery.min.js') !!}
  
  
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
        @include('partials.backheader')
        @include('partials.backsidebar')

            @yield('contents') 

        @include('partials.backfooter')
</div>
{!! HTML::script('backend/plugins/jQuery/jquery-2.2.3.min.js') !!}

{!! HTML::script('backend/bootstrap/js/jquery-ui.min.js') !!}
    {!! HTML::script('backend/iconjs/simple-iconpicker.min.js') !!}
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

{!! HTML::script('backend/bootstrap/js/bootstrap.min.js') !!}
{!! HTML::script('backend/bootstrap/js/raphael-min.js') !!}
{!! HTML::script('backend/plugins/morris/morris.min.js') !!}
{!! HTML::script('backend/plugins/sparkline/jquery.sparkline.min.js') !!}
{!! HTML::script('backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
{!! HTML::script('backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
{!! HTML::script('backend/plugins/knob/jquery.knob.js') !!}
{!! HTML::script('backend/bootstrap/js/moment.min.js') !!}
{!! HTML::script('backend/plugins/daterangepicker/daterangepicker.js') !!}
{!! HTML::script('backend/plugins/datepicker/bootstrap-datepicker.js') !!}
{!! HTML::script('backend/plugins/slimScroll/jquery.slimscroll.min.js') !!}
{!! HTML::script('backend/plugins/fastclick/fastclick.js') !!}
{!! HTML::script('backend/dist/js/app.min.js') !!}
{!! HTML::script('backend/dist/js/pages/dashboard.js') !!}
{!! HTML::script('backend/dist/js/demo.js') !!}

{!! HTML::script('backend/dist/jquery.dataTables.min.js') !!}
{!! HTML::script('backend/dist/dataTables.bootstrap.min.js') !!}
{!! HTML::script('backend/dist/dataTables.responsive.min.js') !!}
{!! HTML::script('backend/dist/responsive.bootstrap.min.js') !!}

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
     {!! HTML::style('backend/iconjs/simple-iconpicker.min.css') !!}
      
      <script>
  initSample();
</script>
  <style>
.dataTables_paginate.paging_simple_numbers
{
  text-align: right;
}
.pagination
{
  margin: 0px;
}
.dataTables_filter
{
  text-align: right;
}
.content-wrapper
{
  min-height: 700px !important;
}
</style>
</body>
</html>


