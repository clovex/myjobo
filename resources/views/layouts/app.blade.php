<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
    <!-- Google Tag Manager -->
<!--script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({‘gtm.start’:
new Date().getTime(),event:‘gtm.js’});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=‘dataLayer’?‘&l=‘+l:‘’;j.async=true;j.src=
‘https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,‘script’,‘dataLayer’,‘GTM-WF468MT’);</script>
 End Google Tag Manager -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="{{ URL::to('frontend/images/favicon.jpg') }}" type="image/x-icon">
        <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') @endif</title>
        <META NAME="description" CONTENT="@if (trim($__env->yieldContent('template_description'))) @yield('template_description') @else
        Myjobo.com connects individuals with jobs and entrepreneurship. Myjobo.com offers free job advertising and job application to jobs posted on the site. Myjobo.com promotes enterpreneurship. Applying Jobonology principles, Myjobo.com believes that not everyone can get a formal job since jobs are scarce and unemployment rates keep rising. Myjobo.com looks at the jobs issue holistically and connects professional bodies, educational institutions, and entrepreneurs to ensure that everyone earns a decent living in a world where formal employment rates are declining. @endif">
<META NAME="keywords" CONTENT="malawi jobs,jobs in malawi,job career searching,international business job opportunities,online jobs free,login job search,accounting jobs vacancies,universal job search,career training online,careers advice online,online apply careers,career advice african,international online business opportunities,business studies for malawi,international business employment opportunities,ad business opportunities,basic information about entrepreneurship,free entrepreneurship seminars,free entrepreneurship work,professional bodies in malawi,jobonology,companies in malawi,malawi myjobocom,malawi volunteer organization,malawi myjobo com,learn job skills online free,malawi employment watch,post cv online,job skills to improve,volunteer in malawi, myjobo.com">
       
        {!! HTML::style('frontend/css/bootstrap.min.css') !!}
        
        {!! HTML::script('frontend/scripts/jquery.min.js') !!}
        {!! HTML::style('frontend/css/style.css') !!}
        {!! HTML::style('frontend/css/colors/green.css') !!}
        {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
        {!! HTML::style('backend/plugins/datepicker/datepicker3.css') !!}
        <style type="text/css">
            @yield('template_fastload_css')

            @if (Auth::User() && (Auth::User()->profile) && (Auth::User()->profile->avatar_status == 0))
                .user-avatar-nav {
                    background: url({{ Gravatar::get(Auth::user()->email) }}) 50% 50% no-repeat;
                    background-size: auto 100%;
                }
            @endif

        </style>

        {{-- Scripts --}}
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>

        @if (Auth::User() && (Auth::User()->profile) && $theme->link != null && $theme->link != 'null')
            <link rel="stylesheet" type="text/css" href="{{ $theme->link }}">
        @endif

        @yield('head')
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48782394-1', 'auto');
  ga('send', 'pageview');

</script>
    </head>
    <body>
     
        <div id="wrapper">
            @include('partials.nav')
            
            @yield('content')

        

        {{-- Scripts --}}
        {!! HTML::script('//maps.googleapis.com/maps/api/js?key='.env("GOOGLEMAPS_API_KEY").'&libraries=places&dummy=.js', array('type' => 'text/javascript')) !!}

        @include('partials.footer')
        
        {!! HTML::script('frontend/scripts/jquery-2.1.3.min.js') !!} 
        {!! HTML::script('frontend/scripts/custom.js') !!}    
        {!! HTML::script('frontend/scripts/jquery.superfish.js') !!}    
        {!! HTML::script('frontend/scripts/jquery.themepunch.tools.min.js') !!}    
        {!! HTML::script('frontend/scripts/jquery.themepunch.revolution.min.js') !!}    
        {!! HTML::script('frontend/scripts/jquery.themepunch.showbizpro.min.js') !!}    
        {!! HTML::script('frontend/scripts/jquery.flexslider-min.js') !!}    
        {!! HTML::script('frontend/scripts/chosen.jquery.min.js') !!}    
        {!! HTML::script('frontend/scripts/jquery.magnific-popup.min.js') !!}    
        {!! HTML::script('frontend/scripts/waypoints.min.js') !!}    
        {!! HTML::script('frontend/scripts/jquery.counterup.min.js') !!}    
        {!! HTML::script('frontend/scripts/jquery.jpanelmenu.js') !!}    
        {!! HTML::script('frontend/scripts/stacktable.js') !!}    
        {!! HTML::script('frontend/scripts/headroom.min.js') !!} 
        {!! HTML::script('frontend/scripts/bootstrap.min.js') !!}
         
    {!! HTML::script('backend/plugins/datepicker/bootstrap-datepicker.js') !!}
        
        {!! HTML::script('backend/dist/jquery.dataTables.min.js') !!}
        {!! HTML::script('backend/dist/dataTables.bootstrap.min.js') !!}
        {!! HTML::script('backend/dist/dataTables.responsive.min.js') !!}

    </div>
         
        @yield('footer_scripts')
       <script type="text/javascript">
var infolinks_pid = 3031802;
var infolinks_wsid = 0;
</script>
<script type="text/javascript" src="//resources.infolinks.com/js/infolinks_main.js"></script>
    </body>
</html>