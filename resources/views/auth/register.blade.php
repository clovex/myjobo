@extends('layouts.without')

@section('template_title')
    Register 
@endsection

@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Register</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Register</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
          <div class="seven columns" style="text-align: center; box-shadow:0px 5px 5px 5px #888888;padding-top: 10px; padding-bottom: 10px;">  
                <h2>Register as a Job Seeker</h2>
                <p style="font-size: 18px !important;">Let employers find you</p>
                <a class="button widget-btn" href="{{ URL::to('register_job_seeker') }}">
                    <i class="fa fa-user" aria-hidden="true"></i> Job Seeker
                </a>
          </div>

          <div class="two columns">&nbsp;</div>

          <div class="seven columns" style="text-align: center; box-shadow:0px 5px 5px 5px #888888;padding-top: 10px; padding-bottom: 10px;">
                <h2>Register as Employer</h2>
                <p style="font-size: 18px !important;">Find your dream employee</p>
                <a class="button widget-btn" href="{{ URL::to('register_employer') }}">
                    <i class="fa fa-user-secret" aria-hidden="true"></i> Employer
                </a>
          </div>

    </div>

    <div class="row">
          <div class="seven columns" style="text-align: center; box-shadow:0px 5px 5px 5px #888888;padding-top: 10px; padding-bottom: 10px;">  
                <h2>Register as a Recruiter</h2>
                <p style="font-size: 18px !important;">Recruit Vetted Staff Quickly</p>
                <a class="button widget-btn" href="{{ URL::to('register_staff_requester') }}">
                    <i class="fa fa-user" aria-hidden="true"></i> Recruiter
                </a>
          </div> 

          <div class="two columns">&nbsp;</div>

          <div class="seven columns" style="text-align: center; box-shadow:0px 5px 5px 5px #888888;padding-top: 10px; padding-bottom: 10px;">
                <h2>Register as Trade or Artisan</h2>
                <p style="font-size: 18px !important;">Let Customers find you easily</p>
                <a class="button widget-btn" href="{{ URL::to('register_trade') }}">
                    <i class="fa fa-user-secret" aria-hidden="true"></i> Trade or Artisan
                </a>
          </div>

    </div>
</div>
@endsection
