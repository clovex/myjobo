@extends('layouts.without')

@section('template_title')
    Trade or Artisan 
@endsection

@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Trade or Artisan</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Trade or Artisan</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
        <div id="login-overlay" class="two columns">&nbsp;</div>
        <div id="login-overlay" class="twelve columns">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel">Create a Trade or Artisan Account</h4>
          </div>
          <div class="modal-body">
              <div class="container">
                <div class="four columns moredetails">
                    <div class="">
                      <p class="lead">Register now for <span class="text-success">FREE</span></p>
                      
                      <div class="four columns">
                            <i class="fa fa-user text-success one columns" style="font-size: 40px;"></i>
                                <b class="three columns" style="font-weight: 600;padding-left: 10px;">It’s free</b>
                                <span class="three columns" style="padding-left: 10px;">it costs you nothing to be registered on Myjobo.com</span>
                            <hr/>
                      </div>

                      <div class="four columns">
                            <i class="fa fa-user text-success one columns" style="font-size: 40px;"></i>
                                <b class="three columns" style="font-weight: 600;padding-left: 10px;">Let customers find you</b>
                                <span class="three columns" style="padding-left: 10px;">your profile will be viewed by thousands</span>
                            <hr/>
                      </div>

                      <div class="four columns">
                            <i class="fa fa-user text-success one columns" style="font-size: 40px;"></i>
                                <b class="three columns" style="font-weight: 600;padding-left: 10px;">Get reviews</b>
                                <span class="three columns" style="padding-left: 10px;">customers will post feedback on your services.</span>
                            <hr/>
                      </div>
                     </div> 
                  </div>
                  <div class="eight columns">
                      <div class="well">
                          {!! Form::open(['route' => 'register', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST'] ) !!}

                        <!--{{ csrf_field() }}-->

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-6">
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Username', 'id' => 'name', 'required', 'autofocus']) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-sm-4 control-label">First Name</label>
                            <div class="col-sm-6">
                                {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name', 'id' => 'first_name','required', 'autofocus']) !!}
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-sm-4 control-label">Last Name</label>
                            <div class="col-sm-6">
                                {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name', 'id' => 'last_name','required', 'autofocus']) !!}
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-4 control-label">E-Mail Address</label>
                            <div class="col-sm-6">
                                {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'E-Mail Address', 'required']) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-sm-4 control-label">Phone</label>
                            <div class="col-sm-6">
                                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone', 'id' => 'phone', 'required', 'autofocus']) !!}
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('speciality') ? ' has-error' : '' }}">
                            <label for="speciality" class="col-sm-4 control-label">Speciality</label>
                            <div class="col-sm-6">
                                <select name="speciality" class="form-control" id="speciality" required autofocus style="padding: 14px 18px;">
                                    <option value="">Speciality</option>
                                    @foreach($speciality as $dd)
                                      <option value="{{ $dd->id }}">{{ $dd->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('speciality'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('speciality') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                            <label for="district" class="col-sm-4 control-label">District</label>
                            <div class="col-sm-6">
                                <select name="district" class="form-control" id="district" required autofocus style="padding: 14px 18px;">
                                    <option value="">District</option>
                                    @foreach($districts as $ddd)
                                      <option value="{{ $ddd->id }}">{{ $ddd->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('district'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('district') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-4 control-label">Password</label>
                            <div class="col-sm-6">
                                {!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password', 'required']) !!}
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        
                        <div class="form-group">
                            <label for="password-confirm" class="col-sm-4 control-label">Confirm Password</label>
                            <div class="col-sm-6">
                                {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password-confirm', 'placeholder' => 'Confirm Password', 'required']) !!}
                            </div>
                        </div>
                         <input type="hidden" name="type" value="5">
                        
                        <!--div class="form-group">
                            <label for="type" class="col-sm-4 control-label">User Type</label>
                            <div class="col-sm-6">
                                <select class="form-control" name="type" required="required" style="padding: 14px 18px;">
                                    <option value="">Select User Type</option>
                                    <option value="4">Employer</option>
                                    <option value="2">Job Seeker</option>
                                </select>
                            </div>
                        </div-->

                        <div class="form-group">
                            <label>&nbsp;</label>
                            <div class="col-sm-6 col-sm-offset-4">
                                <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                            </div>
                        </div>

                        <div class="form-group margin-bottom-2">
                             <label>&nbsp;</label>
                            <div class="col-sm-6 col-sm-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                                 @include('partials.socials')  
                            </div>
                        </div>

                        
                        

                    {!! Form::close() !!}
                      </div>
                  </div>
                  
              </div>
          </div>
      </div>

  </div>
  <div id="login-overlay" class="two columns">&nbsp;</div>
    
</div>
</div>
<style>
.modal-dialog
{
    margin: 30px auto;
    
}
.modal-dialog {
    position: relative;
    
}
.modal-content
{
    /*box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);*/
    
}
.modal-content {
    background-clip: padding-box;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.2);
    border-radius: 6px;
    outline: 0 none;
    position: relative;
    margin-bottom: 20px;
}
.modal-header {
    border-bottom: 1px solid #e5e5e5;
    min-height: 16.43px;
    padding: 15px;
}
.well {
    background-color: #f5f5f5;
    border: 1px solid #e3e3e3;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05) inset;
    /*margin-bottom: 20px;
    margin-top: 20px;*/
    margin:30px;
    min-height: 20px;
    padding: 19px;
}
.lead {
    font-size: 21px;
}
.lead {
    font-weight: 300;
    line-height: 1.4;
    margin-bottom: 20px;
}
.moredetails
{
    margin: 30px;
}
.container .one.column, .container .one.columns
{
    width:10px!important;
}
</style>
@endsection

@section('footer_scripts')

    <script src='https://www.google.com/recaptcha/api.js'></script>
     

@endsection