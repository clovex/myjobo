@extends('layouts.app')

@section('template_title')
    Testimonials | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<style type="text/css">
    .setpdds > p
    {
        margin: 0px;
    }
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Testimonials</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>testimonials</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    
            @foreach($testimonials as $test)
            <div class="row">        
                <div class="col-md-12" style="padding-bottom: 10px;">
                    <div class="col-md-9">
                        <div class="col-md-12 setpdds" style="padding: 0 5%;"> 
                            <h4>{{ $test->name }}</h4>    
                            <i class="fa fa-quote-left" aria-hidden="true" style="position: absolute; left: 0px;"></i>
                            {!! $test->description !!}
                            <i class="fa fa-quote-right" aria-hidden="true" style="position: absolute; right: 0px; bottom: 5%;"></i>
                            <label>{{ $test->company }} , {{ $test->position }}</label> 
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">
                            <img src="{{ URL::to('public/uploads') }}/{{ $test->photo }}" style="width: 75%; margin: 0px auto;">    
                        </div>
                    </div> 
                </div>
                <hr>
            </div>    
            @endforeach
    
<div class="margin-bottom-40"></div>
</div>
@endsection