@extends('layouts.app')

@section('template_title')
    Privacy Policy | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Privacy Policy</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Privacy Policy</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            @foreach($pages as $p)
                {!! $p->contents !!}
            @endforeach
        </div>
    </div>
<div class="margin-bottom-40"></div>
</div>
@endsection