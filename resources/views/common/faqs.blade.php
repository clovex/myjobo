@extends('layouts.app')

@section('template_title')
    Help | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.faqs').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<!--<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />-->

    
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
    .faqHeader {
        font-size: 27px;
        margin: 20px;
    }
    

    .panel-heading [data-toggle="collapse"]:after {
       font-family: FontAwesome;
       content: "\f0da";
        float: right;
        /*color: #F58723;*/
       	color: #454444;
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }
    
    .panel-collapse
    {
    	background-color: #F5F7FA;
	    border: 1px solid transparent;
	    border-radius: 4px;
	    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05)!important;
	    margin-bottom: 20px;
	}
</style>
<div class="clearfix"></div>
<!--div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Frequently Asked Questions</h2>
            <h2>Help</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="">Home</a></li>
                    <li>help</li>
                </ul>
            </nav>
        </div>
    </div>
</div-->

<div class="container">

    <!--<div class="sixteen columns">
        @foreach($faqs as $f)
        <h4>{{ $f->title }}</h4>
        {!! $f->description !!}
        @endforeach
        <div class="margin-bottom-40"></div>
    </div>-->
    <div class="panel-group" id="accordion">
    	<style>
    		.topics-list ul li
    		{
    			box-sizing: border-box;
    			list-style: outside none none;
			    margin: 15px auto 0;
			    padding-left: 0;
			    padding-right: 15px;
			    position: relative;
			     width: 16.6667%;
			     float: left;
   
    		}
    		.topics-list ul li a
    		{
    			background: #f7f7f7 none repeat scroll 0 0;
			    border: 1px solid #d8e0eb;
			    border-radius: 4px;
			    color: #888888;
			    display: block;
			    font-size: 1rem!important;
			    font-weight: 600;
			    line-height: 24px;
			    margin: 0 auto;
			    min-height: 108px;
			    padding: 30px 7.5px 0;
			    text-align: center;
			    
    		}
    		.accordion-toggle
    		{
    			color:#0079d6!important;
    		}
    		hr
    		{
    			margin:0px!important;
    		}
    		.panel-group .panel-heading + .panel-collapse > .list-group, .panel-group .panel-heading + .panel-collapse > .panel-body
    		{
    			border: none!important;
    		}
    		.panel
    		{
    			box-shadow: none!important;
    		}
    	</style>
    	<section class="topics-list">
    		<h2>Help Topics</h2>
    	<ul class="top-list clearfix"> 
    		@foreach($faqscategory as $ff)           
            <li>
				<a class="registration" href="#{!! strtolower(str_replace(' ', '-',(preg_replace('/[^A-Za-z0-9\-]/', '',$ff->title)))) !!}">
                	<i class="fa {{ $ff->icon }}" aria-hidden="true" style="font-size: 22px;"></i><br/>
                    {{ $ff->title }}
                </a>
           	</li>
           	
            @endforeach
        </ul>
        </section>
        
        @foreach($faqscategoryall as $fhed)
        	<div class="faqHeader" id="{!! strtolower(str_replace(' ', '-',(preg_replace('/[^A-Za-z0-9\-]/', '',$fhed->title)))) !!}">{{ $fhed->title }}</div>
        	@foreach($faqs as $f)
        		@if($f->cat_id==$fhed->id)
        		
	        	<div class="panel">
		        	<div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne{!! strtolower(str_replace(' ', '-',(preg_replace('/[^A-Za-z0-9\-]/', '',$f->title)))) !!}">{{ $f->title }}</a>
		                </h4>
		               
		            </div>
		            <hr/>
		            <div id="collapseOne{!! strtolower(str_replace(' ', '-',(preg_replace('/[^A-Za-z0-9\-]/', '',$f->title)))) !!}" class="panel-collapse collapse">
		                <div class="panel-body">
		                	<!--<?php $dess=$f->description;
		                	echo html_entity_decode($dess, ENT_QUOTES, 'UTF-8');
		                    ?>-->
		                    {!! $f->description !!}
		                </div>
		            </div>
	       		</div>
	       		@endif
           @endforeach 
        @endforeach
        
        
        
    	<!--<div class="faqHeader">General questions</div>
    	 
        @foreach($faqs as $f)
        
        <div class="panel panel-default">
        	<div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">{{ $f->title }}</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    {!! $f->description !!}
                </div>
            </div>
       	</div>
           @endforeach       
    </div>-->
</div>
    

</div>

<script>
    $(function() {
        $('.registration').click(function() {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.substr(1) +']');
            if (target.length) {
				$('html,body').animate({
				  scrollTop: target.offset().top
				}, 1000);
				return false;
            }
        });
    });
</script>
@endsection