@extends('layouts.app')

@section('template_title')
    Recruiter resources | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<style type="text/css">
    .setstylebox
    {
        padding: 10% 15%; 
        border: 1px solid #e0e0e0; 
        box-shadow: 0 0 0 2px transparent, 1px 1px 6px rgba(0, 0, 0, 0.2);
        cursor: pointer;
    }
    .setstylebox:hover
    {
        border: 1px solid #26ae61; 
    }
    .setborderrec
    {
       border-top: 1px dashed #26ae61;  
       border-bottom: 1px dashed #26ae61;  
       color: #979482;
    }
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Recruiter resources</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Recruiter resources</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 text-center">
                <div class="col-md-1 col-lg-1 col-sm-1">
                &nbsp;
                </div>
                <div class="col-md-10 col-lg-10 col-sm-10">
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <div class="col-md-12 col-lg-12 col-sm-12 setstylebox">
                        <span class="ln ln-icon-User" style="font-size: 50px !important;"></span>
                        <h4>Getting started</h4>
                        <p>Everything you need to know about the purchase process, how to choose the right site for your organization, and how to get started!</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <div class="col-md-12 col-lg-12 col-sm-12 setstylebox">
                        <span class="ln ln-icon-Money-Bag" style="font-size: 50px !important;"></span>
                        <h4>Editing your site</h4>
                        <p>It's important to organize all your content to make it easy to find and beautiful. We'd love to share some great tips to make your site amazing.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-1">
                &nbsp;
                </div>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-12 col-lg-12 col-sm-12 text-center">
                <div class="col-md-1 col-lg-1 col-sm-1">
                &nbsp;
                </div>
                <div class="col-md-10 col-lg-10 col-sm-10">
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <div class="col-md-12 col-lg-12 col-sm-12 setstylebox">
                        <span class="ln ln-icon-Timer-2" style="font-size: 50px !important;"></span>
                        <h4>Designing Your Site</h4>
                        <p>Everything you need to know about the purchase process, how to choose the right site for your organization, and how to get started!</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <div class="col-md-12 col-lg-12 col-sm-12 setstylebox">
                        <span class="ln ln-icon-Lock-2" style="font-size: 50px !important;"></span>
                        <h4>Launching Your Site</h4>
                        <p>It's important to organize all your content to make it easy to find and beautiful. We'd love to share some great tips to make your site amazing.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-1">
                &nbsp;
                </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 text-center">
            <h3>All help center topics</h3>
            <hr>
        </div> 
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-4">
                <h3 class="setborderrec">Getting Started</h3>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-4">
                <h3 class="setborderrec">Editing Your Site</h3>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-4">
                <h3 class="setborderrec">Designing Your Site</h3>
            </div>
            </div>
            <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-4">
                <h3 class="setborderrec">Launching Your Site</h3>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-4">
                <h3 class="setborderrec">Transitioning</h3>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-4">
                <h3 class="setborderrec">Editing Sections</h3>
            </div>
            </div>

        </div>

    </div>

<div class="margin-bottom-40"></div>
</div>
@endsection