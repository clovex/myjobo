@extends('layouts.app')

@section('template_title')
    Learning Resources | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.career').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<!--script type="text/javascript">
    $(document).ready(function(){
        $("#eduselect2").hide();
        $("#carselect").hide();
        $(".careers").hide();
        
        $("#carselect2").click(function(){
            $("#carselect2").hide();
            $("#carselect").show();
            $("#eduselect").hide();
            $("#eduselect2").show();
            $(".educational").hide();
            $(".careers").slideToggle("slow");

        });
        
        $("#eduselect2").click(function(){
            $("#eduselect2").hide();
            $("#eduselect").show();
            $("#carselect").hide();
            $("#carselect2").show();
            $(".careers").hide();
            $(".educational").slideToggle("slow");
        });
    });
</script-->    
<style type="text/css">
    #eduselect2
    {
        margin-top: 0px !important;
    }
    #carselect2
    {
        margin-top: 0px !important;   
    }
    .searbtn
    {
        background-color: #F45B06 !important; 
        color: #fff !important; 
        height: 45px !important; 
        line-height: 20px !important;
    }
    .seth3
    {
        text-align: center; 
        height: 250px; 
        color: #fff; 
        font-weight: bolder;
        padding-top: 40%;
        line-height: 25px;
    }
    .sethovers
    {
        background-color: #26ae61;
        color: #26ae61;
        height: 250px; 
        border-radius: 5px;
        background-size: cover !important;
    }
    .sethovers:hover h3
    {
        opacity: 0.80;
        background-color: #26ae61;
        border-radius: 5px;
    }
</style>
<div class="clearfix"></div>
<!--div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Learning Resources</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="">Home</a></li>
                    <li>Learning Resources</li>
                </ul>
            </nav>
        </div>
    </div>
</div-->
<!--div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
             <p class="text-center">Browse through information, advice, tools and resources on careers and education for Parents, Teachers, Students and Toddlers. Feel free to send us any suggestions by clicking <a href="#table-form">Here</a></p>
        </div> 
    </div> 
</div-->
<!--
<div id="banner" style="background-image: url({{ URL::to('frontend/images/banner-01.jpg') }}); margin-bottom: 0px;" class="parallax background" data-img-width="2000" data-img-height="1330" data-diff="400">
-->
    <div class="container" style="padding:50px 0px;">
        
        <div class="col-md-12 col-sm-12 col-lg-12">    
            <h2 style="font-size: 46px; letter-spacing: -1px; margin-bottom: 20px; text-align: center;">Find your course. Transform your life
            </h2>
            <hr>
            <div class="col-lg-1 col-md-1 col-sm-1">&nbsp;</div>
            <form method="post" enctype="multipart/form-data" action="{{ URL::to('searchevents') }}">
            {{ csrf_field() }}
            <div class="col-lg-8 col-md-8 col-sm-8">
                <input type="text" class="ico-01" name="keyword" style="height:45px; padding: 6px 12px;" placeholder="Subject or qualification, eg. IT"/>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <button type="submit" class="searbtn btn-block">Search</button>
            </div>
            </form>
            <div class="col-lg-1 col-md-1 col-sm-1">&nbsp;</div>
        </div>
    </div>
<!--</div>-->

<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <h3 class="text-center">Learning Events and Opportunities</h3>
            <hr>

            <div class="row" style="margin-bottom: 10px;">
                @php $l = 1; @endphp
                @foreach($coursescategory as $cc)
                    <div class="col-lg-3 col-md-3 col-sm-3">
                    <a href="{{ URL::to('events') }}/{{ $cc->id }}">
                    <div class="sethovers" style="background: #26ae61 url({{ URL::to('public/uploads/')}}/{{ $cc->image }});">
                            <h3 class="seth3">{{ $cc->name }}</h3>
                        </div>
                    </a>    
                    </div>  
                @if($l%4 == 0)
                    </div><div class="row" style="margin-bottom: 10px;">
                @endif    
                @php $l++ @endphp
                @endforeach
            </div>    
    
            <h3 class="text-center">Educational Resources</h3>
            <hr>
            <div class="educational"> 
                <div class="row" style="margin-bottom: 10px;">
                @php $i = 1; @endphp
                @foreach($careers as $carr)
                @if($carr->type == 1)
                    <div class="col-md-3 col-lg-3 col-sm-3">
                    <div class="col-md-12 col-lg-12 col-sm-12" style="border-bottom: 1px solid #e0e0e0; padding: 0px;">
                    <a href="{{ URL::to('educational') }}/{{ $carr->slug }}">{{ $carr-> title }}</a>
                    </div> 
                    </div>
                @if($i%4 == 0)
                    </div><div class="row" style="margin-bottom: 10px;">
                @endif    
                @php $i++ @endphp
                @endif
                @endforeach

                @foreach($schooltype as $schtype)
                    <div class="col-md-3 col-lg-3 col-sm-3">
                    <div class="col-md-12 col-lg-12 col-sm-12" style="border-bottom: 1px solid #e0e0e0; padding: 0px;">
                    <a href="{{ URL::to('schools') }}/{{ $schtype->slug }}">{{ $schtype-> name }}</a>
                    </div>
                    </div>
                @if($i%4 == 0)
                    </div><div class="row" style="margin-bottom: 10px;">
                @endif    
                @php $i++ @endphp
                @endforeach

                </div>        
            </div>
            
            

           
            <h3 class="text-center">Careers</h3>
            <hr>
            <div class="careers">
               <div class="row" style="margin-bottom: 10px;">
                @php $j = 1; @endphp
                @foreach($careers as $car)
                @if($car->type == 0)
                    <div class="col-md-3 col-lg-3 col-sm-3">
                    <div class="col-md-12 col-lg-12 col-sm-12" style="border-bottom: 1px solid #e0e0e0; padding: 0px;">
                        <a href="{{ URL::to('career') }}/{{ $car->slug }}">{{ $car-> title }}</a>
                    </div>    
                    </div>
                @if($j%4 == 0)
                    </div><div class="row" style="margin-bottom: 10px;">
                @endif    
                @php $j++ @endphp
                @endif
                @endforeach
                </div>
            </div>
            

        </div>


        <!--div class="col-md-4">
            <div class="widget"> 
            <h4>Newly Listed Schools</h4>
            <div class="widget-box">
                <ul class="footer-links datasetul">
                    @foreach($school as $sc)
                    <li style="line-height: 23px;">
                        <a href="{{ URL::to('school') }}/{{ $sc->slug }}">{{ $sc->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            </div>

            <div class="widget text-center">
                <div class="widget-box" style="text-align: center;">
                    <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
                </div>
            </div>
        </div-->
    </div>

    <div class="row"> 
        <hr>
        <div class="col-md-6 col-lg-6 col-sm-6" id="table-form">

            <div class="col-md-11 col-lg-11 col-sm-11" style="padding: 0px;">
                <h4 class="text-center plus1" style="cursor: pointer;">
                    List your Event, School, College or Resource Below
                </h4>
                <h4 class="text-center minus1" style="cursor: pointer;">
                    List your Event, School, College or Resource Below
                </h4>
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1">
                <a class="btn btn-success plus1">
                    <i class="fa fa-plus" aria-hidden="true" style="font-size: 15px"> </i>
                </a>
                <a class="btn btn-danger minus1">
                    <i class="fa fa-minus" aria-hidden="true" style="font-size: 15px"> </i>
                </a>
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12" style="padding: 0px"><hr></div>

            <form method="post" enctype="multipart/form-data" name="contactform" id="contactform" action="{{ URL::to('requiestsend') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>School Name:</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>  

                <div class="form-group">
                    <label>Name of Principal or Headmaster:</label>
                    <input type="text" name="principal" class="form-control" required>
                </div>  

                <div class="form-group">
                    <label>Phone:</label>
                    <input name="phone" type="text" id="phone" class="form-control" required/>
                </div>

                <div class="form-group">
                    <label >Email:</label>
                    <input name="email" type="email" id="email" class="form-control" required/>
                </div>

                <div class="form-group">
                    <label>Website:</label>
                    <input name="website" type="url" id="website" class="form-control" required/>
                </div>


                <div class="form-group">
                    <label>Comments:</label>
                    <textarea name="comment" class="form-control" id="comment" spellcheck="true"></textarea>
                </div>

                <div class="form-group">
                    <label>Company Logo:</label>
                    <input type="file" name="logo" class="form-control">
                </div>

                <div class="form-group">
                    <label>&nbsp;</label>
                    <input type="submit" id="submit" class="btn btn-success" value="Submit" />
                </div>
            </form>

        </div>

        <div class="col-md-6 col-lg-6 col-sm-6">
            
            <div class="col-md-11 col-lg-11 col-sm-11" style="padding: 0px;">
                <h4 class="text-center plus2" style="cursor: pointer;">Courses, Events and Conferences</h4>
                <h4 class="text-center minus2" style="cursor: pointer;">Courses, Events and Conferences</h4>
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1">
                <a class="btn btn-success plus2">
                    <i class="fa fa-plus" aria-hidden="true" style="font-size: 15px"> </i>
                </a>
                <a class="btn btn-danger minus2">
                    <i class="fa fa-minus" aria-hidden="true" style="font-size: 15px"> </i>
                </a>
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12" style="padding: 0px"><hr></div>

            <div class="col-md-12 col-lg-12 col-sm-12" id="condata">
            @foreach($courses as $cou)
                {!! $cou->contents !!}
            @endforeach
            </div>    
        </div>

    </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".minus1").hide();
        $("#contactform").hide();
        $(".plus1").click(function(){
            $(".plus1").hide();
            $(".minus1").show();
            $("#contactform").slideToggle("slow");
        });
        $(".minus1").click(function(){
            $(".minus1").hide();
            $(".plus1").show();
            $("#contactform").slideToggle("slow");
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".minus2").hide();
        $("#condata").hide();
        $(".plus2").click(function(){
            $(".plus2").hide();
            $(".minus2").show();
            $("#condata").slideToggle("slow");
        });
        $(".minus2").click(function(){
            $(".minus2").hide();
            $(".plus2").show();
            $("#condata").slideToggle("slow");
        });
    });
</script>
<style type="text/css">
    .form-control
    {
        padding: 6px 12px !important;
    }
</style>
@endsection