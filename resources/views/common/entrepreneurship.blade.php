@extends('layouts.app')

@section('template_title')
    Entrepreneurship | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.entrepreneurship').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Entrepreneurship</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Entrepreneurship</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8">

            <h4 class="text-center">Listed Businesses, Enterprises and Charitable Organisation</h4>
            <p class="text-center">List your business in our Online Directory so that customers and clients can find you easily. List your business by clicking <a href="#table-form">Here</a></p>

            <div class="row">
                @php $i = 1; @endphp
                @foreach($entrep as $en)
                    <div class="col-md-4">
                        <a href="{{ URL::to('entrepreneurship') }}/{{ $en->slug }}">{{ $en-> title }}</a>
                    </div>
                @if($i%3 == 0)
                    </div><div class="row">
                @endif    
                @php $i++ @endphp    
                @endforeach   
            </div>    

            

        </div>


        <div class="col-md-4"> 
            <h4 class="text-center">Ideas, Innovations & Partnerships</h4>
            <p class="text-center">Pitch your business ideas; Seek collaborators and funding. You can post your pitch <a href="#table-form">Here</a></p>

            <div class="widget">
                <div class="widget-box" style="text-align: center;">
                    @foreach($enter as $en)
                    <a href="{{ json_decode($en->contents)->link }}">
                        <img src="{{ URL::to('public/uploads') }}/{{ json_decode($en->contents)->image }}" style="width: 100%">
                    </a>
                    @endforeach
                </div>
            </div>

            <div class="widget text-center">
                <div class="widget-box" style="text-align: center;">
                    <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row"> 
        <hr>
        <div class="col-md-6" id="table-form">

            <div class="col-md-11" style="padding: 0px;">
            <h4 class="text-center plus1" style="cursor: pointer;">List Your Business, Enterprise and Charitable Organisation</h4>
            <h4 class="text-center minus1" style="cursor: pointer;">List Your Business, Enterprise and Charitable Organisation</h4>
            </div>
            <div class="col-md-1">
                <a class="btn btn-success plus1">
                    <i class="fa fa-plus" aria-hidden="true" style="font-size: 15px"> </i>
                </a>
                <a class="btn btn-danger minus1">
                    <i class="fa fa-minus" aria-hidden="true" style="font-size: 15px"> </i>
                </a>
            </div>
            <div class="col-md-12" style="padding: 0px"><hr></div>
            
            <form method="post" name="contactform" id="contactform">
                <div class="form-group">
                    <label>Company Name:</label>
                    <input name="company_name" type="text" id="company_name" class="form-control" />
                </div>

                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>    

                <div class="form-group">
                    <label>Phone:</label>
                    <input name="phone" type="text" id="phone" class="form-control" />
                </div>

                <div class="form-group">
                    <label >Email:</label>
                    <input name="email" type="email" id="email" class="form-control" />
                </div>

                <div class="form-group">
                    <label>Website:</label>
                    <input name="website" type="url" id="website" class="form-control" />
                </div>


                <div class="form-group">
                    <label>Business Details:</label>
                    <textarea name="comment" class="form-control" id="comment" spellcheck="true"></textarea>
                </div>

                <div class="form-group">
                    <label>Company Logo:</label>
                    <input type="file" name="file" class="form-control">
                </div>

                <div class="form-group">
                    <label>&nbsp;</label>
                    <input type="submit" id="submit" class="btn btn-success" value="Submit" />
                </div>
            </form>

        </div>

        <div class="col-md-6">

            <div class="col-md-11" style="padding: 0px;">
            <h4 class="text-center plus2" style="cursor: pointer;">Events and Exhibitions</h4>
            <h4 class="text-center minus2" style="cursor: pointer;">Events and Exhibitions</h4>
            </div>
            <div class="col-md-1">
                <a class="btn btn-success plus2">
                    <i class="fa fa-plus" aria-hidden="true" style="font-size: 15px"> </i>
                </a>
                <a class="btn btn-danger minus2">
                    <i class="fa fa-minus" aria-hidden="true" style="font-size: 15px"> </i>
                </a>
            </div>
            <div class="col-md-12" style="padding: 0px"><hr></div>

            <div class="col-md-12" id="condata">
               @foreach($contant as $con)
                    {!! $con->contents !!}
               @endforeach
            </div>

        </div>

    </div>

</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".minus1").hide();
        $("#contactform").hide();
        $(".plus1").click(function(){
            $(".plus1").hide();
            $(".minus1").show();
            $("#contactform").slideToggle("slow");
        });
        $(".minus1").click(function(){
            $(".minus1").hide();
            $(".plus1").show();
            $("#contactform").slideToggle("slow");
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".minus2").hide();
        $("#condata").hide();
        $(".plus2").click(function(){
            $(".plus2").hide();
            $(".minus2").show();
            $("#condata").slideToggle("slow");
        });
        $(".minus2").click(function(){
            $(".minus2").hide();
            $(".plus2").show();
            $("#condata").slideToggle("slow");
        });
    });
</script>

@endsection