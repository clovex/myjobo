@extends('layouts.app')

@section('template_title')
    Sitemap | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Sitemap</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Sitemap</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-left: 5%;">
            <ul>
                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('') }}">Home</a>
                </li>
                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('jobs') }}">Jobs</a>
                </li>
                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('services') }}">Services</a>
                </li>
                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('careers') }}">Learning Resources</a>
                </li>
                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('partners') }}">Partners</a>
                </li>
                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('blog') }}">Career Resources</a>
                </li>
                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('faqs') }}">FAQs</a>
                </li>
                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('about_us') }}">About Us</a>
                </li>
                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('contact_us') }}">Contact Us</a>
                </li>
                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('testimonials') }}">Testimonials</a>
                </li>

                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('terms_of_use') }}">Terms of Service</a>
                </li>

                <li class="setlistyle">
                    <i class="fa fa-arrow-circle-o-right setstylei" aria-hidden="true"> </i>
                    <a class="sizeanchr" href="{{ URL::to('privacy_policy') }}">Privacy Policy</a>
                </li>
            </ul>
        </div>
    </div>
<div class="margin-bottom-40"></div>
</div>
<style type="text/css">
    .sizeanchr
    {
        font-size: 16px !important;
    }
    .setstylei
    {
        font-size: 16px; 
        color: #F36510;   
    }
    .setlistyle
    {
        line-height: 22px !important;
    }
</style>
@endsection