@extends('layouts.app')

@section('template_title')
    Advertise a Job & Hire staff | Myjobo.com
@endsection
@section('template_description')
Recruiting in Malawi? Advertise a Job & Hire staff on Myjobo.com. Join hundreds of employers that use Myjobo.com every day.
@endsection
@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.services').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<style type="text/css">
    .setmarhinser>p
    {
        margin: 0px;
    }
    #titlebar span a, #titlebar span
    {
        color:none !important
    }
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h1>Services</h1> 
            <h4>Making Hiring Easier, Simpler, Faster and Cheaper
                <strong class="talk_to_us" style="float: right;">Questions? Contact Us: <a href="#">info@myjobo.com</a></strong>
            </h4>
            <!--<nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Services</li>
                </ul>
            </nav>-->
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <!--<div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 setmarhinser">
            @foreach($s1 as $s11)
                {!! $s11->contents !!}
            @endforeach
            </div>
            </div>-->

            <!--<div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <img src="{{ URL::to('frontend/images/ValidiHire.png') }}" alt="" style="height: 30px; margin:0px auto;" />
                </div>
            </div>-->
            
            <div class="row">
            <div class="col-md-12">
            @foreach($type as $t)
            <h1 class="text-center">{{ $t->name }}</h1>
            <hr>
                <div class="row"> 
                @foreach($services as $ser1)
                @if($ser1->type == $t->id)            
                <div class="col-md-4 col-lg-4 col-sm-4" style="margin-bottom: 20px;">
                    <div class="col-lg-12 col-md-12 col-sm-12" style="border:1px solid #e0e0e0; padding-bottom: 10px; min-height: 250px;">
                        <h2 style="border-bottom: 1px solid #e0e0e0; color: #494949;"><strong>{{ $ser1->title }}</strong></h2>
                        <br>
                        <p>{!! substr(strip_tags($ser1->description),0,100) !!}...</p>

                        <a class="button widget-btn" style="padding: 5px 10px;" href="{{ URL::to('employers')}}/{{ $ser1->slug }}">Find out more</a>
                    </div>
                </div>
                @endif
                @endforeach
                </div>
            @endforeach    

            </div>
            </div>  
        </div>    
    
        
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 text-center">
            <a href="{{ URL::to('testimonials') }}"><h3 style="color: #F45B06;">View examples of Success Stories Here</h3></a>
            @foreach($s6 as $s66)
                <img src="{{ URL::to('public/uploads')}}/{{ $s66->contents }}" style="width: 100px; margin: 0px auto;">
            @endforeach
        </div>
    </div>
</div>


<div id="titlebar" style="margin-bottom: 0px; padding: 0px;">
    <div class="container">
    <div class="col-md-12 col-lg-12 col-sm-12 text-center">
        
        <div class="row">
            <h1>Why Myjobo.com</h1>
            <p style="margin:0px;">&nbsp;</p>
            <div class="col-md-3 col-lg-3 col-sm-3">
                <span class="ln ln-icon-User" style="font-size: 50px !important;"></span>
                @foreach($s2 as $s22)    
                <h4>{{ json_decode($s22->contents)->title }}</h4>
                <p>{{ json_decode($s22->contents)->dec }}</p>
                @endforeach
            </div>

            <div class="col-md-3 col-lg-3 col-sm-3">
                <span class="ln ln-icon-Money-Bag" style="font-size: 50px !important;"></span>        
                @foreach($s3 as $s33)    
                <h4>{{ json_decode($s33->contents)->title }}</h4>
                <p>{{ json_decode($s33->contents)->dec }}</p>
                @endforeach
            </div>

            <div class="col-md-3 col-lg-3 col-sm-3">
                <span class="ln ln-icon-Timer-2" style="font-size: 50px !important;"></span>        
                @foreach($s4 as $s44)    
                <h4>{{ json_decode($s44->contents)->title }}</h4>
                <p>{{ json_decode($s44->contents)->dec }}</p>
                @endforeach
            </div>

            <div class="col-md-3 col-lg-3 col-sm-3">
                <span class="ln ln-icon-Lock-2" style="font-size: 50px !important;"></span>        
                @foreach($s5 as $s55)    
                <h4>{{ json_decode($s55->contents)->title }}</h4>
                <p>{{ json_decode($s55->contents)->dec }}</p>
                @endforeach
            </div>

        </div>
    </div>
</div>
</div>
    
<div class="container">    
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <h4 class="text-center">As featured in:</h4>
            <div class="one carousel column">
                <div id="showbiz_left_2" class="sb-navigation-left-2">
                    <i class="fa fa-angle-left"></i>
                </div>
            </div>
            <div id="our-clients" class="showbiz-container fourteen carousel columns" >
                <div class="showbiz our-clients" data-left="#showbiz_left_2" data-right="#showbiz_right_2">
                <div class="overflowholder">
                    <ul>
                        @foreach($featured as $featured11)
                        <li><a href="{{ $featured11->link }}" target="_blank">
                            <img src="{{ URL::to('public/uploads') }}/{{ $featured11->image }}" alt="" />
                        </a></li>
                    @endforeach  
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                </div>
            </div>
            <div class="one carousel column">
                <div id="showbiz_right_2" class="sb-navigation-right-2">
                    <i class="fa fa-angle-right"></i>
                </div>
            </div>
        </div>
        <div class="margin-bottom-40"></div>
    </div>    
</div>
@endsection