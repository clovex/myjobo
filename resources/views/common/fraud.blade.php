@extends('layouts.app')

@section('template_title')
    Fraud and Security | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Fraud and Security</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="">Home</a></li>
                    <li>Fraud and Security</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
        @foreach($pages as $c)
            {!! $c->contents !!}
        @endforeach
        </div>
    </div>
</div>
@endsection