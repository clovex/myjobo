@extends('layouts.app')

@section('template_title')
    About Us | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.about_us').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div>
	<h1 style="text-align: center;">About Us</h1>
</div>
<!--<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>About Us</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="">Home</a></li>
                    <li>About Us</li>
                </ul>
            </nav>
        </div>
    </div>
</div>-->

<div class="container">
    <div class="sixteen columns text-center">
        <div class="padding-right">
            @foreach($about1 as $a1)
                {!! $a1->contents !!}
            @endforeach        
        </div>    
    </div>
</div>    
    <!--div class="five columns blog">
        
        <div class="widget">
            <h4 style="color: #494949;">Are you an Employer?</h4>
            <div class="widget-box">
                <p>Advertise Jobs, It's Free</p>
                <a href="{{ URL::to('register_employer') }}" class="button widget-btn"><i class="fa fa-user-secret" aria-hidden="true"></i> Click Here</a>
            </div>
        </div>

        <div class="widget">
            <h4 style="color: #494949;">Are you a Job Seeker?</h4>
            <div class="widget-box">
                <p>Upload your CV, It's Free</p>
                <a href="{{ URL::to('register_job_seeker') }}" class="button widget-btn"><i class="fa fa-user" aria-hidden="true"></i> Click Here</a>
            </div>
        </div>

        <div class="widget text-center">
            <div class="widget-box" style="text-align: center;">
                <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
            </div>
        </div>
        
    </div-->
<!--<div id="titlebar" style="margin-bottom: 0px; padding: 0px;">
<div class="container">
    <div class="sixteen columns" style="text-align: center;">
        <p>&nbsp;</p>
        <h1>Why you should Advertise Your Jobs with Myjobo.com</h1>
        <p>&nbsp;</p>
        <div class="container">

            <div class="four columns">
                <span class="ln ln-icon-Checked-User" style="font-size: 50px !important;"></span>
                @foreach($about9 as $a9)
                <h4>{{ json_decode($a9->contents)->title }}</h4>
                <p>{{ json_decode($a9->contents)->dec }}</p>
                @endforeach
            </div>

            <div class="four columns">
                <span class="ln ln-icon-Three-FingersTouch" style="font-size: 50px !important;"></span>        
                 @foreach($about10 as $a10)
                <h4>{{ json_decode($a10->contents)->title }}</h4>
                <p>{{ json_decode($a10->contents)->dec }}</p>
                @endforeach
            </div>

            <div class="four columns">
                <span class="ln ln-icon-Security-Settings" style="font-size: 50px !important;"></span>        
                 @foreach($about11 as $a11)
                <h4>{{ json_decode($a11->contents)->title }}</h4>
                <p>{{ json_decode($a11->contents)->dec }}</p>
                @endforeach
            </div>

            <div class="four columns">
                <span class="ln ln-icon-Key-Lock" style="font-size: 50px !important;"></span>        
                 @foreach($about12 as $a12)
                <h4>{{ json_decode($a12->contents)->title }}</h4>
                <p>{{ json_decode($a12->contents)->dec }}</p>
                @endforeach
            </div>

        </div>
    </div>
</div>
</div>-->
<hr/>
<div class="container">
    <div class="sixteen columns">
        <h4 class="text-center">As featured in:</h4>
        <div class="one carousel column"><div id="showbiz_left_2" class="sb-navigation-left-2"><i class="fa fa-angle-left"></i></div></div>
        <div id="our-clients" class="showbiz-container fourteen carousel columns" >

        <div class="showbiz our-clients" data-left="#showbiz_left_2" data-right="#showbiz_right_2">
            <div class="overflowholder">

                <ul>
                    @foreach($about13 as $a13)
                    <li><a href="{{ $a13->link }}" target="_blank">
                        <img src="{{ URL::to('public/uploads') }}/{{ $a13->image }}" alt="" />
                    </a></li>
                    @endforeach  
                </ul>
                <div class="clearfix"></div>

            </div>
            <div class="clearfix"></div>

        </div>
        </div>

        <!-- Navigation / Right -->
        <div class="one carousel column"><div id="showbiz_right_2" class="sb-navigation-right-2"><i class="fa fa-angle-right"></i></div></div>

    </div>

    <div class="margin-bottom-40"></div>

</div>
@endsection