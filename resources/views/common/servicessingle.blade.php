@extends('layouts.app')

@section('template_title')
    Services | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.services').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            @foreach($services as $s)
            <h2>{{ $s->title }}</h2>
            <!--nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('services') }}">Services</a></li>
                    <li>{{ $s->title }}</li>
                </ul>
            </nav-->
            @endforeach
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-8">
            
                @foreach($services as $ser1)
                @php $i = 1; @endphp
                    <!--div class="row">
                    @if($ser1->type == 0)
                        <img src="{{ URL::to('frontend/images/ValidiHire.png') }}" alt="" style="height: 30px;" />
                    @else
                        <img src="{{ URL::to('frontend/images/ValidiChecks.jpg') }}" alt="" style="height: 30px;" />
                    @endif 
                    </div-->   
                     <div class="row">
                        {!! $ser1->description !!}    
                    </div>
                @endforeach
        </div>    
    
        <div class="col-md-4 col-lg-4 col-sm-4">
            <div class="widget">
            <h4 style="color: #494949;">Are you Recruiting?</h4>
                <div class="widget-box">
                    <p>Advertise jobs now.</p>
                    <a href="{{ URL::to('register_employer') }}" class="button widget-btn"><i class="fa fa-user-secret" aria-hidden="true"></i> Click Here</a>
                </div>
            </div>

            <div class="widget">
                <h4 style="color: #494949;">Are you a Job Seeker?</h4>
                <div class="widget-box">
                    <p>Upload your CV, It's Free</p>
                    <a href="{{ URL::to('register_job_seeker') }}" class="button widget-btn"><i class="fa fa-user" aria-hidden="true"></i> Click Here</a>
                </div>
            </div>

            <div class="widget text-center">
                <div class="widget-box" style="text-align: center;">
                    <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Our Activities</a>
                </div>
            </div>
        </div>    
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 text-center">
            <a href="{{ URL::to('testimonials') }}"><h3 style="color: #F45B06;">View examples of Success Stories Here</h3></a>
            @foreach($s6 as $s66)
                <img src="{{ URL::to('public/uploads')}}/{{ $s66->contents }}" style="width: 100px; margin: 0px auto;">
            @endforeach
        </div>
    </div>
</div>

<div id="titlebar" style="margin-bottom: 0px; padding: 0px;">
    <div class="container">
    <div class="col-md-12 col-lg-12 col-sm-12 text-center">
        
        <div class="row">
            <h1>Why Myjobo.com</h1>
            <div class="col-md-3 col-lg-3 col-sm-3">
                <span class="ln ln-icon-User" style="font-size: 50px !important;"></span>
                @foreach($s2 as $s22)    
                <h4>{{ json_decode($s22->contents)->title }}</h4>
                <p>{{ json_decode($s22->contents)->dec }}</p>
                @endforeach
            </div>

            <div class="col-md-3 col-lg-3 col-sm-3">
                <span class="ln ln-icon-Money-Bag" style="font-size: 50px !important;"></span>        
                @foreach($s3 as $s33)    
                <h4>{{ json_decode($s33->contents)->title }}</h4>
                <p>{{ json_decode($s33->contents)->dec }}</p>
                @endforeach
            </div>

            <div class="col-md-3 col-lg-3 col-sm-3">
                <span class="ln ln-icon-Timer-2" style="font-size: 50px !important;"></span>        
                @foreach($s4 as $s44)    
                <h4>{{ json_decode($s44->contents)->title }}</h4>
                <p>{{ json_decode($s44->contents)->dec }}</p>
                @endforeach
            </div>

            <div class="col-md-3 col-lg-3 col-sm-3">
                <span class="ln ln-icon-Lock-2" style="font-size: 50px !important;"></span>        
                @foreach($s5 as $s55)    
                <h4>{{ json_decode($s55->contents)->title }}</h4>
                <p>{{ json_decode($s55->contents)->dec }}</p>
                @endforeach
            </div>

        </div>
    </div>
</div>
</div>
@endsection