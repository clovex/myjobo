@extends('layouts.app')

@section('template_title')
    Learning Events | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<style type="text/css">
    .setstyle:hover
    {
        background:rgba(38, 174, 97, 0.05);
        border-bottom: 1px solid #26ae61;
        border-top: 1px solid #26ae61;
    }
    .setstyle
    {
        padding-top: 15px;
        padding-bottom: 15px;
        border-bottom: 1px solid #e0e0e0;
        border-top: 1px solid #e0e0e0;
    }
</style>
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.career').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Learning Events and Opportunities</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('careers') }}">Learning Resources</a></li>
                    <li>Learning Events and Opportunities</li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-8">
            @foreach($courses as $cou) 
            <div class="col-md-12 col-lg-12 col-sm-12 setstyle">
                <div class="col-md-3 col-lg-3 col-sm-3">
                    @if($cou->image != null)
                        <img src="{{ URL::to('public/uploads') }}/{{ $cou->image }}" style="width: 100%;">
                    @else
                        <img src="{{ URL::to('public/uploads/nologo.png') }}" style="width: 100%;">
                    @endif    
                </div> 
                <div class="col-md-9 col-lg-9 col-sm-9">    
                <a href="{{ URL::to('events/single') }}/{{ $cou->id }}"><h3>{{ $cou->name }}</h3></a>
                <label><strong>Study method : </strong>{{ $cou->study_method }}</label>
                <label><strong>Duration : </strong>{{ $cou->duration }}</label>
                 {!! substr(strip_tags($cou->description),0,200) !!}...
                <div class="col-lg-12 col-md-12 col-sm-12" style="padding: 0px;">
                <a href="{{ URL::to('events/single') }}/{{ $cou->id }}" style="font-size: 16px !important;padding:0px 15px;">Read more</a>

                <a href="mailto:{{ $cou->email }}" style="font-size: 16px !important; padding:0px 15px;">Enquire now</a>
                </div>
               </div>
            </div> 
            @endforeach
            <div class="pagination-container text-center">
                <nav class="pagination"> 
                    {{ $courses->links() }}
                </nav>
            </div>
        </div>

        <div class="col-md-4 col-lg-4 col-sm-4">
            <div class="widget">
                <h4>Category</h4>
                <div class="widget-box">
                    <ul class="footer-links datasetul">
                        @foreach($coursescategory as $cc)
                        <li>
                            <a href="{{ URL::to('events') }}/{{ $cc->id }}">{{ $cc->name }}</a>
                        </li>
                        @endforeach
                    </ul> 
                </div>    
            </div>

            <div class="widget text-center">
                <div class="widget-box" style="text-align: center;">
                    <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
                </div>
            </div>

        <div class="widget">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                        style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        </div>


    </div>
</div>
@endsection