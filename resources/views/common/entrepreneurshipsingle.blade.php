@extends('layouts.app')

@section('template_title')
    Entrepreneurship | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.entrepreneurship').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            @foreach($data as $d1)
            <h2>{{ $d1->title }}</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('entrepreneurship') }}">Entrepreneurship</a></li>
                    <li>{{ $d1->title }}</li>
                </ul>
            </nav>
            @endforeach
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            @foreach($data as $d2)
                {!! $d2->description !!}
            @endforeach
        </div>
        <div class="col-md-4"> 
            
            <div class="widget">
                <div class="widget-box" style="text-align: center;">
                    @foreach($enter as $en)
                    <a href="{{ json_decode($en->contents)->link }}">
                        <img src="{{ URL::to('public/uploads') }}/{{ json_decode($en->contents)->image }}" style="width: 100%">
                    </a>
                    @endforeach
                </div>
            </div>

            <div class="widget text-center">
                <div class="widget-box" style="text-align: center;">
                    <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <h4>Listed Businesses</h4>
        <hr>
        <div class="row" style="margin-bottom: 10px;">
            @php $i = 1; @endphp
            @foreach($entrep as $en)
                <div class="col-md-3">
                    <a href="{{ URL::to('entrepreneurship') }}/{{ $en->slug }}">{{ $en-> title }}</a>
                </div>
            @if($i%4 == 0)
            </div><div class="row" style="margin-bottom: 10px;">
            @endif    
            @php $i++ @endphp    
            @endforeach   
        </div> 
    </div>    
</div>
@endsection