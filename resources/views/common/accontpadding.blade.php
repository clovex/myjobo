@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Account Activation pending</div>

                <div class="panel-body">
                    <p><strong>If you do not have access your panel, contact Myjobo support using the following contact form: <a href="{{ URL::to('contact_us') }}">Support</a></strong></p>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
