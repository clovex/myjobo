@extends('layouts.app')

@section('template_title')
    Educational | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.career').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            @foreach($career as $c1)
            <h2>{{ $c1->title }}</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('careers') }}">Learning Resources</a></li>
                    <li>{{ $c1->title }}</li>
                </ul>
            </nav>
            @endforeach
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-8">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
            @foreach($career as $c2)
                {!! $c2->description !!}
            @endforeach
        </div>


        <div class="col-md-4 col-lg-4 col-sm-4"> 
            
            <div class="widget"> 
            <h4>Newly Listed Schools</h4>
            <div class="widget-box">
                <ul class="footer-links datasetul">
                    @foreach($school as $sc)
                    <li style="line-height: 23px;">
                        <a href="{{ URL::to('school') }}/{{ $sc->slug }}">{{ $sc->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            </div>

            <div class="widget text-center">
                <div class="widget-box" style="text-align: center;">
                    <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
                </div>
            </div>
            
        </div>
    </div>

    <div class="careers">
        <h4>Occupations</h4>
        <hr>
        <div class="row" style="margin-bottom: 0px;">
            @php $i = 1; @endphp
            @foreach($careers as $carr)
            @if($carr->type == 1)
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <a href="{{ URL::to('educational') }}/{{ $carr->slug }}">{{ $carr-> title }}</a>
                </div>
            @if($i%4 == 0)
                </div><div class="row" style="margin-bottom: 0px;">
            @endif    
            @php $i++ @endphp
            @endif
            @endforeach

            @foreach($schooltype as $schtype)
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <a href="{{ URL::to('schools') }}/{{ $schtype->slug }}">{{ $schtype-> name }}</a>
                </div>
            @if($i%4 == 0)
                </div><div class="row" style="margin-bottom: 0px;">
            @endif    
            @php $i++ @endphp
            @endforeach
        </div> 
    </div>
    <div class="margin-bottom-40"></div>
</div>
@endsection