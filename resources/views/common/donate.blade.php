@extends('layouts.app')

@section('template_title')
    Donate | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection


@section('content')
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Get involved and support us</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Donate</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        
        <div class="col-md-12 text-center">
        <p>We would like you to get involved in ensuring Myjobo.com attain its goals. We are a social enterprise with big ideas but with limited resources. We are a start up and developing channels to sustain our efforts. At this stage, we need your support to set a foundation for a sustainable profitable future. Please support our efforts.</p>
        <hr>
        </div>

        <div class="col-md-6 text-center">
            <h4>Sponsor us</h4>
            <p>We could not have been at this stage in our mission had it not been for the generous support from our sponsors. The work has just started and our needs are massive. We need further sponsorship to achieve our goals of providing free subsidised services to the over 130,000 monthly Myjobo.com visitors. You can sponsor our website or a particular activity or particular features of our website. We will acknowledge your support by putting your logo and company details on our homepage slide.</p>

            <a href="mailto:info@myjobo.com" class="btn btn-success">Sponsor us</a>
        </div>

        <div class="col-md-6 text-center">
            <h4>Donate to Myjobo.com</h4>
            <p>We welcome all sizes of contributions which help us in developing and maintaining such a huge project. We are calling for YOUR help! If you believe in the work that we are doing, you may wish to show your appreciation by donating any amount to our work (no amount is small).</p>
            <h5>I want to donate</h5>
                <form class="form-horizontal" id="form-one-time" target="_blank" method="post" action="https://www.paypal.com/cgi-bin/webscr">
                    <input type="hidden" value="_donations" name="cmd">
                    <input type="hidden" value="enviro@envirofuture.org" name="business">
                    <input type="hidden" value="US" name="lc">
                    <input type="hidden" value="Donation for Myjobo.com" name="item_name">
                    <input type="hidden" value="http://www.myjobo.com" name="return">
                    <input type="hidden" value="http://www.myjobo.com" name="cancel_return">
                    <input type="hidden" value="2" name="rm">
                    
                    <input class="donate-input" name="amount" type="text" value="25" style="margin: 0 auto; padding: 0px; text-align: center; width: 60px;">
                    <label>
                        <input type="radio" checked value="USD" name="currency_code">
                        USD
                        <input type="radio" value="GBP" name="currency_code">
                        GBP
                    </label>
                        <p>
                        <img src="{{ URl::to('frontend/images/payment-cards.png') }}"  alt="card" style="margin: 0 auto;" />
                        </p>
                        <input type="image" src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/silver-pill-paypal-44px.png">
                </form>
        </div>

        <div class="col-md-12 text-center">
            <hr>
            <h4>Thank you for your Support!</h4>
            <p>Your support means a lot to us and the over 25,000 registered users, and over 500,000 yearly visitors to Myjobo.com.</p>
            <p>Together we can continue to making job opportunities accessible to everyone, support young people make appropriate career decisions and access appropriate support; and promote entrepreneurship. We hope for a future where unemployment will be negligible.</p>
        </div>


    </div>

<div class="margin-bottom-40"></div>
</div>
@endsection