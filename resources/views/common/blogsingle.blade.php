@extends('layouts.app')

@section('template_title')
    Career Resources | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.blogss').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<style type="text/css">
    .footer-links.datasetul a
    {
        color: #333 !important;
    }
    .footer-links.datasetul a:hover
    {
        color: #333 !important;
    }
    .post-container
    {
        margin-bottom: 0px !important;
    }
    .post-content
    {
        padding: 15px 0px !important;
    }
    .sociadata li a {
        background-color: #26ae61;
    }
    .sociadata li .twitter::before, .sociadata li .facebook::before
    {
        color: #fff !important;
    }
    .sociadata li a
    {
        text-decoration: none !important; 
    }
    .post-content
    {
        border-bottom: none;
    }
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            @foreach($blogs as $b1)
            <h3>{{ $b1->title }}</h3>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('blog') }}">Career Resources</a></li>
                    <li>{{ $b1->title }}</li>
                </ul>
            </nav>
            @endforeach
        </div>
    </div>
</div>

<div class="container">
    <div class="eleven columns">
        <div class="padding-right">

            <div class="container"> 
                  <div class="eleven columns">
                  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
                  </div>  
            </div>
            @foreach($blogs as $b)
            <div class="post-container">
                <div class="post-content">
                    <div class="meta-tags">
                        <span><strong>Posted on: </strong>{{ Carbon\Carbon::parse($b->created_at)->format('jS F, Y') }}</span><br>
                        <span><strong>By: </strong>{{ $b->author }}</span>
                    </div>
                    {!! $b->description !!}
                </div>
            </div>
            <div class="col-md-12">
                    <h4>Comment</h4>
                    <hr>
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('blogcomment/post') }}">
                    
                        {{ csrf_field() }}
                        <input type="hidden" name="articleid" value="{{ $b->id }}">
                        <div class="row">
                        @if (Auth::guest())
                          <div class="col-md-12 form-group">  
                            <input type="text" name="email" class="form-control" placeholder="Email" style="padding: 6px 12px;">
                          </div>
                        @else
                            <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                        @endif
                          <div class="col-md-12 form-group">  
                            <textarea name="comment" class="form-control" placeholder="Comment"></textarea>
                          </div>

                          <div class="form-group col-md-12">  
                            <button type="submit" class="btn btn-success">Comment</button>
                          </div>
                        </div>
                    </form>
                    @foreach($comment as $com)
                    <div class="col-md-12">
                        <p style="margin-bottom: 0px;"><strong>{{ $com->email }}</strong> </p>
                        <p style="margin-bottom: 0px;">{{ Carbon\Carbon::parse($com->created_at)->format('jS \o\f F, Y g:i:s a') }}</p>
                        <p>{{ $com->comment }}</p>
                        <hr style="margin: 10px 0px 10px">
                    </div>
                    @endforeach
                </div>
            @endforeach
        </div>
        <div class="margin-bottom-40"></div>
    </div>
    <!-- Blog Posts / End -->


    <!-- Widgets -->
    <div class="five columns blog">

        <div class="widget">
            <h4>Category</h4>
            <div class="widget-box">
                <ul class="footer-links datasetul">
                    @foreach($category as $cat)
                    <li>
                        <a href="{{ URL::to('blogs') }}/{{ $cat->slug }}">{{ $cat->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>    
        </div>

        <div class="widget text-center">
            <div class="widget-box" style="text-align: center;">
                <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
            </div>
        </div>

        <div class="widget">
           <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
        </div>

    </div>
    <!-- Widgets / End -->

</div>
@endsection