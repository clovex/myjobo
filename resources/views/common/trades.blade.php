@extends('layouts.app')

@section('template_title')
    Find skilled trades in Malawi|www.myjobo.com 
@endsection
@section('template_description')
Directory of trusted & vetted tradesmen & artisans in Malawi. Search for skilled trades including electricians, carpenters, builders, welders, mechanics. Myjobo.com
@endsection
@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.tradesdata').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<style type="text/css">
  .footer-links.datasetul a
    {
        color: #333 !important;
    }
    .footer-links.datasetul a:hover
    {
        color: #333 !important;
    }
    .setstyle:hover
    {
        background:rgba(38, 174, 97, 0.05);
        border-bottom: 1px solid #26ae61;
        border-top: 1px solid #26ae61;
    }
    .setstyle
    {
        padding-top: 15px;
        padding-bottom: 15px;
        border-bottom: 1px solid #e0e0e0;
        border-top: 1px solid #e0e0e0;
    }
    .sethovers:hover
    {
      color: #26ae61;
    }
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single" style="padding:0px; margin-bottom: 20px;">
    <div class="container">
        <div class="sixteen columns">
            <div class="col-lg-12 col-md-12 col-sm-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

</div>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-md-9 col-lg-9 col-sm-9">
            <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
              <h1 style="color: #a9a9a9;">Amisili Aku Malawi</h1>
              <h3 style="color: #a9a9a9;">Search for a Skilled Trade or Artisan in Malawi Now</h3>
              <hr> 
            </div>
              <form method="POST" enctype="multipart/form-data" action="{{ URL::to('searchtrades') }}">
              {{ csrf_field() }}
                <div class="col-lg-5 col-md-5 col-sm-5 form-group">
                    <input type="text" name="trade" class="form-control" @if(isset($tradename)) value="{{ $tradename }}" @endif style="padding: 6px 12px;" placeholder="eg plumber, electrician, carpenter etc">
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 form-group">
                    <input type="text" name="location" class="form-control" @if(isset($locationname)) value="{{ $locationname }}" @endif style="padding: 6px 12px;" placeholder="Location">
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 form-group">
                    <button type="submit" class="btn btn-default btn-block" style="border:1px solid #a9a9a9; color: #a9a9a9; font-weight:bold; ">Search</button>
                </div>
              </form>  
            </div>

          <div class="row">
          @if(isset($searchby))
              <div class="col-md-12 col-lg-12 col-sm-12">
                <h3>Search results for : {{ $searchby }}</h3>
              </div>
          @endif
          @php $i = 0; @endphp  
          @foreach($trade as $t)
            <div class="col-md-12 col-lg-12 col-sm-12 setstyle">
                <div class="col-md-2 col-lg-2 col-sm-2"> 
                     @if($t->image != null)
                        <img src="{{ URL::to('public/uploads') }}/{{ $t->image }}" style="width: 100%; border-radius: 50%;">
                     @else 
                        <img src="{{ URL::to('frontend/images/profile-placeholder.jpg') }}" style="width: 100%; border-radius: 50%;">
                     @endif
                </div>
                <div class="col-md-10 col-lg-10 col-sm-10">
                <a href="{{ URL::to('trade/single') }}/{{ $t->id }}/{{ strtolower($t->first_name) }}{{ strtolower($t->last_name) }}"><h3 class="sethovers" style="text-transform: capitalize; color: #a9a9a9;">{{ $t->first_name }} {{ $t->last_name }} </h3></a>
                <label> 
                        <span><i class="fa fa-filter" aria-hidden="true"> {{ $t->sname }} </i></span> 
                        &nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-map-marker">  Based in: </i> 
                        @foreach($city as $cc)  
                          @if($cc->id == $t->city) {{ $cc->name }} @endif 
                        @endforeach
                        </span>
                        &nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-calendar"> Member since: </i> {{ Carbon\Carbon::parse($t->created_at)->format('Y') }}</span> 
                </label>
                </div>
                <!--div class="col-md-12 col-lg-12 col-sm-12" style="text-transform: lowercase;">
                {!! substr(strip_tags($t->description),0,200) !!}...
                </div-->
            </div>
          @php $i++; @endphp  
          @endforeach 
          <div class="pagination-container text-center">
                    <nav class="pagination"> 
                       {{ $trade->links() }}
                    </nav>
                </div> 
          @if($i == 0)
                <div class="col-md-12 col-lg-12 col-sm-12 text-center">
                    <h3>No Trades are there according to Search.</h3>
                </div>
            @endif  
          </div>

          </div>  


          <div class="col-md-3 col-lg-3 col-sm-3">
                
            <div class="widget">
                <div class="widget-box">
                  <a class="btn-default btn-block btn-lg text-center" href="{{ URL::to('register_trade') }}" style="border:1px solid #a9a9a9; color: #a9a9a9; font-weight:bold; border-radius: 0px;"><span>I want to register as a trade or artisan</span></a>
                </div>
            </div>

            <div class="widget text-center">
                <div class="widget-box" style="text-align: center;">
                    <a href="{{ URL::to('donate') }}" class="btn-default btn-block btn-lg" style="border:1px solid #a9a9a9; color: #a9a9a9; font-weight:bold; box-shadow: 10px 10px 5px #888888; border-radius: 0px;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Our Activities</a>
                </div>
            </div>

            <div class="widget">
            <h3>Speciality</h3>
              <div class="widget-box">
                  <ul class="footer-links datasetul">
                    @foreach($speciality as $spe)
                    <li class="hideshow" style="display: none;">
                        <a href="{{ URL::to('trade/speciality')}}/{{ $spe->id }}">{{ $spe->name }}</a>
                    </li>
                    @endforeach
                    <button class="btn btn-default" id="btnshowhide" style="border:1px solid #a9a9a9; color: #a9a9a9; font-weight:bold;">Load More</button>
                </ul>
              </div>
              <script>
              $(function(){
                  $(".hideshow").slice(0, 5).show(); 
                  if($(".hideshow:hidden").length == 0)
                  { 
                      $("#btnshowhide").hide();
                  } 
                  $("#btnshowhide").click(function(e){ 
                    e.preventDefault();
                    $(".hideshow:hidden").slice(0, 5).show(); 
                    if($(".hideshow:hidden").length == 0){ 
                      $("#btnshowhide").hide();
                    }
                  });
              });
              </script>  
            </div>

            <div class="widget">
            <div class="widget-box" style="background-color: transparent;">
               <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script> 
            </div>
            </div>


        </div>



        </div>    
    </div>
</div>



<div id="titlebar" class="single" style="padding:0px; margin-bottom: 0px;">
    <div class="container">
        <div class="sixteen columns">
            <div class="col-lg-12 col-md-12 col-sm-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
            </div>
        </div>
    </div>
</div>            
@endsection