@extends('layouts.app')

@section('template_title')
    Partners | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.partners').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<!--div id="titlebar" class="single" style="padding: 15px 0px;">
    <div class="container">
        <div class="sixteen columns">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<! Jobonology Movement -->
<!--ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
        </div>
    </div>
</div-->

<div class="container">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-2 col-lg-2 col-sm-2">
        &nbsp;
        </div>
            <form method="POST" enctype="multipart/form-data" action="{{ URL::to('partner/search') }}">
            {{ csrf_field() }}
            <div class="col-md-6 col-lg-6 col-sm-6 form-group">
                <h3>Find a Partner</h3>
                <input type="text" name="keyword" class="form-control" placeholder="Find - NGOs, Companies, etc" style="padding: 6px 12px;">
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2">
                <h3>&nbsp;</h3>
                <button type="submit" class="button btn-block" style="padding: 4px">Search</button>
            </div>
            </form>    
        <div class="col-md-2 col-lg-2 col-sm-2">
        &nbsp;
        </div>
    </div>

    
    <div class="col-md-12 col-lg-12 col-sm-12">
        <hr>
        <div class="row">
            @php $i = 1; @endphp
            @foreach($partners as $p)
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <div class="col-md-12 col-lg-12 col-sm-12" style="border-bottom: 1px solid #e0e0e0; padding: 0px;">
                    <a href="{{ URL::to('partner') }}/{{ $p->slug }}">{{ $p->name }}</a>
                    ({{ $p->counts }})    
                    </div>
                </div>
                @if($i%3 == 0)
                    </div><div class="row">
                @endif    
                @php $i++ @endphp    
            @endforeach
        </div>    
        <div class="margin-bottom-40"></div>
    </div>
    

</div>
<div class="infobox" style="margin-bottom: 0px;">
    <div class="container">
        <div class="sixteen columns">Want to Join? Register <a href="{{ URL::to('register_employer') }}">Get Started</a></div>
    </div>
</div>
<!--div class="container">
    <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<! Jobonology Movement -->
<!--ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
</div>
</div-->
@endsection