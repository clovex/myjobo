@extends('layouts.app')

@section('template_title')
 @if(isset($metatital))
        {{ $metatital }}
 @else
    Jobs | Find jobs in Malawi- Search for jobs on myjobo.com
 @endif   
@endsection
@section('template_description')
@if(isset($metades))
    {{ $metades }}
@else
Find jobs in Malawi. Search for job vacancies on Myjobo.com. Apply for jobs online. Receive job alerts by email. Myjobo.com.
@endif
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.jobs').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<style type="text/css">
    .setfacebook 
    {
        background-color: #4a6d9d; 
    }
    .setfacebook i
    {
        margin: 0px !important;
        color: #fff !important;
        font-size: 20px !important;
    }
    .settwitter
    {
        background-color: #3bc1ed; 
    }
    .settwitter i
    {
        margin: 0px !important;
        color: #fff !important;
        font-size: 20px !important;
    }
    .setstyle:hover
    {
        background:rgba(38, 174, 97, 0.05);
        border-bottom: 1px solid #26ae61;
        border-top: 1px solid #26ae61;
    }
    .setstyle
    {
        padding-top: 15px;
        padding-bottom: 15px;
        border-bottom: 1px solid #e0e0e0;
        border-top: 1px solid #e0e0e0;
    }

    .footer-links.datasetul a
    {
        color: #333 !important;
    }
    .footer-links.datasetul a:hover
    {
        color: #333 !important;
    }
    .post-container
    {
        margin-bottom: 0px !important;
    }
    .post-content
    {
        padding: 15px 0px !important;
    }
    .sociadata li a {
        background-color: #26ae61;
    }
    .sociadata li .twitter::before, .sociadata li .facebook::before
    {
        color: #fff !important;
    }
    .sociadata li a
    {
        text-decoration: none !important; 
    }
    .setjobfb, .setjobtw
    {
        width: 30px !important;
        height: 30px !important;
    }
    .setjobtw i, .setjobtw::before
    {
        margin: 8px 0px 0px 8px !important;
    }
    .setjobfb i, .setjobfb::before
    {
        margin: 8px 0px 0px 10px !important;
    }
    .menu ul li.sfHover ul li a.sf-with-ul, .menu ul ul li a
    {
        font-size: 13px!important;
    }
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single" style="padding:0px; margin-bottom: 20px;">
    <div class="container">
        <div class="sixteen columns">
            <div class="col-md-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

</div>
            <!--<h2>Jobs</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Jobs</li>
                </ul>
            </nav>-->
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-md-3 col-lg-3 col-sm-3">
                
            <div class="widget">
                <h4 style="color: #494949;">Sign up for Email Job Alerts</h4>
                <div class="widget-box">
                <div id="sendnews">
                {{ csrf_field() }}
                <div class="form-group" style="margin-bottom: 15px;">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Full Name" style="padding: 6px 12px;">
                    <label id="msgname" style="margin: 0px !important"></label>
                </div>

                <div class="form-group" style="margin-bottom: 15px;">
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" style="padding: 6px 12px;">
                    <label id="msgemail" style="margin: 0px !important"></label>
                </div>

                <div class="form-group" style="margin-bottom: 15px;">
                    <button id="send" class="btn btn-default" style="border:1px solid #0000FF; color: #0000FF; font-weight:bold;">Submit</button>
                </div>
                <figure id="submessage" style="font-size: 12px;"></figure>
            </div>
                </div>
            </div>

            <div class="widget">
                <h4 style="color: #494949;">Are you recruiting?</h4>
                <div class="widget-box">
                    <p>Advertise jobs now</p>
                    <a href="{{ URL::to('register_employer') }}" class="btn btn-default btn-block btn-lg" style="border:1px solid #0000FF; color: #0000FF; font-weight:bold;"><i class="fa fa-user-secret" aria-hidden="true"></i> Click Here</a>
                </div>
            </div>

            <div class="widget">
                <h4 style="color: #494949;">Are you looking for a job?</h4>
                <div class="widget-box">
                    <p>Upload your CV, It's Free</p>
                    <a href="{{ URL::to('register_job_seeker') }}" class="btn btn-default btn-block btn-lg" style="border:1px solid #0000FF; color: #0000FF; font-weight:bold;"><i class="fa fa-user" aria-hidden="true"></i> Click Here</a>
                </div>
            </div>

            <div class="widget">
                <h4 style="color: #494949;">Are you a Skilled Trade?</h4>
                <div class="widget-box">
                    <a href="{{ URL::to('partners') }}" class="btn btn-default btn-block btn-lg" style="border:1px solid #0000FF; color: #0000FF; font-weight:bold;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Click Here</a>
                </div>
            </div>

            <div class="widget text-center">
                <div class="widget-box" style="text-align: center;">
                    <a href="{{ URL::to('donate') }}" class="btn btn-default btn-block btn-lg" style="border:1px solid #0000FF; color: #0000FF; font-weight:bold; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Our Activities</a>
                </div>
            </div>

            <!--div class="widget">
                <div class="widget-box">
                    @foreach($common as $com)
                        <a href="{{ json_decode($com->contents)->link }}">
                            <img src="{{ URL::to('public/uploads') }}/{{ json_decode($com->contents)->image }}" style="width: 100%">
                        </a>
                    @endforeach
              </div>   
            </div-->

            <div class="widget">
            <div class="widget-box" style="background-color: transparent;">
               <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script> 
            </div>
            </div>


        </div>

        <div class="col-md-9 col-lg-9 col-sm-9">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 text-center">
                    <h2>Browse job vacancies</h2>
                </div>
            
                <!--div class="col-md-6 text-right">
                    <a href="{{ URL::to('archive') }}" class="btn btn-warning">Job Archives</a>
                </div-->
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="form-group col-md-10 col-lg-10 col-sm-10" style="padding:0px;">
                <input type="text" name="keyword" id="keyword" value="" class="form-control" style="padding:6px 12px;" placeholder="Job title, key words or company name">
                </div>
                <div class="form-group col-md-2 col-lg-2 col-sm-2">
                    <button class="btn btn-default btn-block" id="searchjob" style="border:1px solid #0000FF; color: #0000FF; font-weight:bold;">Search</button>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="col-md-4 col-lg-4 col-sm-4" style="padding: 0px;"><hr style="margin: 15px 0px;"></div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="padding: 0px;"><h5 style="color: #909090; font-size: 15px;">Search By Job Category And Location</h5>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4" style="padding: 0px;"><hr style="margin: 15px 0px;"></div>
            </div>
            <script>
                $(function(){
                    $("#searchjob").click(function(){
                        if ($('#keyword').val() != ''){
                            var cat = $("#keyword").val();
                        }
                        else{
                            var cat = "null";   
                        }

                        $.ajax({
                            'url':'{{ URL::to("jobsfillter/get") }}/'+cat,
                            'success':function(message)
                            {   
                                $(".jonlist").html(message);    
                            }
                        });
                    });
                });
            </script>

            <div class="form-group col-md-6 col-lg-6 col-sm-6">
                <select id="category" class="form-control">
                    <option value="null">Search by Job Category</option>
                    @foreach($jobcategory as $jc)
                        @if($jc->id == 0)

                        @else
                        <option value="{{ $jc->id }}">{{ $jc->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-6 col-lg-6 col-sm-6">
                <select id="location" class="form-control">
                    <option value="null">Search by Job Location</option>
                    @foreach($district as $d)
                        @if($d->id == 0)

                        @else
                        <option value="{{ $d->id }}">{{ $d->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <script>
                $(function(){
                    $("#category").change(function(){
                        var loc = $("#location").val();
                        var cat = $(this).val();
                        $.ajax({
                            'url':'{{ URL::to("jobfillter/get") }}/'+cat+'/'+loc,
                            'success':function(message)
                            {   
                                $(".jonlist").html(message);    
                            }
                        });
                    });
                });
            </script>

            <script>
                $(function(){
                    $("#location").change(function(){
                        var loc = $("#category").val();
                        var cat = $(this).val();
                        $.ajax({
                            'url':'{{ URL::to("jobfillterlocation/get") }}/'+cat+'/'+loc,
                            'success':function(message)
                            {   
                                $(".jonlist").html(message);    
                            }
                        });
                    });
                });
            </script>



            <div class="jonlist">
            @if(isset($searchby))
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <h3>Search results for : {{ $searchby }}</h3>
                </div>
            @endif
            @php $i = 0; @endphp
            @foreach($jobs as $j)
                <div class="col-md-12 col-lg-12 col-sm-12 setstyle">
                    <div class="col-md-1 col-lg-1 col-sm-1" style="padding: 0px !important;">
                       @if($j->logo != null)
                        <img src="{{ URL::to('public/uploads') }}/{{ $j->logo }}" alt="" style="width: 100%;">
                        @else    
                        <img src="{{ URL::to('public/uploads/company.png') }}" alt="" style="width: 100%;">
                        @endif
                    </div>
                    <div class="col-md-11 col-lg-11 col-sm-11">
                        <h4 style="font-size: 16px; line-height: 32px;">{{ $j->title }} 
                        <!--ul class="social-icons sociadata" style="float: right;">
                        <li style="transform:none; margin-top: 0px !important;">
                            <a class="facebook setjobfb" href="https://www.facebook.com/sharer/sharer.php?u={{ URL::to('job') }}/{{ $j->slug }}" target="_blank">
                                <i class="icon-facebook"></i>
                            </a>
                        </li>
                        <li style="transform:none; margin-top: 0px !important;">
                            <a class="twitter setjobtw" href="http://twitter.com/share?text={{ URL::to('job') }}/{{ $j->slug }}" target="_blank">
                                <i class="icon-twitter"></i>
                            </a>
                        </li>
                        </ul-->
                        </h4>
                        <label>
                        <span><i class="fa fa-calendar"> Posted On: </i> {{ Carbon\Carbon::parse($j->created_at)->format('d-m-Y') }}</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-map-marker"> Location: </i> {{ $j->city }}</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-calendar-times-o"> Job Deadline: </i> {{ Carbon\Carbon::parse($j->deadline)->format('d-m-Y') }}</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <strong><a href="{{ URL::to('job') }}/{{ $j->slug }}">View</a></strong>
                        </label>
                    </div> 
                </div>

                <!--div class="col-md-12 setstyle">

                    <div class="col-md-2">
                       @if($j->logo != null)
                        <img src="{{ URL::to('public/uploads') }}/{{ $j->logo }}" alt="" style="width: 100%;">
                        @else    
                        <img src="{{ URL::to('public/uploads/nologo.png') }}" alt="" style="width: 100%;">
                        @endif
                    </div>
                    <div class="col-md-10">
                        <h4>{{ $j->title }}
                        <ul class="social-icons sociadata" style="float: right;">
                        <li style="transform:none;">
                            <a class="facebook setjobfb" href="https://www.facebook.com/sharer/sharer.php?u={{ URL::to('job') }}/{{ $j->slug }}" target="_blank">
                                <i class="icon-facebook"></i>
                            </a>
                        </li>
                        <li style="transform:none;">
                            <a class="twitter setjobtw" href="http://twitter.com/share?text={{ URL::to('job') }}/{{ $j->slug }}" target="_blank">
                                <i class="icon-twitter"></i>
                            </a>
                        </li>
                        </ul>
                        
                        </h4>
                        <label>
                        <span><i class="fa fa-calendar"></i> {{ Carbon\Carbon::parse($j->created_at)->format('jS F, Y') }}</span>
                        <span><i class="fa fa-map-marker"></i> {{ $j->city }}</span>
                        </label>
                        <a href="{{ URL::to('job') }}/{{ $j->slug }}" class="btn btn-info">View More</a>
                    </div> 
                
                </div-->
                @php $i++; @endphp
                @endforeach
                <div class="pagination-container text-center">
                    <nav class="pagination"> 
                       {{ $jobs->links() }}
                    </nav>
                </div>
                </div>        
            </div>
            @if($i == 0)
                <div class="col-md-12 col-lg-12 col-sm-12 text-center">
                    <h3>No Jobs are there according to Search.</h3>
                </div>
            @endif
        </div>        
    </div>    
</div>
<div id="titlebar" class="single" style="padding:0px; margin-bottom: 0px;">
    <div class="container">
        <div class="sixteen columns">
            <div class="col-md-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
            </div>
        </div>
    </div>
</div>            
@endsection