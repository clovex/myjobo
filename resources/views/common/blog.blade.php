@extends('layouts.app')

@section('template_title')
    Career Resources | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.blogss').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<style type="text/css">
    .footer-links.datasetul a
    {
        color: #333 !important;
    }
    .footer-links.datasetul a:hover
    {
        color: #333 !important;
    }
    .post-container
    {
        margin-bottom: 0px !important;
    }
    .post-content
    {
        padding: 15px 0px !important;
    }
    .sociadata li a {
        background-color: #26ae61;
    }
    .sociadata li .twitter::before, .sociadata li .facebook::before
    {
        color: #fff !important;
    }
    .sociadata li a
    {
        text-decoration: none !important; 
    }
    .setfacebook 
    {
        background-color: #4a6d9d; 
    }
    .setfacebook i
    {
        margin: 0px !important;
        color: #fff !important;
        font-size: 20px !important;
    }
    .settwitter
    {
        background-color: #3bc1ed; 
    }
    .settwitter i
    {
        margin: 0px !important;
        color: #fff !important;
        font-size: 20px !important;
    }
</style>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Career Resources</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li>Career Resources</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="eleven columns">
        <div class="padding-right">

            <div class="container"> 
                <div class="eleven columns">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
                  </div>  
            </div>
            @foreach($blogs as $b)
                <div class="post-container">
                <div class="post-content">
                    <a href="{{ URL::to('blog/') }}/{{ $b->slug }}"><h3>{{ $b->title }}</h3></a>
                    <div class="meta-tags">
                        <span><strong>Posted on: </strong>{{ Carbon\Carbon::parse($b->created_at)->format('jS F, Y') }}</span>
                        <span><strong>By: </strong>{{ $b->author }}</span>
                    </div>
                    
                    {!! substr(strip_tags($b->description),0,500) !!}...
                   <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                    <ul class="social-icons sociadata">
                        <li style="transform:none;">
                            <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}" target="_blank">
                                <i class="icon-facebook"></i>
                            </a>
                        </li>
                        <li style="transform:none;">
                            <a class="twitter" href="http://twitter.com/share?text={{ Request::url() }}" target="_blank">
                                <i class="icon-twitter"></i>
                            </a>
                        </li>
                    </ul>
                    </div>
                    </div>
                    <a class="btn btn-success" href="{{ URL::to('blog/') }}/{{ $b->slug }}">Read More</a>
                </div>
            </div>
            @endforeach
        </div>
        <div class="margin-bottom-40"></div>
    </div>
    <!-- Blog Posts / End -->


    <!-- Widgets -->
    <div class="five columns blog">

        <div class="widget">
            <h4>Category</h4>
            <div class="widget-box">
                <ul class="footer-links datasetul">
                    @foreach($category as $cat)
                    <li>
                        <a href="{{ URL::to('blogs') }}/{{ $cat->slug }}">{{ $cat->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>    
        </div>

        <div class="widget text-center">
            <div class="widget-box" style="text-align: center;">
                <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
            </div>
        </div>

        <div class="widget">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                        style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>

    </div>
    <!-- Widgets / End -->

</div>
@endsection