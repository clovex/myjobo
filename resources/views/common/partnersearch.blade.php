@extends('layouts.app')

@section('template_title')
    Partners | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<style type="text/css">
    .setpsrtle
    {
        border-bottom: 1px solid #e0e0e0;
        border-top: 1px solid #e0e0e0;
    }
    .setpsrtle:hover
    { 
        background:rgba(38, 174, 97, 0.05);
        border-bottom: 1px solid #26ae61;
        border-top: 1px solid #26ae61;
    }
</style>
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.partners').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>@if(isset($keyword))
                    {{ $keyword }}
                @endif
            </h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="">Home</a></li>
                    <li><a href="{{ URL::to('partners') }}">Partners</a></li>
                    <li>@if(isset($keyword)) {{ $keyword }} @endif</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-8 col-lg-8 col-sm-8">
        <div class="row">
            @foreach($users as $u)
            @php $id = base64_encode($u->id); @endphp
                <div class="col-md-12 col-lg-12 col-sm-12 setpsrtle" style="padding-top: 15px; padding-bottom: 15px;">
                    <div class="col-md-2 col-lg-2 col-sm-2">
                        @if($u->image != null)
                            <img src="{{ URL::to('public/uploads') }}/{{ $u->image }}" alt="" style="width: 100%;">
                        @else
                            <img src="{{ URL::to('public/uploads/nologo.png') }}" alt="" style="width: 100%;">
                        @endif
                    </div>
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <h4>
                        <a href="{{ URL::to('partner/single') }}/{{ $id }}">{{ $u->name }}</a>
                        </h4>
                        @if($u->website != null)
                        <label>
                            <strong>Website: </strong><a href="{{ $u->website }}" target="_blank">{{ $u->website }}</a>
                        </label>
                        @endif

                        @if($u->address != null)
                        <label>
                            <strong>Address: </strong>{{ $u->address }}
                        </label>
                        @endif

                    </div>
                </div>
            @endforeach
        </div>    
        <div class="margin-bottom-40"></div>
    </div>
    <!-- Blog Posts / End -->


    <!-- Widgets -->
    <div class="col-md-4 col-lg-4 col-sm-4">

        <div class="widget">
            <div class="widget-box">
                @foreach($common as $com)
                    <a href="{{ json_decode($com->contents)->link }}">
                        <img src="{{ URL::to('public/uploads') }}/{{ json_decode($com->contents)->image }}" style="width: 100%">
                    </a>
                @endforeach
            </div>    
        </div>

        <div class="widget">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
        </div>
    </div>

</div>
@endsection