@extends('layouts.app')

@section('template_title')
    Contact Us | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<style type="text/css">
    .form-control
    {
        padding: 6px 12px !important;
    }
</style>
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.contact_us').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<!--div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Contact Us</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="">Home</a></li>
                    <li>Contact Us</li>
                </ul>
            </nav>
        </div>
    </div>
</div-->

<div class="container">

    <div class="eleven columns">
        <h3 class="margin-bottom-15 text-center">Get in touch</h3>
        <hr>
        <section id="" class="padding-right">
            @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    <li>{{$errors->first()}}</li>
                </ul>
            </div>
            @endif    
            <form method="post" name="contactform" id="contactform" action="{{ URL::to('contactsend') }}" onsubmit="return validateform();">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Name:<span>*</span></label>
                    <input name="name" type="text" id="name" class="form-control" required/>
                </div>

                <div class="form-group">
                    <label >Email: <span>*</span></label>
                    <input name="email" type="email" id="email" class="form-control" required/>
                </div>

                <div class="form-group">
                    <label>Phone: <span>*</span></label>
                    <input name="phone" type="text" id="phone" class="form-control" required/>
                </div>

                <div class="form-group">
                    <label>Company:</label>
                    <input name="company" type="text" id="company" class="form-control" />
                </div>

                <div class="form-group">
                    <label>Message:</label>
                    <textarea name="comment" class="form-control" id="comment" spellcheck="true"></textarea>
                </div>
                <div class="form-group">
                    <label>&nbsp;</label>
                    <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                    <label id="valmessage"></label>
                </div>
                <div class="form-group">
                    <label>&nbsp;</label>
                    <input type="submit" id="submit" class="btn btn-success" value="Send Message" />
                </div>
            </form>
            <br>    
        </section>
    </div>
    
    <div class="five columns">
        <h3 class="margin-bottom-10">Details</h3>
        <div class="widget-box" style="padding: 32px 15px;">
            @foreach($pages as $p)
                {!!  $p->contents !!}
            @endforeach
        </div>
</div>
<script type="text/javascript">
    function validateform(){
var captcha_response = grecaptcha.getResponse();
if(captcha_response.length == 0)
{
    // Captcha is not Passed
     $("#valmessage").html("Please Enter Captcha !").fadeIn('show').delay(2000).fadeOut('fast');
    return true;
   
}
else
{
    // Captcha is Passed
    return true;
}
}
</script>

</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection