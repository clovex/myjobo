@extends('layouts.app')

@section('template_title')
    @foreach($jobs as $j3)
        {{ $j3->title }}
    @endforeach 
@endsection
@section('template_description')
    @foreach($jobs as $j4)
        {!! substr(strip_tags($j4->des),0,500) !!}...
    @endforeach 
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.jobs').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single" style="margin-bottom: 0px;">
    <div class="container">
        <div class="sixteen columns">
            @foreach($jobs as $j1)
            <h2>{{ $j1->title }}</h2>
            <!--nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('jobs') }}">Jobs</a></li>
                    <li>{{ $j1->title }}</li>
                </ul>
            </nav-->
            @endforeach
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<hr>
</div>
        @foreach($jobs as $j)
            <div class="col-md-8 col-lg-8 col-sm-8">
                    
                    @if($j->logo != null)
                        <img src="{{ URL::to('public/uploads') }}/{{ $j->logo }}" alt="" style="width: 125px; height: 125px; margin: 0 auto;">
                    @endif
                    {!! $j->des !!}
                    <hr>
                    <h4>Comment on advertised Job</h4>
                    <div id="commentsend">
                        {{ csrf_field() }}
                         <input type="hidden" name="jobid" value="{{ $j->id }}">
                        <div class="row">
                       
                        @if (Auth::guest())
                        <div class="col-md-12 col-lg-12 col-sm-12 form-group">  
                            <input type="text" name="emails" id="emails" class="form-control" placeholder="Email" style="padding: 6px 12px;">
                            <div id="msgemails"></div>
                        </div>    
                        <div class="col-md-12 col-lg-12 col-sm-12 form-group">      
                            <input type="text" name="fullnames" id="fullnames" class="form-control" placeholder="Full Name" style="padding: 6px 12px;">
                            <div id="msgfullnames"></div>
                        </div>
                        @else
                            <input type="hidden" name="emails" id="emails" value="{{ Auth::user()->email }}">
                            @if(Auth::user()->first_name != null)
                            <input type="hidden" name="fullnames" id="fullnames" value="{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}">
                            @else
                            <input type="hidden" name="fullnames" id="fullnames" value="{{ Auth::user()->name }}">
                            @endif
                        @endif
                        <div class="col-md-12 col-lg-12 col-sm-12 form-group">  
                           <textarea name="comments" id="comments" class="form-control" placeholder="Comment"></textarea>
                           <div id="msgnames"></div>
                        </div>

                        <div class="form-group col-md-12 col-lg-12 col-sm-12">  
                            <button id="submit" class="btn btn-success">Comment</button>
                        </div>
                        </div>
                    </div>
                    <div id="setcomment">
                    @foreach($comment as $com)
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <p style="margin-bottom: 0px;"><strong>{{ $com->full_name }}</strong> </p>
                        <p style="margin-bottom: 0px;">{{ Carbon\Carbon::parse($com->created_at)->format('jS \o\f F, Y g:i:s a') }}</p>
                        <p>{!! $com->comment !!}</p>
                        <hr style="margin: 10px 0px 10px">
                    </div>
                    @endforeach
                    </div>
            </div>    
    
            <div class="col-md-4 col-lg-4 col-sm-4">
                
                <p style="margin: 0px;">
                    <strong>Location: </strong>{{ $j->city }}
                </p>
                
                <p style="margin: 0px;">
                    <strong>Employment Type: </strong>@if($j->employment_type->id == 0) - @else {{ $j->employment_type->name }} @endif
                </p>
                @if($j->type == 0)
                <p style="margin: 0px;">
                    <strong>Salary: </strong>@if($j->salary != null) {{ $j->salary }} @else N/A @endif
                </p>
                @endif
                
                <p style="margin: 0px;">
                    <strong>Job deadline: </strong>{{ Carbon\Carbon::parse($j->deadline)->format('jS F, Y') }}
                </p>
                
                @if (Auth::guest())
                    <p style="margin: 0px;">
                        <strong>Company: </strong> {{ $j->company_name }}
                    </p>    

                    <a href="{{ URL::to('logins') }}/{{ $j->slug }}" class="btn btn-danger" style="background-color: #F45B06;color: #fff;">Log in to apply</a>    
                @else

                    <!--h4>Job Posted By:</h4-->       
                    <p style="margin: 0px;">
                        <strong>Company: </strong> {{ $j->company_name }}
                    </p>
                    <!--p style="margin: 0px;">
                        <strong>Contact Name: </strong> {{ $j->contact_name }}
                    </p-->

                    <p style="margin: 0px;">
                        <strong>Website: </strong> <a href="{{ $j->website }}" target="_blank">{{ $j->website }}</a>
                    </p>
                    @if($chk == 0)
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('apply') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="slug" value="{{ $j->slug }}">
                        <div class="form-group">
                            <label>Cover Letter:</label>
                            <textarea class="form-control" name="letter"></textarea>
                            <label>Cv:</label>
                            <input type="file" name="cv" required>
                        </div>
                        <button class="btn btn-danger" type="submit" style="background-color: #F45B06;color: #fff;">Apply Now</button>
                    </form>     
                    @else
                        <label><strong style="color: #26ae61;">Job Already Applied.</strong></label>
                    @endif
                    @if($errors->any())
                        <br>
                        <div class="alert alert-danger">
                            <ul>
                                <li>{{$errors->first()}}</li>
                            </ul>
                        </div>
                    @endif
                @endif
                <div class="widget">
                <div class="widget-box">
                    <h4 style="color: #494949;">Sign up for Email Job Alerts</h4>
                    <div id="sendnews">
                    {{ csrf_field() }}
                        <div class="form-group" style="margin-bottom: 15px;">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Full Name" style="padding: 6px 12px;">
                            <label id="msgname" style="margin: 0px !important"></label>
                        </div>

                        <div class="form-group" style="margin-bottom: 15px;">
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email" style="padding: 6px 12px;">
                            <label id="msgemail" style="margin: 0px !important"></label>
                        </div>

                        <div class="form-group" style="margin-bottom: 15px;">
                            <button id="send" class="btn" style="background-color: #26ae61;color: #fff;">Submit</button>
                        </div>
                        <figure id="submessage" style="font-size: 12px;"></figure>
                    </div>
                </div>
                </div>
            </div>
        @endforeach        
    </div>    
</div>
<div class="container">
    <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
</div>
</div>
<script>
    $(function(){
        $("#submit").click(function(){

            var y = $("#emails").val();
            var atpos = y.indexOf("@");
            var dotpos = y.lastIndexOf(".");
            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=y.length) {
                $("#msgemails").html("Not a valid e-mail address").fadeIn().delay(1000).fadeOut('fast');
                return false;
            }
            var yi = $("#fullnames").val();
            if (yi == "") {
                $("#msgfullnames").html("Full Name Required").fadeIn().delay(1000).fadeOut('fast');
                return false;
            }
            var x = $("#comments").val();
            if (x == "") {
                $("#msgnames").html("Comment Required").fadeIn().delay(1000).fadeOut('fast');
                return false;
            }

            $.ajax({ 
                    'url':'{{ URL::to("jobcomment/send") }}',
                    'type':'POST',
                    'data':{'email':$('#commentsend input[name=emails]').val(),'comment':$('#comments').val(),'jobid':$('#commentsend input[name=jobid]').val(),'full_name':$('#commentsend input[name=fullnames]').val(),'_token': $('#commentsend input[name=_token]').val()},
                    'success':function(message)
                    {   
                        $("#emails").val("");
                        $("#comments").val("");
                        $("#fullnames").val("");
                        $("#setcomment").html(message);    
                    }
                    });
        });
    });
</script>
@endsection