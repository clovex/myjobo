@extends('layouts.app')

@section('template_title')
    School | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.career').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            @foreach($data as $c1)
            <h2>{{ $c1->name }}</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('careers') }}">Learning Resources</a></li>
                    <li>{{ $c1->name }}</li>
                </ul>
            </nav>
            @endforeach
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-8">
          <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
          @foreach($data as $d)
            <div class="col-md-12 col-lg-12 col-sm-12">    
                <div class="col-md-2 col-lg-2 col-sm-2"> 
                @if($d->logo != null)
                    <img src="{{ URL::to('public/uploads') }}/{{ $d->logo }}" alt="" style="width: 100%">
                @else
                    <img src="{{ URL::to('public/uploads/nologo.png') }}" alt="" style="width: 100%">
                @endif
                </div>
                <div class="col-md-10 col-lg-10 col-sm-10">
                    <div class="abc">
                        {!! $d->description !!}
                    </div>

                       <p>  <hr><strong>Principal: </strong>{{ $d->principal }}<br>
                            <strong>Address: </strong> {{ $d->address }} <br>
                            <strong>Website: </strong><a href="{{ $d->website }}" target="_blank" style="width: auto;">{{ $d->website }}</a>
                        </p>
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <h4>Feedback</h4>
                    <hr>
                    <form method="POST" enctype="multipart/form-data" action="{{ URL::to('schoolcategory/post') }}">
                    
                        {{ csrf_field() }}
                        <input type="hidden" name="school_id" value="{{ $d->id }}">
                        <div class="row">
                        @if (Auth::guest())
                          <div class="col-md-12 form-group">  
                            <input type="text" name="email" class="form-control" placeholder="Email" style="padding: 6px 12px;">
                          </div>
                        @else
                            <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                        @endif
                          <div class="col-md-12 form-group">  
                            <textarea name="comment" class="form-control" placeholder="Comment"></textarea>
                          </div>

                          <div class="form-group col-md-12">  
                            <button type="submit" class="btn btn-success">Comment</button>
                          </div>
                        </div>
                    </form>
                    @foreach($comment as $com)
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <p style="margin-bottom: 0px;"><strong>{{ $com->email }}</strong> </p>
                        <p style="margin-bottom: 0px;">{{ Carbon\Carbon::parse($com->created_at)->format('jS \o\f F, Y g:i:s a') }}</p>
                        <p>{{ $com->comment }}</p>
                        <hr style="margin: 10px 0px 10px">
                    </div>
                    @endforeach
                </div>
            </div>
        @endforeach    
        
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4"> 
            
            <div class="widget">
              <div class="widget-box">
                @foreach($common as $com)
                    <a href="{{ json_decode($com->contents)->link }}">
                        <img src="{{ URL::to('public/uploads') }}/{{ json_decode($com->contents)->image }}" style="width: 100%">
                    </a>
                @endforeach
              </div>
            </div>
            <div class="widget">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

            </div>
        </div>
    </div>
    <div class="margin-bottom-40"></div>
</div>
@endsection