@extends('layouts.app')


@section('template_title')
    School | Find jobs in Malawi- Search for jobs on myjobo.com
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.career').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            @foreach($schoolstype as $c1)
            <h2>{{ $c1->name }}</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="{{ URL::to('') }}">Home</a></li>
                    <li><a href="{{ URL::to('careers') }}">Learning Resources</a></li>
                    <li>{{ $c1->name }}</li>
                </ul>
            </nav>
            @endforeach
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-8">
          
          @foreach($data as $d)
            <div class="col-md-12 col-lg-12 col-sm-12" style="border-bottom: 1px solid; padding: 15px 5px;">    
                <div class="col-md-2 col-lg-2 col-sm-2"> 
                @if($d->logo != null)
                    <img src="{{ URL::to('public/uploads') }}/{{ $d->logo }}" alt="" style="width: 100%">
                @else
                    <img src="{{ URL::to('public/uploads/nologo.png') }}" alt="" style="width: 100%">
                @endif
                </div>
                <div class="col-md-10 col-lg-10 col-sm-10">
                    <a href="{{ URL::to('school') }}/{{ $d->slug }}"><h4>{{ $d->name }}</h4></a>
                       <p><strong>Address: </strong> {{ $d->address }} <p>
                       <p><strong>Website: </strong><a href="{{ $d->website }}" target="_blank" style="width: auto;">{{ $d->website }}</a></p>
                </div>
                
            </div>
        @endforeach    
         
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4">
            <div class="widget">
                <div class="widget-box">
                    @foreach($common as $com)
                        <a href="{{ json_decode($com->contents)->link }}">
                            <img src="{{ URL::to('public/uploads') }}/{{ json_decode($com->contents)->image }}" style="width: 100%">
                        </a>
                    @endforeach
                </div>
            </div> 
            
            <div class="widget">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
            </div>
        </div>
    </div>

    <div class="careers">
        <h4>Occupations</h4>
        <hr>
        <div class="row" style="margin-bottom: 0px;">
            @php $i = 1; @endphp
            @foreach($careers as $carr)
            @if($carr->type == 1)
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <a href="{{ URL::to('educational') }}/{{ $carr->slug }}">{{ $carr-> title }}</a>
                </div>
            @if($i%4 == 0)
                </div><div class="row" style="margin-bottom: 0px;">
            @endif    
            @php $i++ @endphp
            @endif
            @endforeach

            @foreach($schooltype as $schtype)
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <a href="{{ URL::to('schools') }}/{{ $schtype->slug }}">{{ $schtype-> name }}</a>
                </div>
            @if($i%4 == 0)
                </div><div class="row" style="margin-bottom: 0px;">
            @endif    
            @php $i++ @endphp
            @endforeach
        </div> 
    </div>
    <div class="margin-bottom-40"></div>
</div>
@endsection