<?php

namespace App\Http\Middleware;
use App\Models\Admin\Users;
use Illuminate\Contracts\Auth\Guard;

use Closure;
class Check
{
	protected $auth;

	public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next, $role)
    {
    	if ($this->auth->user()->status != $role) {
            return redirect('accountpending');
        }

        return $next($request); 
    }


}