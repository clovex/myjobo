<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jobdata;
use App\Models\Jobs;
use App\Models\Admin\Users;


class JobsapiController extends Controller
{

    public function store(Request $request)
    {
        $userscheck = Users::where('email',$request->email)->count();
        if($userscheck != 0)
        { 
            $user = Users::where('email',$request->email)->get(['id','status']);   
            foreach ($user as $u) {
                $ids = $u->id;
                $status = $u->status;
            }
            if($status == 1)
            {    
                $jobs = new Jobdata;
                $string = str_replace(' ', '-', strtolower($request->job_title));
                $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $slugdata2 = str_replace('--', '-', $slugdata1);
                $slugdata = str_replace('--', '-', $slugdata2);
                $seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ'); 
                shuffle($seed); 
                $rand = '';
                foreach (array_rand($seed, 7) as $k) $rand .= $seed[$k];
                       
                $chk = Jobs::where('slug',$slugdata)->count();
                if($chk == 0)
                {
                    $slug = $slugdata;
                }
                else
                {
                    $slug = $slugdata.'-'.$rand;
                }
                $jobs->title = $request->job_title;
                $jobs->city = $request->city;
                $jobs->district = $request->district;
                $jobs->country = $request->country;
                $jobs->job_cat = $request->job_cat;
                $jobs->deadline = $request->deadline;
                $jobs->emp_type = $request->emp_type;
                $jobs->salary = $request->salary;
                $jobs->des = $request->description;
                $jobs->slug = $slug;
                $jobs->user_id = $ids;
                $jobs->company_name = $request->company_name;
                $jobs->contact_name = $request->contact_name;
                $jobs->number = $request->number;
                $jobs->email = $request->email;
                $jobs->website = $request->website;
                $jobs->broadBeanID = $request->broadBeanID;
                $jobs->save();
                return response(array(
                        'error' => false,
                        'message' =>'Job Posted Successfully',
                       ),200);
            }
            else
            {
                return response(array(
                        'error' => true,
                        'message' =>"User don't have Permission",
                       ),202);   
            }    
        }
        else
        {
            return response(array(
                    'error' => true,
                    'message' =>'User Not in Our Records',
                   ),201);   
        }    
    }


    public function storelive(Request $request)
    {
        $userscheck = Users::where('email',$request->email)->count();
        if($userscheck != 0)
        { 
            $user = Users::where('email',$request->email)->get(['id','status']);   
            foreach ($user as $u) {
                $ids = $u->id;
                $status = $u->status;
            }
            if($status == 1)
            {    
                $jobs = new Jobs;
                $string = str_replace(' ', '-', strtolower($request->job_title));
                $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                $slugdata2 = str_replace('--', '-', $slugdata1);
                $slugdata = str_replace('--', '-', $slugdata2);
                $seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ'); 
                shuffle($seed); 
                $rand = '';
                foreach (array_rand($seed, 7) as $k) $rand .= $seed[$k];
                       
                $chk = Jobs::where('slug',$slugdata)->count();
                if($chk == 0)
                {
                    $slug = $slugdata;
                }
                else
                {
                    $slug = $slugdata.'-'.$rand;
                }
                $jobs->title = $request->job_title;
                $jobs->city = $request->city;
                $jobs->district = $request->district;
                $jobs->country = $request->country;
                $jobs->job_cat = $request->job_cat;
                $jobs->deadline = $request->deadline;
                $jobs->emp_type = $request->emp_type;
                $jobs->salary = $request->salary;
                $jobs->des = $request->description;
                $jobs->slug = $slug;
                $jobs->user_id = $ids;
                $jobs->company_name = $request->company_name;
                $jobs->contact_name = $request->contact_name;
                $jobs->number = $request->number;
                $jobs->email = $request->email;
                $jobs->website = $request->website;
                $jobs->broadBeanID = $request->broadBeanID;
                $jobs->save();
                return response(array(
                        'error' => false,
                        'message' =>'data created successfully',
                       ),200);
            }
            else
            {
                return response(array(
                        'error' => true,
                        'message' =>'User Not Permission',
                       ),202);   
            }    
        }
        else
        {
            return response(array(
                    'error' => true,
                    'message' =>'User Not Our Record',
                   ),201);   
        }    
    }
    
    public function display()
    {
        $jobs = Jobdata::all();
        return view('common.jobdata', compact('jobs'));
    }
}

