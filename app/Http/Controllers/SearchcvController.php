<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Models\Cvs;
use App\Models\Job_category;
use App\Models\District;
use DB;
use App\Models\Admin\Users;


class SearchcvController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
        $job_category = Job_category::all();
        $district = District::all();
    	return view('pages.employer.cv.index',compact('job_category','district'));
    }

    public function searchcv(Request $request)
    {
        $cvs = DB::table('resume')
                ->join('users', 'resume.user_id', '=', 'users.id')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->where('role_user.role_id','2')
                ->where(function ($query) use ($request) {
                        $query->where('resume.job_title','like','%'.$request->jobs.'%')
                                ->where('resume.job_category','like','%'.$request->category.'%')
                                ->where('resume.district','like','%'.$request->location.'%');
                    })
                ->select('users.id','users.first_name','users.last_name','resume.job_title','resume.resume','users.image')
                ->limit(200)
                ->paginate(16);
        $job_category = Job_category::all();
        $district = District::all();
        return view('pages.employer.cv.search',compact('job_category','district','cvs'));
    }

    public function profile($id)
    {
        $users = Users::where('id', $id)->with('resume','letter')->get();
        return view('pages.employer.cv.profile',compact('users'));  
    }
}