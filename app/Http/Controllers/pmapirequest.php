<?php
namespace App\Http\Controllers;

include('pmapiauth.php');
include('pmapiendpoint.php');


class PMAPIRequest
{
	protected $Auth;
	protected $version;

	protected $server = 'api.sign-up.to';
	protected $debugMode = false;


	public function __construct(PMAPIAuth $Auth, $version = 1, $server = null, $debugMode = null)
	{
		$this->version = $version;	// will be validated in PMAPIEndpoint
		$this->Auth = $Auth;

		if(!is_null($server))
			$this->server = $server;

		if(!is_null($debugMode))
			$this->debugMode = (bool) $debugMode;
	}


	public function __get($endpoint)
	{
		return new PMAPIEndpoint($this->Auth, $this->server, $this->version, $endpoint,
									$this->debugMode);
	}
}

