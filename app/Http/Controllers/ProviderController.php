<?php

namespace App\Http\Controllers;
use Input;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Admin\Users; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use DB;

class ProviderController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth'); 
    }
    // Profile
    public function index(Request $request)
    {
    	$jobseeker = array();
        if($request->hasFile('image')) 
        {
            $photos = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $photos);
            $jobseeker['image'] = $photos;
        }
        $jobseeker['email'] = $request->email;
        $jobseeker['name'] = $request->companyname;
        $jobseeker['contact_name'] = $request->contact_name;
        $jobseeker['website'] = $request->website;
        $jobseeker['address'] = $request->address;
        $jobseeker['phone'] = $request->phone;
        
        $pages1 = Users::where('id',$request->id)->update($jobseeker);
        return redirect()->back();

    }
}