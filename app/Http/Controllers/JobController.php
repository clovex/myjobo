<?php

namespace App\Http\Controllers;
use Input;

use Illuminate\Http\Request;
use App\Models\Jobs;
use App\Models\Job_category;

use Carbon\Carbon;
class JobController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
    	$job_category = Job_category::orderBy('id', 'asc')->get();
    	return view('pages.user.jobs.search',compact('job_category'));
    }

    public function searchdata()
    {
    	$data = Input::all();

    	$title = $data['job'];
    	$category = $data['category'];

    	$date = Carbon::now()->format('Y-m-d');
       		$jobs = Jobs::where([
        		['deadline', '>=', $date],
        		['job_cat', 'like', '%'.$category.'%'],
        		['title', 'like', '%'.$title.'%']
        	])->orderBy('id', 'desc')->get();

       	foreach($jobs as $j)
        {    
            echo'<li class="setborder">';
            if($j->logo != null)
            {    
                echo '<img src="'.url('public/uploads').'/'.$j->logo.'" alt="">';
            }
            else
            {    
                echo '<img src="'.url('public/uploads/company.png').'" alt="">';
            }

            echo'<div class="job-list-content">
                    <h4>'.$j->title.'</h4>
                    <div class="job-icons">
                        <span><i class="fa fa-calendar"> </i>'.Carbon::parse($j->created_at)->format('jS F, Y').'</span>
                        <span><i class="fa fa-map-marker"> </i>'.$j->city.'</span>
                        
                        <div class="row" style="margin: 0px; margin-top: 5px;">
                            <div class="col-md-6" style="padding: 0px;">
                                <div class="col-md-4">
                                    <span>Share On: </span>
                                </div>    
                                <div class="col-md-2">
                                <a class="setfacebook" style="width: auto; height: auto; color: #fff;text-decoration: none; padding: 7px 10px;" href="https://www.facebook.com/sharer/sharer.php?u='.url('job').'/'.$j->slug.'" target="_blank">
                                    
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                </div>
                                <div class="col-md-2">
                                <a class="settwitter" style="width: auto; height: auto; color: #fff;text-decoration: none; padding: 7px;" href="http://twitter.com/share?text='.url('job').'/'.$j->slug.'" target="_blank">
                                    
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                                </div>
                            </div>
                            
                            <div class="col-md-6 text-right" style="padding: 0px;">
                            
                                <a href="'.url('job').'/'.$j->slug.'" class="btn btn-info" style="width: auto; height: auto; color: #fff;text-decoration: none;">View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                </li>';
        }	

    }


}
