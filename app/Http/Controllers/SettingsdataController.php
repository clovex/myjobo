<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Userdata;
use App\Models\Resumedata;
use App\Models\Job_category;

use App\Models\Admin\Users;
use App\Models\Admin\Role;
use App\Models\Cvs;


class SettingsdataController extends Controller
{

    public function userset()
    {
    	$user = Userdata::orderby('id','ASC')->get();
    	$i=1;
        $j=1;
        $k=1;
    	foreach ($user as $u) 
    	{
    		if($u->user_type == 2)
    		{
                /*$chk = Users::where('email',$u->email)->count();
    			if($chk == 0)
                {    
                    $users = new Users;
    			    $users->id = $u->id;
    			    $users->first_name = $u->first_name;
    			    $users->last_name = $u->last_name;
    			    $users->email = $u->email;
    			    $users->password = $u->password;
    			    $users->activated = 1;
    			    $users->dob = $u->dob;
    			    $users->phone = $u->phone;
    			    $users->city = $u->city;
    			    $users->dist = $u->district;
    			    $users->job_category = $u->job_category;
    			    $users->save();

    			    $roles = new Role;
    			    $roles->role_id = 2;
    			    $roles->user_id = $u->id;
    			    $roles->save();

                    $chkcv = Cvs::where('user_id',$u->id)->count();
                    if($chkcv == 0)
                    {
                        $cv = Cvs::create(['user_id' => $u->id ]);
                    }
                $j++;
                echo "jobseeker = ".$j."<br>";        
                }
                */
    		}
    		else
    		{
                $chk = Users::where('email',$u->email)->count();
                if($chk == 0)
                { 
    			$users = new Users;
    			$users->id = $u->id;
    			$users->name = $u->company_name;
    			$users->email = $u->email;
    			$users->password = $u->password;
    			$users->activated = 1;
    			$users->image = $u->logo;
    			$users->contact_name = $u->contact_name;
    			$users->website = $u->website;
    			$users->district = $u->district;
    			$users->country = $u->country;
    			$users->address = $u->address;
    			$users->description = $u->description;
    			$users->sector = $u->sector;
    			$users->front_logo = $u->front_logo;
    			$users->phone = $u->phone;
                $users->status =1;
    			$users->save();

    			$roles = new Role; 
    			$roles->role_id = 4;
    			$roles->user_id = $u->id;
    			$roles->save();	
                $k++;
                echo "emp =".$k."<br>";
                }
    		  
            }	
    		$i++;
            echo "Total = ".$i."<br>";
    	}
    	
    }


    public function resumeset()
    {
    	$resumedata = Resumedata::orderby('id','ASC')->get();
    	$i=1;
    	foreach ($resumedata as $rd) 
    	{
    		$jc = Job_category::where('name',$rd->job_category)->get();
			$jobcategory = null;    		
    		foreach ($jc as $j) {

    			$jobcategory = $j->id;
    		}
    		
    		$cv = new Cvs;

    		$cv->user_id = $rd->user_id;
    		$cv->job_title = $rd->job_title;
    		$cv->job_category = $jobcategory;
    		$cv->city = $rd->city;
    		$cv->district = $rd->district;
    		$cv->country = $rd->country;
    		$cv->objective = $rd->objective;
    		$cv->work_exeperence = $rd->work_exeperence;
    		$cv->total_exp = $rd->total_exp;
    		$cv->education = $rd->education;
    		$cv->skill = $rd->skill;
    		$cv->salary = $rd->salary;
    		$cv->salary_type = $rd->salary_type;
    		$cv->job_type = $rd->job_type;
    		$cv->resume = $rd->resume;
			$cv->save();

			$i++;
    	}
    	echo $i."<br>";
    }

}