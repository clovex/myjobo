<?php

namespace App\Http\Controllers\Auth;

use App;
use App\Models\User;
use App\Models\Cvs;
use App\Traits\CaptchaTrait;
use App\Traits\CaptureIpTrait;
use App\Traits\ActivationTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use jeremykenedy\LaravelRoles\Models\Permission;
use jeremykenedy\LaravelRoles\Models\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use ActivationTrait;
    use CaptchaTrait;
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/activate';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', [
            'except' => 'logout'
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $data['captcha'] = $this->captchaCheck();

        if (App::environment('local')) {
           $data['captcha'] = true;
        }
        if($data['type'] == 2)
        {    
            return Validator::make($data,
            [
                'name'                  => 'required|max:255',
                'first_name'            => '',
                'last_name'             => '',
                'email'                 => 'required|email|max:255|unique:users',
                'password'              => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
                'type'                  => 'required',
                'g-recaptcha-response'  => '',
                'phone'                 => 'required|regex:/[0-9]/',
                'city'                  => 'required',
                'district'              => 'required',
                'captcha'               => 'required|min:1'
            ], 
            [
                'name.required'                 => trans('auth.userNameRequired'),
                'first_name.required'           => trans('auth.fNameRequired'),
                'last_name.required'            => trans('auth.lNameRequired'),
                'email.required'                => trans('auth.emailRequired'),
                'email.email'                   => trans('auth.emailInvalid'),
                'password.required'             => trans('auth.passwordRequired'),
                'password.min'                  => trans('auth.PasswordMin'),
                'password.max'                  => trans('auth.PasswordMax'),
                'type.required'                 => trans('auth.UserTypeRequired'),
                'phone.required'                => trans('auth.phoneRequired'),
                'city.regex'                    => trans('auth.cityRequired'),
                'district.regex'                => trans('auth.districtRequired'),
                'g-recaptcha-response.required' => trans('auth.captchaRequire'),
                'captcha.min'                   => trans('auth.CaptchaWrong')
            ]
            );
        }

        if($data['type'] == 4)
        { 
            return Validator::make($data,
            [
                'name'                  => 'required|max:255',
                'first_name'            => '',
                'last_name'             => '',
                'email'                 => 'required|email|max:255|unique:users',
                'password'              => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
                'type'                  => 'required',
                'g-recaptcha-response'  => '',
                'phone'                 => 'required|regex:/[0-9]/',
                'contact_name'          => 'required',
                'address'               => 'required',
                'district'               => 'required',
                'captcha'               => 'required|min:1'
            ], 
            [
                'name.required'                 => trans('auth.userNameRequired'),
                'first_name.required'           => trans('auth.fNameRequired'),
                'last_name.required'            => trans('auth.lNameRequired'),
                'email.required'                => trans('auth.emailRequired'),
                'email.email'                   => trans('auth.emailInvalid'),
                'password.required'             => trans('auth.passwordRequired'),
                'password.min'                  => trans('auth.PasswordMin'),
                'password.max'                  => trans('auth.PasswordMax'),
                'type.required'                 => trans('auth.UserTypeRequired'),
                'phone.required'                => trans('auth.phoneRequired'),
                'contact_name.required'         => trans('auth.contact_nameRequired'),
                'address.required'              => trans('auth.addressRequired'),
                'district.required'              => trans('auth.districtRequired'),
                'g-recaptcha-response.required' => trans('auth.captchaRequire'),
                'captcha.min'                   => trans('auth.CaptchaWrong')
            ]
            );

        }

        if($data['type'] == 6)
        {
            return Validator::make($data,
            [
                'name'                  => 'required|max:255',
                'first_name'            => '',
                'last_name'             => '',
                'email'                 => 'required|email|max:255|unique:users',
                'password'              => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
                'type'                  => 'required',
                'g-recaptcha-response'  => '',
                'captcha'               => 'required|min:1'
            ], 
            [
                'name.required'                 => trans('auth.userNameRequired'),
                'first_name.required'           => trans('auth.fNameRequired'),
                'last_name.required'            => trans('auth.lNameRequired'),
                'email.required'                => trans('auth.emailRequired'),
                'email.email'                   => trans('auth.emailInvalid'),
                'password.required'             => trans('auth.passwordRequired'),
                'password.min'                  => trans('auth.PasswordMin'),
                'password.max'                  => trans('auth.PasswordMax'),
                'type.required'                 => trans('auth.UserTypeRequired'),
                'g-recaptcha-response.required' => trans('auth.captchaRequire'),
                'captcha.min'                   => trans('auth.CaptchaWrong')
            ]
            );

        }
        
        if($data['type'] == 5)
        {
            return Validator::make($data,
            [
                'name'                  => 'required|max:255',
                'first_name'            => '',
                'last_name'             => '',
                'email'                 => 'required|email|max:255|unique:users',
                'phone'                 => 'required|regex:/[0-9]/',
                'speciality'            => 'required',
                'password'              => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
                'type'                  => 'required',
                'g-recaptcha-response'  => '',
                'captcha'               => 'required|min:1',
                'district'              => 'required',
            ], 
            [
                'name.required'                 => trans('auth.userNameRequired'),
                'first_name.required'           => trans('auth.fNameRequired'),
                'last_name.required'            => trans('auth.lNameRequired'),
                'email.required'                => trans('auth.emailRequired'),
                'email.email'                   => trans('auth.emailInvalid'),
                'phone.required'                => trans('auth.phoneRequired'),
                'speciality.required'              => trans('auth.specialityRequired'),
                'password.required'             => trans('auth.passwordRequired'),
                'password.min'                  => trans('auth.PasswordMin'),
                'password.max'                  => trans('auth.PasswordMax'),
                'type.required'                 => trans('auth.UserTypeRequired'),
                'g-recaptcha-response.required' => trans('auth.captchaRequire'),
                'captcha.min'                   => trans('auth.CaptchaWrong'),
                'district.required'              => trans('auth.districtRequired'),
            ]
            );

        }    
        



    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $ipAddress  = new CaptureIpTrait;
        $role       = $data['type']; //Role::where('name', '=', 'Unverified')->first();
        if($data['type'] == 2)
        { 
            $user =  User::create([
                'name'              => $data['name'],
                'first_name'        => $data['first_name'],
                'last_name'         => $data['last_name'],
                'email'             => $data['email'],
                'password'          => md5($data['password']),
                'token'             => str_random(64),
                'signup_ip_address' => $ipAddress->getClientIp(),
                'phone'             => $data['phone'],
                'city'              => $data['city'],
                'dist'              => $data['district'],
                'activated'         => !config('settings.activation')
            ]);
        }
        if($data['type'] == 4)
        {   
            $user =  User::create([
                'name'              => $data['name'],
                'first_name'        => $data['first_name'],
                'last_name'         => $data['last_name'],
                'email'             => $data['email'],
                'password'          => md5($data['password']),
                'token'             => str_random(64),
                'signup_ip_address' => $ipAddress->getClientIp(),
                'phone'             => $data['phone'],
                'contact_name'      => $data['contact_name'],
                'address'           => $data['address'],
                'district'          => $data['district'],
                'website'           => $data['website'],
                'activated'         => !config('settings.activation')
            ]);
        }
        if($data['type'] == 6)
        { 
            $user =  User::create([
                'name'              => $data['name'],
                'first_name'        => $data['first_name'],
                'last_name'         => $data['last_name'],
                'email'             => $data['email'],
                'password'          => md5($data['password']),
                'token'             => str_random(64),
                'signup_ip_address' => $ipAddress->getClientIp(),
                'activated'         => !config('settings.activation')
            ]);
        }

        if($data['type'] == 5)
        { 
           
            $user =  User::create([
                'name'              => $data['name'],
                'first_name'        => $data['first_name'],
                'last_name'         => $data['last_name'],
                'email'             => $data['email'],
                'phone'             => $data['phone'],
                'speciality'        => $data['speciality'],
                'city'              => $data['district'],
                'password'          => md5($data['password']),
                'token'             => str_random(64), 
                'signup_ip_address' => $ipAddress->getClientIp(),
                'activated'         => !config('settings.activation')
            ]);
        }


        $user->attachRole($role);
        $this->initiateEmailActivation($user);

        if($role == 2)
        {
            $cv = Cvs::create(['user_id' => $user->id ]);
        }    
        return $user;

    }
}
