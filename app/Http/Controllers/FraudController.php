<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Pages;

class FraudController extends Controller
{

    public function index()
    {
        $qdata = array('page_name' => 'fraud', 'element' => 'content' );
        $pages = Pages::where($qdata)->get();
    	return view('common.fraud',compact('pages'));
    }

}
