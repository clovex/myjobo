<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Staff;
use App\Models\Position;
use App\Models\Admin\Users; 
use App\Models\Industry;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Mail;

class StaffController extends Controller
{
    public function index(Request $request)
    {
    	
    	$user = Auth::user();
		$staff = new Staff;
    	$staff->provider_id = $user->id;
    	$staff->industry = $request->industry;
    	$staff->position = $request->position;
    	$staff->staff = $request->staff;
    	$staff->description = $request->description;
    	$staff->age1 = $request->age1;
    	$staff->age2 = $request->age2;
    	$staff->gender = $request->gender;
    	$staff->experience = $request->experience;
    	$staff->education = $request->education;
    	$staff->salary1 = $request->salary1."-".$request->salary1type;
    	//$staff->salary2 = $request->salary2."-".$request->salary2type;
    	$staff->interview_date = $request->interview_date;
    	$staff->interview_time = $request->interview_time1.':'.$request->interview_time2;
    	$staff->deadline = $request->deadline;
    	$staff->save();
        
        $industry = Industry::where('id',$request->industry)->get();
        foreach ($industry as $ind) {
            $indname = $ind->name;
        }
        
        $position = Position::where('id',$request->position)->get();
        foreach ($position as $pos) {
            $posname = $pos->name;
        }

        $email = "rajnik@xemesolutions.com";
        $data['company'] = $user->name;
        $data['email'] = $user->email;
        $data['industry'] = $indname;
        $data['position'] = $posname;
        $data['staff'] = $request->staff;
        $data['interview'] = $request->interview_date.' '.$request->interview_time1.':'.$request->interview_time2;
        $data['deadline'] = $request->deadline;

        Mail::send('emails.staff',['data' => $data], function($message) use($email)
        {
            $message->to($email, 'Myjobo')->subject("Request Staff");
        });

    	return redirect('managerequest');   
    }

    public function managerequest()
    {
       $user = Auth::user();
       $staff = Staff::where('provider_id',$user->id)->paginate(15);
       $industry = Industry::all();
       $position = Position::all();
       return view('pages.provider.staff.manage',compact('staff','position','industry'));    
    }

    public function deleterequest($id)
    {
        $featured = Staff::where('id',$id);    
        $featured->delete();
        return redirect()->back();  
    }

    public function singlerequest($id)
    {
       $staff = Staff::where('id',$id)->get();
       $industry = Industry::all();
       $position = Position::all();
       return view('pages.provider.staff.edit',compact('staff','position','industry'));
    }

    public function updaterequest(Request $request)
    {
        $data = array(
            'provider_id' => $request->provider_id,
            'industry' => $request->industry,
            'position' => $request->position,
            'staff' => $request->staff,
            'description' => $request->description,
            'age1' => $request->age1,
            'age2' => $request->age2,
            'gender' => $request->gender,
            'experience' => $request->experience,
            'education' => $request->education,
            'salary1' => $request->salary1."-".$request->salary1type,
            //'salary2' => $request->salary2."-".$request->salary2type,
            'interview_date' => $request->interview_date,
            'interview_time' => $request->interview_time1.':'.$request->interview_time2,
            'deadline' => $request->deadline,
            'description' => $request->description
        );
        $pages1 = Staff::where('id',$request->id)->update($data);
        return redirect('managerequest');
    }

    public function deletevetted()
    {
        $user_id = Auth::user()->id;

        $featured = Users::where('id',$user_id);    
        $featured->delete();

        $user = Auth::user();
        Log::info('User Logged Out. ', [$user]); 
        Auth::logout();
        Session::flush();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');    
    }

}
