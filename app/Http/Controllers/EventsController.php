<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Coursescategory;
use App\Models\Courses;
use DB;

class EventsController extends Controller
{

    public function index($id)
    {
    	$courses = Courses::where('category',$id)->paginate(20);
    	$coursescategory = Coursescategory::all();
        return view('common.events',compact('courses','coursescategory'));
    }

    public function single($id)
    {
    	$courses = Courses::where('id',$id)->get();
    	$coursescategory = Coursescategory::all();
        return view('common.eventssingle',compact('courses','coursescategory'));	
    }

    public function searchevent(Request $request)
    {

        $courses = DB::table('courses')
                ->join('coursescategory', 'courses.category', '=', 'coursescategory.id')
                ->where(function ($query) use ($request) {
                        $query->where("courses.name", 'like', '%'.$request->keyword.'%')
                        ->orwhere("courses.qualification", 'like', '%'.$request->keyword.'%')
                        ->orwhere("coursescategory.name", 'like', '%'.$request->keyword.'%');
                    })
                ->select('courses.*')
                ->paginate(15);
        $coursescategory = Coursescategory::all();
        return view('common.eventssearch',compact('courses','coursescategory'));
    }
}