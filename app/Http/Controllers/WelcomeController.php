<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jobs;
use Carbon\Carbon;
use App\Models\Job_category;
use DB;
use App\Models\Cvs;
use App\Models\Testimonials;
use App\Models\Admin\Role;
use App\Models\Admin\Users;
use App\Models\Tracker;
use App\Models\Admin\Pages;
use App\Models\District;


class WelcomeController extends Controller
{

    public function __construct()
    {
        Tracker::hit();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        $t=url(''); 
        if($t == "https://myjobo.com/myjobo")
        {
            return redirect()->back();
        }    
        
    	$date = Carbon::now()->format('Y-m-d');
    	$jobs = Jobs::where('deadline', '>=', $date)->orderBy('id', 'desc')->limit(10)->get(['logo','created_at','city','slug','title','deadline']);
        $category = DB::table('tbl_job_post')
                ->join('tbl_categories', 'tbl_job_post.job_cat', '=', 'tbl_categories.id')
                ->select('tbl_categories.name','tbl_categories.id')
                ->where('tbl_job_post.deadline','>=',$date)
                ->where('tbl_categories.id','!=','0')
                ->groupBy('tbl_job_post.job_cat')
                ->limit(5)
                ->get();
        
        $dist = DB::table('tbl_job_post')
                ->join('tbl_district', 'tbl_job_post.district', '=', 'tbl_district.id')
                ->select('tbl_district.name','tbl_district.id')
                ->where('tbl_job_post.deadline','>=',$date)
                ->groupBy('tbl_job_post.district')
                ->limit(5)
                ->get();

        
        $testimonials = Testimonials::all();
        $logos = Users::where('front_logo','1')->get(['image','id']); 
        
        $featured = DB::table('tbl_job_post')
                ->select('created_at','city','slug','title','logo')
                ->where('deadline','>=',$date)
                ->where('featured','1')
                ->orderBy('id', 'desc')
                ->get();
        return view('welcome',compact('jobs','category','dist','testimonials','logos','data','featured'));
    }


    public function searchjob(Request $request)
    {
        $searchby = $request->keyword;
        $date = Carbon::now()->format('Y-m-d');
        $jobs = DB::table('tbl_job_post')
                ->join('tbl_categories', 'tbl_job_post.job_cat', '=', 'tbl_categories.id')
                ->join('tbl_district', 'tbl_job_post.district', '=', 'tbl_district.id')
                ->where('tbl_job_post.deadline','>=',$date)
                ->where(function ($query) use ($request) {
                        $query->where("tbl_job_post.company_name", 'like', '%'.$request->keyword.'%')
                        ->orwhere("tbl_job_post.title", 'like', '%'.$request->keyword.'%')
                        ->orwhere("tbl_district.name", 'like', '%'.$request->keyword.'%')
                        ->orwhere("tbl_categories.name", 'like', '%'.$request->keyword.'%');
                    })
                ->paginate(15);

        $jobcategory = Job_category::get(['name','id']);
        $qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );
        $common = Pages::where($qdata1)->get();        
        $district = District::all();
        return view('common.jobs', compact('jobs','jobcategory','common','searchby','district'));
    }

    public function searchjoblocation($id)
    {
        $ids = 0;
        $disname = District::where('name',$id)->get();
        foreach ($disname as $dname) {
            $name = $dname->name;
            $ids = $dname->id;
        }
        $searchby = $id;
        $date = Carbon::now()->format('Y-m-d');
        $jobs = Jobs::where('district',$ids)
                ->where('deadline','>=',$date)
                ->paginate(15);
        $metatital = "Find jobs in ".$id."|Myjobo.com"; 
        $metades = "Find a job in ".$id." on Myjobo.com. All new job vacancies in ".$id." are just one click away!";       
        $jobcategory = Job_category::get(['name','id']);
        $qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );
        $common = Pages::where($qdata1)->get();        
        $district = District::all();
        return view('common.jobs', compact('jobs','jobcategory','common','searchby','district','metatital','metades'));
    }

    public function searchjobcategory($id)
    {
        $catname = Job_category::where('id',$id)->get();
        foreach ($catname as $cn) {
            $name = $cn->name;
        }
        $searchby = $name;
         $date = Carbon::now()->format('Y-m-d');
         $jobs = DB::table('tbl_job_post')
                ->join('tbl_categories', 'tbl_job_post.job_cat', '=', 'tbl_categories.id')
                ->where('tbl_job_post.deadline','>=',$date)
                ->where('tbl_categories.id',$id)
                ->paginate(15);
                
        $jobcategory = Job_category::get(['name','id']);
        $qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );
        $common = Pages::where($qdata1)->get();        
        $district = District::all();
        return view('common.jobs', compact('jobs','jobcategory','common','searchby','district'));
    }

}
