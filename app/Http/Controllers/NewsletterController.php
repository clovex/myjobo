<?php

namespace App\Http\Controllers;
use Input;
use Illuminate\Http\Request;
use App\Models\Newsletter;

class NewsletterController extends Controller
{

    public function send()
    {
    	$data = Input::all();
    	$news = Newsletter::where('email',$data['email'])->count();
    	if($news == 0)
    	{	
	    	
	    	$newsletter = new Newsletter;
	    	$newsletter->email = $data['email'];
	    	$newsletter->name = $data['name'];
	    	$newsletter->save();
	    	return 1;
    	}
    	else
    	{
    		return 0;	
    	}	
    }

}
