<?php

namespace App\Http\Controllers;
use Input;
use Illuminate\Http\Request;
use App\Models\Jobs;
use App\Models\Job_category;
use Carbon\Carbon;
use App\Models\District;
use App\Models\Country;
use App\Models\Employment_type;
use App\Models\Admin\Users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use DB;
use Mail;
use Twitter;
use File;

class EmployerController extends Controller
{
	public function __construct()
    {

        $this->middleware('auth'); 
        
    }
    // Profile
    public function profile(Request $request)
    {
        $jobseeker = array();
         if($request->hasFile('image')) 
        {
            $photos = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $photos);
            $jobseeker['image'] = $photos;
        }
        $jobseeker['email'] = $request->email;
        $jobseeker['name'] = $request->companyname;
        $jobseeker['contact_name'] = $request->contact_name;
        $jobseeker['website'] = $request->website;
        $jobseeker['district'] = $request->district;
        $jobseeker['country'] = $request->country;
        $jobseeker['address'] = $request->address;
        $jobseeker['phone'] = $request->phone;
        $jobseeker['description'] = $request->description;

        $pages1 = Users::where('id',$request->id)->update($jobseeker);
        return redirect()->back();
    }

    //Delete Employer

    public function deleteemployer()
    {
        $user_id = Auth::user()->id;

        $featured = Users::where('id',$user_id);    
        $featured->delete();

        $user = Auth::user();
        Log::info('User Logged Out. ', [$user]); 
        Auth::logout();
        Session::flush();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');    
    }


    // Jobs Add 
    public function index()
    {
        $district = District::all();
        $country = Country::all();
        $job_category = Job_category::orderBy('name', 'ASC')->get();
        $employment_type = Employment_type::orderBy('id', 'ASC')->get();
    	return view('pages.employer.jobs.add', compact('district','country','job_category','employment_type'));
    }

    public function postjobs(Request $request)
    {
        $jobs = new Jobs;

        $string = str_replace(' ', '-', strtolower($request->job_title));
        $slugdata1=preg_replace('/[^A-Za-z0-9()\-]/', '', $string);
        $slugdata2 = str_replace('--', '-', $slugdata1);
        $slugdata = str_replace('--', '-', $slugdata2);
        $seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 7) as $k) $rand .= $seed[$k];
               
        $chk = Jobs::where('slug',$slugdata)->count();
        if($chk == 0)
        {
            $slug = $slugdata;
        }
        else
        {
            $sum = $chk+1; 
            $slug = $slugdata.'-'.$rand;
        }

        if($request->hasFile('logo')) 
        {
            $photos = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $photos);
            $jobs->logo = $photos;
        }
        $jobs->title = $request->job_title;
        $jobs->city = $request->city;
        $jobs->district = $request->district;
        $jobs->country = $request->country;
        $jobs->job_cat = $request->job_cat;
        $jobs->deadline = $request->deadline;
        $jobs->emp_type = $request->emp_type;
        $jobs->salary = $request->salary;
        $jobs->des = $request->description;
        $jobs->slug = $slug;


        $jobs->user_id = $request->user_id;
        $jobs->company_name = $request->company_name;
        $jobs->contact_name = $request->contact_name;
        $jobs->number = $request->number;
        $jobs->email = $request->email;
        $jobs->website = $request->website;
        $jobs->save();

        $subjects = "Your Job Advert has been posted on Myjobo.com";
        $email = $request->email;
        $data['slug'] = $slug;

        Mail::send('emails.employes',['data' => $data], function($message) use($email,$subjects)
                {
                $message->to($email, 'Myjobo')->subject($subjects);
                });
        
        $subject = "Job Posted";
        $emails = "info@myjobo.com";
        $datatwo['company'] = $request->company_name;
        $datatwo['email'] = $request->email;
        $datatwo['title'] = $request->job_title;
        Mail::send('emails.adminsend',['data' => $datatwo], function($message) use($emails,$subject)
                {
                $message->to($emails, 'Myjobo')->subject($subject);
                });
        
        include("fb_class.php");
        $message = "A new job has been posted on www.myjobo.com";
        $titles=$request->job_title;
        $targetUrl = url('job').'/'.$slug;
        $imgUrl = url('frontend/images/fblogo.jpg');
        $description = strip_tags($request->description); 
        $facebook = new FacebookApi($facebookData);
        $facebook->share($message,$titles, $targetUrl, $imgUrl, $description, $access_token);

        $tweetMessage = 'Vacancy - '.$titles.'. Check now '.$targetUrl;
        $twitter = Twitter::post('statuses/update', array('status' => $tweetMessage));
        
        return redirect('home');   
    }




    // Manage Jobs
    public function managejob()
    {
        $user_id = Auth::user()->id;

        //$jobs = Jobs::where('user_id',$user_id)->orderBy('id', 'DESC')->with('getcount')->get();
        $jobs = DB::table('tbl_job_post')
                ->select('tbl_job_post.id','tbl_job_post.title','tbl_job_post.created_at','tbl_job_post.deadline','tbl_job_post.slug')
                ->where('tbl_job_post.user_id',$user_id)
                ->groupBy('tbl_job_post.id')
                ->paginate(15);
    	return view('pages.employer.jobs.manage',compact('jobs'));	
    }

    public function deletejob($slug)
    {
        $featured = Jobs::where('slug',$slug);    
        $featured->delete();
        return redirect()->back();  
    }
    public function editjob($slug)
    {
        $jobs = Jobs::where('slug',$slug)->get();
        $district = District::all();
        $country = Country::all();
        $job_category = Job_category::orderBy('id', 'ASC')->get();
        $employment_type = Employment_type::orderBy('id', 'ASC')->get();
        return view('pages.employer.jobs.edit', compact('district','country','job_category','employment_type','jobs'));
    }
    public function updatejob(Request $request)
    {
        $jobs = array();
        if($request->hasFile('logo')) 
        {
            $photos = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $photos);
            $jobs['logo'] = $photos;
        }
        $jobs['title'] = $request->job_title;
        $jobs['city'] = $request->city;
        $jobs['district'] = $request->district;
        $jobs['country'] = $request->country;
        $jobs['job_cat'] = $request->job_cat;
        $jobs['deadline'] = $request->deadline;
        $jobs['emp_type'] = $request->emp_type;
        $jobs['salary'] = $request->salary;
        $jobs['des'] = $request->description;

        $pages1 = Jobs::where('id',$request->id)->update($jobs);
        return redirect('managejob');
    }


}
