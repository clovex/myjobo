<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Speciality;
use DB;
use App\Models\District;
use App\Models\Portfolio;

class TradesController extends Controller
{

    public function index()
    {
    	$trade = DB::table('users')
                ->join('speciality','users.speciality', '=', 'speciality.id')
                ->select('users.*','speciality.name as sname')
                ->orderBy('id', 'desc')
                ->paginate(20);
    	$speciality = Speciality::all();
    	$city = District::all();
    	return view('common.trades',compact('speciality','trade','city'));
    }

    public function single($id,$name)
    {
    	$trade = DB::table('users')
                ->join('speciality','users.speciality', '=', 'speciality.id')
                ->select('users.*','speciality.name as sname')
                ->where('users.id',$id)
                ->get();
    	$speciality = Speciality::all();
    	$city = District::all();
        $portfolio = Portfolio::where('userid',$id)->get();
        $portfoliocount = Portfolio::where('userid',$id)->count();
    	return view('common.tradesingle',compact('speciality','trade','city','portfolio','portfoliocount'));	
    }

    public function search(Request $request)
    {
    	$searchby = $request->keyword;
    	$trade = DB::table('users')
                ->join('speciality','users.speciality', '=', 'speciality.id')
                ->where("speciality.name", 'like', '%'.$request->keyword.'%')
                ->select('users.*','speciality.name as sname')
                ->orderBy('id', 'desc')
                ->paginate(20);

        $speciality = Speciality::all();
    	$city = District::all();
    	return view('common.trades',compact('speciality','trade','city','searchby'));
    }

    public function searchdata(Request $request)
    {
    	$tradename = $request->trade;
    	$locationname = $request->location;
    	$trade = DB::table('users')
                ->join('speciality','users.speciality', '=', 'speciality.id')
                ->where("speciality.name", 'like', '%'.$request->trade.'%')
                ->where("users.cities", 'like', '%'.$request->location.'%')
                ->select('users.*','speciality.name as sname')
                ->orderBy('id', 'desc')
                ->paginate(20);

        $speciality = Speciality::all();
    	$city = District::all();
    	return view('common.trades',compact('speciality','trade','city','tradename','locationname'));	
    }

    public function speciality($id)
    {
        $trade = DB::table('users')
                ->join('speciality','users.speciality', '=', 'speciality.id')
                ->select('users.*','speciality.name as sname')
                ->where('users.speciality',$id)
                ->orderBy('id', 'desc')
                ->paginate(20);
        $speciality = Speciality::all();
        $city = District::all();
        return view('common.trades',compact('speciality','trade','city'));
    }


}
