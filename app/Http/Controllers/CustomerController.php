<?php

namespace App\Http\Controllers;
use Input;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Admin\Users; 
use App\Models\Portfolio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use DB;

class CustomerController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth'); 
    }
    // Profile
    public function index(Request $request)
    {
    	$jobseeker = array();
        if($request->hasFile('image'))  
        {
            $photos = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $photos);
            $jobseeker['image'] = $photos;
        }
        
        $jobseeker['first_name'] = $request->first_name;
        $jobseeker['last_name'] = $request->last_name;
        $jobseeker['phone'] = $request->phone;
        $jobseeker['officephone'] = $request->officephone;
        $jobseeker['location'] = $request->location;
        $jobseeker['gender'] = $request->gender;
        $jobseeker['experience'] = $request->experience;
        $jobseeker['education'] = $request->education;
        $jobseeker['speciality'] = $request->speciality;
        $jobseeker['city'] = $request->city;
        $jobseeker['description'] = $request->description;
        $jobseeker['cities'] = implode(',',$request->cities);
        $jobseeker['modesPayment'] = implode(',',$request->paymentmode);
        $jobseeker['sunavi'] = $request->sunavi;
        $jobseeker['hoursOperation'] = $request->hoursOperationone."-".$request->hoursOperationtwo."-".$request->hoursOperationthree;

        $pages1 = Users::where('id',$request->id)->update($jobseeker);
        return redirect()->back();

    }

    public function delete()
    {
        $user_id = Auth::user()->id;

        $featured = Users::where('id',$user_id);    
        $featured->delete();

        $user = Auth::user();
        Log::info('User Logged Out. ', [$user]); 
        Auth::logout();
        Session::flush();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');    
    }

    public function portfolio()
    {
        $user_id = Auth::user()->id;
        $portfolio = Portfolio::where('userid',$user_id)->get();
        return view('pages.services.portfolio',compact('portfolio'));
    }

    public function portfoliosave(Request $request)
    {
        $user_id = Auth::user()->id;

        if($request->hasFile('image')) 
        {
            $photos = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $photos);
            $portfolio = new Portfolio;
            $portfolio->userid = $user_id;
            $portfolio->image = $photos;
            $portfolio->save();
        }
        return redirect()->back();
    }

    public function portfoliodelete($id)
    {
        $featured = Portfolio::where('id',$id);    
        $featured->delete();
        return redirect()->back();
    }
    
}