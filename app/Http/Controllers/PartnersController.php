<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Pages;
use App\Models\Partners;
use App\Models\Admin\Users;
use App\Models\Job_category;
use App\Models\District;
use DB;

class PartnersController extends Controller
{

    public function index()
    {
    	$qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );
        $common = Pages::where($qdata1)->get();
        $job_category = Job_category::all();
        $district = District::all();
        $partners = DB::table('tbl_sectors')
                ->join('users', 'tbl_sectors.id', '=', 'users.sector')
                ->select(DB::raw("COUNT(users.sector) as counts,tbl_sectors.name,tbl_sectors.slug"))
                ->groupBy('users.sector')
                ->orderby('tbl_sectors.name','ASC')
                ->get();
        //$partners = Partners::orderby('name','ASC')->get();

        return view('common.partners',compact('common','partners','job_category','district'));
    }

    public function getpartners($slug)
    {
    	$qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );
        $common = Pages::where($qdata1)->get();

		$partners = Partners::where('slug',$slug)->get();
		foreach ($partners as $par) 
		{
			$id = $par->id;
		}
		$users = Users::where('sector',$id)->get();    	
		return view('common.partner',compact('partners','users','common'));
    }

    public function getusers($id)
    {
    	$ids = base64_decode($id);

    	$qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );
        $common = Pages::where($qdata1)->get();
		$users = Users::where('id',$ids)->get();    	
		return view('common.partnersingle',compact('users','common'));
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $users = DB::table('users')
                ->where(function ($query) use ($request) {
                        $query->where("users.name", 'like', '%'.$request->keyword.'%');
                })
                ->paginate(15);
        $qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );        
        $common = Pages::where($qdata1)->get();
        return view('common.partnersearch',compact('users','common','keyword'));
    }

}
