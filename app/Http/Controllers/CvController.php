<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Models\Cvs;
use App\Models\Job_category;
use App\Models\District;
use App\Models\Country;

class CvController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index($user_id)
    {
    	//$user_id = Auth::user()->id;
    	$cvs = Cvs::where('user_id',$user_id)->get();
        $district = District::all();
        $country = Country::all();
    	$job_category = Job_category::orderBy('id', 'asc')->get();
    	return view('pages.user.cv.edit',compact('job_category','district','country','cvs'));
    }

    public function post(Request $request)
    {
    	$cv = array();
    	if($request->hasFile('resume')) 
        {
            $photos = time().$request->resume->getClientOriginalName();
            $request->resume->move(public_path('cv'), $photos);
            $cv['resume'] = $photos;
        }
        $cv['job_title'] = $request->job_title;
        $cv['job_category'] = $request->job_category;
        $cv['city'] = $request->city;
        $cv['district'] = $request->district;
        $cv['country'] = $request->country;
        $cv['objective'] = $request->objective;
        $cv['work_exeperence'] = $request->work_exeperence;
        $cv['total_exp'] = $request->total_exp;
        $cv['education'] = $request->education;
        $cv['skill'] = $request->skill;
        $cv['salary'] = $request->salary;
        $cv['salary_type'] = $request->salary_type;
        $cv['job_type'] = $request->job_type;

        $pages1 = Cvs::where('user_id',$request->user_id)->update($cv);
        return redirect('home');
    }


}
