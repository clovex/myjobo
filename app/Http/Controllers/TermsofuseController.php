<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Pages;

class TermsofuseController extends Controller
{

    public function index()
    {
    	$qdata = array('page_name' => 'terms_of_use', 'element' => 'data' );
    	$pages = Pages::where($qdata)->get();
        return view('common.termsofuse', compact('pages'));
    }

}
