<?php

namespace App\Http\Controllers;
use Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Jobs;
use App\Models\Job_category;
use App\Models\Apply;
use Mail;
use App\Models\Cvs; 


use Carbon\Carbon;
class ApplicationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
        $user_id = Auth::user()->id;
        $jobs = Apply::with('job')->where('user_id',$user_id)->get(); 
        $cvs = Cvs::where('user_id',$user_id)->get();
        return view('pages.user.applications.index',compact('jobs','cvs'));
    }
    public function apply(Request $request)
    {
        $broadBeanID = null;
        $jobs = Jobs::where('slug',$request->slug)->get();
        foreach ($jobs as $j) {
            $job_id = $j->id;
            $broadBeanID = $j->broadBeanID;
        }
    
        $user_id = Auth::user()->id;
        $apply = new Apply;
        $apply->job_id = $job_id;
        $apply->user_id = $user_id;
        $apply->letter = $request->letter;
        if($request->hasFile('cv')) 
        {
            $photos = time().$request->cv->getClientOriginalName();
            $request->cv->move(public_path('cv'), $photos);
            $apply->cv = $photos;
        }
        $apply->save();
    
        if($broadBeanID != null)
        {
            ///$broadBeanID = "rajnik@xemesolutions.com"; 
            $pathToFile = null;
            $resumes = null;
            $resumes = $photos;
            $data['email'] = Auth::user()->email;
            $data['coverletter'] = $request->letter;
            $dataemail = Auth::user()->email;
            $dataname = Auth::user()->first_name." ".Auth::user()->last_name; 
            if($resumes == null)
            {
                $data['note'] = "This User not Cv Uploaded";    
            }
            else 
            {
                $data['note'] = ""; 
                $pathToFile = realpath('public/cv')."/".$resumes; 
            }    
            Mail::send('emails.jobapply',['data' => $data], function($message) use($broadBeanID,$resumes,$pathToFile,$dataemail,$dataname)
            { 
                $message->replyTo($dataemail, $dataname);
                $message->to($broadBeanID, 'Myjobo')->subject('Job Apply');
                if($resumes != null)
                {
                    $message->attach($pathToFile);
                }
            });
        }    

        return redirect('jobapplications');
    }

    public function delete($id)
    {
        $job = Apply::where('id',$id);
        $job->delete(); 
        return redirect('jobapplications');
    }

}
