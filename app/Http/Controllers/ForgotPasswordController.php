<?php
namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Admin\Users; 
use Mail;
use Session;
use Illuminate\Support\Facades\Validator;


class ForgotPasswordController extends Controller
{
	
    public function index()
    {
    	return view('auth.forgotpassword');
    }

    public function emailsend(Request $request)
    {
        $chk = Users::where('email',$request->email)->count();
        if($chk == 0)
        {
            Session::flash('message', 'This Email not Our Record!'); 
            return redirect()->back();
        }
        else
        {    
            $token =  Str::random(60); 
            $user = array();
            $user['token'] = $token;
            $pages1 = Users::where('email',$request->email)->update($user);
            
            $email = $request->email; 
            $data['token'] = $token;
            $data['url'] = url('');
        
            Mail::send('emails.reset',['data' => $data], function($message) use($email)
            {
                $message->to($email, 'Myjobo')->subject("Reset Password");
            });
            Session::flash('success', 'Reset Password Email Send!'); 
            return redirect()->back();
        }    
    }

    public function resetpass($token)
    {
        return view('auth.resetpassword',compact('token'));
    }

    public function savepass(Request $request)
    {
        $this->validate(request(),[ 
            'email' => 'required',
            'password'              => 'required|min:6|max:20|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        $qdata1 = array('email' => $request->email, 'token' => $request->token);
        $chk = Users::where($qdata1)->count();
        if($chk == 0)
        {
            Session::flash('message', 'This Email not Vaild!'); 
            return redirect()->back();
        }
        else
        {
            $password = md5($request->password);
            $qdata = array('email' => $request->email, 'token' => $request->token);
            $pages = Users::where($qdata)->update([
            'password' => $password,
            'token' => null
            ]);
            return redirect('login');
        }    
    }
}