<?php
namespace App\Http\Controllers;
use Exception;

abstract class PMAPIException extends Exception
{ /* nothing */ }


class PMAPIRuntimeEnvironmentException extends PMAPIException
{ /* nothing */ }


class PMAPICURLException extends PMAPIException
{ /* nothing */ }


class PMAPIInvalidResponseException extends PMAPIException
{
	public function __construct($what)
	{
		parent::__construct("Invalid server response: $what");
	}
}


class PMAPIUnsupportedMethodException extends PMAPIException
{
	public function __construct($method)
	{
		parent::__construct("Unsupported method '$method'");
	}
}


class PMAPIInvalidValueException extends PMAPIException
{
	public function __construct($field, $value)
	{
		parent::__construct("Invalid $field value '$value'");
	}
}


class PMAPINoSuchFieldException extends PMAPIException
{
	public function __construct($field)
	{
		parent::__construct("No such field '$field'");
	}
}


class PMAPIInvalidArgumentException extends PMAPIException
{
    public function __construct($key, $val)
    {
        parent::__construct("Invalid argument '$key' and value '$val'");
    }
}

