<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Users;
use App\Models\Cvs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class JobseekerController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function put(Request $request)
    {
    	$jobseeker = array();
    	 if($request->hasFile('image')) 
    	{
            $photos = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $photos);
            $jobseeker['image'] = $photos;
        }
        $jobseeker['first_name'] = $request->first_name;
        $jobseeker['last_name'] = $request->last_name;
        $jobseeker['phone'] = $request->phone;
        $jobseeker['city'] = $request->city;
        $jobseeker['dob'] = $request->dob;
        $jobseeker['dist'] = $request->dist;
        $jobseeker['job_category'] = implode(',',$request->job);

       	$pages1 = Users::where('id',$request->id)->update($jobseeker);
       	return redirect()->back();

    }

    public function deletejobseeker()
    {
        $user_id = Auth::user()->id;

        $featured = Users::where('id',$user_id);    
        $featured->delete();

        $cv = Cvs::where('user_id',$user_id);    
        $cv->delete();

        $user = Auth::user();
        Log::info('User Logged Out. ', [$user]);
        Auth::logout();
        Session::flush();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');    
    }
    
    



}