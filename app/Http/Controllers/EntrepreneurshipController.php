<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Entrepreneurship;
use App\Models\Admin\Pages;



class EntrepreneurshipController extends Controller
{

    public function index()
    {
        $qdata12 = array('page_name' => 'entrepreneurship', 'element' => 'content' );
        $contant = Pages::where($qdata12)->get(['contents']);
    	$entrep = Entrepreneurship::orderBy('title', 'ASC')->get(['title','slug']);
        
        $qdata2 = array('page_name' => 'Entrepreneurship', 'element' => 'sidebar' );
        $enter = Pages::where($qdata2)->get();

        return view('common.entrepreneurship', compact('entrep','contant','enter'));
    }

    public function single($slug)
    {
    	$data = Entrepreneurship::where('slug',$slug)->get();
    	$entrep = Entrepreneurship::orderBy('title', 'ASC')->get();
        
        $qdata2 = array('page_name' => 'Entrepreneurship', 'element' => 'sidebar' );
        $enter = Pages::where($qdata2)->get();

        return view('common.entrepreneurshipsingle', compact('data','entrep','enter'));
    }

}
