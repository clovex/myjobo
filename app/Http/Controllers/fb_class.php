<?
namespace App\Http\Controllers;
use App\Models\Facebooks;

include("Facebook.php");
class FacebookApi {
 
    var $consumer;
    var $token;
    var $method;
    var $http_status;
    var $last_api_call;
    var $callback;
    var $connection; 
    var $access_token;
 
    function __construct($data){
        $config = array();
        $config['appId'] = $data['consumer_key'];
        $config['secret'] = $data['consumer_secret'];
 
        $this->connection = new Facebook($config);
 
    }
 
    function share($message,$title, $targetUrl, $imgUrl, $description, $access_token){

        $this->connection->setAccessToken($access_token);
 
        $params["access_token"] = $access_token;
        if(!empty($title)){
            $params["name"] = $title;
        }
		 if(!empty($message)){
            $params["message"] = $message;
        }
        if(!empty($targetUrl)){
            $params["link"] = $targetUrl;
        }
        if(!empty($imgUrl)){
            $params["picture"] = $imgUrl;
        }
        if(!empty($description)){
            $params["description"] = $description;
        }
 
        // post to Facebook
        try {
          $ret = $this->connection->api('/1428315440771300/feed', 'POST', $params);
        } catch(Exception $e) {
          $e->getMessage();
        }
 
        return true;
    }
 
  }
$fbdata = Facebooks::where('id',1)->first();

$facebookData = array();
$facebookData['consumer_key'] = '1502365693182959';
$facebookData['consumer_secret'] = '2aa65260ed2db2bc5f8bd0710f6e71e3';
$access_token=$fbdata->token; 