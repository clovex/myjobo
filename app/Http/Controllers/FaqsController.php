<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faq;
use App\Models\Faqcategory;
use DB;

class FaqsController extends Controller
{

    public function index()
    {
    	
    	//$faqscategory = Faqcategory::get(['title','icon']);
    	$faqscategory = DB::table('faq')
                ->join('faqcategory', 'faq.cat_id', '=', 'faqcategory.id')
				->distinct('faqcategory.title','faqcategory.icon')
                ->select('faqcategory.title','faqcategory.icon')
				->orderBy('faqcategory.orderby', 'ASC')
                ->get();
    	$faqs = Faq::all();
		$faqscategoryall = Faqcategory::orderBy('orderby', 'ASC')->get();
        return view('common.faqs', compact('faqs','faqscategory','faqscategoryall'));
    }

}
