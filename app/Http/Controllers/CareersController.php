<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;
use App\Models\Schooltype;
use App\Models\Careers;
use App\Models\Admin\Pages;
use App\Models\School_comments;
use App\Models\Coursescategory;

class CareersController extends Controller
{

    public function index()
    {
    	$qdata1 = array('page_name' => 'careers', 'element' => 'courses' );
        $courses = Pages::where($qdata1)->get(['contents']);
        $schooltype = Schooltype::all();
        $careers = Careers::all();
        $school = School::limit(10)->get();
        $coursescategory = Coursescategory::all();
        return view('common.careers', compact('courses','schooltype','careers','school','coursescategory'));
    }

    public function career($slug)
    {
    	$career = Careers::where('slug',$slug)->get();
    	$school = School::limit(10)->get();
    	$careers = Careers::all();
        return view('common.careersingle', compact('school','career','careers'));	
    }

    public function educational($slug)
    {
    	$career = Careers::where('slug',$slug)->get();
    	$school = School::limit(10)->get(['name','slug']);
    	$careers = Careers::get();
    	$schooltype = Schooltype::get(['name','slug']);	
        return view('common.educationalsingle', compact('school','career','careers','schooltype'));	
    }

    public function schools($slug)
    {
    	$schoolstype = Schooltype::where('slug',$slug)->get(['id']);
    	foreach ($schoolstype as $d1) {
    		$type = $d1->id;	
    	}	
    	$data = School::where('type',$type)->get(['name','address','slug','website','logo']);
    	$school = School::limit(10)->get(['name','slug']);
    	$careers = Careers::get();
    	$schooltype = Schooltype::get(['name','slug']);

        $qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );
        $common = Pages::where($qdata1)->get();
        
        return view('common.schoolssingle', compact('school','schoolstype','careers','schooltype','data','common'));	
    }

    public function school($slug)
    {
        $data = School::where('slug',$slug)->get();
        foreach ($data as $a) {
            $school_id = $a->id;
        }
        $comment = School_comments::where('school_id',$school_id)->get();
            
        $qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );
        $common = Pages::where($qdata1)->get();    

        return view('common.schoolsingle', compact('data','comment','common'));
    }

    public function schoolcategory(Request $request)
    {
        School_comments::create([
            'school_id' => request('school_id'),
            'email'     => request('email'),
            'comment'   => request('comment')
        ]);

        return redirect()->back();  
    }

}
