<?php

namespace App\Http\Controllers;
use Input;
use Illuminate\Support\Facades\Auth;
use App\Models\Apply;
use DB;
use Illuminate\Http\Request;
use App\Models\Jobs;
use App\Models\Job_category;
use Carbon\Carbon;
use App\Models\Admin\Users;


class CandidatesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
    	$user_id = Auth::user()->id;
    	$jobs = DB::table('tbl_job_post')
            	->join('tbl_apply_job', 'tbl_job_post.id', '=', 'tbl_apply_job.job_id')
            	->join('users', 'tbl_apply_job.user_id', '=', 'users.id')
            	->where('tbl_job_post.user_id',$user_id)  
            	->select('users.id','users.first_name','users.last_name','tbl_job_post.title','tbl_apply_job.created_at','users.image','tbl_apply_job.id as aid','tbl_apply_job.cv') 
            	->orderBy('tbl_apply_job.id','desc')
            	->paginate(16);      
        return view('pages.employer.candidates.index',compact('jobs'));
    }

    public function profiles($id)
    {
    	$users = Users::where('id', $id)->with('resume','letter')->get();
    	return view('pages.employer.candidates.profile',compact('users'));	
    }

    public function jobcandidates($slug)
    {
    	$getjob = Jobs::where('slug',$slug)->get();
    	foreach ($getjob as $gjob) {
    		$id = $gjob->id; 
    	}
    	$jobs = DB::table('tbl_job_post')
            	->join('tbl_apply_job', 'tbl_job_post.id', '=', 'tbl_apply_job.job_id')
            	->join('users', 'tbl_apply_job.user_id', '=', 'users.id')
            	->join('resume', 'users.id', '=', 'resume.user_id')
            	->where('tbl_job_post.id',$id)
            	->select('users.id','users.first_name','users.last_name','tbl_job_post.title','tbl_apply_job.created_at','resume.resume','users.image')
            	->orderBy('tbl_apply_job.id','desc')
            	->paginate(16); 	
        return view('pages.employer.candidates.index',compact('jobs'));

    }
 

    public function delete($id)
    {
        $job = Apply::where('id',$id); 
        $job->delete(); 
        return redirect('candidates');
    }


}
