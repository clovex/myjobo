<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Admin\Pages;
use App\Models\Services;
use App\Models\Featured;
use App\Models\Servicestype;



class ServicesController extends Controller
{

    public function index()
    {

    	$q1 = array('page_name' => 'services', 'element' => 'main' );
        $s1 = Pages::where($q1)->get(['contents']);

        $q2 = array('page_name' => 'services', 'element' => 'services1' );
        $s2 = Pages::where($q2)->get(['contents']);

        $q3 = array('page_name' => 'services', 'element' => 'services2' );
        $s3 = Pages::where($q3)->get(['contents']);

        $q4 = array('page_name' => 'services', 'element' => 'services3' );
        $s4 = Pages::where($q4)->get(['contents']);

        $q5 = array('page_name' => 'services', 'element' => 'services4' );
        $s5 = Pages::where($q5)->get(['contents']);

        $q6 = array('page_name' => 'general', 'element' => 'testlogo');
        $s6 = Pages::where($q6)->get(['contents']);

        $featured = Featured::all();

        $services = Services::all();
        $type = Servicestype::orderBy('set','asc')->get();

        return view('common.services', compact('s1','s2','s3','s4','s5','s6','featured','services','type'));
    }

    public function single($slug)
    {
        $services = Services::where('slug',$slug)->get();

        $q1 = array('page_name' => 'services', 'element' => 'main' );
        $s1 = Pages::where($q1)->get(['contents']);

        $q2 = array('page_name' => 'services', 'element' => 'services1' );
        $s2 = Pages::where($q2)->get(['contents']);

        $q3 = array('page_name' => 'services', 'element' => 'services2' );
        $s3 = Pages::where($q3)->get(['contents']);

        $q4 = array('page_name' => 'services', 'element' => 'services3' );
        $s4 = Pages::where($q4)->get(['contents']);

        $q5 = array('page_name' => 'services', 'element' => 'services4' );
        $s5 = Pages::where($q5)->get(['contents']);

        $q6 = array('page_name' => 'general', 'element' => 'testlogo');
        $s6 = Pages::where($q6)->get(['contents']);

        return view('common.servicessingle', compact('services','s1','s2','s3','s4','s5','s6'));
    }

}
