<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Pages;


class PrivacypolicyController extends Controller
{

    public function index()
    {
    	$qdata = array('page_name' => 'privacy_policy', 'element' => 'data' );
    	$pages = Pages::where($qdata)->get();
        return view('common.privacypolicy', compact('pages'));
    }

}
