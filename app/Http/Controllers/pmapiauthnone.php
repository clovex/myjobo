<?php
namespace App\Http\Controllers;

class PMAPIAuthNone extends PMAPIAuth
{
	public function getHeaders($verb, $version, $endpoint)
	{
		return array();
	}
}
