<?php

namespace App\Http\Controllers;
use Input;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Jobs;
use App\Models\Job_category;
use App\Models\Job_comment;
use App\Models\Apply;
use App\Models\Admin\Pages;
use App\Models\District;
use App\Models\Cvs;

use Carbon\Carbon;
class JobsController extends Controller
{

    public function index()
    {
    	$date = Carbon::now()->format('Y-m-d');
    	$jobs = Jobs::where('deadline', '>=', $date)->orderBy('id', 'desc')->paginate(20,['logo','created_at','city','slug','title','deadline']);
        $jobcategory = Job_category::orderBy('name', 'ASC')->get();
        $district = District::all();
        $qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );
        $common = Pages::where($qdata1)->get();

    	return view('common.jobs', compact('jobs','jobcategory','common','district'));
    }

    public function single($slug)
    {
        $jobscount = Jobs::where('slug', $slug)->with('employment_type','countrys')->count();
        if($jobscount != 0)
        {    
            if(Auth::guest())
            {
                $user_id = null;
            }    
            else
            {    
                $user_id = Auth::user()->id;
        	}   
            
            $jobs = Jobs::where('slug', $slug)->with('employment_type','countrys')->get();
            foreach ($jobs as $j) {
                 $id = $j->id;           
            }
            $query = array('job_id' => $id, 'user_id' => $user_id ); 
            $chk = Apply::where($query)->count();
            $Cvs = Cvs::where('user_id',$user_id)->get();   	
            $comment = Job_comment::where('jobid',$id)->get();
        	return view('common.jobsingle', compact('jobs','comment','chk','Cvs'));	
        }
        else
        {
            return redirect('jobs');
        }    
    }

    public function sendcomment()
    {
        $data = Input::all();
        $job_comment = new Job_comment;
        $job_comment->jobid = $data['jobid'];
        $job_comment->email = $data['email'];
        $job_comment->comment = $data['comment'];
        $job_comment->full_name = $data['full_name'];
        $job_comment->save();

        $comment = Job_comment::where('jobid',$data['jobid'])->get();

        foreach ($comment as $com) 
        {
            echo '<div class="col-md-12 col-lg-12 col-sm-12"><p style="margin-bottom: 0px;"><strong>'.$com->full_name.'</strong> </p> <p style="margin-bottom: 0px;">'.Carbon::parse($com->created_at)->format('jS \o\f F, Y g:i:s a').'</p><p>'.$com->comment.'</p><hr style="margin: 10px 0px 10px"></div>';
        }
    }

    public function filtersdata($id,$loc)
    {
        $date = Carbon::now()->format('Y-m-d');
        if($id == 'null' && $loc == 'null')
        {
            $jobs = Jobs::where([
                    ['deadline', '>=', $date]
                    ])->orderBy('id', 'desc')->get(); 
        }    
        else
        {    
            if($id == 'null')
            {
                    $jobs = Jobs::where([
                    ['deadline', '>=', $date],
                    ['district', '=',$loc]
                    ])->orderBy('id', 'desc')->get(); 
            }   
            else
            {    
                if($loc == 'null')
                {
                    $jobs = Jobs::where([
                        ['deadline', '>=', $date],
                        ['job_cat', '=', $id]
                        ])->orderBy('id', 'desc')->get();
                }    
                else
                {
                    $jobs = Jobs::where([
                        ['deadline', '>=', $date],
                        ['job_cat', '=', $id],
                        ['district', '=',$loc]
                        ])->orderBy('id', 'desc')->get(); 
                }    
            }
        }

        $result = count($jobs);
        if($result != 0)
        {    
        foreach($jobs as $j)
        {    
            echo'<div class="col-md-12 col-lg-12 col-sm-12 setstyle">
                    <div class="col-md-1 col-lg-1 col-sm-1" style="padding: 0px !important;">';
            if($j->logo != null)
            {    
                echo '<img src="'.url('public/uploads').'/'.$j->logo.'" alt="" style="width: 100%;">';
            }
            else
            {    
                echo '<img src="'.url('public/uploads/company.png').'" alt="" style="width: 100%;">';
            }

            echo'</div>
                    <div class="col-md-11 col-lg-11 col-sm-11">
                    <h4 style="font-size: 16px; line-height: 32px;">'.$j->title.'
                    <!--ul class="social-icons sociadata" style="float:right;">
                        <li style="transform:none; margin-top: 0px !important;">
                            <a class="facebook setjobfb" href="https://www.facebook.com/sharer/sharer.php?u='.url('job').'/'.$j->slug.'" target="_blank">
                                <i class="icon-facebook"></i>
                            </a>
                        </li>
                        <li style="transform:none; margin-top: 0px !important;">
                            <a class="twitter setjobtw" href="http://twitter.com/share?text='.url('job').'/'.$j->slug.'" target="_blank">
                                <i class="icon-twitter"></i>
                            </a>
                        </li>
                        </ul-->
                    </h4>
                    <label>
                        <span><i class="fa fa-calendar"> Posted On: </i>&nbsp;'.Carbon::parse($j->created_at)->format('d-m-Y').'</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-map-marker"> Location: </i>&nbsp;'.$j->city.'</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-calendar-times-o"> Job Deadline: </i>&nbsp;'.Carbon::parse($j->deadline)->format('d-m-Y').'</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <strong><a href="'.url('job').'/'.$j->slug.'">View</a></strong>
                    </label>
                    </div> 
                </div>';
        }
        }
        else
        {
            echo '<div class="col-md-12 col-lg-12 col-sm-12 text-center">
                    <h3>No Jobs are there according to this category.</h3>
                 </div>   
            ';
        }    
                /*echo'<div class="pagination-container text-center">
                    <nav class="pagination">'.$jobs->links().'</nav></div>';*/
    }


    public function filterslocations($id,$cat)
    {
        $date = Carbon::now()->format('Y-m-d');
        if($id == 'null' && $cat == 'null')
        {
            $jobs = Jobs::where([
                    ['deadline', '>=', $date]
                    ])->orderBy('id', 'desc')->get(); 
        }    
        else
        {
            if($id == 'null')
            {
                    $jobs = Jobs::where([
                    ['deadline', '>=', $date],
                    ['job_cat', '=',$cat]
                    ])->orderBy('id', 'desc')->get(); 
            }   
            else
            {    
                if($cat == 'null')
                {
                    $jobs = Jobs::where([
                        ['deadline', '>=', $date],
                        ['district', '=', $id]
                        ])->orderBy('id', 'desc')->get();
                }    
                else
                {
                    $jobs = Jobs::where([
                        ['deadline', '>=', $date],
                        ['job_cat', '=', $cat],
                        ['district', '=',$id]
                        ])->orderBy('id', 'desc')->get(); 
                }    
            }
        }
        
        $result = count($jobs);
        if($result != 0)
        {    
        foreach($jobs as $j)
        {    
            echo'<div class="col-md-12 col-lg-12 col-sm-12 setstyle">
                    <div class="col-md-1 col-lg-1 col-sm-1" style="padding: 0px !important;">';
            if($j->logo != null)
            {    
                echo '<img src="'.url('public/uploads').'/'.$j->logo.'" alt="" style="width: 100%;">';
            }
            else
            {    
                echo '<img src="'.url('public/uploads/company.png').'" alt="" style="width: 100%;">';
            }

            echo'</div>
                    <div class="col-md-11 col-lg-11 col-sm-11">
                    <h4 style="font-size: 16px; line-height: 32px;">'.$j->title.'
                    <!--ul class="social-icons sociadata" style="float:right;">
                        <li style="transform:none; margin-top: 0px !important;">
                            <a class="facebook setjobfb" href="https://www.facebook.com/sharer/sharer.php?u='.url('job').'/'.$j->slug.'" target="_blank">
                                <i class="icon-facebook"></i>
                            </a>
                        </li>
                        <li style="transform:none; margin-top: 0px !important;">
                            <a class="twitter setjobtw" href="http://twitter.com/share?text='.url('job').'/'.$j->slug.'" target="_blank">
                                <i class="icon-twitter"></i>
                            </a>
                        </li>
                        </ul-->
                    </h4>
                    <label>
                        <span><i class="fa fa-calendar"> Posted On: </i>&nbsp;'.Carbon::parse($j->created_at)->format('d-m-Y').'</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-map-marker"> Location: </i>&nbsp;'.$j->city.'</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-calendar-times-o"> Job Deadline: </i>&nbsp;'.Carbon::parse($j->deadline)->format('d-m-Y').'</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <strong><a href="'.url('job').'/'.$j->slug.'">View</a></strong>
                    </label>
                    </div> 
                </div>';
        }
        }
        else
        {
            echo '<div class="col-md-12 col-lg-12 col-sm-12 text-center">
                    <h3>No Jobs are there according to this Location.</h3>
                 </div>   
            ';
        }
    }

    public function filters($id)
    {
        if($id == 'null')
        {
            $id ="";
        }
        $date = Carbon::now()->format('Y-m-d');
        $jobs = DB::table('tbl_job_post')
                ->join('tbl_categories', 'tbl_job_post.job_cat', '=', 'tbl_categories.id')
                ->join('tbl_district', 'tbl_job_post.district', '=', 'tbl_district.id')
                ->where('tbl_job_post.deadline','>=',$date)
                ->where(function ($query) use ($id) {
                        $query->where("tbl_job_post.company_name", 'like', '%'.$id.'%')
                        ->orwhere("tbl_job_post.title", 'like', '%'.$id.'%')
                        ->orwhere("tbl_district.name", 'like', '%'.$id.'%')
                        ->orwhere("tbl_categories.name", 'like', '%'.$id.'%');
                    })
                ->orderBy('tbl_job_post.id', 'desc')
                ->get();

        $result = count($jobs);
        if($result != 0)
        {    
        foreach($jobs as $j)
        {    
            echo'<div class="col-md-12 col-lg-12 col-sm-12 setstyle">
                    <div class="col-md-1 col-lg-1 col-sm-1" style="padding: 0px !important;">';
            if($j->logo != null)
            {    
                echo '<img src="'.url('public/uploads').'/'.$j->logo.'" alt="" style="width: 100%;">';
            }
            else
            {    
                echo '<img src="'.url('public/uploads/company.png').'" alt="" style="width: 100%;">';
            }

            echo'</div>
                    <div class="col-md-11 col-lg-11 col-sm-11">
                    <h4 style="font-size: 16px; line-height: 32px;">'.$j->title.'
                    <!--ul class="social-icons sociadata" style="float:right;">
                        <li style="transform:none; margin-top: 0px !important;">
                            <a class="facebook setjobfb" href="https://www.facebook.com/sharer/sharer.php?u='.url('job').'/'.$j->slug.'" target="_blank">
                                <i class="icon-facebook"></i>
                            </a>
                        </li>
                        <li style="transform:none; margin-top: 0px !important;">
                            <a class="twitter setjobtw" href="http://twitter.com/share?text='.url('job').'/'.$j->slug.'" target="_blank">
                                <i class="icon-twitter"></i>
                            </a>
                        </li>
                        </ul-->
                    </h4>
                    <label>
                        <span><i class="fa fa-calendar"> Posted On: </i>&nbsp;'.Carbon::parse($j->created_at)->format('d-m-Y').'</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-map-marker"> Location: </i>&nbsp;'.$j->city.'</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-calendar-times-o"> Job Deadline: </i>&nbsp;'.Carbon::parse($j->deadline)->format('d-m-Y').'</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <strong><a href="'.url('job').'/'.$j->slug.'">View</a></strong>
                    </label>
                    </div> 
                </div>';
        }
        } 
        else
        {
            echo '<div class="col-md-12 col-lg-12 col-sm-12 text-center">
                    <h3>No Jobs are there according to this Location.</h3>
                 </div>   
            ';
        }        

    }

}
