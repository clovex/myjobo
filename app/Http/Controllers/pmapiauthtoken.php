<?php
namespace App\Http\Controllers;

require_once('pmapiauth.php');
require_once('pmapiexceptions.php');

class PMAPIAuthToken extends PMAPIAuth
{
	protected $token;


	public function __construct($token)
	{
		if(!preg_match('%^[a-z0-9/+]{96}$%i', $token))
			throw new PMAPIInvalidValueException('token', $token);

		$this->token = $token;
	}


	public function getHeaders($verb, $version, $endpoint)
	{
		return array
		(
			'Authorization'	=> "SuTToken {$this->token}",
			'Date'			=> $this->getDate(),
			'X-SuT-Nonce'	=> $this->getNonce()
		);
	}
}

