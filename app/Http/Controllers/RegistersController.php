<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Traits\ActivationTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use jeremykenedy\LaravelRoles\Models\Permission;
use jeremykenedy\LaravelRoles\Models\Role;
use App\Models\District;
use App\Models\Speciality;

class RegistersController extends Controller
{
    use ActivationTrait;
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/activate';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', [
            'except' => 'logout'
        ]);
    }

	public function index()
    {
    	return view('auth.registers');
    }

    public function jobseeker()
    {
    	return view('auth.jobseeker');	
    }

    public function employer()
    {   
        $districts = District::all();
    	return view('auth.employer',compact('districts'));	
    }

    public function services()
    {
        $districts = District::all();
        $speciality = Speciality::all();
        return view('auth.services',compact('speciality','districts'));  
    }

    public function services_provider()
    {
        return view('auth.services_provider');   
    }




}