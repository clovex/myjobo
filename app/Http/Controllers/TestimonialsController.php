<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Testimonials;

class TestimonialsController extends Controller
{

    public function index()
    {
    	$testimonials = Testimonials::all();
        return view('common.testimonials', compact('testimonials'));
    }

}
