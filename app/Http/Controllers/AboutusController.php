<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Pages;
use App\Models\Featured;


class AboutusController extends Controller
{

    public function index()
    {
    	$qdata1 = array('page_name' => 'about_us', 'element' => 'data' );
        $about1 = Pages::where($qdata1)->get(['contents']);

        $qdata2 = array('page_name' => 'about_us', 'element' => 'sec_2_des' );
        $about2 = Pages::where($qdata2)->get(['contents']);

        $qdata3 = array('page_name' => 'about_us', 'element' => 'profile1' );
        $about3 = Pages::where($qdata3)->get(['contents']);

        $qdata4 = array('page_name' => 'about_us', 'element' => 'profile2' );
        $about4 = Pages::where($qdata4)->get(['contents']);

        $qdata5 = array('page_name' => 'about_us', 'element' => 'profile3' );
        $about5 = Pages::where($qdata5)->get(['contents']);

        $qdata6 = array('page_name' => 'about_us', 'element' => 'profileimg1' );
        $about6 = Pages::where($qdata6)->get(['contents']);

        $qdata7 = array('page_name' => 'about_us', 'element' => 'profileimg2' );
        $about7 = Pages::where($qdata7)->get(['contents']);

        $qdata8 = array('page_name' => 'about_us', 'element' => 'profileimg3' );
        $about8 = Pages::where($qdata8)->get(['contents']);

        $qdata9 = array('page_name' => 'about_us', 'element' => 'sec_3_des1' );
        $about9 = Pages::where($qdata9)->get(['contents']); 

        $qdata10 = array('page_name' => 'about_us', 'element' => 'sec_3_des2' );
        $about10 = Pages::where($qdata10)->get(['contents']);

        $qdata11 = array('page_name' => 'about_us', 'element' => 'sec_3_des3' );
        $about11 = Pages::where($qdata11)->get(['contents']);

        $qdata12 = array('page_name' => 'about_us', 'element' => 'sec_3_des4' );
        $about12 = Pages::where($qdata12)->get(['contents']);

        $about13 = Featured::get(['image','link']);

        return view('common.aboutus', compact('about1','about2','about3','about4','about5','about6','about7','about8','about9','about10','about11','about12','about13'));
    }

}
