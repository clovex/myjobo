<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Blogs;
use App\Models\Blogs_category;
use App\Models\Blogs_comment;

class BlogController extends Controller
{

    public function index()
    {
    	$blogs = Blogs::orderBy('id', 'desc')->get(['title','description','slug','created_at','author']);
    	$category = Blogs_category::get(['name','slug']);
        return view('common.blog',compact('blogs','category'));
    }

    public function blogscat($slug)
    {
    	$cat = Blogs_category::where('slug',$slug)->get(['id']);
    	foreach ($cat as $c) {
    		$categorys = $c->id;
    	}
    	$blogs = Blogs::where('blog_category',$categorys)->get(['title','description','slug','created_at']);
    	$result = count($blogs);
    	$category = Blogs_category::get(['name','slug']);
        return view('common.blogcat',compact('blogs','category','result'));	
    }

    public function single($slug)
    {
    	$blogs = Blogs::where('slug',$slug)->get();
    	foreach ($blogs as $bb) {
    	   $id = $bb->id;
    	}
    	$comment = Blogs_comment::where('articleid',$id)->get(['comment','email','created_at']);
    	$category = Blogs_category::get(['name','slug']);
        return view('common.blogsingle',compact('blogs','category','comment'));		
    }

    public function postcomment(Request $request)
    {
    	Blogs_comment::create([
            'articleid' => request('articleid'),
            'email'     => request('email'),
            'comment'   => request('comment')
        ]);

        return redirect()->back();
    }

}
