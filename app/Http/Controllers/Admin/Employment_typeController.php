<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employment_type;

class Employment_typeController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $employment_type = Employment_type::all();
    	return view('pages.admin.employment_type.index', compact('employment_type'));
    }

    public function add()
    {
        return view('pages.admin.employment_type.add');   
    }

    public function post(Request $request)
    {

        Employment_type::create([
            'name' => request('name')
        ]);

        return redirect('admin/employment_type');
    }

    public function single($id)
    {
        $employment_type = Employment_type::where('id',$id)->get();
        return view('pages.admin.employment_type.edit', compact('employment_type'));
    }

    public function put(Request $request)
    {
        $data = array(
            'name' => $request->name
        );
        $pages1 = Employment_type::where('id',$request->id)->update($data);
        return redirect('admin/employment_type');
    }

    public function delete($id)
    {
        $featured = Employment_type::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

}
