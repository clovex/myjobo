<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Pages;

class ContactuspageController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
    	$qdata = array('page_name' => 'contact_us', 'element' => 'data' );
    	$pages = Pages::where($qdata)->get();
        return view('pages.admin.pages.contact', compact('pages'));
    }


    public function update(Request $request)
    {
    	$contents = $request->contactus;
    	$qdata = array('page_name' => 'contact_us', 'element' => 'data' );
    	$pages = Pages::where($qdata)->update([
        'contents' => $contents
      	]);
    	return redirect()->back();
    }

}
