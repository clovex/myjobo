<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;

class CountryController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $country = Country::all();
    	return view('pages.admin.country.index', compact('country'));
    }

    public function add()
    {
        return view('pages.admin.country.add');   
    }

    public function post(Request $request)
    {

        Country::create([
            'name' => request('name')
        ]);

        return redirect('admin/country');
    }

    public function single($id)
    {
        $country = Country::where('country_id',$id)->get();
        return view('pages.admin.country.edit', compact('country'));
    }

    public function put(Request $request)
    {
        $data = array(
            'name' => $request->name
        );
        $pages1 = Country::where('country_id',$request->id)->update($data);
        return redirect('admin/country');
    }

    public function delete($id)
    {
        $featured = Country::where('country_id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

}
