<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Entrepreneurship;
use App\Models\Admin\Pages;


class EntrepreneurshipController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $entrep = Entrepreneurship::all();
    	return view('pages.admin.entrepreneurship.index', compact('entrep'));
    }

    public function add()
    {
        return view('pages.admin.entrepreneurship.add');   
    }

    public function post(Request $request)
    {

        $string = str_replace(' ', '-', strtolower(request('title')));
        $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slugdata = str_replace('--', '-', $slugdata1);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = Entrepreneurship::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }

        Entrepreneurship::create([
            'title' => request('title'),
            'description' =>request('description'),
            'slug' => $slug
        ]);

        return redirect('admin/entrepreneurship');
    }

    public function single($id)
    {
        $entrep = Entrepreneurship::where('id',$id)->get();
        return view('pages.admin.entrepreneurship.edit', compact('entrep'));
    }

    public function put(Request $request)
    {
        $data = array(
            'title' => $request->title,
            'description' => $request->description
        );
        $pages1 = Entrepreneurship::where('id',$request->id)->update($data);
        return redirect('admin/entrepreneurship');
    }

    public function delete($id)
    {
        $featured = Entrepreneurship::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

    public function events()
    {
        $qdata12 = array('page_name' => 'entrepreneurship', 'element' => 'content' );
        $about12 = Pages::where($qdata12)->get();

        return view('pages.admin.entrepreneurship.events', compact('about12'));
    }

    public function eventssave(Request $request)
    {
        $contents = $request->description;
        $qdata = array('page_name' => 'entrepreneurship', 'element' => 'content' );
        $pages = Pages::where($qdata)->update([
        'contents' => $contents
        ]);
        return redirect()->back();
    }
}
