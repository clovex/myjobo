<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Role;
use App\Models\Admin\Users;
use DB;
use App\Models\Cvs;
use App\Models\Job_category;
use App\Models\District;
use App\Models\Country;

class JobseekersController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
    	$users = Role::where('role_id','2')->with('users')->orderBy('id', 'DESC')->paginate(100);
        return view('pages.admin.jobseekers.index',compact('users'));
    }

    public function add()
    {
        return view('pages.admin.jobseekers.add');    
    }

    public function single($id)
    {
    	$users = Users::where('id',$id)->get();
        return view('pages.admin.jobseekers.edit',compact('users'));	
    }

    public function post(Request $request)
    {
        $users = new Users;
        $users->name = $request->name;
        $users->first_name = $request->first_name;
        $users->last_name = $request->last_name;
        $users->email = $request->email;
        $users->password = md5($request->password);
        $users->activated = '1';
        $users->save();

        $userid = $users->id;

        $role = new Role;
        $role->role_id = '2';
        $role->user_id = $userid;
        $role->save();

        $cv = new Cvs;
        $role->user_id = $userid;
        $role->save();

        return redirect('admin/jobseekers');
    }

    public function put(Request $request)
    {
    	$users = array();
    	$users['name'] = $request->name;
        $users['first_name'] = $request->first_name;
        $users['last_name'] = $request->last_name;
        $users['email'] = $request->email;
        $users['phone'] = $request->phone;
        $users['dob'] = $request->dob;
        $users['city'] = $request->city;
        $users['dist'] = $request->dist;
       
        $pages1 = Users::where('id',$request->id)->update($users);
        return redirect('admin/jobseekers');
    }

    public function delete($id)
    {
        $users = Users::where('id',$id);    
        $users->delete();

        return redirect()->back();
    }

    public function deactive($id)
    {
    	$users = array();
    	$users['activated'] = 0;
       
        $pages1 = Users::where('id',$id)->update($users);
        return redirect()->back();	
    }

    public function active($id)
    {
    	$users = array();
    	$users['activated'] = 1;
       
        $pages1 = Users::where('id',$id)->update($users);
        return redirect()->back();	
    }

    public function search(Request $request)
    {
        $users = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->where('role_user.role_id','=','2')
                ->where(function ($query) use ($request) {
                        $query->where("first_name", 'like', '%'.$request->search.'%')
                        ->orwhere("last_name", 'like', '%'.$request->search.'%')
                        ->orwhere("email", 'like', '%'.$request->search.'%');
                    })
                ->paginate(100);
        return view('pages.admin.jobseekers.search',compact('users'));
    }

    public function cv($id)
    {
        $cvs = Cvs::where('user_id',$id)->get();
        $district = District::all();
        $country = Country::all();
        $job_category = Job_category::orderBy('id', 'asc')->get();
        return view('pages.admin.jobseekers.cv',compact('job_category','district','country','cvs'));
    }

    public function cvpost(Request $request)
    {
        $cv = array();
        if($request->hasFile('resume')) 
        {
            $photos = time().$request->resume->getClientOriginalName();
            $request->resume->move(public_path('cv'), $photos);
            $cv['resume'] = $photos;
        }
        $cv['job_title'] = $request->job_title;
        $cv['job_category'] = $request->job_category;
        $cv['city'] = $request->city;
        $cv['district'] = $request->district;
        $cv['country'] = $request->country;
        $cv['objective'] = $request->objective;
        $cv['work_exeperence'] = $request->work_exeperence;
        $cv['total_exp'] = $request->total_exp;
        $cv['education'] = $request->education;
        $cv['skill'] = $request->skill;
        $cv['salary'] = $request->salary;
        $cv['salary_type'] = $request->salary_type;
        $cv['job_type'] = $request->job_type;

        $pages1 = Cvs::where('user_id',$request->user_id)->update($cv);
       return redirect('admin/jobseekers');
    }
}
