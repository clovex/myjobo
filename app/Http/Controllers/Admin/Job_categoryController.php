<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Job_category;

class Job_categoryController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $job_category = Job_category::all();
    	return view('pages.admin.job_category.index', compact('job_category'));
    }

    public function add()
    {
        return view('pages.admin.job_category.add');   
    }

    public function post(Request $request)
    {

        Job_category::create([
            'name' => request('name')
        ]);

        return redirect('admin/job_category');
    }

    public function single($id)
    {
        $job_category = Job_category::where('id',$id)->get();
        return view('pages.admin.job_category.edit', compact('job_category'));
    }

    public function put(Request $request)
    {
        $data = array(
            'name' => $request->name
        );
        $pages1 = Job_category::where('id',$request->id)->update($data);
        return redirect('admin/job_category');
    }

    public function delete($id)
    {
        $featured = Job_category::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

}
