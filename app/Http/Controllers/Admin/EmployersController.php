<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Role;
use App\Models\Admin\Users;
use App\Models\District;
use App\Models\Country;
use App\Models\Partners;
use DB;

class EmployersController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
    	$users = Role::where('role_id','4')->with('users')->orderBy('id', 'DESC')->paginate(100);
        return view('pages.admin.employers.index',compact('users'));
    }

    public function delete($id)
    {
        $users = Users::where('id',$id);    
        $users->delete();

        return redirect()->back();
    }

    public function deactive($id)
    {
    	$users = array();
    	$users['status'] = 0;
       
        $pages1 = Users::where('id',$id)->update($users);
        return redirect()->back();	
    }

    public function active($id)
    {
    	$users = array();
    	$users['status'] = 1;
        $users['activated'] = 1;
       
        $pages1 = Users::where('id',$id)->update($users);
        return redirect()->back();	
    }


    public function disallowed($id)
    {
        $users = array();
        $users['cv_search'] = 0;
       
        $pages1 = Users::where('id',$id)->update($users);
        return redirect()->back();  
    }

    public function allowed($id)
    {
        $users = array();
        $users['cv_search'] = 1;
       
        $pages1 = Users::where('id',$id)->update($users);
        return redirect()->back();  
    }

    public function add()
    {
        return view('pages.admin.employers.add');    
    }

    public function single($id)
    {
    	$users = Users::where('id',$id)->get();
    	$district = District::all();
    	$country = Country::all();
    	$partners = Partners::all();
        return view('pages.admin.employers.edit',compact('users','district','country','partners'));	
    }

    public function post(Request $request)
    {
        $users = new Users;
        $users->name = $request->name;
        $users->first_name = "";
        $users->last_name = "";
        $users->email = $request->email;
        $users->password = md5($request->password);
        $users->activated = '1';
        $users->status = '1';
        $users->save();

        $role = new Role;
        $role->role_id = '4';
        $role->user_id = $users->id;
        $role->save();
        return redirect('admin/employers');
    }

    public function put(Request $request)
    {
    	$users = array();

    	if($request->hasFile('logo')) 
        {
            $photos = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $photos);
            $users['image'] = $photos;
        }

    	$users['name'] = $request->name;
        $users['contact_name'] = $request->contact_name;
        $users['email'] = $request->email;
        $users['phone'] = $request->phone;
        $users['website'] = $request->website;
        $users['district'] = $request->district;
        $users['country'] = $request->country;
        $users['address'] = $request->address;
        $users['sector'] = $request->sector;
        $users['description'] = $request->description;
       	if($request->front_logo != null)
        {
            $users['front_logo'] = 1;
        }
        else
        {
        	$users['front_logo'] = null;	
        }	

        $pages1 = Users::where('id',$request->id)->update($users);
        return redirect('admin/employers');
    }

    public function search(Request $request)
    {
        $users = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->where('role_user.role_id','=','4')
                ->where(function ($query) use ($request) {
                        $query->where("name", 'like', '%'.$request->search.'%')
                        ->orwhere("email", 'like', '%'.$request->search.'%');
                    })
                ->paginate(100);
        return view('pages.admin.employers.search',compact('users'));
    }

}
