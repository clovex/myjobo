<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blogs_category;

class Blogs_categoryController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $blog_category = Blogs_category::all();
    	return view('pages.admin.blog.category', compact('blog_category'));
    }

    public function add()
    {
        return view('pages.admin.blog.categoryadd');   
    }

    public function post(Request $request)
    {

        $string = str_replace(' ', '-', strtolower(request('name')));
        $slugdata=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = Blogs_category::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }


        Blogs_category::create([
            'name' => request('name'),
            'slug' => $slug
            
        ]);

        return redirect('admin/blog_category');
    }

    public function single($id)
    {
        $blog_category = Blogs_category::where('id',$id)->get();
        return view('pages.admin.blog.categoryedit', compact('blog_category'));
    }

    public function put(Request $request)
    {
        $string = str_replace(' ', '-', strtolower(request('name')));
        $slugdata=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = Blogs_category::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }

        $data = array(
            'name' => $request->name,
            'slug' => $slug
            
        );
        $pages1 = Blogs_category::where('id',$request->id)->update($data);
        return redirect('admin/blog_category');
    }

    public function delete($id)
    {
        $featured = Blogs_category::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }
}
