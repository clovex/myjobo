<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Schooltype;

class SchooltypeController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $schooltype = Schooltype::all();
    	return view('pages.admin.schooltype.index', compact('schooltype'));
    }

    public function add()
    {
        return view('pages.admin.schooltype.add');   
    }

    public function post(Request $request)
    {
        $schooltype = new Schooltype;
        $string = str_replace(' ', '-', strtolower(request('name')));
        $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slugdata = str_replace('--', '-', $slugdata1);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = Schooltype::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }
        
        $schooltype->name = $request->name;
        $schooltype->slug = $slug;
        $schooltype->save();
        return redirect('admin/school_type');
    }

    public function single($id)
    {
        $schooltype = Schooltype::where('id',$id)->get();
        return view('pages.admin.schooltype.edit', compact('schooltype'));
    }

    public function put(Request $request)
    {
        $schooltype = array();
        
        $schooltype['name'] = $request->name;
        
        $pages1 = Schooltype::where('id',$request->id)->update($schooltype);
        return redirect('admin/school_type');
    }

    public function delete($id)
    {
        $schooltype = Schooltype::where('id',$id);    
        $schooltype->delete();

        return redirect()->back();  
    }

}
