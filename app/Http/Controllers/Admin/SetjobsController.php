<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Jobs;
use App\Models\District;
use App\Models\Country;
use App\Models\Job_category;
use App\Models\Employment_type;
ini_set('memory_limit', '-1');

class SetjobsController extends Controller
{

	public function allset()
    {
        $job = Jobs::orderBy('id', 'ASC')->get();
        $i = 1;
        foreach ($job as $j) 
        {
        
            $string = str_replace(' ', '-', strtolower($j->title));
            $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
            $slugdata2 = str_replace('--', '-', $slugdata1);
            $slugdata = str_replace('--', '-', $slugdata2);
            $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
            shuffle($seed); 
            $rand = '';
            foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
               
            $chk = Jobs::where('slug',$slugdata)->count();
            if($chk == 0)
            {
                 $slug = $slugdata;
            }
            else
            {
                $slug = $slugdata.'-'.$rand;
            }
			$catchk = Job_category::where('name',$j->job_cat)->count();
			if($catchk != 0)
            {
	            $cat = Job_category::where('name',$j->job_cat)->get();
	            foreach ($cat as $c) 
	            {
	                $job_cat = $c->id; 
	            }
	        }
	        else
	        {
	        	$job_cat = 0;
	        } 

            $empchk = Employment_type::where('name',$j->emp_type)->count();
            if($empchk != 0)
            {	
            	$emp = Employment_type::where('name',$j->emp_type)->get();
            	foreach ($emp as $e) 
	            {
	                $emp_type = $e->id;
	            }
	        }
	        else
	        {
	        	$emp_type = 0;
	        }

	        $data = array();

	        $data['slug'] = $slug;
        	$data['job_cat'] = $job_cat;
        	$data['emp_type'] = $emp_type;

        	
        	$pages1 = Jobs::where('id',$j->id)->update($data);
        	echo $i++; 
        }    
            
    }



}