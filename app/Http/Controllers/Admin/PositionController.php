<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Position;

class PositionController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
        $position = Position::all();
        return view('pages.admin.position.index', compact('position'));
    }

    public function post(Request $request)
    {

        Position::create([
            'name' => request('name')
        ]);

        return redirect()->back();  
    }

    public function single($id)
    {
        $position = Position::where('id',$id)->get();
        return view('pages.admin.position.edit', compact('position'));
    }

    public function put(Request $request)
    {
        $data = array(
            'name' => $request->name
        );
        $pages1 = Position::where('id',$request->id)->update($data);
        return redirect('admin/position');
    }

    public function delete($id)
    {
        $featured = Position::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

}