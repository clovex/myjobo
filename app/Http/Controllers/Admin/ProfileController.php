<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Users;


class ProfileController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
    	return view('pages.admin.profile');
    }

    public function post(Request $request)
    {
    	$user = array();
    	if($request->hasFile('image')) 
    	{
            $photos = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $photos);
            $user['image'] = $photos;
        }
        $user['first_name'] = $request->first_name;
        $user['last_name'] = $request->last_name;
        $user['email'] = $request->email;

        if($request->password != null)
        {
        	$user['password'] = md5($request->password);
        }	
        
       	$pages1 = Users::where('id',$request->id)->update($user);
       	return redirect()->back();
    }

}