<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Pages;

class PrivacypolicyController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
    	$qdata = array('page_name' => 'privacy_policy', 'element' => 'data' );
    	$pages = Pages::where($qdata)->get();
        return view('pages.admin.pages.privacy_policy', compact('pages'));
    }


    public function put(Request $request)
    {
    	$contents = $request->description;
    	$qdata = array('page_name' => 'privacy_policy', 'element' => 'data' );
    	$pages = Pages::where($qdata)->update([
        'contents' => $contents
      	]);
    	return redirect()->back();
    }

}
