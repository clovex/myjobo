<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\Models\Job_comment;


class Job_commentsController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index() 
    {
    	$date = Carbon::now()->format('Y-m-d');
        $comments = DB::table('tbl_job_post')
                ->join('tbl_article_comments', 'tbl_job_post.id', '=', 'tbl_article_comments.articleid')
                ->select('tbl_article_comments.comment','tbl_article_comments.id')
                ->where('tbl_job_post.deadline','>=',$date)
                ->get();
    	return view('pages.admin.job_category.comments', compact('comments'));
    }
    public function delete($id)
    {
    	$featured = Job_comment::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }
}