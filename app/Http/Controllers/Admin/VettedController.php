<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Role;
use App\Models\Admin\Users;
use DB;
use App\Models\District;
use App\Models\Speciality;


class VettedController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
    	$users = Role::where('role_id','5')->with('users')->orderBy('id', 'DESC')->paginate(100);
        return view('pages.admin.vetted.index',compact('users'));
    }

    public function add()
    {
        return view('pages.admin.vetted.add');    
    }

    public function post(Request $request)
    {
        $users = new Users;
        $users->name = $request->name;
        $users->first_name = $request->first_name;
        $users->last_name = $request->last_name;
        $users->email = $request->email;
        $users->password = md5($request->password);
        $users->activated = '1';
        $users->status = '1';
        if($request->hasFile('image'))  
        {
            $photos = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $photos);
            $users->image = $photos;
        }
        $users->save();

        $role = new Role;
        $role->role_id = '5';
        $role->user_id = $users->id;
        $role->save();
        return redirect('admin/trade');
    }

    public function single($id)
    {
        $speciality = Speciality::all();
        $city = District::all();
    	$users = Users::where('id',$id)->get();
        return view('pages.admin.vetted.edit',compact('users','speciality','city'));	
    }

    public function put(Request $request)
    {
    	$users = array();
    	$users['name'] = $request->name;
        $users['first_name'] = $request->first_name;
        $users['last_name'] = $request->last_name;
        $users['email'] = $request->email;
        $users['phone'] = $request->phone;
        $users['gender'] = $request->gender;
        $users['experience'] = $request->experience;
        $users['education'] = $request->education;
        $users['speciality'] = $request->speciality;
        $users['city'] = $request->city;
        $users['cities'] = implode(',',$request->cities);
        if($request->hasFile('image'))  
        {
            $photos = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $photos);
            $users['image'] = $photos;
        }
        $pages1 = Users::where('id',$request->id)->update($users);
        return redirect('admin/trade');
    }

    public function delete($id)
    {
        $users = Users::where('id',$id);    
        $users->delete();

        return redirect()->back();
    }

    public function deactive($id)
    {
    	$users = array();
    	$users['status'] = 0;
       
        $pages1 = Users::where('id',$id)->update($users);
        return redirect()->back();	
    }

    public function active($id)
    {
    	$users = array();
    	$users['status'] = 1;
        $users['activated'] = 1;
       
        $pages1 = Users::where('id',$id)->update($users);
        return redirect()->back();	
    }

    public function search(Request $request)
    {
        $users = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->where('role_user.role_id','=','5')
                ->where(function ($query) use ($request) {
                        $query->where("first_name", 'like', '%'.$request->search.'%')
                        ->orwhere("last_name", 'like', '%'.$request->search.'%')
                        ->orwhere("email", 'like', '%'.$request->search.'%');
                    })
                ->paginate(100);
        return view('pages.admin.vetted.search',compact('users'));
    }

}