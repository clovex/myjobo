<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Pages;

class GeneralController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $q1 = array('page_name' => 'general', 'element' => 'logo' );
        $s1 = Pages::where($q1)->get();

        $q2 = array('page_name' => 'general', 'element' => 'sociallink' );
        $s2 = Pages::where($q2)->get();

        $q3 = array('page_name' => 'general', 'element' => 'about' ); 
        $s3 = Pages::where($q3)->get();

        $q4 = array('page_name' => 'general', 'element' => 'testlogo'); 
        $s4 = Pages::where($q4)->get();

    	return view('pages.admin.general.index', compact('s1','s2','s3','s4'));
    }

    public function logo(Request $request)
    {
    	if($request->hasFile('logo')) 
        {
            $imageName1 = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $imageName1);

            $qdata4 = array('page_name' => 'general', 'element' => 'logo' );
            $pages4 = Pages::where($qdata4)->update(['contents' => $imageName1]);
        }
        return redirect()->back();
    }

    public function testimonialslogo(Request $request)
    {
        if($request->hasFile('testlogo')) 
        {
            $imageName1 = time().$request->testlogo->getClientOriginalName();
            $request->testlogo->move(public_path('uploads'), $imageName1);

            $qdata4 = array('page_name' => 'general', 'element' => 'testlogo' );
            $pages4 = Pages::where($qdata4)->update(['contents' => $imageName1]);
        }
        return redirect()->back(); 
    }

    public function links(Request $request)
    {
    	$sec1 = array(
            'facebook' => $request->facebook,
            'google' => $request->google,
            'linkedin' => $request->linkedin,
            'twitter' => $request->twitter,
            'youtube' => $request->youtube 
        );
        $s1 = json_encode($sec1);
        $qdata1 = array('page_name' => 'general', 'element' => 'sociallink' );
        $pages1 = Pages::where($qdata1)->update(['contents' => $s1]);

        return redirect()->back();
    }


    public function fabout(Request $request)
    {
        $qdata1 = array('page_name' => 'general', 'element' => 'about' ); 
        $pages1 = Pages::where($qdata1)->update(['contents' => $request->content]);        

        return redirect()->back();
    }

}