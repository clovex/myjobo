<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Jobs;
use App\Models\District;
use App\Models\Country;
use App\Models\Job_category;
use App\Models\Employment_type;
ini_set('memory_limit', '-1');
use Mail;
use Twitter;
use File;

class JobsController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $jobs = Jobs::orderBy('id', 'DESC')->paginate(100);
        $category = Job_category::orderBy('id','ASC')->get();
    	return view('pages.admin.jobs.index', compact('jobs','category'));
    }

    public function add()
    {
        $district = District::all();
        $country = Country::all();
        $job_category = Job_category::orderBy('name', 'ASC')->get();
        $employment_type = Employment_type::orderBy('id', 'ASC')->get();
        return view('pages.admin.jobs.add', compact('district','country','job_category','employment_type'));
    }

    public function post(Request $request)
    {
        $jobs = new Jobs;

        $string = str_replace(' ', '-', strtolower($request->job_title));
        $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slugdata2 = str_replace('--', '-', $slugdata1);
        $slugdata = str_replace('--', '-', $slugdata2);
        $seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 7) as $k) $rand .= $seed[$k];
               
        $chk = Jobs::where('slug',$slugdata)->count();
        if($chk == 0)
        {
            $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }

        if($request->hasFile('logo')) 
        {
            $photos = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $photos);
            $jobs->logo = $photos;
        }
        $jobs->type = $request->type;
        $jobs->user_id = $request->user_id;
        $jobs->company_name = $request->company_name;
        $jobs->contact_name = $request->contact_name;
        $jobs->number = $request->number;
        $jobs->email = $request->email;
        $jobs->secondary_email = $request->secondary_email;
        $jobs->website = $request->website;
        $jobs->title = $request->job_title;
        $jobs->city = $request->city;
        $jobs->district = $request->district;
        $jobs->country = $request->country;
        $jobs->job_cat = $request->job_cat;
        $jobs->deadline = $request->deadline;
        $jobs->emp_type = $request->emp_type;
        $jobs->salary = $request->salary;
        $jobs->des = $request->description;
        if($request->featured == null)
        {
            $featureddata = 0;
        }
        else
        {
            $featureddata = 1;   
        }    
        $jobs->featured = $featureddata;
        $jobs->slug = $slug;
        $jobs->save();
 
        $subjects = "Your Job Advert has been posted on Myjobo.com";
        $email = $request->email;
        $data['slug'] = $slug;

        Mail::send('emails.employes',['data' => $data], function($message) use($email,$subjects)
          {
                $message->to($email, 'Myjobo')->subject($subjects);
              });
        
        include("fb_class.php");
        $message = "A new job has been posted on www.myjobo.com";
        $titles=$request->job_title;
        $targetUrl = url('job').'/'.$slug;
        $imgUrl = url('frontend/images/fblogo.jpg');
        $description = strip_tags($request->description);
        $facebook = new FacebookApi($facebookData);
        $facebook->share($message,$titles, $targetUrl, $imgUrl, $description, $access_token);
        $tweetMessage = 'Vacancy - '.$titles.'. Check now '.$targetUrl;
        $twitter = Twitter::post('statuses/update', array('status' => $tweetMessage));
        return redirect('admin/jobs');

    }

    public function single($id)
    {
        $district = District::all();
        $country = Country::all();
        $job_category = Job_category::orderBy('name', 'ASC')->get();
        $employment_type = Employment_type::orderBy('id', 'ASC')->get();
        $jobs = Jobs::where('id',$id)->get();
        return view('pages.admin.jobs.edit', compact('jobs','district','country','job_category','employment_type'));
    }


    public function put(Request $request)
    {
        $jobs = array();
        if($request->hasFile('logo')) 
        {
            $photos = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $photos);
            $jobs['logo'] = $photos;
        }
        $jobs['type'] = $request->type;
        $jobs['company_name'] = $request->company_name;
        $jobs['contact_name'] = $request->contact_name;
        $jobs['number'] = $request->number;
        $jobs['email'] = $request->email;
        $jobs['secondary_email'] = $request->secondary_email;
        $jobs['website'] = $request->website;
        $jobs['title'] = $request->job_title;
        $jobs['city'] = $request->city;
        $jobs['district'] = $request->district;
        $jobs['country'] = $request->country;
        $jobs['job_cat'] = $request->job_cat;
        $jobs['deadline'] = $request->deadline;
        $jobs['emp_type'] = $request->emp_type;
        $jobs['salary'] = $request->salary;
        $jobs['des'] = $request->description;
        if($request->featured == null)
        {
            $featureddata = 0;
        }
        else
        {
            $featureddata = 1;   
        }    
        $jobs['featured'] = $featureddata;
        
        $pages1 = Jobs::where('id',$request->id)->update($jobs);
        return redirect('admin/jobs');
    }

    public function delete($id)
    {
        $jobs = Jobs::where('id',$id);    
        $jobs->delete();

        return redirect()->back();
    }
    
    public function search(Request $request)
    {
        $jobs = Jobs::where('company_name','like','%'.$request->search.'%')
                    ->orwhere('title','like','%'.$request->search.'%')
                    ->orderBy('id', 'DESC')
                    ->paginate(100);
        $category = Job_category::orderBy('id','ASC')->get();
        return view('pages.admin.jobs.index', compact('jobs','category'));
    }
    

}
