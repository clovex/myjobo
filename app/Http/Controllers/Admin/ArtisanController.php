<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Role;
use App\Models\Admin\Users;
use DB;
use App\Models\Position;

class ArtisanController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
    	$users = Role::where('role_id','6')->with('users')->orderBy('id', 'DESC')->paginate(100);
        return view('pages.admin.artisan.index',compact('users'));
    }

    public function add()
    {
        return view('pages.admin.artisan.add');    
    }

    public function post(Request $request)
    {
        $users = new Users;
        $users->name = $request->name;
        $users->first_name = "";
        $users->last_name = "";
        $users->email = $request->email;
        $users->password = md5($request->password);
        $users->activated = '1';
        $users->status = '1';
        $users->save();

        $role = new Role;
        $role->role_id = '6';
        $role->user_id = $users->id;
        $role->save();
        return redirect('admin/staff-requester');
    }

    public function single($id)
    {
        $positions = Position::all();
    	$users = Users::where('id',$id)->get();
        return view('pages.admin.artisan.edit',compact('users','positions'));	
    }

    public function put(Request $request)
    {
    	$users = array();
    	$users['name'] = $request->name;
        $users['contact_name'] = $request->contact_name;
        $users['email'] = $request->email;
        $users['phone'] = $request->phone;
        $users['website'] = $request->website;
        $users['gender'] = $request->gender;
        $users['address'] = $request->address;
        
        $pages1 = Users::where('id',$request->id)->update($users);
        return redirect('admin/staff-requester');
    }

    public function delete($id)
    {
        $users = Users::where('id',$id);    
        $users->delete();

        return redirect()->back(); 
    }

    public function deactive($id)
    {
    	$users = array();
    	$users['status'] = 0;
       
        $pages1 = Users::where('id',$id)->update($users);
        return redirect()->back();	
    } 

    public function active($id)
    {
    	$users = array();
    	$users['status'] = 1;
        $users['activated'] = 1;
       
        $pages1 = Users::where('id',$id)->update($users);
        return redirect()->back();	
    }

    public function search(Request $request)
    {
        $users = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->where('role_user.role_id','=','6')
                ->where(function ($query) use ($request) {
                        $query->where("first_name", 'like', '%'.$request->search.'%')
                        ->orwhere("last_name", 'like', '%'.$request->search.'%')
                        ->orwhere("email", 'like', '%'.$request->search.'%');
                    })
                ->paginate(100);
        return view('pages.admin.artisan.search',compact('users'));
    }

}