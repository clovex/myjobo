<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Pages;

class FraudController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
    	$qdata = array('page_name' => 'fraud', 'element' => 'content' );
    	$pages = Pages::where($qdata)->get();
        return view('pages.admin.pages.fraud', compact('pages'));
    }


    public function update(Request $request)
    {
    	$contents = $request->description;
    	$qdata = array('page_name' => 'fraud', 'element' => 'content' );
    	$pages = Pages::where($qdata)->update([
        'contents' => $contents
      	]);
    	return redirect()->back();
    }

}
