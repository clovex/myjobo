<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Pages;
use App\Models\Services;
use App\Models\Servicestype;


class ServicesController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
        $q1 = array('page_name' => 'services', 'element' => 'main' );
        $s1 = Pages::where($q1)->get();

        $q2 = array('page_name' => 'services', 'element' => 'services1' );
        $s2 = Pages::where($q2)->get();

        $q3 = array('page_name' => 'services', 'element' => 'services2' );
        $s3 = Pages::where($q3)->get();

        $q4 = array('page_name' => 'services', 'element' => 'services3' );
        $s4 = Pages::where($q4)->get();

        $q5 = array('page_name' => 'services', 'element' => 'services4' );
        $s5 = Pages::where($q5)->get();

    	return view('pages.admin.services.pages', compact('s1','s2','s3','s4','s5'));
    }

    public function servicesone(Request $request)
    {
        $contents = $request->services;
        $qdata = array('page_name' => 'services', 'element' => 'main' );
        $pages = Pages::where($qdata)->update([
        'contents' => $contents
        ]);
        return redirect()->back();
    }

    public function servicestwo(Request $request)
    {
        $sec1 = array(
            'title' => $request->stitle1,
            'dec' => $request->sdes1, 
        );
        $s1 = json_encode($sec1);
        $qdata1 = array('page_name' => 'services', 'element' => 'services1' );
        $pages1 = Pages::where($qdata1)->update(['contents' => $s1]);

        $sec2 = array(
            'title' => $request->stitle2,
            'dec' => $request->sdes2, 
        );
        $s2 = json_encode($sec2);
        $qdata2 = array('page_name' => 'services', 'element' => 'services2' );
        $pages2 = Pages::where($qdata2)->update(['contents' => $s2]);

        $sec3 = array(
            'title' => $request->stitle3,
            'dec' => $request->sdes3, 
        );
        $s3 = json_encode($sec3);
        $qdata3 = array('page_name' => 'services', 'element' => 'services3' );
        $pages3 = Pages::where($qdata3)->update(['contents' => $s3]);

        $sec4 = array(
            'title' => $request->stitle4,
            'dec' => $request->sdes4, 
        );
        $s4 = json_encode($sec4);
        $qdata4 = array('page_name' => 'services', 'element' => 'services4' );
        $pages4 = Pages::where($qdata4)->update(['contents' => $s4]);

        return redirect()->back();
    }

    public function get()
    {
        $services = Services::all();
        $type = Servicestype::all();
        return view('pages.admin.services.index', compact('services','type'));
    }

    public function add()
    {
        $type = Servicestype::all();
        return view('pages.admin.services.add',compact('type'));   
    }

    public function post(Request $request)
    {

        $string = str_replace(' ', '-', strtolower(request('title')));
        $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slugdata = str_replace('--', '-', $slugdata1);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = Services::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }

        Services::create([
            'title' => request('title'),
            'description' =>request('description'),
            'type' =>request('type'),
            'slug' => $slug
        ]);

        return redirect('admin/services');
    }

    public function single($id)
    {
        $services = Services::where('id',$id)->get();
        $type = Servicestype::all();
        return view('pages.admin.services.edit', compact('services','type'));
    }

    public function put(Request $request)
    {
        $data = array(
            'title' => $request->title,
            'description' => $request->description,
            'type' =>$request->type
        );
        $pages1 = Services::where('id',$request->id)->update($data);
        return redirect('admin/services');
    }

    public function delete($id)
    {
        $featured = Services::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }


}    