<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Partners;

class PartnersController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $partners = Partners::all();
    	return view('pages.admin.partners.index', compact('partners'));
    }

    public function add()
    {
        return view('pages.admin.partners.add');   
    }

    public function post(Request $request)
    {
        $partners = new Partners;
        $string = str_replace(' ', '-', strtolower(request('name')));
        $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slugdata = str_replace('--', '-', $slugdata1);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = Partners::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }
        
        $partners->name = $request->name;
        $partners->slug = $slug;
        $partners->save();
        return redirect('admin/partners');
    }

    public function single($id)
    {
        $partners = Partners::where('id',$id)->get();
        return view('pages.admin.partners.edit', compact('partners'));
    }

    public function put(Request $request)
    {
        $partners = array();

        $string = str_replace(' ', '-', strtolower(request('name')));
        $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slugdata = str_replace('--', '-', $slugdata1);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = Partners::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }
        
        $partners['name'] = $request->name;
        $partners['slug'] = $slug;

        $pages1 = Partners::where('id',$request->id)->update($partners);
        return redirect('admin/partners');
    }

    public function delete($id)
    {
        $partners = Partners::where('id',$id);    
        $partners->delete();

        return redirect()->back();  
    }

}
