<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Applicants;

class ApplicantsController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $applicants = Applicants::with(['jobs','users'])->paginate(100);
    	return view('pages.admin.applicants.index', compact('applicants'));
    }
}