<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Speciality;

class SpecialityController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
        $speciality = Speciality::all();
    	return view('pages.admin.speciality.index', compact('speciality'));
    }

    public function post(Request $request)
    {
    	$speciality = new Speciality;
    	$speciality->name = $request->name;
    	$speciality->save();
    	return redirect()->back();
    }

    public function single($id)
    {
    	$speciality = Speciality::where('id',$id)->get();
    	return view('pages.admin.speciality.edit', compact('speciality'));	
    }

    public function update(Request $request)
    {
    	$speciality = array();
    	$speciality['name'] = $request->name;
        
        $pages1 = Speciality::where('id',$request->id)->update($speciality);
        return redirect('admin/speciality');
    }

    public function delete($id)
    {
        $featured = Speciality::where('id',$id);    
        $featured->delete();
        return redirect()->back();  
    }
}