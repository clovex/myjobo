<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Faqcategory;

class FaqsController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $faqs = Faq::all();
    	return view('pages.admin.pages.faq', compact('faqs'));
    }

    public function add()
    {
    	$faqscategory = Faqcategory::all();
        return view('pages.admin.pages.faqadd', compact('faqscategory'));   
    }

    public function post(Request $request)
    {
    	//$dec=htmlentities(request('description'), ENT_QUOTES, 'UTF-8', false);
    	
        Faq::create([
        	'cat_id' => request('faqcat'),
            'title' => request('title'),
            'description' =>request('description')
        ]);

        return redirect('admin/faqs');
    }

    public function single($id)
    {
    	$faqscategory = Faqcategory::all();
        $faqs = Faq::where('id',$id)->get();
        return view('pages.admin.pages.faqedit', compact('faqs','faqscategory'));
    }

    public function put(Request $request)
    {
        $data = array(
            'title' => $request->title,
            'description' => $request->description,
            'cat_id'=>$request->faqcat
        );
        $pages1 = Faq::where('id',$request->id)->update($data);
        return redirect('admin/faqs');
    }

    public function delete($id)
    {
        $featured = Faq::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }
	public function postcategory()
    {
    	$faqscategory = Faqcategory::all();
    	return view('pages.admin.pages.faqaddcategory',compact('faqscategory')); 
    }
	public function addpostcategory(Request $request)
    {
        Faqcategory::create([
            'title' => request('title'),
            'icon' =>request('inputid2')
        ]);

        return redirect('admin/faqs/postcategory');
    }
	public function deletecategory($id)
    {
        $featuredcategory = Faqcategory::where('id',$id);    
        $featuredcategory->delete();

        return redirect()->back();   
    }
	public function singlecategory($id)
    {
        $faqscategory = Faqcategory::where('id',$id)->get();
        return view('pages.admin.pages.faqcategoryedit', compact('faqscategory'));
    }
	 public function updatecategory(Request $request)
    {
        $data = array(
            'title' => $request->title,
            'icon' => $request->inputid2,
            'orderby' => $request->orderby
        );
        $pages1 = Faqcategory::where('id',$request->id)->update($data);
        return redirect('admin/faqs/postcategory');
    }
}
