<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Testimonials;

class TestimonialsController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $testimonials = Testimonials::all();
    	return view('pages.admin.testimonials.index', compact('testimonials'));
    }

    public function add()
    {
        return view('pages.admin.testimonials.add');   
    }

    public function post(Request $request)
    {
        $testimonial = new Testimonials;
        
        if($request->hasFile('photo')) 
        {
            $photos = time().$request->photo->getClientOriginalName();
            $request->photo->move(public_path('uploads'), $photos);
            $testimonial->photo = $photos;
        }

        if($request->hasFile('logo')) 
        {
            $logos = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $logos);
            $testimonial->logo = $logos;
        }

        $testimonial->name = $request->name;
        $testimonial->company = $request->company;
        $testimonial->position = $request->position;
        $testimonial->description = $request->description;
        $testimonial->save();
        return redirect('admin/testimonials');
    }

    public function single($id)
    {
        $testimonials = Testimonials::where('id',$id)->get();
        return view('pages.admin.testimonials.edit', compact('testimonials'));
    }

    public function put(Request $request)
    {
        $testimonial = array();
        
        if($request->hasFile('photo')) 
        {
            $photos = time().$request->photo->getClientOriginalName();
            $request->photo->move(public_path('uploads'), $photos);
            $testimonial['photo'] = $photos;
        }

        if($request->hasFile('logo')) 
        {
            $logos = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $logos);
            $testimonial['logo'] = $logos;
        }

        $testimonial['name'] = $request->name;
        $testimonial['company'] = $request->company;
        $testimonial['position'] = $request->position;
        $testimonial['description'] = $request->description;
        
        $pages1 = Testimonials::where('id',$request->id)->update($testimonial);
        return redirect('admin/testimonials');
    }

    public function delete($id)
    {
        $featured = Testimonials::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

}
