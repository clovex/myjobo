<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Industry;

class IndustryController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
        $industry = Industry::all();
        return view('pages.admin.industry.index', compact('industry'));
    }

    public function post(Request $request)
    {

        Industry::create([
            'name' => request('name')
        ]);

        return redirect()->back();  
    }

    public function single($id)
    {
        $industry = Industry::where('id',$id)->get();
        return view('pages.admin.industry.edit', compact('industry'));
    }

    public function put(Request $request)
    {
        $data = array(
            'name' => $request->name
        );
        $pages1 = Industry::where('id',$request->id)->update($data);
        return redirect('admin/industry');
    }

    public function delete($id)
    {
        $featured = Industry::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

}