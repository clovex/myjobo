<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\Schooltype;


class SchoolController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $school = School::with('types')->get();
    	return view('pages.admin.school.index', compact('school'));
    }

    public function add()
    {
        $schooltype = Schooltype::all();
        return view('pages.admin.school.add', compact('schooltype'));   
    }

    public function post(Request $request)
    {
        $school = new School;
        
        if($request->hasFile('logo')) 
        {
            $logos = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $logos);
            $school->logo = $logos;
        }

        $string = str_replace(' ', '-', strtolower(request('name')));
        $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slugdata = str_replace('--', '-', $slugdata1);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = School::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }

        $school->name = $request->name;
        $school->principal = $request->principal;
        $school->email = $request->email;
        $school->phone = $request->phone;
        $school->address = $request->address;
        $school->website = $request->website;
        $school->type = $request->type;
        $school->description = $request->description;
        $school->slug = $slug;
        $school->save();
        return redirect('admin/school');
    }

    public function single($id)
    {
        $school = School::where('id',$id)->get();
        $schooltype = Schooltype::all();
        return view('pages.admin.school.edit', compact('school','schooltype'));
    }

    public function put(Request $request)
    {
        $school = array();
        
        if($request->hasFile('logo')) 
        {
            $logos = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $logos);
            $school['logo'] = $logos;
        }

        $school['name'] = $request->name;
        $school['principal'] = $request->principal;
        $school['phone'] = $request->phone;
        $school['email'] = $request->email;
        $school['address'] = $request->address;
        $school['website'] = $request->website;
        $school['type'] = $request->type;
        $school['description'] = $request->description;        
        $pages1 = School::where('id',$request->id)->update($school);
        return redirect('admin/school');
    }

    public function delete($id)
    {
        $school = School::where('id',$id);    
        $school->delete();

        return redirect()->back();  
    }

}
