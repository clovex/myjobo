<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blogs;
use App\Models\Blogs_category;


class BlogsController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $blogs = Blogs::with('category')->orderBy('id', 'desc')->get();
        //$blogs = Blogs::all();
        return view('pages.admin.blog.blog', compact('blogs'));
    }

    public function add()
    {
        $category = Blogs_category::all();
        return view('pages.admin.blog.blogadd', compact('category'));   
    }

    public function post(Request $request)
    {

        $string = str_replace(' ', '-', strtolower(request('title')));
        $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slugdata = str_replace('--', '-', $slugdata1);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = Blogs::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }


        Blogs::create([
            'blog_category' => request('category'),
            'title' => request('title'),
            'author' => request('author'),
            'description' => request('description'),
            'slug' => $slug
            
        ]);

        return redirect('admin/blogs');
    }

    public function single($id)
    {
        $blogs = Blogs::where('id',$id)->get();
        $category = Blogs_category::all();
        return view('pages.admin.blog.blogedit', compact('blogs','category'));
    }

    public function put(Request $request)
    {
        $string = str_replace(' ', '-', strtolower(request('title')));
        $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slugdata = str_replace('--', '-', $slugdata1);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = Blogs::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }

        $data = array(
            'blog_category' => request('category'),
            'title' => request('title'),
            'author' => request('author'),
            'description' => request('description'),
            'slug' => $slug
        );
        $pages1 = Blogs::where('id',$request->id)->update($data);
        return redirect('admin/blogs');
    }

    public function delete($id)
    {
        $featured = Blogs::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }
}
