<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Careers;

use App\Models\Admin\Pages;


class CareersController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $careers = Careers::all();
    	return view('pages.admin.careers.index', compact('careers'));
    }

    public function add()
    {
        return view('pages.admin.careers.add');   
    }

    public function post(Request $request)
    {

        $string = str_replace(' ', '-', strtolower(request('title')));
        $slugdata1=preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $slugdata = str_replace('--', '-', $slugdata1);
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'); 
        shuffle($seed); 
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
           
        $chk = Careers::where('slug',$slugdata)->count();
        if($chk == 0)
        {
             $slug = $slugdata;
        }
        else
        {
            $slug = $slugdata.'-'.$rand;
        }

        Careers::create([
            'title' => request('title'),
            'description' =>request('description'),
            'type' => request('type'),
            'slug' => $slug
        ]);

        return redirect('admin/careers');
    }

    public function single($id)
    {
        $careers = Careers::where('id',$id)->get();
        return view('pages.admin.careers.edit', compact('careers'));
    }

    public function put(Request $request)
    {
        $data = array(
            'title' => $request->title,
            'description' => $request->description,
            'type' => $request->type
        );
        $pages1 = Careers::where('id',$request->id)->update($data);
        return redirect('admin/careers');
    }

    public function delete($id)
    {
        $featured = Careers::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

    public function courses()
    {
        $qdata1 = array('page_name' => 'careers', 'element' => 'courses' );
        $courses = Pages::where($qdata1)->get();

        return view('pages.admin.careers.courses', compact('courses'));
    }

    public function coursesdata(Request $request)
    {
        $contents = $request->description;
        $qdata = array('page_name' => 'careers', 'element' => 'courses' );
        $pages = Pages::where($qdata)->update([
        'contents' => $contents
        ]);
        return redirect()->back();
    }


}
