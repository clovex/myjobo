<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Coursescategory;
use App\Models\Courses;

class CoursesController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
    	$courses = Courses::all();
        return view('pages.admin.courses.index',compact('courses'));
    }

    public function add()
    {
    	$coursescategory = Coursescategory::all();
    	return view('pages.admin.courses.add',compact('coursescategory'));
    }
    public function post(Request $request)
    {
    	$courses = new Courses;

        if($request->hasFile('image')) 
        {
            $imageName3 = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $imageName3);
            $courses->image = $imageName3;
        }
    	
    	$courses->name = $request->name;
    	$courses->category = $request->category;
    	$courses->email = $request->email;
    	$courses->duration = $request->duration;
    	$courses->qualification = $request->qualification;
    	$courses->study_method = $request->study_method;
    	$courses->price = $request->price;
    	$courses->requirement = $request->requirement;
    	$courses->description = $request->description;

    	$courses->save();
    	return redirect('admin/courses');		
    }

    public function single($id)
    {
        $courses = Courses::where('id',$id)->get();
        $coursescategory = Coursescategory::all();
        return view('pages.admin.courses.edit',compact('coursescategory','courses'));
    }

    public function put(Request $request)
    {
        $courses = array();

        if($request->hasFile('image')) 
        {
            $imageName3 = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $imageName3);
            $courses['image'] = $imageName3;
        }
        $courses['name'] = $request->name;
        $courses['category'] = $request->category;
        $courses['email'] = $request->email;
        $courses['duration'] = $request->duration;
        $courses['qualification'] = $request->qualification;
        $courses['study_method'] = $request->study_method;
        $courses['price'] = $request->price;
        $courses['requirement'] = $request->requirement;
        $courses['description'] = $request->description;
        
        $pages1 = Courses::where('id',$request->id)->update($courses);
        return redirect('admin/courses');
    }

    public function delete($id)
    {
        $courses = Courses::where('id',$id);    
        $courses->delete();
        return redirect()->back();
    }


    public function category()
    {
    	$coursescategory = Coursescategory::all();
    	return view('pages.admin.courses.category',compact('coursescategory'));
    }

    public function categoryadd(Request $request)
    {
    	$coursescategory = new Coursescategory;
    	if($request->hasFile('image')) 
        {
            $photos = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $photos);
            $coursescategory->image = $photos;
        }
        $coursescategory->name = $request->name;
        $coursescategory->save();
        return redirect()->back();
    }

    public function categorysingle($id)
    {
    	$coursescategory = Coursescategory::where('id',$id)->get();
    	return view('pages.admin.courses.categoryedit',compact('coursescategory'));	
    }

    public function categoryupdate(Request $request)
    {
    	$data = array();
         if($request->hasFile('image')) 
        {
            $photos = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $photos);
            $data['image'] = $photos;
        }
        $data['name'] = $request->name;
        $pages1 = Coursescategory::where('id',$request->id)->update($data);
        return redirect('admin/coursescategory');
    }

    public function categorydelete($id)
    {
    	$coursescategory = Coursescategory::where('id',$id);    
        $coursescategory->delete();
        return redirect()->back();
    }



}