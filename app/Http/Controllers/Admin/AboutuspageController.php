<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Pages;
use App\Models\Featured;

class AboutuspageController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
        $qdata1 = array('page_name' => 'about_us', 'element' => 'data' );
        $about1 = Pages::where($qdata1)->get();

        $qdata2 = array('page_name' => 'about_us', 'element' => 'sec_2_des' );
        $about2 = Pages::where($qdata2)->get();

        $qdata3 = array('page_name' => 'about_us', 'element' => 'profile1' );
        $about3 = Pages::where($qdata3)->get();

        $qdata4 = array('page_name' => 'about_us', 'element' => 'profile2' );
        $about4 = Pages::where($qdata4)->get();

        $qdata5 = array('page_name' => 'about_us', 'element' => 'profile3' );
        $about5 = Pages::where($qdata5)->get();

        $qdata6 = array('page_name' => 'about_us', 'element' => 'profileimg1' );
        $about6 = Pages::where($qdata6)->get();

        $qdata7 = array('page_name' => 'about_us', 'element' => 'profileimg2' );
        $about7 = Pages::where($qdata7)->get();

        $qdata8 = array('page_name' => 'about_us', 'element' => 'profileimg3' );
        $about8 = Pages::where($qdata8)->get();

        $qdata9 = array('page_name' => 'about_us', 'element' => 'sec_3_des1' );
        $about9 = Pages::where($qdata9)->get(); 

        $qdata10 = array('page_name' => 'about_us', 'element' => 'sec_3_des2' );
        $about10 = Pages::where($qdata10)->get();

        $qdata11 = array('page_name' => 'about_us', 'element' => 'sec_3_des3' );
        $about11 = Pages::where($qdata11)->get();

        $qdata12 = array('page_name' => 'about_us', 'element' => 'sec_3_des4' );
        $about12 = Pages::where($qdata12)->get();

        $about13 = Featured::all();


    	return view('pages.admin.pages.about', compact('about1','about2','about3','about4','about5','about6','about7','about8','about9','about10','about11','about12','about13'));
    }

    public function aboutone(Request $request)
    {
        $contents = $request->aboutus;
        $qdata = array('page_name' => 'about_us', 'element' => 'data' );
        $pages = Pages::where($qdata)->update([
        'contents' => $contents
        ]);
        return redirect()->back();
    }


    public function abouttwo(Request $request)
    {
        $contents = $request->title;
        $qdata = array('page_name' => 'about_us', 'element' => 'sec_2_des' );
        $pages = Pages::where($qdata)->update(['contents' => $contents]);

        $profile1 = array(
            'name' => $request->name1,
            'qua' => $request->qua1,
            'des' => $request->des1,
            'email' => $request->email1 
        );
        $p1 = json_encode($profile1);
        $qdata1 = array('page_name' => 'about_us', 'element' => 'profile1' );
        $pages1 = Pages::where($qdata1)->update(['contents' => $p1]);

        $profile2 = array(
            'name' => $request->name2,
            'qua' => $request->qua2,
            'des' => $request->des2,
            'email' => $request->email2 
        );
        $p2 = json_encode($profile2);
        $qdata2 = array('page_name' => 'about_us', 'element' => 'profile2' );
        $pages2 = Pages::where($qdata2)->update(['contents' => $p2]);

        $profile3 = array(
            'name' => $request->name3,
            'qua' => $request->qua3,
            'des' => $request->des3,
            'email' => $request->email3 
        );
        $p3 = json_encode($profile3);

        $qdata3 = array('page_name' => 'about_us', 'element' => 'profile3' );
        $pages3 = Pages::where($qdata3)->update(['contents' => $p3]);

        if($request->hasFile('image1')) 
        {
            $imageName1 = time().$request->image1->getClientOriginalName();
            $request->image1->move(public_path('uploads'), $imageName1);

            $qdata4 = array('page_name' => 'about_us', 'element' => 'profileimg1' );
            $pages4 = Pages::where($qdata4)->update(['contents' => $imageName1]);
        }

 
        if($request->hasFile('image2')) 
        {
            $imageName2 = time().$request->image2->getClientOriginalName();
            $request->image2->move(public_path('uploads'), $imageName2);

            $qdata5 = array('page_name' => 'about_us', 'element' => 'profileimg2' );
            $pages5 = Pages::where($qdata5)->update(['contents' => $imageName2]);
        }

        if($request->hasFile('image3')) 
        {
            $imageName3 = time().$request->image3->getClientOriginalName();
            $request->image3->move(public_path('uploads'), $imageName3);

            $qdata6 = array('page_name' => 'about_us', 'element' => 'profileimg3' );
            $pages6 = Pages::where($qdata6)->update(['contents' => $imageName3]);
        }

        return redirect()->back();
    }



    public function aboutthree(Request $request)
    {
        $sec1 = array(
            'title' => $request->title1,
            'dec' => $request->dec1, 
        );
        $s1 = json_encode($sec1);
        $qdata1 = array('page_name' => 'about_us', 'element' => 'sec_3_des1' );
        $pages1 = Pages::where($qdata1)->update(['contents' => $s1]);

        $sec2 = array(
            'title' => $request->title2,
            'dec' => $request->dec2, 
        );
        $s2 = json_encode($sec2);
        $qdata2 = array('page_name' => 'about_us', 'element' => 'sec_3_des2' );
        $pages2 = Pages::where($qdata2)->update(['contents' => $s2]);

        $sec3 = array(
            'title' => $request->title3,
            'dec' => $request->dec3, 
        );
        $s3 = json_encode($sec3);
        $qdata3 = array('page_name' => 'about_us', 'element' => 'sec_3_des3' );
        $pages3 = Pages::where($qdata3)->update(['contents' => $s3]);

        $sec4 = array(
            'title' => $request->title4,
            'dec' => $request->dec4, 
        );
        $s4 = json_encode($sec4);
        $qdata4 = array('page_name' => 'about_us', 'element' => 'sec_3_des4' );
        $pages4 = Pages::where($qdata4)->update(['contents' => $s4]);


        return redirect()->back();

    }

    public function aboutfour(Request $request)
    {
        if($request->hasFile('img')) 
        {
            $imageName3 = time().$request->img->getClientOriginalName();
            $request->img->move(public_path('uploads'), $imageName3);

            Featured::create([
            'image' =>$imageName3,
            'link' =>request('link')
            ]);
        }


        return redirect()->back();
    }


    public function aboutdelete($id)
    {
        $featured = Featured::where('f_id',$id);    
        $featured->delete();

        return redirect()->back();  
    }


    
}
