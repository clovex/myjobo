<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;

class CityController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
        $city = City::all();
    	return view('pages.admin.city.index', compact('city'));
    }

    public function post(Request $request)
    {
    	$city = new City;
    	$city->name = $request->name;
    	$city->save();
    	return redirect()->back();
    }

    public function single($id)
    {
    	$city = City::where('id',$id)->get();
    	return view('pages.admin.city.edit', compact('city'));	
    }

    public function update(Request $request)
    {
    	$city = array();
    	$city['name'] = $request->name;
        
        $pages1 = City::where('id',$request->id)->update($city);
        return redirect('admin/city');
    }

    public function delete($id)
    {
        $featured = City::where('id',$id);    
        $featured->delete();
        return redirect()->back();  
    }
}