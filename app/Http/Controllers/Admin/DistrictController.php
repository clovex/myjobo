<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\District;

class DistrictController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $district = District::all();
        return view('pages.admin.district.index', compact('district'));
    }

    public function add()
    {
        return view('pages.admin.district.add');   
    }

    public function post(Request $request)
    {

        District::create([
            'name' => request('name')
        ]);

        return redirect('admin/district');
    }

    public function single($id)
    {
        $district = District::where('id',$id)->get();
        return view('pages.admin.district.edit', compact('district'));
    }

    public function put(Request $request)
    {
        $data = array(
            'name' => $request->name
        );
        $pages1 = District::where('id',$request->id)->update($data);
        return redirect('admin/district');
    }

    public function delete($id)
    {
        $featured = District::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

}