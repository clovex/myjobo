<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Staff;
use App\Models\Position;
use App\Models\Industry;
use DB;
use App\Models\Admin\Users;

class TradesController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index() 
    {
        $trades = Staff::with('usersdata')->orderBy('id', 'DESC')->paginate(100);
        $position = Position::all();
        $industry = Industry::all();
    	return view('pages.admin.trades.index', compact('trades','position','industry'));
    }

    public function add()
    {
        $position = Position::all();
        $industry = Industry::all();
        return view('pages.admin.trades.add',compact('position','industry'));   
    }

    public function post(Request $request)
    {
        $staff = new Staff;
        $staff->provider_id = $request->provider_id;
        $staff->industry = $request->industry;
        $staff->position = $request->position;
        $staff->staff = $request->staff;
        $staff->description = $request->description;
        $staff->age1 = $request->age1;
        $staff->age2 = $request->age2;
        $staff->gender = $request->gender;
        $staff->experience = $request->experience;
        $staff->education = $request->education;
        $staff->salary1 = $request->salary1."-".$request->salary1type;
        //$staff->salary2 = $request->salary2."-".$request->salary2type;
        $staff->interview_date = $request->interview_date;
        $staff->interview_time = $request->interview_time1.':'.$request->interview_time2;
        $staff->deadline = $request->deadline;
        $staff->description = $request->description;
        $staff->save();
        return redirect('admin/requests');
    }

    public function single($id)
    {
        $position = Position::all();
        $industry = Industry::all();
        $trades = Staff::where('id',$id)->get();
        return view('pages.admin.trades.edit', compact('trades','position','industry'));
    }

    public function put(Request $request)
    {
        $data = array(
            'provider_id' => $request->provider_id,
            'industry' => $request->industry,
            'position' => $request->position,
            'staff' => $request->staff,
            'description' => $request->description,
            'age1' => $request->age1,
            'age2' => $request->age2,
            'gender' => $request->gender,
            'experience' => $request->experience,
            'education' => $request->education,
            'salary1' => $request->salary1."-".$request->salary1type,
            //'salary2' => $request->salary2."-".$request->salary2type,
            'interview_date' => $request->interview_date,
            'interview_time' => $request->interview_time1.':'.$request->interview_time2,
            'deadline' => $request->deadline,
            'description' => $request->description
        );
        $pages1 = Staff::where('id',$request->id)->update($data);
        return redirect('admin/requests');
    }

    public function delete($id)
    {
        $featured = Staff::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

    public function search(Request $request)
    {
        $trades = DB::table('post_services')
                ->join('users', 'post_services.provider_id', '=', 'users.id')
                ->join('industry', 'post_services.industry', '=', 'industry.id')
                ->join('position', 'post_services.position', '=', 'position.id')
                ->where(function ($query) use ($request) {
                        $query->where("position.name", 'like', '%'.$request->search.'%')
                        ->orwhere("industry.name", 'like', '%'.$request->search.'%');
                    })
                ->select('post_services.id','post_services.industry','post_services.position','post_services.deadline','users.name')
                ->paginate(100);    
        $position = Position::all();
        $industry = Industry::all();
        return view('pages.admin.trades.search',compact('trades','position','industry'));
    }

    public function view($id)
    {
        $trades = DB::table('post_services')
                ->join('users', 'post_services.provider_id', '=', 'users.id')
                ->join('industry', 'post_services.industry', '=', 'industry.id')
                ->join('position', 'post_services.position', '=', 'position.id')
                ->where('post_services.id',$id)
                ->select('post_services.*','users.name','users.email','users.phone','users.contact_name','users.website')
                ->paginate(100);    
        $position = Position::all();
        $industry = Industry::all();
        return view('pages.admin.trades.view',compact('trades','position','industry'));
    }

}
