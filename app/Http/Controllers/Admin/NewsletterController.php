<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use App\Models\Postnewsletter;
use DB;
use Mail;

class NewsletterController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function get()
    {
        $newsletter = Newsletter::orderBy('id', 'DESC')->paginate(100);
        return view('pages.admin.newsletter.index', compact('newsletter'));
    }

    public function delete($id)
    {
        $featured = Newsletter::where('id',$id);    
        $featured->delete();
        return redirect()->back();  
    }

    public function memberadd()
    {
        return view('pages.admin.newsletter.memberadd');
    }

    public function membersave(Request $request)
    {
        $newsletter = new Newsletter;

        $newsletter->name = $request->name;
        $newsletter->email = $request->email;
        $newsletter->save();

        return redirect('admin/newsletter');
    }

    public function membersingle($id)
    {
        $newsletter = Newsletter::where('id',$id)->get(); 
        return view('pages.admin.newsletter.memberedit',compact('newsletter')); 
    }

    public function memberupdate(Request $request)
    {
        $data = array(
            'name' => request('name'),
            'email' => request('email')
        );
        $pages1 = Newsletter::where('id',$request->id)->update($data);
        return redirect('admin/newsletter');
    }

    public function search(Request $request)
    {
        $newsletter = DB::table('tbl_newsletter')
                        ->where("name", 'like', '%'.$request->search.'%')
                        ->orwhere("email", 'like', '%'.$request->search.'%')
                        ->paginate(100);

        return view('pages.admin.newsletter.search',compact('newsletter'));
    }

    public function all()
    {
        $newsletter = Postnewsletter::all();
        return view('pages.admin.newsletter.newsletter',compact('newsletter'));
    }

    public function post()
    {
        return view('pages.admin.newsletter.add');   
    }

    public function insert(Request $request)
    {
        Postnewsletter::create([
            'subject' => request('subject'),
            'text' => request('message')
        ]);
        return redirect('admin/newsletters');
    }

    public function edit($id)
    {
        $newsletter = Postnewsletter::where('id',$id)->get();
        return view('pages.admin.newsletter.edit',compact('newsletter'));   
    }

    public function update(Request $request)
    {
        $data = array(
            'subject' => request('subject'),
            'text' => request('message')
        );
        $pages1 = Postnewsletter::where('id',$request->id)->update($data);
        return redirect('admin/newsletters');
    }

    public function deletenews($id)
    {
        $featured = Postnewsletter::where('id',$id);    
        $featured->delete();
        return redirect()->back();  
    }

    public function send()
    {
        $newsletter = Postnewsletter::all();
        return view('pages.admin.newsletter.send',compact('newsletter'));     
    }

    public function sends(Request $request)
    {
        $data = Postnewsletter::where('id',$request->newsletter)->get();
        $subjects = "";
        foreach ($data as $d) {
            $subjects = $d->subject;
            $messages = $d->text;
        }

        if($request->job_seekers != null)
        {
            $jobseekers = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->select('users.email')
                ->where('role_user.role_id','2')
                ->get();

            foreach ($jobseekers as $jobseeker) 
            {
                $email  = $jobseeker->email;
                Mail::send('emails.newsletter',['user' => $messages], function($message) use($email,$subjects)
                {
                $message->to($email, 'Myjobo')->subject($subjects);
                });
            }    

        }

        if($request->employers != null)
        {
            $employers = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->select('users.email')
                ->where('role_user.role_id','4')
                ->get();

            foreach ($employers as $employer) 
            {
                $email  = $employer->email;
                Mail::send('emails.newsletter',['user' => $messages], function($message) use($email,$subjects)
                {
                $message->to($email, 'Myjobo')->subject($subjects);
                });
            }    

        }

        if($request->others != null)
        {
            $others = DB::table('tbl_newsletter')
                ->select('email')
                ->get();

            foreach ($others as $other) 
            {
                $email  = $other->email;
                Mail::send('emails.newsletter',['user' => $messages], function($message) use($email,$subjects)
                {
                $message->to($email, 'Myjobo')->subject($subjects);
                });
            }    

        }    
        
        return redirect('home');
    }
}
