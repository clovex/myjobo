<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Paymentmode;

class PaymentmodeController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
        $payment = Paymentmode::all();
    	return view('pages.admin.payment.index', compact('payment'));
    }

    public function post(Request $request)
    {
    	$payment = new Paymentmode;
    	$payment->type = $request->type;
    	$payment->save();
    	return redirect()->back();
    }

    public function single($id)
    {
    	$payment = Paymentmode::where('id',$id)->get();
    	return view('pages.admin.payment.edit', compact('payment'));	
    }

    public function update(Request $request)
    {
    	$payment = array();
    	$payment['type'] = $request->name;
        
        $pages1 = Paymentmode::where('id',$request->id)->update($payment);
        return redirect('admin/paymentmode');
    }

    public function delete($id)
    {
        $featured = Paymentmode::where('id',$id);    
        $featured->delete();
        return redirect()->back();  
    }
}