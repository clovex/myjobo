<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blogs_comment;
 
class Blogs_commentsController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
        $blogs_comment = Blogs_comment::all();
    	return view('pages.admin.blog.comments', compact('blogs_comment'));
    }

    public function delete($id)
    {
    	$featured = Blogs_comment::where('id',$id);    
        $featured->delete();

        return redirect()->back();  
    }

}