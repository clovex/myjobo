<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Pages;

class SidebarController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function index()
    {
    	$qdata1 = array('page_name' => 'all', 'element' => 'sidebar' );
        $common = Pages::where($qdata1)->get();

        $qdata2 = array('page_name' => 'Entrepreneurship', 'element' => 'sidebar' );
        $enter = Pages::where($qdata2)->get();

    	return view('pages.admin.pages.sidebar',compact('common','enter'));
    }

    public function common(Request $request)
    {
    	if($request->hasFile('image')) 
        {
            $imageName3 = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $imageName3);

            $sec4 = array(
            'image' => $imageName3,
            'link' => $request->link, 
        	);
        	$s4 = json_encode($sec4);
       	 	$qdata4 = array('page_name' => 'all', 'element' => 'sidebar' );
        	$pages4 = Pages::where($qdata4)->update(['contents' => $s4]);
		} 

		return redirect()->back();

    }

    public function enter(Request $request)
    {
    	if($request->hasFile('image')) 
        {
            $imageName3 = time().$request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $imageName3);

            $sec4 = array(
            'image' => $imageName3,
            'link' => $request->link, 
        	);
        	$s4 = json_encode($sec4);
       	 	$qdata4 = array('page_name' => 'Entrepreneurship', 'element' => 'sidebar' );
        	$pages4 = Pages::where($qdata4)->update(['contents' => $s4]);
		} 

		return redirect()->back();

    }

}