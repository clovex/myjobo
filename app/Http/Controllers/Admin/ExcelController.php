<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use App\Models\Newsletter;
use App\Models\Jobs;
use DB;
use Excel;
use Carbon\Carbon;
ini_set('memory_limit','3072M');

class ExcelController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth'); 
    }

    public function newsletter()
    {
    	$datas = Newsletter::select('name','email','created_at')->get();

        $data = array();        
        foreach ($datas as $d) 
        { 
            $pp['name'] = $d->name;
            $pp['email'] = $d->email;  
            $pp['created_at'] = Carbon::parse($d->created_at)->format('d-m-Y'); 
            $data[] = $pp;  
        }        

    	$excel = new \PHPExcel();
        $excel->createSheet();
        $excel->setActiveSheetIndex(1);
        $excel->getActiveSheet()->setTitle('Newsletter');

        $objWorksheet = $excel->getActiveSheet();
        $objWorksheet->fromArray($data);
        $writer = new \PHPExcel_Writer_Excel2007($excel);
        $writer->setIncludeCharts(TRUE);
        $writer->save(storage_path().'/Newsletter.xlsx');
    }

    public function jobs()
    {
        $datas = Jobs::select('company_name','email','number','title','created_at')->get();
        $data = array();

        foreach ($datas as $d) 
        { 
            $pp['company_name'] = $d->company_name;
            $pp['email'] = $d->email;
            $pp['number'] = $d->number;
            $pp['title'] = $d->title;  
            $pp['created_at'] = Carbon::parse($d->created_at)->format('d-m-Y'); 
            $data[] = $pp;  
        }

        $excel = new \PHPExcel();
        $excel->createSheet();
        $excel->setActiveSheetIndex(1);
        $excel->getActiveSheet()->setTitle('Jobs');

        $objWorksheet = $excel->getActiveSheet();
        $objWorksheet->fromArray($data);
        $writer = new \PHPExcel_Writer_Excel2007($excel);
        $writer->setIncludeCharts(TRUE);
        $writer->save(storage_path().'/jobs.xlsx');
    }

    public function jobseekers()
    {
        $datas = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->select('users.first_name','users.last_name','users.email','users.phone','users.created_at')
                ->where('role_user.role_id','2')
                ->get();
        $data = array();        
        foreach ($datas as $d) 
        {
            $pp['name'] = $d->first_name." ".$d->last_name;
            $pp['email'] = $d->email;  
            $pp['phone'] = $d->phone;   
            $pp['created_at'] = Carbon::parse($d->created_at)->format('d-m-Y');
            $data[] = $pp;  
        }        

        $excel = new \PHPExcel();
        $excel->createSheet();
        $excel->setActiveSheetIndex(1);
        $excel->getActiveSheet()->setTitle('jobseekers');

        $objWorksheet = $excel->getActiveSheet();
        $objWorksheet->fromArray($data);
        $writer = new \PHPExcel_Writer_Excel2007($excel);
        $writer->setIncludeCharts(TRUE);
        $writer->save(storage_path().'/jobseekers.xlsx');
    }

    public function employers()
    {
        $datas = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->select('users.name','users.last_name','users.email','users.phone','users.created_at')
                ->where('role_user.role_id','4')
                ->get();
        $data = array();        
        foreach ($datas as $d) 
        {
            $pp['name'] = $d->name;
            $pp['email'] = $d->email;  
            $pp['phone'] = $d->phone;   
            $data[] = $pp;  
        }        

        $excel = new \PHPExcel();
        $excel->createSheet(); 
        $excel->setActiveSheetIndex(1);
        $excel->getActiveSheet()->setTitle('employers');

        $objWorksheet = $excel->getActiveSheet();
        $objWorksheet->fromArray($data);
        $writer = new \PHPExcel_Writer_Excel2007($excel);
        $writer->setIncludeCharts(TRUE);
        $writer->save(storage_path().'/employers.xlsx');
    }
}
