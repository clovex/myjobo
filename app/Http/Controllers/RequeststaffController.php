<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Position;
use App\Models\Industry;

class RequeststaffController extends Controller
{

    public function index()
    {
    	$position = Position::all();
    	$industry = Industry::all();
    	return view('common.staff',compact('position','industry'));
    }

}
