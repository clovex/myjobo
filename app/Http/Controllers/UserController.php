<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Models\District;
use App\Models\Country;
use App\Models\Job_category;
use App\Models\Admin\Role;
use App\Models\Jobs;
use App\Models\Applicants;
use App\Models\Cvs;
use App\Models\Newsletter;
use App\Models\Tracker;
use Carbon\Carbon;
use App\Models\Position;
use App\Models\School;
use App\Models\Courses;
use App\Models\City;
use App\Models\Speciality;
use App\Models\Paymentmode;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();

        if ($user->isAdmin()) 
        {
            $empcount = Role::where('role_id','4')->count();
            $jobseekercount = Role::where('role_id','2')->count();
            $jobscount = Jobs::count();
            $applyjobscount = Applicants::count();
            $cvcount = Cvs::count();
            $newslettercount = Newsletter::count();
            $viewcount = Tracker::sum('hits');
            $tradesregcount = Role::where('role_id','5')->count();
            $staffregcount = Role::where('role_id','6')->count();
            $schoolcount = School::count();
            $coursescount = Courses::count();
            $featuredjobscount = Jobs::where('featured','1')->count();

            $empcurrent = Role::where('role_id','4')->where('created_at', '>=', Carbon::now()->startOfMonth())->count();

            $tradecurrent = Role::where('role_id','5')->where('created_at', '>=', Carbon::now()->startOfMonth())->count();
            
            $jobseekercurrent = Role::where('role_id','2')->where('created_at', '>=', Carbon::now()->startOfMonth())->count();

            $staffrcurrent = Role::where('role_id','6')->where('created_at', '>=', Carbon::now()->startOfMonth())->count();
            
            $jobcurrent = Jobs::where('created_at', '>=', Carbon::now()->startOfMonth())->count();

            $coursecurrent = Courses::where('created_at', '>=', Carbon::now()->startOfMonth())->count();
            
            $viewcurrent = Tracker::where('created_at', '>=', Carbon::now()->startOfMonth())->sum('hits');
            
            $newslettercurrent = Newsletter::where('created_at', '>=', Carbon::now()->startOfMonth())->count();

            return view('pages.admin.home',compact('empcount','jobseekercount','jobscount','applyjobscount','cvcount','newslettercount','viewcount','empcurrent','jobseekercurrent','jobcurrent','viewcurrent','tradesregcount','schoolcount','coursescount','staffregcount','featuredjobscount','tradecurrent','staffrcurrent','coursecurrent','newslettercurrent'));
        }

        if ($user->isUser()) {
            $job_category = Job_category::all();
            return view('pages.user.home',compact('job_category'));

        }

        if ($user->isEmployer()) 
        {
            $user = Auth::user();
            if($user->status == 0)
            {
                return redirect('accountpending');
            }   
            else
            { 
            $district = District::all();
            $country = Country::all();
            return view('pages.employer.home',compact('district','country'));
            }
        }

        if ($user->isCustomer()) 
        {
            $user = Auth::user();
            if($user->status == 0)
            {
                return redirect('accountpending');
            }   
            else
            {
            $paymentmode = Paymentmode::all();
            $city = District::all();
            $speciality = Speciality::all();
            return view('pages.services.home',compact('city','speciality','paymentmode'));
            }
        }

        if ($user->isProvider()) 
        {
            $user = Auth::user();
            if($user->status == 0)
            {
                return redirect('accountpending');
            }   
            else
            {
            return view('pages.provider.home');
            }
        }
    }

}