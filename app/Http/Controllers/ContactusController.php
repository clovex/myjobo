<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Pages;
//use Illuminate\Support\Facades\Mail;
use Mail;

class ContactusController extends Controller
{

    public function index()
    {
    	$qdata = array('page_name' => 'contact_us', 'element' => 'data' );
    	$pages = Pages::where($qdata)->get(['contents']);
        return view('common.contactus', compact('pages'));
    } 

    public function send(Request $request)
    {
        /*include("pmapirequest.php");
        include("pmapiauthhash.php");
        define('CID', 42277); // Company ID
        define('UID', 12425); // User ID 
        define('HASH', '2645c76161357820efc351dd8f9142ef'); // API access hash
        $request = new PMAPIRequest(new PMAPIAuthHash(UID, CID, HASH));
        $args = array(
        'to' =>'rajnik@xemesolutions.com',   
        'text' => 'demo',
        'subject' => 'demo',
        'fromemail' => 'info@myjobo.com',
        'fromname' => 'Myjobo',
        'name' => 'Myjobo.com',
        'replyemail' => 'no-reply@myjobo.com',
        );*/
    	$email = "info@myjobo.com"; 
        $data['name'] = $request->name; 
        $data['email'] = $request->email;
        $data['company'] = $request->company;
        $data['comment'] = $request->comment;
        $data['phone'] = $request->phone;

        Mail::send('emails.contact',['data' => $data], function($message) use($email)
        {
	       $message->to($email, 'Myjobo')->subject('Feedback Form');
        });
        
        if( count(Mail::failures()) > 0 ) 
        {
            foreach(Mail::failures as $email_address) 
            {
                echo "$email_address <br />";
            }
        } 
        else 
        {
            echo "Mail sent successfully!";
        }
        
        return redirect()->back()->withErrors(["Your Feedback Successfull Send."]);
    }

    public function requiestsend(Request $request)
    {
        $img = "";
        if($request->hasFile('logo')) 
        {
            $img = time().$request->logo->getClientOriginalName();
            $request->logo->move(public_path('uploads'), $img);  

        }
        $email = "info@myjobo.com";
        $data['name'] = $request->name;
        $data['principal'] = $request->principal;
        $data['phone'] = $request->phone;
        $data['email'] = $request->email;
        $data['website'] = $request->website;
        $data['comment'] = $request->comment;
        $data['logo'] = $img;

        Mail::send('emails.school',['data' => $data], function($message) use($email)
        {
            $message->to($email, 'Myjobo')->subject("Event, School, College or Resource Request");
        });

        return redirect()->back()->withErrors(["Your Request Successfull Send."]);
    }

}
