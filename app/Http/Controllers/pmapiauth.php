<?php
namespace App\Http\Controllers;

include('pmapiexceptions.php');


abstract class PMAPIAuth
{
	const DATE_RFC1123 = '%a, %d %b %Y %H:%M:%S'; // %h (alias of %b) does not work on win32.


	abstract public function getHeaders($verb, $version, $endpoint);


	protected function getDate()
	{
		// The HTTP "Date:" header must contain an English-language string.  If the current date/
		// time locale is not en_*, attempt to change it in order to format the "Date:" header
		// correctly.
		$locale_changed = false;
		$time_locale = setlocale(LC_TIME, '0');
		if(strpos('en_', $time_locale) !== 0)
		{
			// Try to select any English-language locale for date/times.
			if(!setlocale(LC_TIME, array('en_US', 'en_US.utf8', 'en_GB', 'en_GB.utf8', 'en_CA', 'en_CA.utf8', 'en_AU', 'en_AU.utf8', 'en_NZ', 'en_NZ.utf8', 'eng', 'english-uk', 'uk', 'american', 'american english', 'american-english', 'english-american', 'english-us', 'english-usa', 'us')))
				throw new PMAPIRuntimeEnvironmentException("Cannot set en_* locale for request");

			$locale_changed = true;
		}

		$sDate = gmstrftime(self::DATE_RFC1123 . ' GMT');

		// If the date/time locale was changed above, attempt to restore the original setting.
		if($locale_changed)
			setlocale(LC_TIME, $time_locale);

		return $sDate;
	}


	protected function getNonce()
	{
		return sha1(uniqid(mt_rand(), true));
	}
}

