<?php
namespace App\Http\Controllers;

include('pmapiauth.php');
include('pmapiexceptions.php');


class PMAPIAuthHash extends PMAPIAuth
{
	protected $uid;
	protected $cid;
	protected $hash;

	const SUT_API_HASH_LEN = 32;


	public function __construct($uid, $cid, $hash)
	{
		$this->uid = (int) $uid;
		if($this->uid < 1)
			throw new PMAPIInvalidValueException('user ID', $uid);

		$this->cid = (int) $cid;
		if($this->cid < 1)
			throw new PMAPIInvalidValueException('company ID', $cid);

		if((strlen($hash) != self::SUT_API_HASH_LEN)
			|| (strspn($hash, '0123456789abcdef') != self::SUT_API_HASH_LEN))
			throw new PMAPIInvalidValueException('API hash', $hash);

		$this->hash = $hash;
	}


	public function getHeaders($verb, $version, $endpoint)
	{
		$headers = array
		(
			'Date'			=> $this->getDate(),
			'X-SuT-CID'		=> $this->cid,
			'X-SuT-UID'		=> $this->uid,
			'X-SuT-Nonce'	=> $this->getNonce()
		);

		$headers_ = array(strtoupper($verb) . " /v$version/$endpoint");
		foreach($headers as $header => $value)
			$headers_[] = "$header: $value";
		$headers_[] = $this->hash;

		$sig_string = join("\r\n", $headers_);
		$signature = sha1($sig_string);

		$headers['Authorization'] = "SuTHash signature=\"$signature\"";

		return $headers;
	}
}

