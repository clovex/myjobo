<?php
namespace App\Http\Controllers;

require_once('pmapiauth.php');
require_once('pmapiexceptions.php');
require_once('pmapiresponse.php');

class PMAPIEndpoint
{
	const API_SCHEME = 'https';
	const USER_AGENT = 'PMAPI client v0.1';

	protected $Auth;
	protected $version;
	protected $endpoint;
	protected $server;
	protected $debugMode;


	public function __construct(PMAPIAuth $Auth, $server, $version, $endpoint, $debugMode = false)
	{
		if(!function_exists('curl_init'))
			throw new PMAPIRuntimeEnvironmentException('This class requires the cURL extension');

		if(!preg_match('/^\d+$/', $version))
			throw new PMAPIInvalidValueException('version', $version);

		if(!preg_match('/^[a-z_]+$/i', $endpoint))
			throw new PMAPIInvalidValueException('endpoint', $endpoint);

		$this->Auth = $Auth;
		$this->server = rtrim($server, '/');
		$this->version = (int) $version;
		$this->endpoint = $endpoint;
		$this->debugMode = (bool) $debugMode;
	}


	public function __call($verb, $args)
	{
		if(in_array($verb, array('delete', 'get', 'head', 'post', 'put')))
		{
			return $this->doRequest($verb, count($args) ? $args[0] : null);
		}
		else
			throw new PMAPIUnsupportedMethodException($verb);
	}


	protected function doRequest($verb, array $args = null)
	{
        if (is_array($args) && $args)
        {
            foreach ($args as $key => $val)
            {
                if (!strlen($key))
                {
                    throw new PMAPIInvalidArgumentException($key, $val);
                }
            }
        }

		$Response = new PMAPIResponse($verb);

		$url = self::API_SCHEME . "://{$this->server}/v{$this->version}/{$this->endpoint}";

		$headers = $this->Auth->getHeaders($verb, $this->version, $this->endpoint);
		$headers['Accept'] = $Response->getResponseFormat();

		$curl = curl_init();
		$curl_opts = array();

		switch($verb)
		{
			case 'post':
				$curl_opts[CURLOPT_POST] = true;
				if(!empty($args))
				{
					$curl_opts[CURLOPT_POSTFIELDS] = json_encode($args);
					$headers['Content-Type'] = 'application/json';
				}
				break;

			case 'put':
				$fp = fopen('php://temp', 'w');
				fwrite($fp, json_encode((array) $args));
				$len = ftell($fp);
				fseek($fp, 0);
				$curl_opts[CURLOPT_PUT] = 1;
				$curl_opts[CURLOPT_INFILE] = $fp;
				$curl_opts[CURLOPT_INFILESIZE] = $len;
				$headers['Content-Type'] = 'application/json';
				break;

			case 'delete':
			case 'head':
				$curl_opts[CURLOPT_CUSTOMREQUEST] = strtoupper($verb);
			case 'get':
				if(count($args))
					$url .= '?' . http_build_query((array) $args);
				break;
		}

		$headers_ = array();
		foreach($headers as $k => $v)
			$headers_[] = strtoupper(str_replace('X_SUT', 'HTTP_X_SUT', str_replace('_', '-', $k))) . ": $v";

		$curl_opts += array
		(
			CURLOPT_URL				=> $url,
			CURLOPT_HTTPHEADER		=> $headers_,
			CURLOPT_WRITEFUNCTION	=> $Response->responseCallback(),
			CURLOPT_HEADERFUNCTION	=> $Response->headerCallback(),
		);

		if($this->debugMode)
			$curl_opts[CURLOPT_SSL_VERIFYPEER] = false;

		curl_setopt_array($curl, $curl_opts);
		if(!curl_exec($curl))
			throw new PMAPICURLException(curl_error($curl), curl_errno($curl));

		$Response->finalise(curl_getinfo($curl, CURLINFO_HTTP_CODE));

		return $Response;
	}
}

