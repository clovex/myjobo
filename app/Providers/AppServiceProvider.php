<?php

namespace App\Providers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Models\Admin\Pages;
use Twitter;
use File;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        view()->composer('partials.nav', function ($view) {
            $q1 = array('page_name' => 'general', 'element' => 'logo' );
            $logoData = Pages::where($q1)->get(['contents']);
            $view->logoData = $logoData;
        });

        view()->composer('partials.footer', function ($view) {
            $q2 = array('page_name' => 'general', 'element' => 'sociallink' );
            $links = Pages::where($q2)->get(['contents']);
            $view->links = $links;
        });

        //view()->composer('partials.footer', function ($view) {
            //$twitter = Twitter::getUserTimeline(['count' => 3, 'format' => 'array']);
            //$view->twitter = $twitter;
        //});

        view()->composer('partials.footer', function ($view) {
            $q2 = array('page_name' => 'general', 'element' => 'about' );
            $abouts = Pages::where($q2)->get(['contents']);
            $view->abouts = $abouts;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
