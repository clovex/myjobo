<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use Carbon\Carbon;
class DemoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct(); 
    } 

    /**
     * Execute the console command.
     * 
     * @return mixed
     */
    public function handle()
    {
        $users = DB::table('users')
                ->join('role_user', 'role_user.user_id', '=', 'users.id')
                ->where('role_id','2')
                ->orderBy('users.id', 'desc')
                ->get();
        $date = Carbon::now()->format('Y-m-d');
            $jobs = DB::table('tbl_job_post')
                        ->where('tbl_job_post.deadline','>=',$date)
                        ->inRandomOrder()
                        ->limit(5)
                        ->get();
        $dates = Carbon::now()->format('d M Y');                        
        foreach ($users as $user) 
        {
            $email  = $user->email;
            Mail::send('emails.welcome',['jobs' => $jobs], function($message) use($email,$dates)
            {
                $message->to($email, 'Myjobo')->subject('Myjobo.com Daily Jobs Digest Jobseekers - '.$dates);
            });
        }                     
            

        $this->info('Demo:Cron Cummand Run successfully!');
    }
}
