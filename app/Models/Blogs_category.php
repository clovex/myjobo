<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blogs_category extends Model
{
	protected $fillable = ['name', 'slug'];
	public $table = "blog_category";

	public function blog()
    {
        return $this->hasOne('App\Models\Blogs','id','blog_category');
    }
}
