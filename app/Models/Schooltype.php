<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schooltype extends Model
{
	protected $guarded = [];
	public $table = "tbl_school_types";

	public function school()
    {
        return $this->hasOne('App\Models\School','id','type');
    }
}
