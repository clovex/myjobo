<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entrepreneurship extends Model
{
	protected $fillable = ['title', 'description','slug'];
	public $table = "entrepreneurship";
}
