<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blogs_comment extends Model
{ 
	protected $guarded = [];
	public $table = "tbl_article_comments";
}
