<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
	protected $guarded = [];
	public $table = "tbl_job_post";

	public function applicants()
    {
        return $this->hasMany('App\Models\Applicants','id','id');
    }

    public function employment_type()
    {
        return $this->belongsTo('App\Models\Employment_type','emp_type','id');
    }

    public function countrys()
    {
        return $this->belongsTo('App\Models\District','district','id');
    }

    public function job()
    {
        return $this->hasOne('App\Models\Apply','id','job_id');
    }
}
