<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resumedata extends Model
{
	protected $guarded = [];
	public $table = "tbl_resume";
}
