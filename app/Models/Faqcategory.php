<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faqcategory extends Model
{
	protected $guarded = [];
	public $table = "faqcategory";
}
