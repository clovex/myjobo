<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School_comments extends Model
{
	protected $guarded = [];
	public $table = "tbl_school_comments";
}
