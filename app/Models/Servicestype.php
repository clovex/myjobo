<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servicestype extends Model
{
	protected $guarded = [];
	public $table = "services_type";

}
