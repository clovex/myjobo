<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Applicants extends Model
{
	public $table = "tbl_apply_job";

	public function jobs()
    {
        return $this->belongsTo('App\Models\Jobs','job_id','id');
    }
    public function users()
    {
        return $this->belongsTo('App\Models\Admin\Users','user_id','id');
    }
}
