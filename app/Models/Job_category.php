<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job_category extends Model
{
	protected $fillable = ['name'];
	public $table = "tbl_categories";

}
