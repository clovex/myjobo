<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Blogs extends Model
{
	protected $fillable = ['blog_category', 'title','author','description','slug'];
	public $table = "tbl_blogs";

	public function category()
    {
        return $this->belongsTo('App\Models\Blogs_category','blog_category','id');
    }

}
