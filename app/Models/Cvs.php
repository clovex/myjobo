<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cvs extends Model
{
	protected $guarded = [];
	public $table = "resume";

	public function data()
    {
        return $this->hasOne('App\Models\Admin\Users','user_id','id');
    }
}
