<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
	protected $guarded = [];
	public $table = "tbl_schools";

	public function types()
    {
        return $this->belongsTo('App\Models\Schooltype','type','id');
    }


}
