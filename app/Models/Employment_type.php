<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employment_type extends Model
{
	protected $fillable = ['name'];
	public $table = "employment_type";

	public function jobs()
    {
        return $this->hasOne('App\Models\Jobs','id','emp_type');
    }
}
