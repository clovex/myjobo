<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Apply extends Model
{
	protected $guarded = [];
	public $table = "tbl_apply_job";

	public function job()
    {
        return $this->belongsTo('App\Models\Jobs','job_id','id');
    }

    public function data()
    {
        return $this->hasOne('App\Models\Admin\Users','user_id','id');
    }
}
