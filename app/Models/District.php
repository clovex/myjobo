<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
	protected $fillable = ['name'];
	public $table = "tbl_district";

	public function jobs()
    {
        return $this->hasOne('App\Models\Jobs','id','district');
    }

}
