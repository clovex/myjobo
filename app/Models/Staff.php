<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
	protected $guarded = [];
	public $table = "post_services";

	public function usersdata()
    {
        return $this->belongsTo('App\Models\Admin\Users','provider_id','id');
    }

}
