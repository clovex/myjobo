<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userdata extends Model
{
	protected $guarded = [];
	public $table = "tbl_user";
}
