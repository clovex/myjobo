<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paymentmode extends Model
{
	protected $guarded = [];
	public $table = "paymentmode";
}
