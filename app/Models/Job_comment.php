<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job_comment extends Model
{
	protected $guarded = [];
	public $table = "tbl_comments";
}
