<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
		protected $guarded = [];
		public $table = 'users';

	public function resume()
    {
        return $this->belongsTo('App\Models\Cvs','id','user_id');
    }
    public function letter()
    {
    	return $this->belongsTo('App\Models\Apply','id','user_id');	
    }	

    public function roles()
    {
        return $this->hasOne('App\Models\Admin\Role','id','user_id');
    }

    public function staffdata()
    {
        return $this->hasOne('App\Models\Staff','id','provider_id');
    }

    public function applicants()
    {
        return $this->hasOne('App\Models\Applicants','id','user_id');
    }
}