<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
		protected $guarded = [];
		public $table = 'role_user';

	public function users()
    {
        return $this->belongsTo('App\Models\Admin\Users','user_id','id');
    }	
}