<?php 
if (!isset($_SESSION))session_start();
class Website extends Master_website{
	
	/* USER LOGIN FUNCTION*/
	function login_user($table_name,$data){
		
		 $login = $this->login($table_name,$data);
		 if($login!='false'){
		 $this->session_write('detail', $login);
		 return 'true';
		 }else{ $_session['error']="Incorrect Username Password"; return 'false';}
		 
		}
		
	/* Hit counter*/
	 function hit(){
  			
		  $query = "select * from tbl_hit where id = 2";
 		  $data = mysql_fetch_assoc(mysql_query($query));
 		  $total = $data['count'];
		  $new = $total+1;
 		  mysql_query("update tbl_hit set count = '".$new."' where id = 2");
  
 }
	
	
	
	/* LOGOUT FUNCTION*/	
	function logout(){	
	
  
    session_start();
    unset($_SESSION['id']);
    unset($_SESSION['username']);
	unset($_SESSION['oauth_uid']);
    unset($_SESSION['oauth_provider']);
    	
	unset($_SESSION['detail']);
	$_SESSION['detail']="";	
	
	session_destroy();
	
    return true;	
	
	
	}
	/* FUNCTION FOR EMPLOYEE PROFILE DETAIL*/
	function view_profile($id)
	{
	$user_data = $this->view($id);
	return $user_data;
	}	
	/* COUNTRY LIST FUNCTION*/	
	function country_list()
	{
	 $country_data = $this->country();
	 return $country_data;	
	}
	/* DISTRICT LIST FUNCTION*/	
	function district_list()
	{
	 $country_data = $this->district();
	 return $country_data;	
	}
	/* FUNCTION FOR VIEW RESUME */
	function view_resume($id){
	$resume_data = $this->resume($id);
	return $resume_data;
	}
	/* EMPLOYMENT TYPE LIST FUNCTION*/	
	function job_type()
	{
	 $country_data = $this->job();
	 return $country_data;	
	}
	
	function blog_list()
	{
	 $country_data = $this->blog();
	 return $country_data;	
	}
	
	/*SEARCH RESUME*/
	function search_resume($data){
		
		$result = $this->search_cv($data);
		return $result;
	}
	/*FORGOT PASSWORD */
	function forgot_password($table_name,$data){
		
		$result = $this->forgot_password_user($table_name,$data);
		return $result;
	}
	/*FORGOT PASSWORD */
	function change_password($table_name,$data){
		
		$result = $this->change_password_user($table_name,$data);
		return $result;
	}
	/*SEARCH JOBS*/
	function search_jobs($data){
		
		$result = $this->search_job($data);
		return $result;
	}
	/*JOBS SEARCHES*/
	function job_search($data){
		
		$result = $this->job_searches($data);
		return $result;
	}
	
	/* USER REGISTRATION FUNCTION*/
	function seeker_registration($table_name,$data,$image)
	{
	  $reg_data = $this->registration($table_name,$data,$image);
	   if($reg_data=='true')
	      { 
		  $_SESSION['success']="Registered successfully.";
		  return $reg_data;
		  }else{ 
		   $_SESSION['error']="Registration Failed. Try again.";
		   return $reg_data;
		  }	
	}
	
	function delete_account($table_name,$data)
	{
	  $result = $this->deleteaccount($table_name,$data);
	   return $result;
	}
	/* USER UPDATE FROFILE FUNCTION */
	function update_profile($table_name,$data,$image)
	{
		$reg_data = $this->updateprofile($table_name,$data,$image);
	     if($reg_data=='true')
	      { 
		  $_SESSION['success']="Profile updated Successfully.";
		  return $reg_data;
		  }else{ 
		   $_SESSION['error']="Profile update  Failed. Try again.";
		   return $reg_data;
		  }	
	}
	
	/*UPDATE PROFILE OF SEEKAR FUNCTION */
	function update_profile_seekar($table_name,$data){
		$update_date = $this->updateprofileseekar($table_name,$data);
		if($update_date=='true')
	      { 
		  $_SESSION['success']="Profile updated Successfully.";
		  return $update_date;
		  }else{ 
		   $_SESSION['error']="Profile update  Failed. Try again.";
		   return $update_date;
		  }	
	}
	/* JOB POST FUNCTION */
	function job_post($table_name,$data)
	{
		$reg_data = $this->add_job_post($table_name,$data);
	     if($reg_data=='true')
	      { 
		  $_SESSION['success']="Job Posted Successfully.";
		  return $reg_data;
		  }else{ 
		   $_SESSION['error']="Job Posting Failed. Try again.";
		   return $reg_data;
		  }	
	}
	/*RESUME POST*/
	function resume_post($table_name,$data,$file)
	{
		$response = $this->add_resume($table_name,$data,$file);
		if($response=='true')
	      { 
		  $_SESSION['success']="CV Posted Successfully.";
		  return $response;
		  }else{ 
		   $_SESSION['error']="CV Posting Failed. Try again.";
		   return $response;
		  }	
		
	}
	
	/*RESUME POST*/
	function suggest_post($table_name,$data)
	{
		$response = $this->add_suggest($table_name,$data);
		if($response=='true')
	      { 
		  $_SESSION['success']="Posted Successfully.";
		  return $response;
		  }else{ 
		   $_SESSION['error']="Posting Failed. Try again.";
		   return $response;
		  }	
		
	}
	
	/*RESUME UPDATE*/
	function resume_update($table_name,$data,$file)
	{
		$response = $this->update_resume($table_name,$data,$file);
		if($response=='true')
	      { 
		  $_SESSION['success']="CV Updated Successfully.";
		  return $response;
		  }else{ 
		   $_SESSION['error']="CV Update Failed. Try again.";
		   return $response;
		  }	
		
	}
	
	/* VIEW ALL JOB POST */
	function view_job_post()
	{
	$data = $this->view_job();	
	return $data;	
	}
	/* VIEW JOB POST */
	function view_job_post_data($id)
	{
	$data = $this->view_job_post_abc($id);	
	return $data;	
	}
	
	/* VIEW JOB DETAIL */
	function view_job_data($id)
	{
	$data = $this->view_job_data_abc($id);	
	return $data;	
	}
	
	
	/* MONTH NAME */
	function month_name($month){
	$data = $this->month($month);
	return $data;	
	}
	/* LATEST RESUME */	
	function view_latest_resume(){
	$data = $this->view_latestresume();
	return $data;	
	}
	/* DELETE RESUME */
	function delete_resume($table_name,$id,$file){
	$response = $this->deleteresume($table_name,$id,$file);
	return $response;	
	}
	
	/* UPDATE JOB POST*/
	function update_post($table_name,$post)	
	{
	$update = $this->update_post_data($table_name,$post);
	return $update;
	}
	
	/*Page Content*/
	function page_data_content($id)
	{
	$p_data= $this->page_info($id);
	return $p_data;	
	}
	
	/*Blog Content*/
	function blog_data_content($id)
	{
	$p_data= $this->blog_info($id);
	return $p_data;	
	}
	
	/*Blog Comments*/
	function fetch_blog_comment($id)
	{
	$p_data= $this->blog_comment($id);
	return $p_data;	
	}
	
	/*Post Blog Comment*/
	function comment($user_id, $data, $blog_id)
	{
	$p_data= $this->do_blog_comment($user_id, $data, $blog_id);
	 header.("location:../blog_page.php");
		  
	}
	
	
	/*NEW JOBS*/
	function new_jobs()
	{
	$jobs = $this->recent_jobs();
	return $jobs;
	}
	
	/*NEW JOBS*/
	function applied_jobs()
	{
	$jobs = $this->my_jobs();
	return $jobs;
	}
	
	/*APPLY TO JOBS*/
	function apply($jobid,$cover)
	{	
	$record = $this->job_apply($jobid,$cover);
	return $record;	
	}
	/*CHECK APPLY TO JOBS*/
	function checkapply($jobid)
	{	
	$record = $this->check_job_apply($jobid);
	return $record;	
	}
	/*CHECK CV*/
	function check_hascv()
	{	
	$record = $this->check_has_cv();
	return $record;	
	}
	/*CHECK Pass CODE*/
	function checkcode($code)
	{	
	$record = $this->check_code($code);
	return $record;	
	}
	/*SHOW JOBS ON FRONT*/
	function jobs_for_front($limit){
	$record = $this->front_job($limit);
	return $record;		
	}
	/*SHOW ALL JOBS */
	function alljobs($limitFrom,$no_of_record_per_page){
	$record = $this->jobs($limitFrom,$no_of_record_per_page);
	return $record;		
	}
	
	function internshipjobs($limitFrom,$no_of_record_per_page){
	$record = $this->internship_jobs($limitFrom,$no_of_record_per_page);
	return $record;		
	}
	/*FILTER JOBS */
	function filter_job($job_cat){
	$record = $this->filterjobpost($job_cat);
	return $record;		
	}
	/*SHOW NEWS ON FRONT*/
	function get_news($limit,$pg){
	$record = $this->news($limit,$pg);
	return $record;		
	}
	
	/*SHOW NEWS ON NEWS LIST*/
	function news_in_detail($data){
	$record = $this->news_detail($data);
	return $record;		
	}
	
	/*SHOW COMPANIES ON FRONT*/
	function company_for_front(){	
	$record = $this->companies();
	return $record;		
	}
	
	function allcompanies(){	
	$record = $this->all_companies();
	return $record;		
	}
	
	/*SHOW COMPANIES ON COMPANY LIST*/
	function company_name($data){	
	$record = $this->company_list($data);
	return $record;		
	}
	
	/*APPLIED JOB THROUGH USER*/
	function applied_job_user(){
	$record = $this->applied();
	return $record;		
	}
	/*VIEW ALL OPPORTUNITY*/
	function opportunity_list(){
	$record = $this->opportunity();
	return $record;		
	}
	/*VIEW ALL OPPORTUNITY*/
	function career_list(){
	$record = $this->career();
	return $record;		
	}
	/*VIEW ALL BUSINESS RESOURCE*/
	function b_resource_list(){
	$b_record = $this->resource();
	return $b_record;		
	}
	/*VIEW ALL Career Center*/
	function center_list(){
	$b_record = $this->career_center();
	return $b_record;		
	}
	/*VIEW ALL BUSINESS*/
	function business_list(){
	$record = $this->business();
	return $record;		
	}
		/*VIEW ALL BUSINESS*/
	function career_opportunity_list(){
	$record = $this->career_list_opportunity();
	return $record;		
	}
	
	/* view_hot_career */
	function view_hot_career(){
	$table_name = 'tbl_hot_career';	
	$record = $this->hot_career($table_name);
	return $record;
		
	}
	
	/* view_hot_career/buss by id */
	function view_hot_career_byid($table_name,$id){
	$record = $this->hot_career_byid($table_name,$id);
	return $record;
		
	}

	/* view_hot_business */
	function view_hot_business(){
	$table_name = 'tbl_hot_business';	
	$record = $this->hot_career($table_name);
	return $record;
		
	}
	
	/* view_hot_business/career by id */
	function view_hot_business_byid($id){
	$table_name = 'tbl_hot_business';	
	$record = $this->hot_career_byid($table_name,$id);
	return $record;
		
	}
	
	/* view seminar */
	function view_seminar(){
	
	$table_name = 'tbl_seminar';	
	$record = $this->seminar($table_name);
	return $record;
		
	}
	
	/* view events */
	function view_events(){
	
	$table_name = 'tbl_events';	
	$record = $this->seminar($table_name);
	return $record;
		
	}
	
	function view_about_career(){
		
		$table_name = 'tbl_about_career';
		$record = $this->seminar($table_name);
		return $record;
		}
		
	function view_about_business(){
		
		$table_name = 'tbl_about_business';
		$record = $this->seminar($table_name);
		return $record;
		}
	
	/*VIEW Page data*/
	function page_data($id){
	$record = $this->page($id);
	return $record;		
	}
	/*VIEW Business Page data*/
	function business_page_data($id){
	$record = $this->business_page($id);
	return $record;		
	}
	
	/*VIEW Business Page data*/
	function career_page_data($id){
	$record = $this->career_page($id);
	return $record;		
	}
	
	/*VIEW Entrepreneurship Page data*/
	function entrepreneurship_page_data($id){
	$record = $this->entrepreneurship_page($id);
	return $record;		
	}
	
	/*VIEW Entrepreneurship Page data*/
	function center_page_data($id){
	$record = $this->center($id);
	return $record;		
	}
	
	/*VIEW Business Page data*/
	function business_page_data2($id){
	$record = $this->business_page2($id);
	return $record;		
	}
	
	/*View Slider Images*/
	function view_slider_images(){
	$view = $this->view_slider();
	return $view;
		}
		
	/*View Ad Left Post*/
	function view_ad_post_left(){
    $view = $this->ad_post_left();
	return $view;		
		}
		
	/*View Ad Right Post*/
	function view_ad_post_right(){
    $view = $this->ad_post_right();
	return $view;		
		}
		
	/* View Contact Detail*/
	function view_contact_us($id){
		
		$query = mysql_fetch_assoc(mysql_query("select * from  tbl_page where id='".$id."' and status=1"));
		return $query;
		}	
}
?>