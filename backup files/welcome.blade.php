@extends('layouts.app')

@section('content')
<style type="text/css">
.setimg {
    border: 2px solid #ccc;
    height: 50px;
    margin: 10px 0;
    width: 100%;
}
.footer-links.datasetul a
    {
        color: #333 !important;
    }
    .footer-links.datasetul a:hover
    {
        color: #333 !important;
    }
    .post-container
    {
        margin-bottom: 0px !important;
    }
    .post-content
    {
        padding: 15px 0px !important;
    }
    .sociadata li a {
        background-color: #26ae61;
    }
    .sociadata li .twitter::before, .sociadata li .facebook::before
    {
        color: #fff !important;
    }
    .sociadata li a
    {
        text-decoration: none !important; 
    }
    .menu1 ul > li > a
    {
        background-color: rgba(255, 255, 255, 0.1);
        border: 1px solid rgba(255, 255, 255, 0.3);
        color: #fff !important;
        margin: 5px 5px !important;
        padding: 5px 10px !important;
    }
    .setacol
    {
        background-color: none !important;
        line-height: 14px !important;
    }
    .menu1 > ul ul li:hover > a
    {
        background-color:#26ae61 !important;
    }
    .setstyle:hover
    {
        background:rgba(38, 174, 97, 0.05);
        border-bottom: 1px solid #26ae61;
        border-top: 1px solid #26ae61;
    }
    .setstyle
    {
        padding-top: 15px;
        padding-bottom: 15px;
        border-bottom: 1px solid #e0e0e0;
        border-top: 1px solid #e0e0e0;
    }
    .setjobfb, .setjobtw
    {
        width: 30px !important;
        height: 30px !important;
    }
    .setjobtw i, .setjobtw::before
    {
        margin: 8px 0px 0px 8px !important;
    }
    .setjobfb i, .setjobfb::before
    {
        margin: 8px 0px 0px 10px !important;
    }
    .setsliderbtn
    {
        background-color: #26ae61; 
        box-shadow: 5px 5px 5px #888888; 
        font-size: 17px !important; 
        margin: 10px 0px !important;
        padding: 10px 5px !important;
    }
    .setslider2btn
    {
        background-color: #f45b06 !important; 
        box-shadow: 5px 5px 5px #888888; 
        font-size: 17px !important; 
        margin: 10px 0px !important;
        padding: 10px 5px !important;
    }
    .setslider2btn2
    {
        background-color: #269F5B !important; 
        box-shadow: 5px 5px 5px #888888; 
        font-size: 13px !important; 
        margin: 10px 0px !important;
        padding: 10px 5px !important;
    }
    .button.widget-btn.btn-block.setsliderbtn
    {
        font-size: 16px!important;
        float: left;
        background-color: #F45B06;
        padding: 3px!important;
    }
    .button.widget-btn.btn-block.setslider2btn
    {
        font-size: 13px!important;
    }
    .menu ul li.sfHover ul li a.sf-with-ul, .menu ul ul li a
    {
        font-size: 13px!important;
    }
    .widget-box
    {
    	padding: 15px 35px!important;
    }

</style>
<div class="clearfix"></div>


<!-- Banner
================================================== -->
<div id="banner" style="background-image: url({{ URL::to('frontend/images/25900945412_5d41d0b3fb_k.jpg') }}); z-index:9;" class="parallax background" data-img-width="2000" data-img-height="1330" data-diff="400">
    <div class="container">
        <div class="col-md-8">
            <div class="search-container">
                <h2 style="text-align: center;font-size: 2.8em;font-weight: 600">Start your job search now...</h2>
                <form>
                    <div class="form-group">
                    <input type="text" class="ico-01" style="padding:0px;height:45px;width: 80%; margin: 0px;text-align: center;" placeholder="job title, keywords or company name" value=""/>
                    <!--<button style="position: absolute; right: 0px; background-color: transparent; color: #494949; width: 10%; right: 5px; height: 64px;"><i class="fa fa-search"></i></button>-->
                    <button style="position: absolute; right: 0px; background-color: #F45B06; color: #fff; width: auto; right: 5px; height: 45px;">Search</button>
                    </div>
                </form>
                <div class="col-md-12" style="padding: 0px;">
                    <hr style="margin:15px 0px 10px 0px !important;">
                </div>
                <!-- Browse Jobs -->
                <div class="browse-jobs" style="margin-top: 0px;">
                <div class="menu menu1">
                    <ul style="margin: 0px !important;">
                        <li style="padding: 10px 0px; margin: 0px;">Browse job vacancies by</li>
                        <li style="cursor: pointer;"><a>Category</a>
                            <ul style="width: auto;">
                            @foreach($category as $cat)
                                <li><a class="setacol" href="index.html">{{ $cat->name }}</a></li>
                            @endforeach  
                                <li><a class="setacol" href="">More</a></li>      
                            </ul>
                        </li>
                        <li style="padding: 10px 5px;">or</li>
                        <li style="cursor: pointer;"><a>Location</a>
                            <ul style="width: auto;">
                            @foreach($dist as $dis)
                                <li><a class="setacol" href="index.html">{{ $dis->name }}</a></li>
                            @endforeach  
                                <li><a class="setacol" href="{{ URL::to('jobs') }}">More</a></li>  
                            </ul>
                        </li>
                    </ul>
                </div>
                <a href="" style="visibility: hidden;"></a>     
                </div>
                
                <!-- Announce >
                <div class="announce">
                    
                </div-->

            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
            <div class="search-container">
                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-8 text-center" style="background-color: rgba(255,255,255,0.7); padding: 5% 5%;">
                    <h3 style="color: #494949; font-size: 20px;font-weight: 600">How can we help you?</h3>
                    <hr style="margin: 0px 0 15px;">
                    <a href="{{ URL::to('register_job_seeker') }}" class="button widget-btn btn-block setsliderbtn">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i> I am looking for a job</a>
                    <a href="{{ URL::to('register_employer') }}" class="button widget-btn btn-block setsliderbtn" style="background-color: #269F5B;"><i class="fa fa-users" aria-hidden="true"></i> I want to recruit staff</a>
                    <a href="{{ URL::to('register_employer') }}" class="button widget-btn btn-block setsliderbtn" ><i class="fa fa-graduation-cap" aria-hidden="true"></i> I want to advertise jobs</a>
                    
                </div>

                </div> 
                <div class="col-md-2">&nbsp;</div>
            </div>
            </div>
        </div>
    </div>
</div>



<div class="container">
    <div class="col-md-12 text-center">
        <!--<h2>We’ve over <strong>{{ $countjobs }}</strong> job offers for you!</h2>-->
        <h2 style="font-size: 2.33333rem;">Find your dream job with Malawi's #1 job site</h2>
        <h4 class="margin-bottom-25">Browse our latest vacancies below</h4>
    </div>
    <div class="col-md-8">
        <div class="col-md-12">
            <div class="row">
                @foreach($jobs as $j)
                <div class="col-md-12 setstyle">
                    <div class="col-md-1" style="padding: 0px !important;">
                       @if($j->logo != null)
                        <img src="{{ URL::to('public/uploads') }}/{{ $j->logo }}" alt="" style="width: 100%;">
                        @else    
                        <img src="{{ URL::to('public/uploads/nologo.png') }}" alt="" style="width: 100%;">
                        @endif
                    </div>
                    <div class="col-md-11">
                        <h4 style="font-size: 16px; line-height: 32px;">{{ $j->title }} 
                        <ul class="social-icons sociadata" style="float: right;">
                        <li style="transform:none; margin-top: 0px !important;">
                            <a class="facebook setjobfb" href="https://www.facebook.com/sharer/sharer.php?u={{ URL::to('job') }}/{{ $j->slug }}" target="_blank">
                                <i class="icon-facebook"></i>
                            </a>
                        </li>
                        <li style="transform:none; margin-top: 0px !important;">
                            <a class="twitter setjobtw" href="http://twitter.com/share?text={{ URL::to('job') }}/{{ $j->slug }}" target="_blank">
                                <i class="icon-twitter"></i>
                            </a>
                        </li>
                        </ul>
                        </h4>
                        <label>
                        <span><i class="fa fa-calendar"> Posted On: </i> {{ Carbon\Carbon::parse($j->created_at)->format('jS F, Y') }}</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <span><i class="fa fa-map-marker"> Location: </i> {{ $j->city }}</span>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <a href="{{ URL::to('job') }}/{{ $j->slug }}">View More</a>
                        </label>
                    </div> 
                </div>
                @endforeach
            </div>
            <div class="text-right" style="margin-bottom: 25px;font-weight: 700;">        
                <a href="{{ URL::to('jobs') }}"><i class="fa fa-plus-circle"></i> Show me more job vacancies</a>
            </div>
        </div>
    </div>

    <!--<div class="col-md-4">
        <div class="widget text-center">
            <div class="widget-box" style="text-align: center;">
                <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
            </div>
        </div>

        <div class="widget">
            <div class="widget-box" style="text-align: center;">
                <a href="https://talentguardians.org/" target="_blank" class="button widget-btn" style="background-color: #26ae61; box-shadow: 10px 10px 5px #888888;">Internship & Volunteering Opportunities</a>
            </div>
        </div>

        <div class="widget">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:block;"
                 data-ad-client="ca-pub-7726299907154841"
                 data-ad-slot="2292045413"
                 data-ad-format="auto"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>-->

    <div class="col-md-4">
        <div class="widget text-center">
            <div class="widget-box" style="text-align: center;">
                <a href="{{ URL::to('donate') }}" class="button widget-btn" style="background-color: #F45B06; box-shadow: 10px 10px 5px #888888;"><i class="fa fa-life-ring" aria-hidden="true"></i> Support Us</a>
            </div>
            <div class="widget-box" style="text-align: center;">
                <a href="https://talentguardians.org/" target="_blank" class="button widget-btn" style="background-color: #26ae61; box-shadow: 10px 10px 5px #888888;">Internship & Volunteering Opportunities</a>
            </div>
        </div>

       

        <div class="widget">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:block;"
                 data-ad-client="ca-pub-7726299907154841"
                 data-ad-slot="2292045413"
                 data-ad-format="auto"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>

</div>
<div class="container">
    <hr>    
</div>
<div class="container">
    <div class="col-md-12 text-center">
        <h3 class="" style="font-size: 2.33333rem;">Recruitment made simple</h3>
        <h6 class="margin-bottom-25">Recruit screened, vetted staff in the fastest and cheapest way ever</h6>
        <!--<ul id="popular-categories">
            <li><a href=""><i class="ln ln-icon-Truck"></i> Drivers</a></li>
            <li><a href=""><i class="ln ln-icon-Phone-3"></i> Call Center Agents</a></li>
            <li><a href=""><i class="ln ln-icon-Worker"></i> Supervisors</a></li>
            <li><a href=""><i class="ln ln-icon-Student-Female"></i> Receptionists</a></li>
            <li><a href=""><i class="ln ln-icon-Male"></i> Security guard</a></li>
            <li><a href=""><i class="ln ln-icon-Plates"></i> Waiters</a></li>
            <li><a href=""><i class="ln ln-icon-Cash-register2"></i> Cashier</a></li>
            <li><a href=""><i class="ln  ln-icon-Laptop-3"></i> IT</a></li>
        </ul>-->
        <ul id="popular-categories">
            <li><a href=""><i class="ln ln-icon-Cash-register2"></i> Cashier</a></li>
            <li><a href=""><i class="ln ln-icon-Truck"></i> Drivers</a></li>
            <li><a href=""><i class="ln ln-icon-Student-Female"></i> Receptionists</a></li>
            <li><a href=""><i class="ln ln-icon-Female-2"></i> Secretaries</a></li>
            <li><a href=""><i class="ln ln-icon-Plates"></i> Waiters</a></li>
            <li><a href=""><i class="ln ln-icon-Male"></i> Security Guards</a></li>
            <li><a href=""><i class="ln ln-icon-Beer"></i> Bartenders</a></li>
            <li><a href=""><i class="ln  ln-icon-Laptop-3"></i> Data Clerks</a></li>
        </ul>
        <div class="clearfix"></div>
        <div class="margin-top-30"></div>

        <a href="browse-categories.html" class="centered">Browse All Categories</a>
        <div class="margin-bottom-50"></div>
    </div>
</div>

<div id="banner" style="background-image: url({{ URL::to('frontend/images/Matthijs-de-Bruijne-via-httpgdr.cascoprojects.org_.jpg') }}); margin-bottom: 0px;" class="parallax background" data-img-width="2000" data-img-height="1330" data-diff="400">

    <div class="container" style="padding:150px 0px;">
    <h2 style="position: absolute;width:100%;top:27px;text-align: center;font-size: 2.8em;font-weight: 600;color: #fff;">Amisili aku Malawi</h2>
        <div class="col-md-7">    
            <h2 style="color: #fff; font-size: 46px; letter-spacing: -1px; margin-bottom: 30px;">Helping you find the right vetted skilled trades.
            </h2>
            
            <p style="font-size:18px !important; color:#fff;">Search through our reviewed and monitored trades and artisans for free.</p>
                    
            <input class="ico-01" style="" placeholder="Trade or Artisan (eg plumber, electrician, carpenter etc)" value="" type="text">
            
            <div class="caption sfb" data-x="center" data-y="400" data-speed="400" data-start="1600" data-easing="easeOutExpo" style="margin-top: 30px;">
                <a href="" class="slider-button">Get Started</a>
            </div>
        </div>

        <div class="col-md-5">
            <div class="row">
                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-8 text-center" style="background-color: rgba(255,255,255,0.7); padding: 5% 5%;">
                    <h3 style="color: #494949; font-size: 20px;">How can we help you?</h3>
                    <hr style="margin: 0px 0 15px;">
                    <a href="{{ URL::to('register_services') }}" class="button widget-btn btn-block setslider2btn2">
                        <i class="fa fa-wrench" aria-hidden="true"></i>  I am looking for skilled trades</a>
                    <a href="{{ URL::to('register_services_provider') }}" class="button widget-btn btn-block setslider2btn" ><i class="fa fa-briefcase" aria-hidden="true"></i> I want to register as a trade or artisan</a>
                </div>

                </div> 
                <div class="col-md-2">&nbsp;</div>
            </div>
        </div>
    </div>
</div>        


<div class="section-background top-0" style="border:none !important; margin: 0px !important; padding: 25px 0px;">
    <div class="container">
         <div class="col-md-12 text-center">
            <h2 style="font-size: 2.33333rem;">Our Services</h2>
        </div>
        <div class="col-md-3 text-center">
            <a href="">
            <div class="icon-box rounded alt" style="margin: 10px auto;">
                <i class="ln ln-icon-Note "></i>
                <h4>Job Advertising</h4>
            </div>
            </a>
        </div>

        <div class="col-md-3 text-center">
            <a href="">
            <div class="icon-box rounded alt" style="margin: 10px auto;">
                <i class="ln ln-icon-Business-Man"></i>
                <h4>Vetted Staff</h4>
            </div>
            </a>
        </div>

        <div class="col-md-3 text-center" style="margin: 10px auto;">
            <a href="">
            <div class="icon-box rounded alt">
                <i class="ln ln-icon-Search-onCloud"></i>
                <h4>CV Database</h4>
            </div>
            </a>
        </div>

        <div class="col-md-3 text-center" style="margin: 10px auto;">
            <a href="">
            <div class="icon-box rounded alt">
                <i class="ln ln-icon-Business-ManWoman"></i>
                <h4>Psychodiagnostics Tests</h4>
            </div>
            </a>
        </div>

    </div>
</div>






<!-- Testimonials -->
<div id="testimonials">
    <!-- Slider -->
    <div class="container">
        <div class="sixteen columns">
            <div class="testimonials-slider">
                  <ul class="slides">
                    @foreach($testimonials as $test)
                    <li>
                      <p>{!! substr(strip_tags($test->description),0,300) !!}...
                      <span>{{ $test->name }}</span>
                      <a href="{{ URL::to('testimonials') }}"><strong style="color: #26ae61;">View More</strong></a>
                      </p>
                    </li>
                    @endforeach
                  </ul>
            </div>
        </div>
    </div>
</div>

<!--<div id="counters">
    <div class="container">

        <div class="four columns">
            <div class="counter-box">
                <span class="counter">{{ $countjobs }}</span>
                <p>Job Offers</p>
            </div>
        </div>

        <div class="four columns">
            <div class="counter-box">
                <span class="counter">{{ $countuser }}</span>
                <p>Members</p>
            </div>
        </div>

        <div class="four columns">
            <div class="counter-box">
                <span class="counter">{{ $countresume }}</span>
                <p>Resumes Posted</p>
            </div>
        </div>

        <div class="four columns">
            <div class="counter-box">
                <span class="counter">{{ $countemp }}</span>
                <p>Employers</p>
            </div>
        </div>

    </div>
</div>-->


<h3 class="centered-headline" style="margin-top: 0px;">Partners Who Have Trusted Us <span>The list of Partners who have put their trust in us includes:</span></h3>
<div class="clearfix"></div>

<div class="container">

    <div class="sixteen columns">

        <!-- Navigation / Left -->
        <div class="one carousel column"><div id="showbiz_left_2" class="sb-navigation-left-2" style="margin-top: 75px;"><i class="fa fa-angle-left"></i></div></div>

        <!-- ShowBiz Carousel -->
        <div id="our-clients" class="showbiz-container fourteen carousel columns" >

        <!-- Portfolio Entries -->
        <div class="showbiz our-clients" data-left="#showbiz_left_2" data-right="#showbiz_right_2">
            <div class="overflowholder">

                <ul>
                    @foreach($logos as $l)
                    @php $id = base64_encode($l->id); @endphp
                    <li>
                        <a href="{{ URL::to('partner/single') }}/{{ $id }}">
                            <img src="{{ URL::to('public/uploads') }}/{{ $l->image }}" alt="" style="width: 150px; height: 150px;" />
                        </a>
                    </li>
                    @endforeach
                </ul>
                <div class="clearfix"></div>

            </div>
            <div class="clearfix"></div>

        </div>
        </div>

        <!-- Navigation / Right -->
        <div class="one carousel column"><div id="showbiz_right_2" class="sb-navigation-right-2" style="margin-top: 75px;"><i class="fa fa-angle-right"></i></div></div>

    </div>

</div>
@endsection            
