@extends('layouts.app')

@section('template_title')
    Partners 
@endsection

@section('content')
<script>
$(document).ready(function(){
    $('.setmenu').each(function(){
          $(this).removeAttr('id');
      });
      $('.partners').each(function(){
        $(this).attr('id', 'current');
    });
    });
</script>
<div class="clearfix"></div>
<div id="titlebar" class="single">
    <div class="container">
        <div class="sixteen columns">
            <h2>Our Partners</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="">Home</a></li>
                    <li>Partners</li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-8">
        <div class="row">
            @php $i = 1; @endphp
            @foreach($partners as $p)
                <div class="col-md-4">
                    <a href="{{ URL::to('partner') }}/{{ $p->slug }}">{{ $p->name }}</a>    
                </div>
                @if($i%3 == 0)
                    </div><div class="row">
                @endif    
                @php $i++ @endphp    
            @endforeach
        </div>    
        <div class="margin-bottom-40"></div>
    </div>
    <!-- Blog Posts / End -->


    <!-- Widgets -->
    <div class="col-md-4">

        <div class="widget">
            <div class="widget-box">
                @foreach($common as $com)
                    <a href="{{ json_decode($com->contents)->link }}">
                        <img src="{{ URL::to('public/uploads') }}/{{ json_decode($com->contents)->image }}" style="width: 100%">
                    </a>
                @endforeach
            </div>    
        </div>

        <div class="widget">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Jobonology Movement -->
<ins class="adsbygoogle"
     style="display:block;"
     data-ad-client="ca-pub-7726299907154841"
     data-ad-slot="2292045413"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
        </div>
    </div>

</div>
@endsection